/*
 * Created on 2019-10-27
 * 
 * Copyright 2019 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.trigger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.text.ParseException;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;

import io.gitlab.schedule4j.util.TestHelper;

/**
 * @author Dirk Buchhorn
 */
public class CronTriggerConfigTest extends AbstractScheduleTriggerConfigTest
{
	private static final String CRON_EXP = "0 0 0 * * ?";

	@Override
	CronTriggerConfig getTriggerConfig(TriggerKey key, CalculateNextEventRule calculateNextEventRule, ZoneId zone,
		ZonedDateTime startTime, ZonedDateTime endTime) throws ParseException
	{
		CronTriggerConfig triggerConfig = new CronTriggerConfig(key, calculateNextEventRule, startTime, endTime,
			CRON_EXP);
		if (zone != null)
		{
			triggerConfig = triggerConfig.withZone(zone);
		}
		return triggerConfig;
	}

	@Test
	void createTriggerTest() throws ParseException, CreateTriggerException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2019-11-03T10:00:00");
		ZonedDateTime endTime = TestHelper.createZonedDateTime("2019-12-03T10:00:00");
		CronTriggerConfig config = getTriggerConfig(TriggerKey.from("test"), CalculateNextEventRule.NEXT, null,
			startTime, endTime);
		CronTrigger trigger = config.createTrigger();
		assertNotNull(trigger);
		assertEquals(TriggerKey.from("test"), trigger.getKey());
		assertEquals(CRON_EXP, trigger.getCronExpression().getCronExpression());
		assertEquals(CalculateNextEventRule.NEXT, trigger.getCalculateNextEventRule());
		assertEquals(startTime, trigger.getStartTime());
		assertEquals(endTime, trigger.getEndTime());
	}

	@Test
	void withTriggerKeyTest() throws ParseException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2019-11-03T10:00:00");
		CronTriggerConfig config = getTriggerConfig(TriggerKey.from("test"), CalculateNextEventRule.NEXT, null,
			startTime, null);
		CronTriggerConfig config2 = config.withTriggerKey(TriggerKey.from("test2"));
		assertNotNull(config2);
		assertEquals(TriggerKey.from("test"), config.getKey());
		assertEquals(TriggerKey.from("test2"), config2.getKey());
		assertNotEquals(System.identityHashCode(config), System.identityHashCode(config2));
	}

	@Test
	void withCalculateNextEventRuleTest() throws ParseException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2019-11-03T10:00:00");
		CronTriggerConfig config = getTriggerConfig(TriggerKey.from("test"), CalculateNextEventRule.NEXT, null,
			startTime, null);
		CronTriggerConfig config2 = config.withCalculateNextEventRule(CalculateNextEventRule.NEAREST);
		assertNotNull(config2);
		assertEquals(CalculateNextEventRule.NEXT, config.getCalculateNextEventRule());
		assertEquals(CalculateNextEventRule.NEAREST, config2.getCalculateNextEventRule());
		assertNotEquals(System.identityHashCode(config), System.identityHashCode(config2));
	}

	@Test
	void withZoneTest() throws ParseException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2019-11-03T10:00:00");
		CronTriggerConfig config = getTriggerConfig(TriggerKey.from("test"), CalculateNextEventRule.NEXT, null,
			startTime, null);
		CronTriggerConfig config2 = config.withZone(ZoneId.systemDefault());
		assertNotNull(config2);
		assertEquals(startTime.getZone(), config.getZone());
		assertEquals(ZoneId.systemDefault(), config2.getZone());
		assertNotEquals(System.identityHashCode(config), System.identityHashCode(config2));
	}

	@Test
	void withStartTimeTest() throws ParseException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2019-11-03T10:00:00");
		ZonedDateTime startTime2 = TestHelper.createZonedDateTime("2019-11-04T10:00:00");
		CronTriggerConfig config = getTriggerConfig(TriggerKey.from("test"), CalculateNextEventRule.NEXT, null,
			startTime, null);
		CronTriggerConfig config2 = config.withStartTime(startTime2);
		assertNotNull(config2);
		assertEquals(startTime, config.getStartTime());
		assertEquals(startTime2, config2.getStartTime());
		assertNotEquals(System.identityHashCode(config), System.identityHashCode(config2));
	}

	@Test
	void withEndTimeTest() throws ParseException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2019-11-03T10:00:00");
		ZonedDateTime endTime = TestHelper.createZonedDateTime("2019-11-04T18:00:00");
		ZonedDateTime endTime2 = TestHelper.createZonedDateTime("2019-11-04T19:00:00");
		CronTriggerConfig config = getTriggerConfig(TriggerKey.from("test"), CalculateNextEventRule.NEXT, null,
			startTime, endTime);
		CronTriggerConfig config2 = config.withEndTime(endTime2);
		assertNotNull(config2);
		assertEquals(endTime, config.getEndTime());
		assertEquals(endTime2, config2.getEndTime());
		assertNotEquals(System.identityHashCode(config), System.identityHashCode(config2));
	}

	@Test
	void withCronExpressionTest() throws ParseException
	{
		String cronExp2 = "0/5 0 0 * * ?";
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2019-11-03T10:00:00");
		CronTriggerConfig config = getTriggerConfig(TriggerKey.from("test"), CalculateNextEventRule.NEXT, null,
			startTime, null);
		CronTriggerConfig config2 = config.withCronExpression(cronExp2);
		assertNotNull(config2);
		assertEquals(CRON_EXP, config.getCronExpression());
		assertEquals(cronExp2, config2.getCronExpression());
		assertNotEquals(System.identityHashCode(config), System.identityHashCode(config2));
	}
}
