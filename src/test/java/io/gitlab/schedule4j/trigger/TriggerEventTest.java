/*
 * Created on 19.12.2015
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.trigger;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;

import io.gitlab.schedule4j.util.TestHelper;

/**
 * @author Dirk Buchhorn
 */
public class TriggerEventTest
{
	@Test
	void constructorTest_calculationTimeIsNull_throwsException()
	{
		ZonedDateTime fireTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		assertThrows(IllegalArgumentException.class, () -> new TriggerEvent(fireTime, null));
	}

	@Test
	void constructorTest_fireTimeIsNull_throwsException()
	{
		ZonedDateTime calculationTime = TestHelper.createZonedDateTime("2010-01-01T00:00:00");
		assertThrows(IllegalArgumentException.class, () -> new TriggerEvent(null, calculationTime));
	}

	@Test
	void constructorTest_allParameterAreGiven_triggerEventIsCreated()
	{
		ZonedDateTime fireTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		ZonedDateTime calculationTime = TestHelper.createZonedDateTime("2010-01-01T00:00:00");
		TriggerEvent triggerEvent = new TriggerEvent(fireTime, calculationTime);
		assertAll(() -> assertEquals(fireTime, triggerEvent.getFireTime()),
			() -> assertEquals(calculationTime, triggerEvent.getCalculationTime()));
	}

	@Test
	void getFireTimeTest()
	{
		ZonedDateTime fireTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		ZonedDateTime calculationTime = TestHelper.createZonedDateTime("2010-01-01T00:00:00");
		TriggerEvent te = new TriggerEvent(fireTime, calculationTime);
		assertEquals(fireTime, te.getFireTime());
	}

	@Test
	void getCalculationTimeTest()
	{
		ZonedDateTime fireTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		ZonedDateTime calculationTime = TestHelper.createZonedDateTime("2010-01-01T00:00:00");
		TriggerEvent te = new TriggerEvent(fireTime, calculationTime);
		assertEquals(calculationTime, te.getCalculationTime());
	}

	@Test
	void isFireTimeAfterTest()
	{
		ZonedDateTime fireTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		ZonedDateTime calculationTime = TestHelper.createZonedDateTime("2010-01-01T00:00:00");
		TriggerEvent te = new TriggerEvent(fireTime, calculationTime);

		assertAll(() -> assertFalse(te.isFireTimeAfter(new TriggerEvent(fireTime, calculationTime)), "test 1"),
			() -> assertTrue(
				te.isFireTimeAfter(
					new TriggerEvent(TestHelper.createZonedDateTime("2010-01-01T00:00:00"), calculationTime)),
				"test 2"),
			() -> assertFalse(
				te.isFireTimeAfter(
					new TriggerEvent(TestHelper.createZonedDateTime("2010-01-01T01:01:00"), calculationTime)),
				"test 3"),
			() -> assertFalse(te.isFireTimeAfter(null), "test 4"));
	}

	@Test
	void isFireTimeBeforeTest()
	{
		ZonedDateTime fireTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		ZonedDateTime calculationTime = TestHelper.createZonedDateTime("2010-01-01T00:00:00");
		TriggerEvent te = new TriggerEvent(fireTime, calculationTime);

		assertAll(() -> assertFalse(te.isFireTimeBefore(new TriggerEvent(fireTime, calculationTime)), "test 1"),
			() -> assertTrue(
				te.isFireTimeBefore(
					new TriggerEvent(TestHelper.createZonedDateTime("2010-01-01T01:01:00"), calculationTime)),
				"test 2"),
			() -> assertFalse(
				te.isFireTimeBefore(
					new TriggerEvent(TestHelper.createZonedDateTime("2010-01-01T00:00:00"), calculationTime)),
				"test 3"),
			() -> assertFalse(te.isFireTimeBefore(null), "test 4"));
	}

	@Test
	void isFireTimeEqualsTest()
	{
		ZonedDateTime fireTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		ZonedDateTime calculationTime = TestHelper.createZonedDateTime("2010-01-01T00:00:00");
		TriggerEvent te = new TriggerEvent(fireTime, calculationTime);

		assertAll(() -> assertTrue(te.isFireTimeEquals(new TriggerEvent(fireTime, calculationTime)), "test 1"),
			() -> assertFalse(
				te.isFireTimeEquals(
					new TriggerEvent(TestHelper.createZonedDateTime("2010-01-01T00:00:00"), calculationTime)),
				"test 2"),
			() -> assertFalse(
				te.isFireTimeEquals(
					new TriggerEvent(TestHelper.createZonedDateTime("2010-01-01T01:01:00"), calculationTime)),
				"test 3"));
	}

	@Test
	void hashCodeTest()
	{
		ZonedDateTime fireTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		ZonedDateTime calculationTime = TestHelper.createZonedDateTime("2010-01-01T00:00:00");
		TriggerEvent te1 = new TriggerEvent(fireTime, calculationTime);
		TriggerEvent te2 = new TriggerEvent(fireTime, calculationTime);
		TriggerEvent te3 = new TriggerEvent(TestHelper.createZonedDateTime("2010-01-01T02:00:00"), calculationTime);

		assertAll(() -> assertEquals(te1.hashCode(), te2.hashCode(), "test 1"),
			() -> assertNotEquals(te3.hashCode(), te2.hashCode(), "test 2"));
	}

	@Test
	void equalsTest()
	{
		ZonedDateTime fireTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		ZonedDateTime calculationTime = TestHelper.createZonedDateTime("2010-01-01T00:00:00");
		TriggerEvent te1 = new TriggerEvent(fireTime, calculationTime);
		TriggerEvent te2 = new TriggerEvent(fireTime, calculationTime);
		TriggerEvent te3 = new TriggerEvent(TestHelper.createZonedDateTime("2010-01-01T02:00:00"), calculationTime);
		TriggerEvent te4 = new TriggerEvent(fireTime, calculationTime.plusSeconds(1));

		assertAll(() -> assertEquals(te1, te1, "test 1"), //
			() -> assertEquals(te1, te2, "test 2"), //
			() -> assertFalse(te1.equals(null), "test 3"), //
			() -> assertFalse(te1.equals(te3), "test 4"), //
			() -> assertFalse(te1.equals(te4), "test 5"));
	}

	@Test
	void toStringTest()
	{
		ZonedDateTime fireTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		ZonedDateTime calculationTime = TestHelper.createZonedDateTime("2010-01-01T00:00:00");
		TriggerEvent te1 = new TriggerEvent(fireTime, calculationTime);
		assertTrue(te1.toString().startsWith("TriggerEvent ("));
	}
}
