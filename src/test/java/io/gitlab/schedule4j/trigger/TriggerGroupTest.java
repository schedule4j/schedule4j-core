/*
 * Created on 19.12.2015
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.trigger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.ParseException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import io.gitlab.schedule4j.util.TestHelper;

/**
 * @author Dirk Buchhorn
 */
public class TriggerGroupTest
{
	@Test
	void constructorTest() throws CreateTriggerException
	{
		TriggerGroupConfig triggerGroupConfig = createTriggerGroupConfig();
		TriggerGroup triggerGroup = new TriggerGroup(triggerGroupConfig);
		assertEquals(triggerGroupConfig.getKey(), triggerGroup.getKey());
		assertEquals(2, triggerGroup.getSize());

		for (TriggerKey triggerKey : triggerGroupConfig.getTriggerKeys())
		{
			assertTrue(triggerGroup.getTrigger(triggerKey).isPresent());
		}
	}

	@Test
	void getTriggersTest() throws CreateTriggerException
	{
		TriggerGroupConfig triggerGroupConfig = createTriggerGroupConfig();
		TriggerGroup triggerGroup = new TriggerGroup(triggerGroupConfig);
		assertEquals(triggerGroupConfig.getKey(), triggerGroup.getKey());
		assertEquals(2, triggerGroup.getTriggers().size());

		for (Trigger trigger : triggerGroup.getTriggers())
		{
			assertTrue(triggerGroupConfig.getTriggerConfig(trigger.getKey()).isPresent());
		}
	}

	@Test
	void getTriggerKeysTest() throws CreateTriggerException
	{
		TriggerGroupConfig triggerGroupConfig = createTriggerGroupConfig();
		TriggerGroup triggerGroup = new TriggerGroup(triggerGroupConfig);
		assertEquals(triggerGroupConfig.getKey(), triggerGroup.getKey());
		assertEquals(2, triggerGroup.getTriggerKeys().size());

		for (TriggerKey triggerKey : triggerGroupConfig.getTriggerKeys())
		{
			assertTrue(triggerGroup.getTrigger(triggerKey).isPresent());
		}
	}

	@Test
	void getNextTriggerEventTest() throws CreateTriggerException
	{
		TriggerGroupConfig triggerGroupConfig = createTriggerGroupConfig();
		TriggerGroup triggerGroup = new TriggerGroup(triggerGroupConfig);

		ZonedDateTime calculationTime = TestHelper.createZonedDateTime("2010-01-01T00:00:00");
		TriggerEvent triggerEvent = null;
		// TODO change test times
		triggerEvent = triggerGroup.getNextTriggerEvent(null, calculationTime);
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T00:10:00"), triggerEvent.getFireTime());
		calculationTime = TestHelper.createZonedDateTime("2010-01-01T00:05:00");
		triggerEvent = triggerGroup.getNextTriggerEvent(null, calculationTime);
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T00:10:00"), triggerEvent.getFireTime());
		calculationTime = TestHelper.createZonedDateTime("2010-01-01T00:10:00");
		triggerEvent = triggerGroup.getNextTriggerEvent(triggerEvent, calculationTime);
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T00:15:00"), triggerEvent.getFireTime());
	}

	@Test
	void getFirstFireTimeTest() throws CreateTriggerException, ParseException
	{
		TriggerGroupConfig triggerGroupConfig = createTriggerGroupConfig();
		TriggerGroup triggerGroup = new TriggerGroup(triggerGroupConfig);

		assertNull(triggerGroup.getFirstFireTime());

		triggerGroupConfig = triggerGroupConfig.withAdd(new CronTriggerConfig(TriggerKey.from("cronTrigger1"),
			TestHelper.createZonedDateTime("2010-01-01T00:00:00"), null, "0 0/10 * * * ?"));
		triggerGroup = new TriggerGroup(triggerGroupConfig);
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T00:00:00"), triggerGroup.getFirstFireTime());
	}

	@Test
	void getLastFireTimeTest() throws CreateTriggerException, ParseException
	{
		TriggerGroupConfig triggerGroupConfig = createTriggerGroupConfig();
		TriggerGroup triggerGroup = new TriggerGroup(triggerGroupConfig);

		assertNull(triggerGroup.getLastFireTime());

		triggerGroupConfig = triggerGroupConfig.withAdd(new CronTriggerConfig(TriggerKey.from("cronTrigger1"),
			TestHelper.createZonedDateTime("2010-01-01T00:00:00"),
			TestHelper.createZonedDateTime("2010-01-01T12:00:00"), "0 0/10 * * * ?"));
		triggerGroup = new TriggerGroup(triggerGroupConfig);
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T11:50:00"), triggerGroup.getLastFireTime());
	}

	private TriggerGroupConfig createTriggerGroupConfig()
	{
		try
		{
			List<TriggerConfig> configCollection = new ArrayList<TriggerConfig>();
			configCollection.add(new CronTriggerConfig(TriggerKey.from("cronTrigger1"), "0 0/10 * * * ?")
				.withZone(ZoneId.of("Europe/Berlin")));
			configCollection.add(new CronTriggerConfig(TriggerKey.from("cronTrigger2"), "0 0/15 * * * ?")
				.withZone(ZoneId.of("Europe/Berlin")));
			TriggerGroupConfig triggerGroupConfig = new TriggerGroupConfig(TriggerKey.from("test"), configCollection);
			return triggerGroupConfig;
		}
		catch (ParseException e)
		{
			throw new RuntimeException(e);
		}
	}
}
