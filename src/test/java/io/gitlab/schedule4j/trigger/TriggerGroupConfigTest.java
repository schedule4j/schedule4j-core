/*
 * Created on 2020-01-12
 * 
 * Copyright 2020 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.trigger;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * @author Dirk Buchhorn
 */
public class TriggerGroupConfigTest
{
	@Test
	void constructorTest_triggerKeyIsNull_throwsException() throws ParseException, CreateTriggerException
	{
		assertThrows(IllegalArgumentException.class, () -> new TriggerGroupConfig(null));
	}

	@Test
	void constructorWithTriggerKeyTest()
	{
		TriggerKey key = TriggerKey.from("test");
		TriggerGroupConfig triggerGroupConfig = new TriggerGroupConfig(key);
		assertAll(() -> assertEquals(0, triggerGroupConfig.getSize(), "wrong size"),
			() -> assertEquals(key, triggerGroupConfig.getKey(), "wrong key"));
	}

	@Test
	void constructorWithTriggerKeyAndEmptyCollectionTest()
	{
		TriggerKey key = TriggerKey.from("test");
		TriggerGroupConfig triggerGroupConfig = new TriggerGroupConfig(key, Collections.emptyList());
		assertAll(() -> assertEquals(0, triggerGroupConfig.getSize(), "wrong size"),
			() -> assertEquals(key, triggerGroupConfig.getKey(), "wrong key"));
	}

	@Test
	void constructorWithTriggerKeyAndArrayTest() throws ParseException
	{
		TriggerKey key = TriggerKey.from("test");
		TriggerConfig[] triggerConfigs = createTriggerConfigArray();
		TriggerGroupConfig triggerGroupConfig = new TriggerGroupConfig(key, triggerConfigs);
		assertAll(() -> assertEquals(2, triggerGroupConfig.getSize(), "wrong size"),
			() -> assertEquals(key, triggerGroupConfig.getKey(), "wrong key"),
			() -> testTriggerConfigCollection(triggerGroupConfig));
	}

	@Test
	void constructorWithTriggerKeyAndCollectionTest() throws ParseException
	{
		TriggerKey key = TriggerKey.from("test");
		Collection<TriggerConfig> configCollection = createTriggerConfigCollection();
		TriggerGroupConfig triggerGroupConfig = new TriggerGroupConfig(key, configCollection);
		assertAll(() -> assertEquals(2, triggerGroupConfig.getSize(), "wrong size"),
			() -> assertEquals(key, triggerGroupConfig.getKey(), "wrong key"),
			() -> testTriggerConfigCollection(triggerGroupConfig));
	}

	@Test
	void getTriggerConfigTest() throws ParseException
	{
		TriggerKey key = TriggerKey.from("test");
		Collection<TriggerConfig> configCollection = createTriggerConfigCollection();
		TriggerGroupConfig triggerGroupConfig = new TriggerGroupConfig(key, configCollection);
		assertNotNull(triggerGroupConfig.getTriggerConfigs());
		assertAll(() -> assertEquals(2, triggerGroupConfig.getTriggerConfigs().size(), "wrong size"),
			() -> assertFalse(triggerGroupConfig.getTriggerConfig(TriggerKey.from("cronTrigger3")).isPresent(),
				"trigger config not present"),
			() -> testTriggerConfigCollection(triggerGroupConfig));
	}

	@Test
	void getTriggerConfigsTest() throws ParseException
	{
		TriggerKey key = TriggerKey.from("test");
		Collection<TriggerConfig> configCollection = createTriggerConfigCollection();
		TriggerGroupConfig triggerGroupConfig = new TriggerGroupConfig(key, configCollection);
		assertNotNull(triggerGroupConfig.getTriggerConfigs());
		assertAll(() -> assertEquals(2, triggerGroupConfig.getTriggerConfigs().size(), "wrong size"),
			() -> testTriggerConfigCollection(triggerGroupConfig));
	}

	@Test
	void withAddArrayTest_parameterIsNull_cloneIsCreated() throws ParseException
	{
		TriggerKey key = TriggerKey.from("test");
		TriggerConfig[] triggerConfigs = null;
		TriggerGroupConfig triggerGroupConfig = new TriggerGroupConfig(key);

		TriggerGroupConfig triggerGroupConfigNew = triggerGroupConfig.withAdd(triggerConfigs);
		assertAll(() -> assertEquals(0, triggerGroupConfigNew.getSize(), "wrong size"),
			() -> assertEquals(key, triggerGroupConfigNew.getKey(), "wrong key"));
	}

	@Test
	void withAddArrayTest() throws ParseException
	{
		TriggerKey key = TriggerKey.from("test");
		TriggerConfig[] triggerConfigs = createTriggerConfigArray();
		TriggerGroupConfig triggerGroupConfig = new TriggerGroupConfig(key);

		TriggerGroupConfig triggerGroupConfigNew = triggerGroupConfig.withAdd(triggerConfigs);
		assertAll(() -> assertEquals(2, triggerGroupConfigNew.getSize(), "wrong size"),
			() -> assertEquals(key, triggerGroupConfigNew.getKey(), "wrong key"),
			() -> testTriggerConfigCollection(triggerGroupConfigNew));
	}

	@Test
	void withAddCollectionTest_parameterIsNull_cloneIsCreated() throws ParseException
	{
		TriggerKey key = TriggerKey.from("test");
		Collection<TriggerConfig> configCollection = null;
		TriggerGroupConfig triggerGroupConfig = new TriggerGroupConfig(key);

		TriggerGroupConfig triggerGroupConfigNew = triggerGroupConfig.withAdd(configCollection);
		assertAll(() -> assertEquals(0, triggerGroupConfigNew.getSize(), "wrong size"),
			() -> assertEquals(key, triggerGroupConfigNew.getKey(), "wrong key"));
	}

	@Test
	void withAddCollectionTest() throws ParseException
	{
		TriggerKey key = TriggerKey.from("test");
		Collection<TriggerConfig> configCollection = createTriggerConfigCollection();
		TriggerGroupConfig triggerGroupConfig = new TriggerGroupConfig(key);

		TriggerGroupConfig triggerGroupConfigNew = triggerGroupConfig.withAdd(configCollection);
		assertAll(() -> assertEquals(2, triggerGroupConfigNew.getSize(), "wrong size"),
			() -> assertEquals(key, triggerGroupConfigNew.getKey(), "wrong key"),
			() -> testTriggerConfigCollection(triggerGroupConfigNew));
	}

	@Test
	void withTriggerKeyTest() throws ParseException
	{
		TriggerKey key = TriggerKey.from("test");
		TriggerKey key2 = TriggerKey.from("test2");
		CronTriggerConfig cronTriggerConfig = new CronTriggerConfig(TriggerKey.from("cronTrigger"), "0 0/10 * * * ?");
		TriggerGroupConfig triggerGroupConfig = new TriggerGroupConfig(key, cronTriggerConfig);

		TriggerGroupConfig triggerGroupConfigNew = triggerGroupConfig.withTriggerKey(key2);
		assertAll(() -> assertEquals(key2, triggerGroupConfigNew.getKey(), "wrong key"),
			() -> assertEquals(1, triggerGroupConfigNew.getSize(), "wrong size"),
			() -> assertTrue(triggerGroupConfigNew.getTriggerConfig(TriggerKey.from("cronTrigger")).isPresent(),
				"trigger config not present"));
	}

	@Test
	void createTriggerTest() throws ParseException, CreateTriggerException
	{
		TriggerKey key = TriggerKey.from("test");
		CronTriggerConfig cronTriggerConfig = new CronTriggerConfig(TriggerKey.from("cronTrigger"), "0 0/10 * * * ?");
		TriggerGroupConfig triggerGroupConfig = new TriggerGroupConfig(key, cronTriggerConfig);
		TriggerGroup triggerGroup = triggerGroupConfig.createTrigger();
		assertNotNull(triggerGroup);
		assertEquals(triggerGroupConfig, triggerGroup.getTriggerConfig());
	}

	private void testTriggerConfigCollection(TriggerGroupConfig triggerGroupConfig)
	{
		for (TriggerConfig triggerConfig : triggerGroupConfig.getTriggerConfigs())
		{
			if (triggerConfig != null)
			{
				assertTrue(triggerGroupConfig.getTriggerConfig(triggerConfig.getKey()).isPresent());
			}
		}
	}

	private Collection<TriggerConfig> createTriggerConfigCollection()
	{
		try
		{
			List<TriggerConfig> configCollection = new ArrayList<TriggerConfig>();
			configCollection.add(new CronTriggerConfig(TriggerKey.from("cronTrigger1"), "0 0/10 * * * ?"));
			configCollection.add(null);
			configCollection.add(new CronTriggerConfig(TriggerKey.from("cronTrigger2"), "0 0/15 * * * ?"));
			return configCollection;
		}
		catch (ParseException e)
		{
			throw new RuntimeException(e);
		}
	}

	private TriggerConfig[] createTriggerConfigArray()
	{
		Collection<TriggerConfig> triggerConfigCollection = createTriggerConfigCollection();
		return triggerConfigCollection.toArray(new TriggerConfig[triggerConfigCollection.size()]);
	}
}
