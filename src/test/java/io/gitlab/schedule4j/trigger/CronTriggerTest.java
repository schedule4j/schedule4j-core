/*
 * Created on 18.11.2015
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.trigger;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.text.ParseException;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;

import io.gitlab.schedule4j.util.TestHelper;

/**
 * @author Dirk Buchhorn
 */
public class CronTriggerTest extends AbstractScheduleTriggerTest
{
	@Override
	public AbstractScheduleTrigger<?> createInstance(TriggerKey key)
	{
		try
		{
			return new CronTrigger(new CronTriggerConfig(key, "0 0/1 * * * ?"));
		}
		catch (ParseException e)
		{
			fail(e.getMessage());
		}
		return null;
	}

	@Override
	public AbstractScheduleTrigger<?> createInstance(TriggerKey key, ZonedDateTime startTime, ZonedDateTime endTime)
	{
		try
		{
			return new CronTrigger(new CronTriggerConfig(key, startTime, endTime, "0 0/1 * * * ?"));
		}
		catch (ParseException e)
		{
			fail(e.getMessage());
		}
		return null;
	}

	@Test
	void constructorTest_cnfigWithoutStartAndEndTime_cronTriggerIsCreated() throws ParseException
	{
		TriggerKey key = new TriggerKey("test");
		String cronExpression = "0 0/1 * * * ?";

		CronTrigger trigger = new CronTrigger(new CronTriggerConfig(key, cronExpression));
		assertAll(() -> assertEquals(key, trigger.getKey(), "wrong trigger key"), () ->
		{
			assertNotNull(trigger.getCronExpression().getCronExpression(), "cron expression was null");
			assertEquals(cronExpression, trigger.getCronExpression().getCronExpression(), "wrong cron expression");
		}, () -> assertNull(trigger.getStartTime(), "start time should be null"),
			() -> assertNull(trigger.getEndTime(), "end time should be null"));
	}

	@Test
	void constructorTest_cnfigWithStartAndWithoutEndTime_cronTriggerIsCreated() throws ParseException
	{
		TriggerKey key = new TriggerKey("test");
		String cronExpression = "0 0/1 * * * ?";
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T00:00:00");

		CronTrigger trigger = new CronTrigger(new CronTriggerConfig(key, startTime, null, cronExpression));
		assertAll(() -> assertEquals(key, trigger.getKey(), "wrong trigger key"),
			() -> assertEquals(cronExpression, trigger.getCronExpression().getCronExpression(),
				"wrong cron expression"),
			() -> assertEquals(startTime, trigger.getStartTime(), "wrong start time"),
			() -> assertNull(trigger.getEndTime(), "end time should be null"));
	}

	@Test
	void constructorTest_cnfigWithoutStartAndWithEndTime_cronTriggerIsCreated() throws ParseException
	{
		TriggerKey key = new TriggerKey("test");
		String cronExpression = "0 0/1 * * * ?";
		ZonedDateTime endTime = TestHelper.createZonedDateTime("2010-01-02T00:00:00");

		CronTrigger trigger = new CronTrigger(new CronTriggerConfig(key, null, endTime, cronExpression));
		assertAll(() -> assertEquals(key, trigger.getKey(), "wrong trigger key"),
			() -> assertEquals(cronExpression, trigger.getCronExpression().getCronExpression(),
				"wrong cron expression"),
			() -> assertNull(trigger.getStartTime(), "start time should be null"),
			() -> assertEquals(endTime, trigger.getEndTime(), "wrong end time"));
	}

	@Test
	void constructorTest_configIsNull_throwsException() throws ParseException
	{
		assertThrows(IllegalArgumentException.class,
			() -> new CronTrigger(new CronTriggerConfig(new TriggerKey("test"), null)));
	}

	@Test
	void getFirstFireTimeTest_withoutStartTimeAndEndTime_noFirstFireTime() throws ParseException
	{
		CronTrigger trigger = new CronTrigger(
			new CronTriggerConfig(new TriggerKey("test"), null, null, "0 0/1 * * * ?"));
		assertNull(trigger.getFirstFireTime());
	}

	@Test
	void getFirstFireTimeTest_withStartTimeAndWithoutEndTime_firstFireTimeExist() throws ParseException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T00:00:00");
		CronTrigger trigger = new CronTrigger(
			new CronTriggerConfig(new TriggerKey("test"), startTime, null, "0 0/1 * * * ?"));
		assertEquals(startTime, trigger.getFirstFireTime());
	}

	@Test
	void getFirstFireTimeTest_withStartTimeAndEndTime_firstFireTimeExist() throws ParseException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T00:00:10");
		ZonedDateTime endTime = TestHelper.createZonedDateTime("2010-01-01T00:01:00");
		CronTrigger trigger = new CronTrigger(
			new CronTriggerConfig(new TriggerKey("test"), startTime, endTime, "0 0/1 * * * ?"));
		assertNull(trigger.getFirstFireTime());
	}

	@Test
	void getLastFireTimeTest_withoutStartTimeAndEndTime_noLastFireTime() throws ParseException
	{
		CronTrigger trigger = new CronTrigger(
			new CronTriggerConfig(new TriggerKey("test"), null, null, "0 0/1 * * * ?"));
		assertNull(trigger.getLastFireTime());
	}

	@Test
	void getLastFireTimeTest_withStartTimeAndWithoutEndTime_noLastFireTime() throws ParseException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T00:00:10");
		CronTrigger trigger = new CronTrigger(
			new CronTriggerConfig(new TriggerKey("test"), startTime, null, "0 0/1 * * * ?"));
		assertNull(trigger.getLastFireTime());
	}

	@Test
	void getLastFireTimeTest_withoutStartTimeAndWithEndTime_lastFireTimeExist() throws ParseException
	{
		ZonedDateTime endTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		CronTrigger trigger = new CronTrigger(
			new CronTriggerConfig(new TriggerKey("test"), null, endTime, "0 0/1 * * * ?"));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T00:59:00"), trigger.getLastFireTime());
	}

	@Test
	void getNextTriggerEventTest_nearestRule_nextEventIsCreated() throws ParseException
	{
		TriggerEvent pastTriggerEvent = new TriggerEvent(TestHelper.createZonedDateTime("2010-01-01T00:02:00"),
			TestHelper.createZonedDateTime("2010-01-01T00:01:00"));

		// we need a start time, so the time zone is right
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T00:00:00");
		CronTrigger trigger = new CronTrigger(
			new CronTriggerConfig(new TriggerKey("test"), startTime, null, "0 0/1 * * * ?")
				.withCalculateNextEventRule(CalculateNextEventRule.NEAREST));
		TriggerEvent te = trigger.getNextTriggerEvent(pastTriggerEvent,
			TestHelper.createZonedDateTime("2010-01-01T00:05:00"));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T00:05:00"), te.getFireTime());
	}

	@Test
	void getNextTriggerEventTest_nextRule_nextEventIsCreated() throws ParseException
	{
		TriggerEvent pastTriggerEvent = new TriggerEvent(TestHelper.createZonedDateTime("2010-01-01T00:02:00"),
			TestHelper.createZonedDateTime("2010-01-01T00:01:00"));

		// we need a start time, so the time zone is right
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T00:00:00");
		CronTrigger trigger = new CronTrigger(
			new CronTriggerConfig(new TriggerKey("test"), startTime, null, "0 0/1 * * * ?")
				.withCalculateNextEventRule(CalculateNextEventRule.NEXT));
		TriggerEvent te = trigger.getNextTriggerEvent(pastTriggerEvent,
			TestHelper.createZonedDateTime("2010-01-01T00:05:00"));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T00:06:00"), te.getFireTime());
	}

	@Test
	void getNextTriggerEventTest_oneByOneRule_nextEventIsCreated() throws ParseException
	{
		TriggerEvent pastTriggerEvent = new TriggerEvent(TestHelper.createZonedDateTime("2010-01-01T00:02:00"),
			TestHelper.createZonedDateTime("2010-01-01T00:01:00"));

		CronTrigger trigger = new CronTrigger(new CronTriggerConfig(new TriggerKey("test"), null, null, "0 0/1 * * * ?")
			.withCalculateNextEventRule(CalculateNextEventRule.ONE_BY_ONE));
		TriggerEvent te = trigger.getNextTriggerEvent(pastTriggerEvent,
			TestHelper.createZonedDateTime("2010-01-01T00:05:00"));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T00:03:00"), te.getFireTime());
	}

	@Test
	void getNextTriggerEventTest_fromLastEvent_noNextEvent() throws ParseException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T12:00:00");
		ZonedDateTime endTime = TestHelper.createZonedDateTime("2010-01-01T14:00:00");
		CronTrigger trigger = new CronTrigger(
			new CronTriggerConfig(new TriggerKey("test"), startTime, endTime, "0 0/1 * * * ?"));
		ZonedDateTime lastFireTime = trigger.getLastFireTime();
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T13:59:00"), lastFireTime);
		TriggerEvent te = trigger.getNextTriggerEvent(new TriggerEvent(lastFireTime, lastFireTime), lastFireTime);
		assertNull(te);
	}

	@Test
	void getNextFireTimeTest_withoutStartTimeAndEndTime_nextFireTimeIsCalculated() throws ParseException
	{
		CronTrigger trigger = new CronTrigger(
			new CronTriggerConfig(new TriggerKey("test"), null, null, "0 0/1 * * * ?"));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T00:01:00"),
			trigger.getNextFireTime(TestHelper.createZonedDateTime("2010-01-01T00:00:00")));
	}

	@Test
	void getNextFireTimeTest_withStartTimeAndWithoutEndTime_nextFireTimeIsCalculated() throws ParseException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		CronTrigger trigger = new CronTrigger(
			new CronTriggerConfig(new TriggerKey("test"), startTime, null, "0 0/1 * * * ?"));
		assertAll(
			() -> assertEquals(TestHelper.createZonedDateTime("2010-01-01T01:00:00"),
				trigger.getNextFireTime(TestHelper.createZonedDateTime("2010-01-01T00:00:00"))),
			() -> assertEquals(TestHelper.createZonedDateTime("2010-01-01T01:01:00"),
				trigger.getNextFireTime(TestHelper.createZonedDateTime("2010-01-01T01:00:00"))));
	}

	@Test
	void getNextFireTimeTest_withStartTimeAndEndTime_nextFireTimeIsCalculated() throws ParseException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		ZonedDateTime endTime = TestHelper.createZonedDateTime("2010-01-01T01:10:00");
		CronTrigger trigger = new CronTrigger(
			new CronTriggerConfig(new TriggerKey("test"), startTime, endTime, "0 0/1 * * * ?"));
		assertAll(
			() -> assertEquals(TestHelper.createZonedDateTime("2010-01-01T01:09:00"),
				trigger.getNextFireTime(TestHelper.createZonedDateTime("2010-01-01T01:08:00"))),
			() -> assertNull(trigger.getNextFireTime(TestHelper.createZonedDateTime("2010-01-01T01:09:00"))));
	}

	@Test
	void getPreviousFireTimeTest_withoutStartTimeAndEndTime_previousFireTimeIsCalculated() throws ParseException
	{
		CronTrigger trigger = new CronTrigger(
			new CronTriggerConfig(new TriggerKey("test"), null, null, "0 0/1 * * * ?"));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T00:59:00"),
			trigger.getPreviousFireTime(TestHelper.createZonedDateTime("2010-01-01T01:00:00")));
	}

	@Test
	void getPreviousFireTimeTest_withStartTimeAndWithoutEndTime_previousFireTimeIsCalculated() throws ParseException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		CronTrigger trigger = new CronTrigger(
			new CronTriggerConfig(new TriggerKey("test"), startTime, null, "0 0/1 * * * ?"));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T01:00:00"),
			trigger.getPreviousFireTime(TestHelper.createZonedDateTime("2010-01-01T01:01:00")));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T01:09:00"),
			trigger.getPreviousFireTime(TestHelper.createZonedDateTime("2010-01-01T01:10:00")));
		assertNull(trigger.getPreviousFireTime(TestHelper.createZonedDateTime("2010-01-01T01:00:00")));
	}

	@Test
	void getPreviousFireTimeTest_withStartTimeAndEndTime_previousFireTimeIsCalculated() throws ParseException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		ZonedDateTime endTime = TestHelper.createZonedDateTime("2010-01-01T01:10:00");
		CronTrigger trigger = new CronTrigger(
			new CronTriggerConfig(new TriggerKey("test"), startTime, endTime, "0 0/1 * * * ?"));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T01:09:00"),
			trigger.getPreviousFireTime(TestHelper.createZonedDateTime("2010-01-01T01:10:00")));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T01:09:00"),
			trigger.getPreviousFireTime(TestHelper.createZonedDateTime("2010-01-01T01:11:00")));
	}
}
