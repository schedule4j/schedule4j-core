/*
 * Created on 18.11.2015
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.trigger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;

import io.gitlab.schedule4j.util.TestHelper;

/**
 * @author Dirk Buchhorn
 */
public class SimpleTriggerTest extends AbstractScheduleTriggerTest
{
	@Override
	public AbstractScheduleTrigger<?> createInstance(TriggerKey key)
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T00:00:00");
		ZonedDateTime endTime = null;
		return new SimpleTrigger(new SimpleTriggerConfig(key, startTime, endTime, 0, 1));
	}

	@Override
	public AbstractScheduleTrigger<?> createInstance(TriggerKey key, ZonedDateTime startTime, ZonedDateTime endTime)
	{
		return new SimpleTrigger(new SimpleTriggerConfig(key, startTime, endTime, 0, 1));
	}

	@Test
	void constructorTest()
	{
		TriggerKey key = new TriggerKey("test");
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T00:00:00");
		ZonedDateTime endTime = TestHelper.createZonedDateTime("2010-01-02T00:00:00");

		SimpleTrigger trigger = new SimpleTrigger(new SimpleTriggerConfig(key, startTime, endTime, 10, 100));
		assertEquals(key, trigger.getKey());
		assertEquals(startTime, trigger.getStartTime());
		assertEquals(endTime, trigger.getEndTime());
		assertEquals(10, trigger.getRepeatCount());
		assertEquals(100, trigger.getRepeatInterval());
	}

	@Test
	void getFirstFireTimeTest()
	{
		TriggerKey key = new TriggerKey("test");
		SimpleTrigger trigger = null;
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T00:00:00");
		ZonedDateTime endTime = null;

		trigger = new SimpleTrigger(new SimpleTriggerConfig(key, startTime, endTime, 0, 1));
		assertEquals(startTime, trigger.getFirstFireTime());

		trigger = new SimpleTrigger(new SimpleTriggerConfig(key, startTime, endTime, 1, 1000 * 60));
		assertEquals(startTime, trigger.getFirstFireTime());
	}

	@Test
	void getLastFireTimeTest()
	{
		TriggerKey key = new TriggerKey("test");
		SimpleTrigger trigger = null;
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T00:00:00");
		ZonedDateTime endTime = null;

		trigger = new SimpleTrigger(new SimpleTriggerConfig(key, startTime, endTime, 0, 1));
		assertEquals(startTime, trigger.getLastFireTime());

		trigger = new SimpleTrigger(new SimpleTriggerConfig(key, startTime, endTime, 1, 1000 * 60));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T00:01:00"), trigger.getLastFireTime());

		trigger = new SimpleTrigger(new SimpleTriggerConfig(key, startTime, endTime, 10, 1000 * 60));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T00:10:00"), trigger.getLastFireTime());

		endTime = TestHelper.createZonedDateTime("2010-01-01T00:05:00");
		trigger = new SimpleTrigger(new SimpleTriggerConfig(key, startTime, endTime, 10, 1000 * 60));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T00:04:00"), trigger.getLastFireTime());

		endTime = TestHelper.createZonedDateTime("2010-01-01T00:05:01");
		trigger = new SimpleTrigger(new SimpleTriggerConfig(key, startTime, endTime, 10, 1000 * 60));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T00:05:00"), trigger.getLastFireTime());

		endTime = TestHelper.createZonedDateTime("2010-01-01T00:10:01");
		trigger = new SimpleTrigger(new SimpleTriggerConfig(key, startTime, endTime, 10, 1000 * 60));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T00:10:00"), trigger.getLastFireTime());
	}

	@Test
	void getNextTriggerEventTest()
	{
		TriggerKey key = new TriggerKey("test");
		SimpleTrigger trigger = null;
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		ZonedDateTime endTime = null;
		SimpleTriggerConfig triggerConfig = new SimpleTriggerConfig(key, startTime, endTime, 10, 1000 * 60);

		trigger = new SimpleTrigger(triggerConfig);// every minute
		TriggerEvent pastTriggerEvent = new TriggerEvent(TestHelper.createZonedDateTime("2010-01-01T01:02:00"),
			TestHelper.createZonedDateTime("2010-01-01T01:01:00"));
		TriggerEvent te = null;

		triggerConfig = triggerConfig.withCalculateNextEventRule(CalculateNextEventRule.NEAREST);
		trigger = new SimpleTrigger(triggerConfig);
		te = trigger.getNextTriggerEvent(pastTriggerEvent, TestHelper.createZonedDateTime("2010-01-01T01:05:00"));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T01:05:00"), te.getFireTime());

		triggerConfig = triggerConfig.withCalculateNextEventRule(CalculateNextEventRule.NEXT);
		trigger = new SimpleTrigger(triggerConfig);
		te = trigger.getNextTriggerEvent(pastTriggerEvent, TestHelper.createZonedDateTime("2010-01-01T01:05:00"));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T01:06:00"), te.getFireTime());

		triggerConfig = triggerConfig.withCalculateNextEventRule(CalculateNextEventRule.ONE_BY_ONE);
		trigger = new SimpleTrigger(triggerConfig);
		te = trigger.getNextTriggerEvent(pastTriggerEvent, TestHelper.createZonedDateTime("2010-01-01T01:05:00"));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T01:03:00"), te.getFireTime());

		// next event from last event test
		startTime = TestHelper.createZonedDateTime("2010-01-01T12:00:00");
		endTime = TestHelper.createZonedDateTime("2010-01-01T14:00:00");
		// every minute
		triggerConfig = new SimpleTriggerConfig(key, startTime, endTime, 200, 1000 * 60);
		trigger = new SimpleTrigger(triggerConfig);
		ZonedDateTime lastFireTime = trigger.getLastFireTime();
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T13:59:00"), lastFireTime);
		te = trigger.getNextTriggerEvent(new TriggerEvent(lastFireTime, lastFireTime), lastFireTime);
		assertNull(te);
	}

	@Test
	void getNextFireTimeTest()
	{
		TriggerKey key = new TriggerKey("test");
		SimpleTrigger trigger = null;
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		ZonedDateTime endTime = null;

		trigger = new SimpleTrigger(new SimpleTriggerConfig(key, startTime, endTime, 10, 1000 * 60));
		assertEquals(startTime, trigger.getNextFireTime(TestHelper.createZonedDateTime("2010-01-01T00:00:00")));

		endTime = TestHelper.createZonedDateTime("2010-01-01T01:05:00");
		trigger = new SimpleTrigger(new SimpleTriggerConfig(key, startTime, endTime, 10, 1000 * 60));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T01:01:00"),
			trigger.getNextFireTime(TestHelper.createZonedDateTime("2010-01-01T01:00:00")));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T01:04:00"),
			trigger.getNextFireTime(TestHelper.createZonedDateTime("2010-01-01T01:03:00")));
		assertNull(trigger.getNextFireTime(TestHelper.createZonedDateTime("2010-01-01T01:04:00")));

		endTime = TestHelper.createZonedDateTime("2010-01-01T01:05:01");
		trigger = new SimpleTrigger(new SimpleTriggerConfig(key, startTime, endTime, 10, 1000 * 60));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T01:01:00"),
			trigger.getNextFireTime(TestHelper.createZonedDateTime("2010-01-01T01:00:00")));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T01:04:00"),
			trigger.getNextFireTime(TestHelper.createZonedDateTime("2010-01-01T01:03:00")));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T01:05:00"),
			trigger.getNextFireTime(TestHelper.createZonedDateTime("2010-01-01T01:04:00")));
	}

	@Test
	void getPreviousFireTimeTest()
	{
		TriggerKey key = new TriggerKey("test");
		SimpleTrigger trigger = null;
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		ZonedDateTime endTime = null;

		trigger = new SimpleTrigger(new SimpleTriggerConfig(key, startTime, endTime, 10, 1000 * 60));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T01:10:00"),
			trigger.getPreviousFireTime(TestHelper.createZonedDateTime("2010-01-01T02:00:00")));

		endTime = TestHelper.createZonedDateTime("2010-01-01T01:05:00");
		trigger = new SimpleTrigger(new SimpleTriggerConfig(key, startTime, endTime, 10, 1000 * 60));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T01:04:00"),
			trigger.getPreviousFireTime(TestHelper.createZonedDateTime("2010-01-01T01:05:00")));
		assertEquals(TestHelper.createZonedDateTime("2010-01-01T01:04:00"),
			trigger.getPreviousFireTime(TestHelper.createZonedDateTime("2010-01-01T01:06:00")));
		assertEquals(startTime, trigger.getPreviousFireTime(TestHelper.createZonedDateTime("2010-01-01T01:01:00")));
		assertNull(trigger.getPreviousFireTime(TestHelper.createZonedDateTime("2010-01-01T01:00:00")));
	}

	@Test
	void getStartTimeTest()
	{
		TriggerKey key = new TriggerKey("test");
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		ZonedDateTime endTime = TestHelper.createZonedDateTime("2010-01-01T01:05:00");
		SimpleTrigger trigger = new SimpleTrigger(new SimpleTriggerConfig(key, startTime, endTime, 10, 1000 * 60));
		assertEquals(startTime, trigger.getStartTime());
	}

	@Test
	void getEndTimeTest()
	{
		TriggerKey key = new TriggerKey("test");
		SimpleTrigger trigger = null;
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		ZonedDateTime endTime = null;
		trigger = new SimpleTrigger(new SimpleTriggerConfig(key, startTime, endTime, 10, 1000 * 60));
		assertNull(trigger.getEndTime());

		endTime = TestHelper.createZonedDateTime("2010-01-01T01:05:00");
		trigger = new SimpleTrigger(new SimpleTriggerConfig(key, startTime, endTime, 10, 1000 * 60));
		assertEquals(endTime, trigger.getEndTime());
	}
}
