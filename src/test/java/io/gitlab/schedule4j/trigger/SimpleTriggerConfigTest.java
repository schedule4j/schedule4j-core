/*
 * Created on 2019-11-03
 * 
 * Copyright 2019 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.trigger;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.text.ParseException;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;

import io.gitlab.schedule4j.util.TestHelper;

/**
 * @author Dirk Buchhorn
 */
public class SimpleTriggerConfigTest extends AbstractScheduleTriggerConfigTest
{

	@Override
	SimpleTriggerConfig getTriggerConfig(TriggerKey key, CalculateNextEventRule calculateNextEventRule, ZoneId zone,
		ZonedDateTime startTime, ZonedDateTime endTime)
	{
		SimpleTriggerConfig triggerConfig = new SimpleTriggerConfig(key, calculateNextEventRule, startTime, endTime, 10,
			5);
		if (zone != null)
		{
			triggerConfig = triggerConfig.withZone(zone);
		}
		return triggerConfig;
	}

	@Test
	void getRepeatCountTest()
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2019-11-03T10:00:00");
		SimpleTriggerConfig config = getTriggerConfig(TriggerKey.from("test"), CalculateNextEventRule.NEXT, null,
			startTime, null);
		assertEquals(10, config.getRepeatCount());
	}

	@Test
	void getRepeatIntervalTest()
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2019-11-03T10:00:00");
		SimpleTriggerConfig config = getTriggerConfig(TriggerKey.from("test"), CalculateNextEventRule.NEXT, null,
			startTime, null);
		assertEquals(5, config.getRepeatInterval());
	}

	@Test
	void createTriggerTest_withoutEndTime_triggerIsCreated() throws ParseException, CreateTriggerException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2019-11-03T10:00:00");
		SimpleTriggerConfig config = new SimpleTriggerConfig(TriggerKey.from("test"), startTime, 10, 100);
		SimpleTrigger trigger = config.createTrigger();
		assertNotNull(trigger);
		assertAll(() -> assertEquals(TriggerKey.from("test"), trigger.getKey(), "wrong key"),
			() -> assertEquals(CalculateNextEventRule.NEAREST, trigger.getCalculateNextEventRule(), "wrong rule"),
			() -> assertEquals(startTime, trigger.getStartTime(), "wrong start time"),
			() -> assertNull(trigger.getEndTime(), "wrong end time"),
			() -> assertEquals(10, trigger.getRepeatCount(), "wrong repeat count"),
			() -> assertEquals(100, trigger.getRepeatInterval(), "wrong repeat interval"));
	}

	@Test
	void createTriggerTest_withAllParameters_triggerIsCreated() throws ParseException, CreateTriggerException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2019-11-03T10:00:00");
		ZonedDateTime endTime = TestHelper.createZonedDateTime("2019-11-04T10:00:00");
		SimpleTriggerConfig config = getTriggerConfig(TriggerKey.from("test"), CalculateNextEventRule.NEXT, null,
			startTime, endTime);
		SimpleTrigger trigger = config.createTrigger();
		assertNotNull(trigger);
		assertAll(() -> assertEquals(TriggerKey.from("test"), trigger.getKey(), "wrong key"),
			() -> assertEquals(CalculateNextEventRule.NEXT, trigger.getCalculateNextEventRule(), "wrong rule"),
			() -> assertEquals(startTime, trigger.getStartTime(), "wrong start time"),
			() -> assertEquals(endTime, trigger.getEndTime(), "wrong end time"),
			() -> assertEquals(10, trigger.getRepeatCount(), " wrong repeat count"),
			() -> assertEquals(5, trigger.getRepeatInterval(), "wrong repeat interval"));
	}

	@Test
	void withTriggerKeyTest()
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2019-11-03T10:00:00");
		SimpleTriggerConfig config = getTriggerConfig(TriggerKey.from("test"), CalculateNextEventRule.NEXT, null,
			startTime, null);
		SimpleTriggerConfig config2 = config.withTriggerKey(TriggerKey.from("test2"));
		assertNotNull(config2);
		assertAll(() -> assertEquals(TriggerKey.from("test"), config.getKey(), "config has wrong key"),
			() -> assertEquals(TriggerKey.from("test2"), config2.getKey(), "config2 has wrong key"),
			() -> assertNotEquals(System.identityHashCode(config), System.identityHashCode(config2),
				"hash codes should not equals"));
	}

	@Test
	void withCalculateNextEventRuleTest() throws ParseException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2019-11-03T10:00:00");
		SimpleTriggerConfig config = getTriggerConfig(TriggerKey.from("test"), CalculateNextEventRule.NEXT, null,
			startTime, null);
		SimpleTriggerConfig config2 = config.withCalculateNextEventRule(CalculateNextEventRule.NEAREST);
		assertNotNull(config2);
		assertAll(
			() -> assertEquals(CalculateNextEventRule.NEXT, config.getCalculateNextEventRule(),
				"config has wrong rule"),
			() -> assertEquals(CalculateNextEventRule.NEAREST, config2.getCalculateNextEventRule(),
				"config2 has wrong rule"),
			() -> assertNotEquals(System.identityHashCode(config), System.identityHashCode(config2),
				"hash codes should not equals"));
	}

	@Test
	void withZoneTest() throws ParseException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2019-11-03T10:00:00");
		SimpleTriggerConfig config = getTriggerConfig(TriggerKey.from("test"), CalculateNextEventRule.NEXT, null,
			startTime, null);
		SimpleTriggerConfig config2 = config.withZone(ZoneId.systemDefault());
		assertNotNull(config2);
		assertAll(() -> assertEquals(startTime.getZone(), config.getZone(), "config has wrong zone"),
			() -> assertEquals(ZoneId.systemDefault(), config2.getZone(), "config2 has wrong zone"),
			() -> assertNotEquals(System.identityHashCode(config), System.identityHashCode(config2),
				"hash codes should not equals"));
	}

	@Test
	void withStartTimeTest() throws ParseException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2019-11-03T10:00:00");
		ZonedDateTime startTime2 = TestHelper.createZonedDateTime("2019-11-04T10:00:00");
		SimpleTriggerConfig config = getTriggerConfig(TriggerKey.from("test"), CalculateNextEventRule.NEXT, null,
			startTime, null);
		SimpleTriggerConfig config2 = config.withStartTime(startTime2);
		assertNotNull(config2);
		assertAll(() -> assertEquals(startTime, config.getStartTime(), "config has wrong start time"),
			() -> assertEquals(startTime2, config2.getStartTime(), "config2 has wrong start time"),
			() -> assertNotEquals(System.identityHashCode(config), System.identityHashCode(config2),
				"hash codes should not equals"));
	}

	@Test
	void withEndTimeTest() throws ParseException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2019-11-03T10:00:00");
		ZonedDateTime endTime = TestHelper.createZonedDateTime("2019-11-04T18:00:00");
		ZonedDateTime endTime2 = TestHelper.createZonedDateTime("2019-11-04T19:00:00");
		SimpleTriggerConfig config = getTriggerConfig(TriggerKey.from("test"), CalculateNextEventRule.NEXT, null,
			startTime, endTime);
		SimpleTriggerConfig config2 = config.withEndTime(endTime2);
		assertNotNull(config2);
		assertAll(() -> assertEquals(endTime, config.getEndTime(), "config has wrong end time"),
			() -> assertEquals(endTime2, config2.getEndTime(), "config2 has wrong end time"),
			() -> assertNotEquals(System.identityHashCode(config), System.identityHashCode(config2),
				"hash codes should not equals"));
	}

	@Test
	void withRepeatCountTest()
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2019-11-03T10:00:00");
		SimpleTriggerConfig config = getTriggerConfig(TriggerKey.from("test"), CalculateNextEventRule.NEXT, null,
			startTime, null);
		SimpleTriggerConfig config2 = config.withRepeatCount(20);
		assertNotNull(config2);
		assertAll(() -> assertEquals(10, config.getRepeatCount(), "config has wrong repeat count"),
			() -> assertEquals(20, config2.getRepeatCount(), "config2 has wrong repeat count"),
			() -> assertNotEquals(System.identityHashCode(config), System.identityHashCode(config2),
				"hash codes should not equals"));
	}

	@Test
	void withRepeatIntervalTest()
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2019-11-03T10:00:00");
		SimpleTriggerConfig config = getTriggerConfig(TriggerKey.from("test"), CalculateNextEventRule.NEXT, null,
			startTime, null);
		SimpleTriggerConfig config2 = config.withRepeatInterval(15);
		assertNotNull(config2);
		assertAll(() -> assertEquals(5, config.getRepeatInterval(), "config has wrong repeat interval"),
			() -> assertEquals(15, config2.getRepeatInterval(), "config2 has wrong repeat interval"),
			() -> assertNotEquals(System.identityHashCode(config), System.identityHashCode(config2),
				"hash codes should not equals"));
	}
}
