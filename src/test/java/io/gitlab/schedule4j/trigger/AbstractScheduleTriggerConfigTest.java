/*
 * Created on 2019-10-27
 * 
 * Copyright 2019 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.trigger;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;

import io.gitlab.schedule4j.util.TestHelper;

/**
 * @author Dirk Buchhorn
 */
public abstract class AbstractScheduleTriggerConfigTest
{
	abstract AbstractScheduleTriggerConfig getTriggerConfig(TriggerKey key,
		CalculateNextEventRule calculateNextEventRule, ZoneId zone, ZonedDateTime startTime, ZonedDateTime endTime)
		throws Exception;

	private ZonedDateTime getStartTime()
	{
		return TestHelper.createZonedDateTime("2019-10-27T12:00:00");
	}

	private ZonedDateTime getEndTime()
	{
		return TestHelper.createZonedDateTime("2019-10-27T13:00:00");
	}

	@Test
	void getKeyTest() throws Exception
	{
		TriggerKey key = TriggerKey.from("test");
		AbstractScheduleTriggerConfig triggerConfig = getTriggerConfig(key, CalculateNextEventRule.NEAREST,
			ZoneId.systemDefault(), getStartTime(), getEndTime());
		assertEquals(key, triggerConfig.getKey());
	}

	@Test
	void getCalculateNextEventRuleTest() throws Exception
	{
		TriggerKey key = TriggerKey.from("test");
		AbstractScheduleTriggerConfig triggerConfig = getTriggerConfig(key, CalculateNextEventRule.NEAREST,
			ZoneId.systemDefault(), getStartTime(), getEndTime());
		assertEquals(CalculateNextEventRule.NEAREST, triggerConfig.getCalculateNextEventRule());
	}

	@Test
	void getZoneTest() throws Exception
	{
		TriggerKey key = TriggerKey.from("test");
		AbstractScheduleTriggerConfig triggerConfig = getTriggerConfig(key, CalculateNextEventRule.NEAREST,
			ZoneId.systemDefault(), getStartTime(), getEndTime());
		assertEquals(ZoneId.systemDefault(), triggerConfig.getZone());
	}

	@Test
	void getStartTimeTest() throws Exception
	{
		TriggerKey key = TriggerKey.from("test");
		AbstractScheduleTriggerConfig triggerConfig = getTriggerConfig(key, CalculateNextEventRule.NEAREST, null,
			getStartTime(), getEndTime());
		assertEquals(getStartTime(), triggerConfig.getStartTime());
	}

	@Test
	void getEndTimeTest() throws Exception
	{
		TriggerKey key = TriggerKey.from("test");
		AbstractScheduleTriggerConfig triggerConfig = getTriggerConfig(key, CalculateNextEventRule.NEAREST, null,
			getStartTime(), getEndTime());
		assertEquals(getEndTime(), triggerConfig.getEndTime());
	}

	@Test
	void correctZoneForDateTimeOrSetZoneTest() throws Exception
	{
		ZoneId systemZone = ZoneId.systemDefault();
		ZoneId z = null;
		// find a different zone
		for (String id : ZoneId.getAvailableZoneIds())
		{
			if (!id.equals(systemZone.getId()))
			{
				z = ZoneId.of(id);
				break;
			}
		}
		final ZoneId zone = z;
		TriggerKey key = TriggerKey.from("test");
		AbstractScheduleTriggerConfig triggerConfig = getTriggerConfig(key, CalculateNextEventRule.NEAREST, zone,
			getStartTime(), getEndTime());
		assertAll(() -> assertEquals(zone, triggerConfig.getStartTime().getZone()),
			() -> assertEquals(zone, triggerConfig.getEndTime().getZone()));
	}

	@Test
	void startTimeIsNullAndNotOptionalTest() throws Exception
	{
		TriggerKey key = TriggerKey.from("test");
		AbstractScheduleTriggerConfig triggerConfig = getTriggerConfig(key, CalculateNextEventRule.NEAREST, null,
			getStartTime(), getEndTime());
		if (!triggerConfig.isStartTimeOptional())
		{
			assertThrows(IllegalArgumentException.class,
				() -> getTriggerConfig(key, CalculateNextEventRule.NEAREST, null, null, getEndTime()));
		}
	}
}
