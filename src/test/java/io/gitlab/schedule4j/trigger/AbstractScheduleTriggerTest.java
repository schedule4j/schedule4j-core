/*
 * Created on 18.11.2015
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.trigger;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;

import io.gitlab.schedule4j.util.TestHelper;

/**
 * @author Dirk Buchhorn
 */
public abstract class AbstractScheduleTriggerTest
{
	public abstract AbstractScheduleTrigger<?> createInstance(TriggerKey key);

	public abstract AbstractScheduleTrigger<?> createInstance(TriggerKey key, ZonedDateTime startTime,
		ZonedDateTime endTime);

	@Test
	void constructorTest_trickerConfigIsNull_throwsException()
	{
		assertThrows(IllegalArgumentException.class, () -> new TestScheduleTrigger(null, 0L));
	}

	@Test
	void constructorTriggerKeyTest_trickerKeyIsNull_throwsException()
	{
		assertThrows(IllegalArgumentException.class, () -> createInstance(null));
	}

	@Test
	void constructorTriggerKeyTest_triggerKeyIsNotNull_triggerIsCreated()
	{
		assertNotNull(createInstance(new TriggerKey("test")));
	}

	@Test
	void startTimeAfterEndTimeTest()
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-02T00:00:00");
		ZonedDateTime endTime = TestHelper.createZonedDateTime("2010-01-01T00:00:00");
		assertThrows(IllegalArgumentException.class, () -> createInstance(TriggerKey.from("test"), startTime, endTime));
	}

	@Test
	void getCalculateNextEventRuleTest()
	{
		AbstractScheduleTrigger<?> trigger = createInstance(new TriggerKey("test"));
		assertNotNull(trigger.getCalculateNextEventRule());
	}

	private class TestScheduleTrigger extends AbstractScheduleTrigger<SimpleTriggerConfig>
	{
		protected TestScheduleTrigger(SimpleTriggerConfig triggerConfig, long precisionInNanos)
		{
			super(triggerConfig, precisionInNanos);
		}

		@Override
		public ZonedDateTime getFirstFireTime()
		{
			return null;
		}

		@Override
		public ZonedDateTime getLastFireTime()
		{
			return null;
		}

		@Override
		public ZonedDateTime getNextFireTime(ZonedDateTime calculationTime)
		{
			return null;
		}

		@Override
		public ZonedDateTime getPreviousFireTime(ZonedDateTime dateTime)
		{
			return null;
		}
	}
}
