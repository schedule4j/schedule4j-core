/*
 * Created on 09.10.2015
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.trigger;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

import io.gitlab.schedule4j.common.KeyTest;

/**
 * @author Dirk Buchhorn
 */
public class TriggerKeyTest extends KeyTest<TriggerKey>
{
	@Override
	protected TriggerKey create(TriggerKey parentKey, String name)
	{
		return TriggerKey.from(parentKey, name);
	}

	@Override
	protected TriggerKey create(String... keyNames)
	{
		return TriggerKey.from(keyNames);
	}

	@Test
	void fromTest_keyNamesIsNull_returnNull()
	{
		assertNull(TriggerKey.from((String[]) null));
	}

	@Test
	void fromTest_keyNamesIsEmpty_returnNull()
	{
		assertNull(TriggerKey.from(new String[0]));
	}

	@Test
	void fromTest_keyNamesWithOneKeyName_parentKeyIsNull()
	{
		TriggerKey key = TriggerKey.from("test");
		assertAll(() -> assertNotNull(key, "key was null"), //
			() -> assertEquals("test", key.getName(), "wrong key name"), //
			() -> assertNull(key.getParentKey(), "parent key was not null"));
	}

	@Test
	void fromTest_keyNamesWithTwoKeyNames_keyHasOneParent()
	{
		TriggerKey key = TriggerKey.from("parent", "test");
		assertAll(() -> assertNotNull(key, "key was null"), //
			() -> assertEquals("test", key.getName(), "wrong key name"), //
			() -> assertNotNull(key.getParentKey(), "parent key was null"), //
			() -> assertEquals("parent", key.getParentKey().getName(), "wrong parent key name"),
			() -> assertNull(key.getParentKey().getParentKey()));
	}

	@Test
	void fromTest_keyNamesWithThreeKeyNames_keyHasTwoParents()
	{
		TriggerKey key = TriggerKey.from("group", "subGroup", "test");
		assertAll(() -> assertNotNull(key, "key was null"), //
			() -> assertEquals("test", key.getName(), "wrong key name"), //
			() -> assertNotNull(key.getParentKey(), "subGroup was null"), //
			() -> assertEquals("subGroup", key.getParentKey().getName(), "wrong subGroup key name"),
			() -> assertNotNull(key.getParentKey().getParentKey(), "group was null"), //
			() -> assertEquals("group", key.getParentKey().getParentKey().getName(), "wrong group key name"),
			() -> assertNull(key.getParentKey().getParentKey().getParentKey()));
	}

	@Test
	void fromTest_noParent_parentKeyIsNull()
	{
		TriggerKey key = TriggerKey.from(null, "test");
		assertAll(() -> assertNotNull(key, "key was null"), //
			() -> assertEquals("test", key.getName(), "wrong key name"), //
			() -> assertNull(key.getParentKey(), "parent key was not null"));
	}

	@Test
	void fromTest_withParent_parentKeyIsNull()
	{
		TriggerKey key = TriggerKey.from(TriggerKey.from("parent"), "test");
		assertAll(() -> assertNotNull(key, "key was null"), //
			() -> assertEquals("test", key.getName(), "wrong key name"), //
			() -> assertNotNull(key.getParentKey(), "parent key was null"), //
			() -> assertEquals("parent", key.getParentKey().getName(), "wrong parent key name"),
			() -> assertNull(key.getParentKey().getParentKey()));
	}
}
