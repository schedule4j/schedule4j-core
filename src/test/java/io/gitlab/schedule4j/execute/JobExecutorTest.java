/*
 * Created on 16.09.2017
 * 
 * Copyright 2017 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.execute;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.Serializable;
import java.text.ParseException;
import java.time.ZonedDateTime;
import java.util.Map;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import io.gitlab.schedule4j.job.JobDescriptor;
import io.gitlab.schedule4j.job.JobExecutionContext;
import io.gitlab.schedule4j.job.JobKey;
import io.gitlab.schedule4j.job.SimpleTestJob;
import io.gitlab.schedule4j.trigger.SimpleTriggerConfig;
import io.gitlab.schedule4j.trigger.TriggerEvent;
import io.gitlab.schedule4j.trigger.TriggerKey;

/**
 * @author Dirk Buchhorn
 */
public class JobExecutorTest
{
	private static final Logger logger = (Logger) LoggerFactory.getLogger(JobExecutor.class);

	private ListAppender<ILoggingEvent> appender;

	@BeforeEach
	void beforeEach()
	{
		appender = new ListAppender<>();
		appender.start();
		logger.addAppender(appender);
	}

	@AfterEach
	void afterEach()
	{
		logger.detachAppender(appender);
	}

	@Test
	void constructorTest_JobExecutionContextIsNull_throwsIllegalArgumentException()
	{
		assertThrows(IllegalArgumentException.class, () -> new JobExecutor(null, new TestJobExecutionListener()));
	}

	@Test
	void constructorTest_JobExecutionListenerIsNull_throwsIllegalArgumentException()
	{
		assertThrows(IllegalArgumentException.class, () -> new JobExecutor(createContext(), null));
	}

	@Test
	void constructorTest() throws ParseException
	{
		JobExecutionContext context = createContext();
		JobExecutor jobExecutor = new JobExecutor(context, new TestJobExecutionListener());
		assertEquals(context, jobExecutor.getJobExecutionContext());
	}

	@Test
	void runTest_jobThrowsNoException_listenerIsCalled() throws ParseException
	{
		TestJobExecutionListener listener = new TestJobExecutionListener();
		JobExecutor jobExecutor = new JobExecutor(createContext(), listener);
		Map<String, Serializable> map = jobExecutor.getJobExecutionContext().getJobDescriptor().getJobDataMap();
		jobExecutor.run();

		assertAll(() -> assertTrue(map.containsKey(SimpleTestJob.EXECUTED_FLAG)),
			() -> assertEquals(1, listener.runningCount, "runningCount was not 1"),
			() -> assertEquals(1, listener.executedCount, "executedCount was not 1"),
			() -> assertEquals(0, listener.exceptionCount, "exceptionCount was not 0"),
			() -> assertEquals(0, listener.notExecutedCount, "notExecutedCount was not 0"));
	}

	@Test
	void runTest_jobThrowsException_listenerIsCalled() throws ParseException
	{
		TestJobExecutionListener listener = new TestJobExecutionListener();
		JobExecutor jobExecutor = new JobExecutor(createContext(), listener);
		Map<String, Serializable> map = jobExecutor.getJobExecutionContext().getJobDescriptor().getJobDataMap();
		map.put(SimpleTestJob.THROW_EXCEPTION_FLAG, Boolean.TRUE);
		jobExecutor.run();

		assertAll(() -> assertTrue(map.containsKey(SimpleTestJob.EXECUTED_FLAG)),
			() -> assertEquals(1, listener.runningCount, "runningCount was not 1"),
			() -> assertEquals(1, listener.executedCount, "executedCount was not 1"),
			() -> assertEquals(1, listener.exceptionCount, "exceptionCount was not 1"),
			() -> assertEquals(0, listener.notExecutedCount, "notExecutedCount was not 0"));
	}

	@Test
	void priorityTest() throws ParseException
	{
		JobExecutor jobExecutor = new JobExecutor(createContext(), new TestJobExecutionListener());
		int priority = jobExecutor.getJobExecutionContext().getJobDescriptor().getPriority();
		jobExecutor.setPriority(priority + 10);

		assertAll(
			() -> assertEquals(priority, jobExecutor.getJobExecutionContext().getJobDescriptor().getPriority(),
				"priority in the JobDescriptor has changed"),
			() -> assertEquals(priority + 10, jobExecutor.getPriority(), "get wrong priority"));
	}

	@Test
	void plusPriorityTest_plusPositiveValue_priorityIsIncreased() throws ParseException
	{
		JobExecutor jobExecutor = new JobExecutor(createContext(), new TestJobExecutionListener());
		jobExecutor.plusPriority(10);

		assertEquals(10, jobExecutor.getPriority());
	}

	@Test
	void plusPriorityTest_plusNegativeValue_priorityIsDecreased() throws ParseException
	{
		JobExecutor jobExecutor = new JobExecutor(createContext(), new TestJobExecutionListener());
		jobExecutor.plusPriority(-10);

		assertEquals(-10, jobExecutor.getPriority());
	}

	@Test
	void plusPriorityTest_minusPositiveValue_priorityIsDecreased() throws ParseException
	{
		JobExecutor jobExecutor = new JobExecutor(createContext(), new TestJobExecutionListener());
		jobExecutor.minusPriority(10);

		assertEquals(-10, jobExecutor.getPriority());
	}

	@Test
	void plusPriorityTest_minusNegativeValue_priorityIsIncreased() throws ParseException
	{
		JobExecutor jobExecutor = new JobExecutor(createContext(), new TestJobExecutionListener());
		jobExecutor.minusPriority(-10);

		assertEquals(10, jobExecutor.getPriority());
	}

	@Test
	void fireJobRunningTest_fireJobRunningCalled_listenerIsCalled() throws ParseException
	{
		TestJobExecutionListener listener = new TestJobExecutionListener();
		JobExecutor jobExecutor = new JobExecutor(createContext(), listener);
		jobExecutor.fireJobRunning();

		assertAll(() -> assertEquals(1, listener.runningCount, "runningCount was not 1"),
			() -> assertEquals(0, listener.executedCount, "executedCount was not 0"),
			() -> assertEquals(0, listener.exceptionCount, "exceptionCount was not 0"),
			() -> assertEquals(0, listener.notExecutedCount, "notExecutedCount was not 0"));
	}

	@Test
	void fireJobRunningTest_listenerThrowsException_exceptionIsCatched() throws ParseException
	{
		TestJobExecutionListener listener = new TestJobExecutionListener();
		JobExecutor jobExecutor = new JobExecutor(createContext(), listener);
		listener.throwException = true;
		jobExecutor.fireJobRunning();

		assertAll(() -> assertEquals(1, listener.runningCount, "runningCount was not 1"),
			() -> assertEquals(0, listener.executedCount, "executedCount was not 0"),
			() -> assertEquals(0, listener.exceptionCount, "exceptionCount was not 0"),
			() -> assertEquals(0, listener.notExecutedCount, "notExecutedCount was not 0"));
	}

	@Test
	void fireJobRunningTest_methodIsCalledTwice_listenerIsCalledOnce() throws ParseException
	{
		TestJobExecutionListener listener = new TestJobExecutionListener();
		JobExecutor jobExecutor = new JobExecutor(createContext(), listener);
		jobExecutor.fireJobRunning();
		jobExecutor.fireJobRunning();

		assertAll(() -> assertEquals(1, listener.runningCount, "runningCount was not 1"),
			() -> assertEquals(0, listener.executedCount, "executedCount was not 0"),
			() -> assertEquals(0, listener.exceptionCount, "exceptionCount was not 0"),
			() -> assertEquals(0, listener.notExecutedCount, "notExecutedCount was not 0"), () ->
			{
				assertEquals(1, appender.list.size(), "size of captured log messages was not 1");
				assertAll(() -> assertEquals(Level.WARN, appender.list.get(0).getLevel(), "wrong log level"),
					() -> assertEquals("Method fireJobRunning can only be called once",
						appender.list.get(0).getMessage(), "wrong message"));
			});
	}

	@Test
	void fireJobRunningTest_fireJobNotExecutedWasAlreadyCalled_logWarning() throws ParseException
	{
		TestJobExecutionListener listener = new TestJobExecutionListener();
		JobExecutor jobExecutor = new JobExecutor(createContext(), listener);
		jobExecutor.fireJobNotExecuted(null);
		jobExecutor.fireJobRunning();

		assertEquals(1, appender.list.size(), "size of captured log messages was not 1");
		assertAll(() -> assertEquals(Level.WARN, appender.list.get(0).getLevel(), "wrong log level"),
			() -> assertEquals("Method fireJobNotExecuted was already called", appender.list.get(0).getMessage(),
				"wrong message"));
	}

	@Test
	void fireJobExecutedTest_noException_listenerIsCalled() throws ParseException
	{
		TestJobExecutionListener listener = new TestJobExecutionListener();
		JobExecutor jobExecutor = new JobExecutor(createContext(), listener);
		// jobExecutor.fireJobRunning(); // we don't call it, it must be called by the JobExecutor
		jobExecutor.fireJobExecuted(null);

		assertAll(() -> assertEquals(1, listener.runningCount, "runningCount was not 1"),
			() -> assertEquals(1, listener.executedCount, "executedCount was not 1"),
			() -> assertEquals(0, listener.exceptionCount, "exceptionCount was not 0"),
			() -> assertEquals(0, listener.notExecutedCount, "notExecutedCount was not 0"));
	}

	@Test
	void fireJobExecutedTest_withException_listenerIsCalled() throws ParseException
	{
		TestJobExecutionListener listener = new TestJobExecutionListener();
		JobExecutor jobExecutor = new JobExecutor(createContext(), listener);
		jobExecutor.fireJobRunning();
		jobExecutor.fireJobExecuted(new NullPointerException());

		assertAll(() -> assertEquals(1, listener.runningCount, "runningCount was not 1"),
			() -> assertEquals(1, listener.executedCount, "executedCount was not 1"),
			() -> assertEquals(1, listener.exceptionCount, "exceptionCount was not 1"),
			() -> assertEquals(0, listener.notExecutedCount, "notExecutedCount was not 0"));
	}

	@Test
	void fireJobExecutedTest_methodIsCalledTwice_listenerIsCalledOnce() throws ParseException
	{
		TestJobExecutionListener listener = new TestJobExecutionListener();
		JobExecutor jobExecutor = new JobExecutor(createContext(), listener);
		jobExecutor.fireJobRunning();
		jobExecutor.fireJobExecuted(null);
		jobExecutor.fireJobExecuted(null);

		assertAll(() -> assertEquals(1, listener.runningCount, "runningCount was not 1"),
			() -> assertEquals(1, listener.executedCount, "executedCount was not 1"),
			() -> assertEquals(0, listener.exceptionCount, "exceptionCount was not 0"),
			() -> assertEquals(0, listener.notExecutedCount, "notExecutedCount was not 0"), () ->
			{
				assertEquals(1, appender.list.size(), "size of captured log messages was not 1");
				assertAll(() -> assertEquals(Level.WARN, appender.list.get(0).getLevel(), "wrong log level"),
					() -> assertEquals("Method fireJobExecuted can only be called once",
						appender.list.get(0).getMessage(), "wrong message"));
			});
	}

	@Test
	void fireJobExecutedTest_listenerThrowsException_exceptionIsCatched() throws ParseException
	{
		TestJobExecutionListener listener = new TestJobExecutionListener();
		JobExecutor jobExecutor = new JobExecutor(createContext(), listener);
		jobExecutor.fireJobRunning();
		listener.throwException = true;
		jobExecutor.fireJobExecuted(null);

		assertAll(() -> assertEquals(1, listener.runningCount, "runningCount was not 1"),
			() -> assertEquals(1, listener.executedCount, "executedCount was not 1"),
			() -> assertEquals(0, listener.exceptionCount, "exceptionCount was not 0"),
			() -> assertEquals(0, listener.notExecutedCount, "notExecutedCount was not 0"));
	}

	@Test
	void fireJobExecutedTest_fireJobNotExecutedWasAlreadyCalled_logWarning() throws ParseException
	{
		TestJobExecutionListener listener = new TestJobExecutionListener();
		JobExecutor jobExecutor = new JobExecutor(createContext(), listener);
		jobExecutor.fireJobRunning();
		jobExecutor.fireJobNotExecuted(null);
		jobExecutor.fireJobExecuted(null);

		assertEquals(1, appender.list.size(), "size of captured log messages was not 1");
		assertAll(() -> assertEquals(Level.WARN, appender.list.get(0).getLevel(), "wrong log level"),
			() -> assertEquals("Method fireJobNotExecuted was already called", appender.list.get(0).getMessage(),
				"wrong message"));
	}

	@Test
	void fireJobNotExecutedTest_noException_listenerIsCalled() throws ParseException
	{
		TestJobExecutionListener listener = new TestJobExecutionListener();
		JobExecutor jobExecutor = new JobExecutor(createContext(), listener);
		jobExecutor.fireJobNotExecuted(null);

		assertAll(() -> assertEquals(0, listener.runningCount, "runningCount was not 0"),
			() -> assertEquals(0, listener.executedCount, "executedCount was not 0"),
			() -> assertEquals(0, listener.exceptionCount, "exceptionCount was not 0"),
			() -> assertEquals(1, listener.notExecutedCount, "notExecutedCount was not 1"));
	}

	@Test
	void fireJobNotExecutedTest_withException_listenerIsCalled() throws ParseException
	{
		TestJobExecutionListener listener = new TestJobExecutionListener();
		JobExecutor jobExecutor = new JobExecutor(createContext(), listener);
		jobExecutor.fireJobNotExecuted(new NullPointerException());

		assertAll(() -> assertEquals(0, listener.runningCount, "runningCount was not 0"),
			() -> assertEquals(0, listener.executedCount, "executedCount was not 0"),
			() -> assertEquals(1, listener.exceptionCount, "exceptionCount was not 1"),
			() -> assertEquals(1, listener.notExecutedCount, "notExecutedCount was not 1"));
	}

	@Test
	void fireJobNotExecutedTest_methodIsCalledTwice_listenerIsCalledOnce() throws ParseException
	{
		TestJobExecutionListener listener = new TestJobExecutionListener();
		JobExecutor jobExecutor = new JobExecutor(createContext(), listener);
		jobExecutor.fireJobNotExecuted(null);
		jobExecutor.fireJobNotExecuted(null);

		assertAll(() -> assertEquals(0, listener.runningCount, "runningCount was not 0"),
			() -> assertEquals(0, listener.executedCount, "executedCount was not 0"),
			() -> assertEquals(0, listener.exceptionCount, "exceptionCount was not 0"),
			() -> assertEquals(1, listener.notExecutedCount, "notExecutedCount was not 1"), () ->
			{
				assertEquals(1, appender.list.size(), "size of captured log messages was not 1");
				assertAll(() -> assertEquals(Level.WARN, appender.list.get(0).getLevel(), "wrong log level"),
					() -> assertEquals("Method fireJobNotExecuted can only be called once",
						appender.list.get(0).getMessage(), "wrong message"));
			});
	}

	@Test
	void fireJobNotExecutedTest_listenerThrowsException_exceptionIsCatched() throws ParseException
	{
		TestJobExecutionListener listener = new TestJobExecutionListener();
		JobExecutor jobExecutor = new JobExecutor(createContext(), listener);
		listener.throwException = true;
		jobExecutor.fireJobNotExecuted(null);

		assertAll(() -> assertEquals(0, listener.runningCount, "runningCount was not 0"),
			() -> assertEquals(0, listener.executedCount, "executedCount was not 0"),
			() -> assertEquals(0, listener.exceptionCount, "exceptionCount was not 0"),
			() -> assertEquals(1, listener.notExecutedCount, "notExecutedCount was not 1"));
	}

	@Test
	void fireJobNotExecutedTest_fireJobExecutedWasAlreadyCalled_logWarning() throws ParseException
	{
		TestJobExecutionListener listener = new TestJobExecutionListener();
		JobExecutor jobExecutor = new JobExecutor(createContext(), listener);
		jobExecutor.fireJobRunning();
		jobExecutor.fireJobExecuted(null);
		jobExecutor.fireJobNotExecuted(null);

		assertEquals(1, appender.list.size(), "size of captured log messages was not 1");
		assertAll(() -> assertEquals(Level.WARN, appender.list.get(0).getLevel(), "wrong log level"),
			() -> assertEquals("Method fireJobExecuted was already called", appender.list.get(0).getMessage(),
				"wrong message"));
	}

	private JobDescriptor createJobDescriptor()
	{
		return new JobDescriptor(JobKey.from("test-job"), SimpleTestJob.class,
			new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10, 100));
	}

	private JobExecutionContext createContext() throws ParseException
	{
		JobDescriptor jobDescriptor = createJobDescriptor();
		TriggerEvent triggerEvent = new TriggerEvent(ZonedDateTime.now(), ZonedDateTime.now());
		JobExecutionContext ctx = new JobExecutionContext(jobDescriptor, triggerEvent, ZonedDateTime.now());
		return ctx;
	}

	private class TestJobExecutionListener implements JobExecutionListener
	{
		private boolean throwException = false;
		private int runningCount;
		private int executedCount;
		private int exceptionCount;
		private int notExecutedCount;

		@Override
		public void jobRunning(JobExecutionContext ctx)
		{
			runningCount++;
			if (throwException)
			{
				throw new RuntimeException("Something went wrong");
			}
		}

		@Override
		public void jobExecuted(JobExecutionContext ctx, Exception exception)
		{
			executedCount++;
			if (exception != null)
			{
				exceptionCount++;
			}
			if (throwException)
			{
				throw new RuntimeException("Something went wrong");
			}
		}

		@Override
		public void jobNotExecuted(JobExecutionContext ctx, Exception exception)
		{
			notExecutedCount++;
			if (exception != null)
			{
				exceptionCount++;
			}
			if (throwException)
			{
				throw new RuntimeException("Something went wrong");
			}
		}
	}
}
