/*
 * Created on 16.09.2017
 * 
 * Copyright 2017 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.execute;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;

import io.gitlab.schedule4j.priority.BoundedPriorityBlockingQueue;
import io.gitlab.schedule4j.priority.Priority;

/**
 * @author Dirk Buchhorn
 */
public class BlockingQueueWrapperTest
{
	@Test
	void constructorTest_queueIsNull_throwsIllegalArgumentException()
	{
		assertThrows(IllegalArgumentException.class, () -> new BlockingQueueWrapper<Runnable, TestRunnable>(null));
	}

	@Test
	void constructorTest()
	{
		BlockingQueueWrapper<Runnable, TestRunnable> queueWrapper = new BlockingQueueWrapper<>(
			new BoundedPriorityBlockingQueue<TestRunnable>(100));
		assertEquals(0, queueWrapper.size());
	}

	@Test
	void addTest_addElement_throwsUnsupportedOperationException()
	{
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(
			new BoundedPriorityBlockingQueue<TestRunnable>(100));
		assertThrows(UnsupportedOperationException.class, () -> bqw.add(new TestRunnable()));
	}

	@Test
	void addAllTest_addElements_throwsUnsupportedOperationException()
	{
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(
			new BoundedPriorityBlockingQueue<TestRunnable>(100));
		ArrayList<TestRunnable> list = new ArrayList<>();
		assertThrows(UnsupportedOperationException.class, () -> bqw.addAll(list));
	}

	@Test()
	void clearTest_queueContainsOneElement_queueIsEmpty()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		queue.add(new TestRunnable());
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		assertEquals(1, queue.size());

		bqw.clear();
		assertEquals(0, queue.size());
	}

	@Test()
	void containsTest()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		TestRunnable tr = new TestRunnable();
		queue.add(tr);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);

		assertAll(() -> assertTrue(bqw.contains(tr), "queue does not contain expected object"),
			() -> assertFalse(bqw.contains(new TestRunnable()), "queue does contain an unexpected object"));
	}

	@Test()
	void containsAllTest()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		TestRunnable tr1 = new TestRunnable();
		queue.add(tr1);
		TestRunnable tr2 = new TestRunnable();
		queue.add(tr2);
		TestRunnable tr3 = new TestRunnable();
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);

		List<TestRunnable> list = new ArrayList<>();
		list.add(tr1);
		list.add(tr2);
		List<TestRunnable> list2 = new ArrayList<>(list);
		list2.add(tr3);

		assertAll(() -> assertTrue(bqw.containsAll(list), "queue does not contain all list elements"),
			() -> assertFalse(bqw.containsAll(list2), "queue does contain all list elements"));
	}

	@Test()
	void drainToTest()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		queue.add(new TestRunnable());
		queue.add(new TestRunnable());
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		List<Runnable> list = new ArrayList<>();
		bqw.drainTo(list);
		assertAll(() -> assertEquals(0, queue.size(), "queue size was not 0"),
			() -> assertEquals(2, list.size(), "list size was not 2"));
	}

	@Test()
	void drainToMaxTest()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		queue.add(new TestRunnable());
		queue.add(new TestRunnable());
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		List<Runnable> list = new ArrayList<>();
		bqw.drainTo(list, 1);
		assertAll(() -> assertEquals(1, queue.size(), "queue size was not 1"),
			() -> assertEquals(1, bqw.size(), "wrapper queue size was not 1"),
			() -> assertEquals(1, list.size(), "list size was not 1"));
	}

	@Test
	void elementTest()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		TestRunnable tr1 = new TestRunnable();
		queue.add(tr1);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		assertEquals(tr1, bqw.element());
	}

	@Test
	void isEmpty_noElementsInQueue_returnTrue()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		assertTrue(bqw.isEmpty());
	}

	@Test
	void isEmpty_elementsInQueue_returnFalse()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		queue.add(new TestRunnable());
		assertFalse(bqw.isEmpty());
	}

	@Test
	void iteratorRemoveTest_noHasNextMethodWasCalled_throwsIllegalStateException()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		assertThrows(IllegalStateException.class, () -> bqw.iterator().remove());
	}

	@Test
	void iteratorRemoveTest_iteratorHasElements_elementIsRemoved()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		TestRunnable tr1 = new TestRunnable();
		queue.add(tr1);
		queue.add(new TestRunnable());
		Iterator<Runnable> iter = bqw.iterator();
		assertTrue(iter.hasNext());
		assertEquals(tr1, iter.next());
		iter.remove();
		assertEquals(1, bqw.size());
	}

	@Test
	void iteratorRemoveTest_removeIsCalledTwice_throwsIllegalStateException()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		queue.add(new TestRunnable());
		Iterator<Runnable> iter = bqw.iterator();
		iter.next();
		iter.remove();
		assertThrows(IllegalStateException.class, () -> iter.remove());
	}

	@Test
	void iteratorNextTest_iteratorHasNoNextElement_throwsNoSuchElementException()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		Iterator<Runnable> iter = bqw.iterator();
		assertThrows(NoSuchElementException.class, () -> iter.next());
	}

	@Test
	void offerTest_throwsUnsupportedOperationException()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		assertThrows(UnsupportedOperationException.class, () -> bqw.offer(new TestRunnable()));
	}

	@Test
	void offerTimeoutTest_throwsUnsupportedOperationException() throws InterruptedException
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		assertThrows(UnsupportedOperationException.class,
			() -> bqw.offer(new TestRunnable(), 10, TimeUnit.MILLISECONDS));
	}

	@Test
	void peekTest_emptyQueue_returnNull()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		assertNull(bqw.peek());
	}

	@Test
	void peekTest_queueHasElements_returnTheHeadElement()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		TestRunnable tr1 = new TestRunnable();
		queue.add(tr1);
		queue.add(new TestRunnable());
		assertEquals(tr1, bqw.peek());
		assertEquals(2, bqw.size());
	}

	@Test
	void pollTest_emptyQueue_returnNull()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		assertNull(bqw.poll());
	}

	@Test
	void pollTest_queueHasElements_returnTheHeadElement()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		TestRunnable tr1 = new TestRunnable();
		queue.add(tr1);
		queue.add(new TestRunnable());
		assertEquals(tr1, bqw.poll());
		assertEquals(1, bqw.size());
	}

	@Test
	void pollTimeoutTest_emptyQueue_returnNull() throws InterruptedException
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		assertNull(bqw.poll(10, TimeUnit.MILLISECONDS));
	}

	@Test
	void pollTimeoutTest_queueHasElements_returnTheHeadElement() throws InterruptedException
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		TestRunnable tr1 = new TestRunnable();
		queue.add(tr1);
		queue.add(new TestRunnable());
		assertEquals(tr1, bqw.poll(10, TimeUnit.MILLISECONDS));
		assertEquals(1, bqw.size());
	}

	@Test
	void putTest_throwsUnsupportedOperationException() throws InterruptedException
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		TestRunnable tr1 = new TestRunnable();
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		assertThrows(UnsupportedOperationException.class, () -> bqw.put(tr1));
	}

	@Test
	void remainingCapacity()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		assertEquals(100, bqw.remainingCapacity());
		queue.add(new TestRunnable());
		assertEquals(99, bqw.remainingCapacity());
	}

	@Test
	void removeTest()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		TestRunnable tr1 = new TestRunnable();
		queue.add(tr1);
		assertAll(() -> assertEquals(tr1, bqw.remove(), "wrong object was removed"), //
			() -> assertEquals(0, bqw.size(), "queue size was not 0"));
	}

	@Test
	void removeTest_objectIsInQueue_objectIsRemoved()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		TestRunnable tr1 = new TestRunnable();
		queue.add(tr1);
		TestRunnable tr2 = new TestRunnable();
		queue.add(tr2);
		assertAll(() -> assertTrue(bqw.remove(tr2), "object could not be removed"), //
			() -> assertEquals(1, queue.size(), "queue size was not 1"), //
			() -> assertEquals(tr1, bqw.peek(), "queue contains the wrong object"));
	}

	@Test
	void removeTest_objectIsNotInQueue_objectCouldNotRemoved()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		TestRunnable tr1 = new TestRunnable();
		queue.add(tr1);
		assertAll(() -> assertFalse(bqw.remove(new TestRunnable()), "object could be removed"), //
			() -> assertEquals(1, bqw.size(), "queue size was not 1"));
	}

	@Test
	void removeAllTest_queueContainsAllListElements_elementsAreRemoved()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		TestRunnable tr1 = new TestRunnable();
		queue.add(tr1);
		TestRunnable tr2 = new TestRunnable();
		queue.add(tr2);
		List<Runnable> list = new ArrayList<>();
		list.add(tr1);
		assertAll(() -> assertTrue(bqw.removeAll(list), "queue was not changed"), //
			() -> assertEquals(1, bqw.size(), "queue size was not 1"));
	}

	@Test
	void removeAllTest_queueContainsNotAllListElements_elementsAreRemoved()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		TestRunnable tr1 = new TestRunnable();
		queue.add(tr1);
		TestRunnable tr2 = new TestRunnable();
		queue.add(tr2);
		List<Runnable> list = new ArrayList<>();
		list.add(tr1);
		list.add(new TestRunnable());
		assertAll(() -> assertTrue(bqw.removeAll(list), "queue was not changed"), //
			() -> assertEquals(1, bqw.size(), "queue size was not 1"));
	}

	@Test
	void removeAllTest_queueContainsNoListElements_nothingIsRemoved()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		TestRunnable tr1 = new TestRunnable();
		queue.add(tr1);
		List<Runnable> list = new ArrayList<>();
		list.add(new TestRunnable());
		assertAll(() -> assertFalse(bqw.removeAll(list), "queue was changed"), //
			() -> assertEquals(1, bqw.size(), "queue size was not 1"));
	}

	@Test
	void retainAllTest_throwsUnsupportedOperationException()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		List<Runnable> list = new ArrayList<>();
		assertThrows(UnsupportedOperationException.class, () -> bqw.retainAll(list));
	}

	@Test
	void sizeTest_emptyQueue_queueSizeIsZero()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		assertEquals(0, bqw.size());
	}

	@Test
	void sizeTest_addTwoElements_queueSizeIsTwo()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		queue.add(new TestRunnable());
		queue.add(new TestRunnable());
		assertEquals(2, bqw.size());
	}

	@Test
	void takeTest() throws InterruptedException
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		TestRunnable tr1 = new TestRunnable();
		queue.add(tr1);
		assertAll(() -> assertEquals(tr1, bqw.take(), "wrong element"), //
			() -> assertEquals(0, queue.size(), "queue size was not 0"));
	}

	@Test
	void toArrayTest_emptyQueue_arrayIsEmpty()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		Object[] a = bqw.toArray();
		assertAll(() -> assertNotNull(a, "array was null"), //
			() -> assertEquals(0, a.length, "array length was not 0"));
	}

	@Test
	void toArrayTest_queueHasOneElement_returnArrayOfLengthOne()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		TestRunnable tr1 = new TestRunnable();
		queue.add(tr1);
		Object[] a = bqw.toArray();
		assertAll(() -> assertNotNull(a, "array was null"), //
			() -> assertEquals(1, a.length, "array length was not 1"), () -> assertEquals(tr1, a[0], "wrong element"));
	}

	@Test
	void toArrayTest_queueHasOneElement_returnTypedArrayOfLengthOne()
	{
		BoundedPriorityBlockingQueue<TestRunnable> queue = new BoundedPriorityBlockingQueue<TestRunnable>(100);
		BlockingQueueWrapper<Runnable, TestRunnable> bqw = new BlockingQueueWrapper<>(queue);
		TestRunnable tr1 = new TestRunnable();
		queue.add(tr1);

		TestRunnable[] a = bqw.toArray(new TestRunnable[1]);
		assertAll(() -> assertNotNull(a, "array was null"), //
			() -> assertEquals(1, a.length, "array length was not 1"), () -> assertEquals(tr1, a[0], "wrong element"));
	}

	private class TestRunnable implements Runnable, Priority
	{
		@Override
		public void run()
		{
		}

		@Override
		public int getPriority()
		{
			return 0;
		}

		@Override
		public void setPriority(int priority)
		{
		}

		@Override
		public void plusPriority(int priority)
		{
		}

		@Override
		public void minusPriority(int priority)
		{
		}
	}
}
