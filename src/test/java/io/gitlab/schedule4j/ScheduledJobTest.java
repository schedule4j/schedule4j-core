/*
 * Created on 31.10.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;

import io.gitlab.schedule4j.job.JobDescriptor;
import io.gitlab.schedule4j.job.JobKey;
import io.gitlab.schedule4j.job.JobListener;
import io.gitlab.schedule4j.job.SimpleJobListener;
import io.gitlab.schedule4j.job.SimpleTestJob;
import io.gitlab.schedule4j.trigger.SimpleTriggerConfig;
import io.gitlab.schedule4j.trigger.Trigger;
import io.gitlab.schedule4j.trigger.TriggerConfig;
import io.gitlab.schedule4j.trigger.TriggerKey;
import io.gitlab.schedule4j.util.TestHelper;

/**
 * @author Dirk Buchhorn
 */
public class ScheduledJobTest
{
	private Trigger getTrigger()
	{
		return new SimpleTriggerConfig(TriggerKey.from("test"), TestHelper.createZonedDateTime("2016-10-31T00:00:00"),
			10, 10000).createTrigger();
	}

	private ScheduledJob createScheduledJob()
	{
		Trigger trigger = getTrigger();
		JobKey jobKey = JobKey.from("test");
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jobDescriptor = new JobDescriptor(jobKey, SimpleTestJob.class, triggerConfig);
		return new ScheduledJob(jobDescriptor, trigger);
	}

	@Test
	void constructorTest_allParameterGiven_scheduledJobIsCreated()
	{
		Trigger trigger = getTrigger();
		JobKey jobKey = JobKey.from("test");
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jobDescriptor = new JobDescriptor(jobKey, SimpleTestJob.class, triggerConfig);
		ScheduledJob sj = new ScheduledJob(jobDescriptor, trigger);
		assertAll(() -> assertEquals(jobKey, sj.getJobKey(), "wrong job key"),
			() -> assertEquals(trigger, sj.getTrigger(), "wrong trigger"),
			() -> assertEquals(jobDescriptor, sj.getJobDescriptor(), "wrong job descriptor"));
	}

	@Test
	void constructorTest_jobDescriptorIsNull_throwsException()
	{
		Trigger trigger = getTrigger();
		assertThrows(IllegalArgumentException.class, () -> new ScheduledJob(null, trigger));
	}

	@Test
	void constructorTest_triggerIsNull_throwsException()
	{
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleTestJob.class, triggerConfig);
		assertThrows(IllegalArgumentException.class, () -> new ScheduledJob(jobDescriptor, null));
	}

	@Test
	void setTriggerTest()
	{
		ScheduledJob sj = createScheduledJob();
		Trigger trigger2 = new SimpleTriggerConfig(TriggerKey.from("test2"), ZonedDateTime.now(), 5, 1000)
			.createTrigger();
		sj.setTrigger(trigger2);

		assertEquals(trigger2, sj.getTrigger());
	}

	@Test
	void setJobDescriptorTest_jobKeyIsWrong_throwsException()
	{
		ScheduledJob sj = createScheduledJob();
		JobKey jobKey = JobKey.from(sj.getJobKey().getName() + "2");
		TriggerConfig triggerConfig = sj.getJobDescriptor().getTriggerConfig();
		JobDescriptor jobDescriptor = new JobDescriptor(jobKey, SimpleTestJob.class, triggerConfig);
		assertThrows(IllegalArgumentException.class, () -> sj.setJobDescriptor(jobDescriptor));
	}

	@Test
	void setJobDescriptorTest_jobDescriptorIsCorrect_jobDiscriptorIsChanged()
	{
		ScheduledJob sj = createScheduledJob();
		JobDescriptor jd = sj.getJobDescriptor();
		JobDescriptor jdNew = new JobDescriptor(jd.getKey(), SimpleTestJob.class, jd.getTriggerConfig(), true);
		sj.setJobDescriptor(jdNew);
		assertEquals(jdNew, sj.getJobDescriptor());
	}

	@Test
	void addJobListenerTest()
	{
		ScheduledJob sj = createScheduledJob();

		JobListener jobListener = new SimpleJobListener();
		JobListener jobListener2 = new SimpleJobListener();
		assertEquals(0, sj.getJobListenerNotifier().getSize());
		sj.addJobListener(jobListener);
		assertEquals(1, sj.getJobListenerNotifier().getSize());
		sj.addJobListener(jobListener2);
		assertEquals(2, sj.getJobListenerNotifier().getSize());
	}

	@Test
	void removeJobListenerTest()
	{
		ScheduledJob sj = createScheduledJob();

		JobListener jobListener = new SimpleJobListener();
		JobListener jobListener2 = new SimpleJobListener();
		sj.addJobListener(jobListener);
		sj.addJobListener(jobListener2);
		assertEquals(2, sj.getJobListenerNotifier().getSize());
		sj.removeJobListener(jobListener2);
		assertEquals(1, sj.getJobListenerNotifier().getSize());
		sj.removeJobListener(jobListener);
		assertEquals(0, sj.getJobListenerNotifier().getSize());
	}

	@Test
	void jobPausedTest()
	{
		Trigger trigger = getTrigger();
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleTestJob.class, triggerConfig);
		ScheduledJob sj = new ScheduledJob(jobDescriptor, trigger);
		assertFalse(sj.isJobPaused());
		sj.setJobPaused(true);
		assertTrue(sj.isJobPaused());
		sj.setJobPaused(false);
		assertFalse(sj.isJobPaused());
	}

	@Test
	void unscheduleTest()
	{
		Trigger trigger = getTrigger();
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleTestJob.class, triggerConfig);
		ScheduledJob sj = new ScheduledJob(jobDescriptor, trigger);
		assertFalse(sj.isUnscheduled());
		sj.setUnscheduled();
		assertTrue(sj.isUnscheduled());
	}

	@Test
	void getScheduledJobInfoTest()
	{
		ScheduledJob scheduledJob = createScheduledJob();
		assertNotNull(scheduledJob.getScheduledJobInfo());
		assertEquals(scheduledJob.getJobKey(), scheduledJob.getScheduledJobInfo().getKey());
	}

	@Test
	void getScheduledJobStatisticsTest()
	{
		Trigger trigger = getTrigger();
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleTestJob.class, triggerConfig);
		ScheduledJob sj = new ScheduledJob(jobDescriptor, trigger);
		ScheduledJobStatistics stat = sj.getScheduledJobStatistics();
		assertNotNull(stat);
		assertAll(() -> assertEquals(0, stat.getWaitingCount(), "waiting count was not zero"),
			() -> assertEquals(0, stat.getRunningCount(), "running count was not zero"),
			() -> assertEquals(0, stat.getExecutionCount(), "execution count was not zero"));
	}

	@Test
	void jobWaitingTest()
	{
		Trigger trigger = getTrigger();
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleTestJob.class, triggerConfig);
		ScheduledJob sj = new ScheduledJob(jobDescriptor, trigger);
		sj.jobWaiting();
		ScheduledJobStatistics stat = sj.getScheduledJobStatistics();
		assertNotNull(stat);
		assertAll(() -> assertEquals(1, stat.getWaitingCount(), "waiting count was not one"),
			() -> assertEquals(0, stat.getRunningCount(), "running count was not zero"),
			() -> assertEquals(0, stat.getExecutionCount(), "execution count was not zero"));
	}

	@Test
	void jobRunningTest()
	{
		Trigger trigger = getTrigger();
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleTestJob.class, triggerConfig);
		ScheduledJob sj = new ScheduledJob(jobDescriptor, trigger);
		sj.jobWaiting();
		sj.jobRunning();
		ScheduledJobStatistics stat = sj.getScheduledJobStatistics();
		assertNotNull(stat);
		assertEquals(0, stat.getWaitingCount());
		assertEquals(1, stat.getRunningCount());
		assertEquals(0, stat.getExecutionCount());
	}

	@Test
	void jobExecutedTest()
	{
		Trigger trigger = getTrigger();
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleTestJob.class, triggerConfig);
		ScheduledJob sj = new ScheduledJob(jobDescriptor, trigger);
		sj.jobWaiting();
		sj.jobRunning();
		sj.jobExecuted();
		ScheduledJobStatistics stat = sj.getScheduledJobStatistics();
		assertNotNull(stat);
		assertAll(() -> assertEquals(0, stat.getWaitingCount(), "waiting count was not zero"),
			() -> assertEquals(0, stat.getRunningCount(), "running count was not zero"),
			() -> assertEquals(1, stat.getExecutionCount(), "execution count was not one"));
	}

	@Test
	void jobNotExecutedTest()
	{
		Trigger trigger = getTrigger();
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleTestJob.class, triggerConfig);
		ScheduledJob sj = new ScheduledJob(jobDescriptor, trigger);
		sj.jobWaiting();
		sj.jobNotExecuted();
		ScheduledJobStatistics stat = sj.getScheduledJobStatistics();
		assertNotNull(stat);
		assertAll(() -> assertEquals(0, stat.getWaitingCount(), "waiting count was not zero"),
			() -> assertEquals(0, stat.getRunningCount(), "running count was not zero"),
			() -> assertEquals(0, stat.getExecutionCount(), "execution count was not zero"));
	}

	@Test
	void setLastScheduleFireTimeTest()
	{
		Trigger trigger = getTrigger();
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleTestJob.class, triggerConfig);
		ScheduledJob sj = new ScheduledJob(jobDescriptor, trigger);
		ZonedDateTime zdt = ZonedDateTime.now();
		assertNull(sj.getLastScheduleFireTime());
		sj.setLastScheduleFireTime(zdt);
		assertEquals(zdt, sj.getLastScheduleFireTime());
	}

	@Test
	void setNextScheduleFireTimeTest()
	{
		Trigger trigger = getTrigger();
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleTestJob.class, triggerConfig);
		ScheduledJob sj = new ScheduledJob(jobDescriptor, trigger);
		ZonedDateTime zdt = ZonedDateTime.now();
		assertNull(sj.getNextScheduleFireTime());
		sj.setNextScheduleFireTime(zdt);
		assertEquals(zdt, sj.getNextScheduleFireTime());
	}
}
