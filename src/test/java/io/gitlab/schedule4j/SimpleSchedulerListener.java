/*
 * Created on 10.09.2017
 * 
 * Copyright 2017 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j;

import java.util.concurrent.atomic.AtomicInteger;

import lombok.ToString;

/**
 * @author Dirk Buchhorn
 */
@ToString
public class SimpleSchedulerListener implements SchedulerListener
{
	private volatile boolean schedulerStopped = false;
	private AtomicInteger schedulerPausedCount = new AtomicInteger();
	private AtomicInteger schedulerResumedCount = new AtomicInteger();
	private AtomicInteger jobScheduledCount = new AtomicInteger();
	private AtomicInteger jobUnscheduledCount = new AtomicInteger();
	private AtomicInteger jobPausedCount = new AtomicInteger();
	private AtomicInteger jobResumedCount = new AtomicInteger();

	@Override
	public void schedulerStopped()
	{
		schedulerStopped = true;
	}

	@Override
	public void schedulerPaused()
	{
		schedulerPausedCount.incrementAndGet();
	}

	@Override
	public void schedulerResumed()
	{
		schedulerResumedCount.incrementAndGet();
	}

	@Override
	public void jobScheduled(ScheduledJob scheduledJob)
	{
		jobScheduledCount.incrementAndGet();
	}

	@Override
	public void jobUnscheduled(ScheduledJob scheduledJob)
	{
		jobUnscheduledCount.incrementAndGet();
	}

	@Override
	public void jobPaused(ScheduledJob scheduledJob)
	{
		jobPausedCount.incrementAndGet();
	}

	@Override
	public void jobResumed(ScheduledJob scheduledJob)
	{
		jobResumedCount.incrementAndGet();
	}

	public boolean isSchedulerStopped()
	{
		return schedulerStopped;
	}

	public int getSchedulerPausedCount()
	{
		return schedulerPausedCount.get();
	}

	public int getSchedulerResumedCount()
	{
		return schedulerResumedCount.get();
	}

	public int getJobScheduledCount()
	{
		return jobScheduledCount.get();
	}

	public int getJobUnscheduledCount()
	{
		return jobUnscheduledCount.get();
	}

	public int getJobPausedCount()
	{
		return jobPausedCount.get();
	}

	public int getJobResumedCount()
	{
		return jobResumedCount.get();
	}
}
