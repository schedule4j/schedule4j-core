/*
 * Created on 18.07.2017
 * 
 * Copyright 2017 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.schedule;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import io.gitlab.schedule4j.ScheduledJob;
import io.gitlab.schedule4j.job.JobDescriptor;
import io.gitlab.schedule4j.job.JobKey;
import io.gitlab.schedule4j.job.SimpleTestJob;
import io.gitlab.schedule4j.trigger.CreateTriggerException;
import io.gitlab.schedule4j.trigger.SimpleTriggerConfig;
import io.gitlab.schedule4j.trigger.TriggerEvent;
import io.gitlab.schedule4j.trigger.TriggerKey;
import io.gitlab.schedule4j.util.TestHelper;

/**
 * @author Dirk Buchhorn
 */
public class ScheduleTest
{
	@Test
	void clearTest_scheduleContainsEvents_eventsAreRemoved()
	{
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		schedule.add(e1);
		assertEquals(1, schedule.count());
		schedule.clear();
		assertEquals(0, schedule.count());
	}

	@Test
	void containsTest()
	{
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		assertFalse(schedule.contains(e1.getJobKey()));
		schedule.add(e1);
		assertTrue(schedule.contains(e1.getJobKey()));
	}

	@Test
	void getScheduleEventTest_noEventForTheKey_returnEmptyOptional()
	{
		Schedule schedule = new Schedule();
		Optional<ScheduleEvent> e = schedule.getScheduleEvent(JobKey.from("test"));
		assertFalse(e.isPresent());
	}

	@Test
	void getScheduleEventTest_eventForTheKeyExist_returnTheEvent()
	{
		Schedule schedule = new Schedule();
		assertEquals(0, schedule.count());
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		schedule.add(e1);
		Optional<ScheduleEvent> e = schedule.getScheduleEvent(e1.getJobKey());
		assertTrue(e.isPresent());
		assertEquals(e1, e.get());
	}

	@Test
	void getFirstScheduleTimeTest_noEvents_returnEmptyOptional()
	{
		Schedule schedule = new Schedule();
		Optional<ZonedDateTime> firstScheduleTime = schedule.getFirstScheduleTime();
		assertFalse(firstScheduleTime.isPresent());
	}

	@Test
	void getFirstScheduleTimeTest_eventsScheduled_returnFirstScheduleTime()
	{
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		ScheduleEvent e2 = createScheduleEvent("test2", "2017-07-18T10:00:00");
		schedule.add(e1);
		schedule.add(e2);
		Optional<ZonedDateTime> firstScheduleTime = schedule.getFirstScheduleTime();
		assertTrue(firstScheduleTime.isPresent());
		assertEquals(e2.getScheduleTime(), firstScheduleTime.get());
	}

	@Test
	void countTest_scheduleIsEmpty_countIsZero()
	{
		Schedule schedule = new Schedule();
		assertEquals(0, schedule.count());
	}

	@Test
	void countTest_scheduleContainsOneEvent_countIsOne()
	{
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		schedule.add(e1);
		assertEquals(1, schedule.count());
	}

	@Test
	void countTest_scheduleContainsTTwoEvent_countIsTwo()
	{
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		ScheduleEvent e2 = createScheduleEvent("test2", "2017-07-18T12:00:00");
		schedule.add(e1);
		schedule.add(e2);
		assertEquals(2, schedule.count());
	}

	@Test
	void addTest_scheduleIsEmpty_eventIsAdded()
	{
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		assertAll(() -> assertTrue(schedule.add(e1), "event could not be added"), //
			() -> assertEquals(1, schedule.count(), "count was not 1"));
	}

	@Test
	void addTest_scheduleCotainsEventForJobKey_eventIsNotAdded()
	{
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		schedule.add(e1);
		ScheduleEvent e2 = createScheduleEvent("test", "2017-07-18T13:00:00");
		assertAll(() -> assertFalse(schedule.add(e2), "event was added"),
			() -> assertEquals(1, schedule.count(), "count was not 1"));
	}

	@Test
	void addTest_scheduleCotainsEventForAnotherJobKey_eventIsAdded()
	{
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		schedule.add(e1);
		ScheduleEvent e2 = createScheduleEvent("test2", "2017-07-18T13:00:00");
		assertAll(() -> assertTrue(schedule.add(e2), "event was not added"),
			() -> assertEquals(2, schedule.count(), "count was not 2"));
	}

	@Test
	void replaceByJobKeyTest_scheduleIsEmpty_nothingIsReplaced()
	{
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		assertFalse(schedule.replace(e1));
	}

	@Test
	void replaceByJobKeyTest_noEventIsFound_nothingIsReplaced()
	{
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		schedule.add(e1);
		ScheduleEvent e2 = createScheduleEvent("test2", "2017-07-18T12:00:00");
		assertAll(() -> assertFalse(schedule.replace(e2), "event was replaced"),
			() -> assertEquals(e1, schedule.getScheduleEvent(e1.getJobKey()).get(), "wrong event"));
	}

	@Test
	void replaceByJobKeyTest_eventIsFound_eventIsReplaced()
	{
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		ScheduleEvent e3 = createScheduleEvent("test2", "2017-07-18T13:00:00");
		schedule.add(e1);
		schedule.add(e3);
		ScheduleEvent e2 = createScheduleEvent("test", "2017-07-18T16:00:00");
		assertAll(() -> assertTrue(schedule.replace(e2), "event was not replaced"),
			() -> assertEquals(e2, schedule.getScheduleEvent(e1.getJobKey()).get(), "wrong event"),
			() -> assertEquals(e3, schedule.getScheduleEvent(e3.getJobKey()).get(), "another event was replaced"));
	}

	@Test
	void replaceByOldEventTest_oldEventDoesNotExist_nothingIsReplaced()
	{
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		ScheduleEvent e3 = createScheduleEvent("test", "2017-07-18T13:00:00");
		schedule.add(e3);
		ScheduleEvent e2 = createScheduleEvent("test", "2017-07-18T14:00:00");
		assertAll(() -> assertFalse(schedule.replace(e1, e2), "event was replaced"),
			() -> assertEquals(e3, schedule.getScheduleEvent(e1.getJobKey()).get(), "wrong event"));
	}

	@Test
	void replaceByOldEventTest_newEventAndOldEventHaveDifferentJobKey_nothingIsReplaced()
	{
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		schedule.add(e1);
		ScheduleEvent e2 = createScheduleEvent("test2", "2017-07-18T13:00:00");
		assertAll(() -> assertFalse(schedule.replace(e1, e2), "event was replaced"),
			() -> assertEquals(e1, schedule.getScheduleEvent(e1.getJobKey()).get(), "wrong event"));
	}

	@Test
	void replaceByOldEventTest_oldEventExist_eventIsReplaced()
	{
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		schedule.add(e1);
		ScheduleEvent e3 = createScheduleEvent("test2", "2017-07-18T13:00:00");
		schedule.add(e3);
		ScheduleEvent e2 = createScheduleEvent("test", "2017-07-18T13:00:00");
		assertAll(() -> assertTrue(schedule.replace(e1, e2), "event could not be replaced"),
			() -> assertEquals(e2, schedule.getScheduleEvent(e1.getJobKey()).get(), "event was not replaced"),
			() -> assertEquals(e3, schedule.getScheduleEvent(e3.getJobKey()).get(), "wrong event was replaced"));
	}

	@Test
	void removeByJobKeyTest_jobKeyNotFound_nothingIsRemoved()
	{
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		ScheduleEvent e2 = createScheduleEvent("test2", "2017-07-18T13:00:00");
		schedule.add(e1);
		schedule.add(e2);
		assertAll(() -> assertFalse(schedule.remove(JobKey.from("notFound")), "event was removed"),
			() -> assertTrue(schedule.contains(e1.getJobKey()), "event one not found"),
			() -> assertTrue(schedule.contains(e2.getJobKey()), "event two not found"));
	}

	@Test
	void removeByJobKeyTest_jobKeyFound_eventWasRemoved()
	{
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		ScheduleEvent e2 = createScheduleEvent("test2", "2017-07-18T13:00:00");
		schedule.add(e1);
		schedule.add(e2);
		assertAll(() -> assertTrue(schedule.remove(e1.getJobKey()), "event could not be removed"),
			() -> assertFalse(schedule.contains(e1.getJobKey()), "event one found"),
			() -> assertTrue(schedule.contains(e2.getJobKey()), "event two not found"));
	}

	@Test
	void removeByJobKeyTest_jobKeyFoundMoreThanOneEventForAScheduletime_eventWasRemoved()
	{
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		ScheduleEvent e2 = createScheduleEvent("test2", "2017-07-18T12:00:00");
		schedule.add(e1);
		schedule.add(e2);
		assertAll(() -> assertTrue(schedule.remove(e1.getJobKey()), "event could not be removed"),
			() -> assertFalse(schedule.contains(e1.getJobKey()), "event one found"),
			() -> assertTrue(schedule.contains(e2.getJobKey()), "event two not found"));
	}

	@Test
	void removeByScheduleEventTest_scheduleEventNotFound_nothingIsRemoved()
	{
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		ScheduleEvent e2 = createScheduleEvent("test2", "2017-07-18T13:00:00");
		ScheduleEvent e3 = createScheduleEvent("test3", "2017-07-18T13:00:00");
		schedule.add(e1);
		schedule.add(e2);
		assertAll(() -> assertFalse(schedule.remove(e3), "event was removed"),
			() -> assertTrue(schedule.contains(e1.getJobKey()), "event one not found"),
			() -> assertTrue(schedule.contains(e2.getJobKey()), "event two not found"));
	}

	@Test
	void removeByScheduleEventTest_scheduleEventFound_eventWasRemoved()
	{
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		ScheduleEvent e2 = createScheduleEvent("test2", "2017-07-18T13:00:00");
		schedule.add(e1);
		schedule.add(e2);
		assertAll(() -> assertTrue(schedule.remove(e1), "event could not be removed"),
			() -> assertFalse(schedule.contains(e1.getJobKey()), "event one not found"),
			() -> assertTrue(schedule.contains(e2.getJobKey()), "event two not found"));
	}

	@Test
	void removeByScheduleEventTest_jobKeyFoundMoreThanOneEventForAScheduletime_eventWasRemoved()
	{
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		ScheduleEvent e2 = createScheduleEvent("test2", "2017-07-18T12:00:00");
		schedule.add(e1);
		schedule.add(e2);
		assertAll(() -> assertTrue(schedule.remove(e1), "event could not be removed"),
			() -> assertFalse(schedule.contains(e1.getJobKey()), "event one not found"),
			() -> assertTrue(schedule.contains(e2.getJobKey()), "event two not found"));
	}

	@Test
	void getScheduleEventsTest_noEventsFound_returnEmptyList()
	{
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		ScheduleEvent e2 = createScheduleEvent("test2", "2017-07-18T13:00:00");
		schedule.add(e1);
		schedule.add(e2);
		ZonedDateTime zdt = TestHelper.createZonedDateTime("2017-07-18T10:00:00");
		List<ScheduleEvent> list = schedule.getScheduleEvents(zdt);
		assertNotNull(list);
		assertEquals(0, list.size());
	}

	@Test
	void getScheduleEventsTest_eventsFound_returnTheEvents()
	{
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test", "2017-07-18T12:00:00");
		ScheduleEvent e2 = createScheduleEvent("test2", "2017-07-18T13:00:00");
		ScheduleEvent e3 = createScheduleEvent("test3", "2017-07-18T14:00:00");
		schedule.add(e1);
		schedule.add(e2);
		schedule.add(e3);
		ZonedDateTime zdt = TestHelper.createZonedDateTime("2017-07-18T13:00:00");
		List<ScheduleEvent> list = schedule.getScheduleEvents(zdt);
		assertNotNull(list);
		assertEquals(2, list.size(), "wrong size");
		assertAll(() -> assertEquals(e1, list.get(0), "wrong event one"),
			() -> assertEquals(e2, list.get(1), "wrong event two"));
	}

	@Test
	void getScheduleEventsTest_differentTimeZone_correctEventsAreReturned()
	{
		// test the Schedule with different time zones
		ZonedDateTime zdt1 = TestHelper.createZonedDateTime("2017-09-09T19:10:20");
		ZonedDateTime zdt2 = zdt1.withZoneSameInstant(ZoneId.of("America/New_York"));
		ZonedDateTime zdt3 = TestHelper.createZonedDateTime("2017-09-09T19:15:00");
		assertTrue(zdt1.isEqual(zdt2));
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test1", zdt1);
		ScheduleEvent e2 = createScheduleEvent("test2", zdt2);
		ScheduleEvent e3 = createScheduleEvent("test3", zdt3);
		schedule.add(e1);
		schedule.add(e2);
		schedule.add(e3);
		List<ScheduleEvent> list = schedule.getScheduleEvents(zdt1);
		assertEquals(2, list.size());
		assertAll(() -> assertTrue(list.contains(e1), "event one is not in the list"),
			() -> assertTrue(list.contains(e2), "event two is not in the list"));
	}

	@Test
	void toStringTest() throws CreateTriggerException
	{
		ZonedDateTime zdt1 = TestHelper.createZonedDateTime("2017-09-09T19:10:20");
		Schedule schedule = new Schedule();
		ScheduleEvent e1 = createScheduleEvent("test1", zdt1);
		ScheduleEvent e2 = createScheduleEvent("test2", zdt1);
		ScheduleEvent e3 = createScheduleEvent("test3", zdt1.plusMinutes(1));
		schedule.add(e1);
		schedule.add(e2);
		schedule.add(e3);
		String s = schedule.toString();
		assertEquals(
			"Schedule (2017-09-09T19:10:20+02:00[Europe/Berlin] [test1, test2]); 2017-09-09T19:11:20+02:00[Europe/Berlin] [test3])",
			s);
	}

	private ScheduleEvent createScheduleEvent(String key, ZonedDateTime scheduleTime)
	{
		SimpleTriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from(key), ZonedDateTime.now(), 10,
			1000);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from(key), SimpleTestJob.class, triggerConfig);
		ScheduledJob scheduledJob = new ScheduledJob(jobDescriptor, triggerConfig.createTrigger());
		TriggerEvent triggerEvent = new TriggerEvent(scheduleTime, ZonedDateTime.now());
		ScheduleEvent scheduleEvent = new ScheduleEvent(scheduledJob, triggerEvent);
		return scheduleEvent;
	}

	private ScheduleEvent createScheduleEvent(String key, String scheduleTime)
	{
		SimpleTriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from(key), ZonedDateTime.now(), 10,
			1000);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from(key), SimpleTestJob.class, triggerConfig);
		ScheduledJob scheduledJob = new ScheduledJob(jobDescriptor, triggerConfig.createTrigger());
		TriggerEvent triggerEvent = new TriggerEvent(TestHelper.createZonedDateTime(scheduleTime), ZonedDateTime.now());
		ScheduleEvent scheduleEvent = new ScheduleEvent(scheduledJob, triggerEvent);
		return scheduleEvent;
	}
}
