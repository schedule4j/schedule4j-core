/*
 * Created on 23.07.2017
 * 
 * Copyright 2017 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.schedule;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;

import io.gitlab.schedule4j.ScheduledJob;
import io.gitlab.schedule4j.job.JobDescriptor;
import io.gitlab.schedule4j.job.JobKey;
import io.gitlab.schedule4j.job.SimpleTestJob;
import io.gitlab.schedule4j.trigger.SimpleTriggerConfig;
import io.gitlab.schedule4j.trigger.Trigger;
import io.gitlab.schedule4j.trigger.TriggerConfig;
import io.gitlab.schedule4j.trigger.TriggerEvent;
import io.gitlab.schedule4j.trigger.TriggerKey;
import io.gitlab.schedule4j.util.TestHelper;

/**
 * @author Dirk Buchhorn
 */
class ScheduleEventTest
{
	@Test
	void constructorTest_allArgumentsGiven_objectCreated()
	{
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleTestJob.class, triggerConfig);
		ScheduledJob scheduledJob = new ScheduledJob(jobDescriptor, createTrigger());
		TriggerEvent triggerEvent = new TriggerEvent(TestHelper.createZonedDateTime("2017-07-18T12:00:00"),
			ZonedDateTime.now());
		ScheduleEvent scheduleEvent = new ScheduleEvent(scheduledJob, triggerEvent);
		assertAll(() -> assertEquals(jobDescriptor.getKey(), scheduleEvent.getJobKey(), "wrong job key"),
			() -> assertEquals(triggerEvent.getFireTime(), scheduleEvent.getScheduleTime(), "wrong schedule time"),
			() -> assertEquals(scheduledJob, scheduleEvent.getScheduledJob(), "wrong scheduled job"),
			() -> assertEquals(triggerEvent, scheduleEvent.getTriggerEvent(), "wrong trigger event"));
	}

	@Test
	void constructorTest_scheduledJobIsNull_throwsException()
	{
		TriggerEvent triggerEvent = new TriggerEvent(TestHelper.createZonedDateTime("2017-07-18T12:00:00"),
			ZonedDateTime.now());
		assertThrows(IllegalArgumentException.class, () -> new ScheduleEvent(null, triggerEvent));
	}

	@Test
	void constructorTest_triggerEventIsNull_throwsException()
	{
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleTestJob.class, triggerConfig);
		ScheduledJob scheduledJob = new ScheduledJob(jobDescriptor, createTrigger());
		assertThrows(IllegalArgumentException.class, () -> new ScheduleEvent(scheduledJob, null));
	}

	@Test
	void hashCodeTest()
	{
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleTestJob.class, triggerConfig);
		ScheduledJob scheduledJob = new ScheduledJob(jobDescriptor, createTrigger());
		TriggerEvent triggerEvent1 = new TriggerEvent(TestHelper.createZonedDateTime("2017-07-18T12:00:00"),
			ZonedDateTime.now());
		TriggerEvent triggerEvent2 = new TriggerEvent(TestHelper.createZonedDateTime("2017-07-18T13:00:00"),
			ZonedDateTime.now());

		ScheduleEvent scheduleEvent1 = new ScheduleEvent(scheduledJob, triggerEvent1);
		ScheduleEvent scheduleEvent2 = new ScheduleEvent(scheduledJob, triggerEvent1);
		ScheduleEvent scheduleEvent3 = new ScheduleEvent(scheduledJob, triggerEvent2);
		assertAll(() -> assertEquals(scheduleEvent1.hashCode(), scheduleEvent2.hashCode(), "hash should equals"),
			() -> assertNotEquals(scheduleEvent3.hashCode(), scheduleEvent2.hashCode(),
				"hash codes should not equals"));
	}

	@Test
	void euqalsTest()
	{
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleTestJob.class, triggerConfig);
		ScheduledJob scheduledJob = new ScheduledJob(jobDescriptor, createTrigger());
		JobDescriptor jobDescriptor2 = new JobDescriptor(JobKey.from("test2"), SimpleTestJob.class, triggerConfig);
		ScheduledJob scheduledJob2 = new ScheduledJob(jobDescriptor2, createTrigger());
		TriggerEvent triggerEvent = new TriggerEvent(TestHelper.createZonedDateTime("2017-07-18T12:00:00"),
			ZonedDateTime.now());
		TriggerEvent triggerEvent2 = new TriggerEvent(TestHelper.createZonedDateTime("2017-07-18T13:00:00"),
			ZonedDateTime.now());

		ScheduleEvent scheduleEvent1 = new ScheduleEvent(scheduledJob, triggerEvent);
		ScheduleEvent scheduleEvent2 = new ScheduleEvent(scheduledJob, triggerEvent);
		ScheduleEvent scheduleEvent3 = new ScheduleEvent(scheduledJob, triggerEvent2);
		ScheduleEvent scheduleEvent4 = new ScheduleEvent(scheduledJob2, triggerEvent2);
		assertAll(() -> assertEquals(scheduleEvent1, scheduleEvent2, "test 1"),
			() -> assertNotEquals(null, scheduleEvent1, "test 2"),
			() -> assertNotEquals(scheduleEvent1, scheduleEvent3, "test 3"),
			() -> assertNotEquals(scheduleEvent1, scheduleEvent4, "test 4"));
	}

	@Test
	void toStringTest()
	{
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleTestJob.class, triggerConfig);
		ScheduledJob scheduledJob = new ScheduledJob(jobDescriptor, createTrigger());
		TriggerEvent triggerEvent = new TriggerEvent(TestHelper.createZonedDateTime("2017-07-18T12:00:00"),
			ZonedDateTime.now());
		ScheduleEvent scheduleEvent = new ScheduleEvent(scheduledJob, triggerEvent);
		assertEquals("test (2017-07-18T12:00:00+02:00[Europe/Berlin])", scheduleEvent.toString());
	}

	private Trigger createTrigger()
	{
		SimpleTriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test"), ZonedDateTime.now(), 10,
			1000);
		return triggerConfig.createTrigger();
	}
}
