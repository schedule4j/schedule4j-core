/*
 * Created on 29.10.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.common;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collection;

import org.junit.jupiter.api.Test;

/**
 * @author Dirk Buchhorn
 */
public class KeyValueStoreTest
{
	@Test
	void addTest()
	{
		KeyValueStore<TestKey, String> store = new KeyValueStore<TestKey, String>();

		assertAll(() -> assertEquals(0, store.size(), "KeyValueStore was not empty"), () ->
		{
			store.add(new TestKey("test"), "value");
			assertEquals(1, store.size(), "KeyValueStore size was not 1");
		}, () ->
		{
			store.add(new TestKey("test"), "value2");
			assertEquals(1, store.size(), "KeyValueStore size was not 1");
		}, () ->
		{
			store.add(new TestKey("test2"), "value2");
			assertEquals(2, store.size(), "KeyValueStore size was not 2");
		});
	}

	@Test
	void clearTest()
	{
		KeyValueStore<TestKey, String> store = new KeyValueStore<TestKey, String>();
		store.add(new TestKey("test"), "test");
		store.add(new TestKey("test2"), "test");

		store.clear();
		assertEquals(0, store.size());
	}

	@Test
	void containsKeyTest()
	{
		KeyValueStore<TestKey, String> store = new KeyValueStore<TestKey, String>();
		store.add(new TestKey("test"), "test");
		store.add(new TestKey("test2"), "test");

		assertAll(() -> assertTrue(store.containsKey(new TestKey("test")), "KeyValueStore does not contain key 'test'"),
			() -> assertTrue(store.containsKey(new TestKey("test2")), "KeyValueStore does not contain key 'test2'"),
			() -> assertFalse(store.containsKey(new TestKey("test3")), "KeyValueStore does contain key 'test3'"));
	}

	@Test
	void findTest()
	{
		TestKey parent = new TestKey("parent");
		KeyValueStore<TestKey, String> store = new KeyValueStore<TestKey, String>();
		store.add(new TestKey(parent, "test"), "test");
		store.add(new TestKey(parent, "test2"), "test2");
		store.add(new TestKey("test3"), "test3");

		assertAll(() ->
		{
			Collection<String> col = store.find(parent);
			assertEquals(2, col.size());
			assertTrue(col.contains("test"));
			assertTrue(col.contains("test2"));
		}, () ->
		{
			Collection<String> col = store.find(new TestKey(parent, "test"));
			assertEquals(1, col.size());
			assertTrue(col.contains("test"));
		}, () ->
		{
			Collection<String> col = store.find(new TestKey("test3"));
			assertEquals(1, col.size());
			assertTrue(col.contains("test3"));
		});
	}

	@Test
	void getTest()
	{
		TestKey parent = new TestKey("parent");
		KeyValueStore<TestKey, String> store = new KeyValueStore<TestKey, String>();
		store.add(new TestKey(parent, "test"), "test");
		store.add(new TestKey(parent, "test2"), "test2");
		store.add(new TestKey("test3"), "test3");

		assertAll(() -> assertEquals("test", store.get(new TestKey(parent, "test")), "wrong value for key 'test'"),
			() -> assertEquals("test2", store.get(new TestKey(parent, "test2")), "wrong value for key 'test2'"),
			() -> assertEquals("test3", store.get(new TestKey("test3")), "wrong value for key 'test3'"),
			() -> assertNull(store.get(new TestKey("test4")), "wrong value for key 'test4'"));
	}

	@Test
	void getAllTest()
	{
		TestKey parent = new TestKey("parent");
		KeyValueStore<TestKey, String> store = new KeyValueStore<TestKey, String>();
		store.add(new TestKey(parent, "test"), "test");
		store.add(new TestKey(parent, "test2"), "test2");
		store.add(new TestKey("test3"), "test3");
		Collection<String> col = store.getAll();

		assertAll(() -> assertEquals(3, col.size()), () -> assertTrue(col.contains("test")),
			() -> assertTrue(col.contains("test2")), () -> assertTrue(col.contains("test3")));
	}

	@Test
	void removeTest()
	{
		TestKey parent = new TestKey("parent");
		KeyValueStore<TestKey, String> store = new KeyValueStore<TestKey, String>();
		store.add(new TestKey(parent, "test"), "test");
		store.add(new TestKey(parent, "test2"), "test2");
		store.add(new TestKey("test3"), "test3");
		assertAll(() -> assertEquals(3, store.size()), () ->
		{
			store.remove(new TestKey(parent, "test"));
			assertEquals(2, store.size(), "key 'test' was not removed");
		}, () ->
		{
			store.remove(new TestKey(parent, "test2"));
			assertEquals(1, store.size(), "key 'test2' was not removed");
		}, () ->
		{
			store.remove(new TestKey(parent, "test4"));
			assertEquals(1, store.size(), "something was removed for non existing key 'test4' and parent");
		}, () ->
		{
			store.remove(new TestKey("test4"));
			assertEquals(1, store.size(), "something was removed for non existing key 'test4'");
		}, () ->
		{
			store.remove(new TestKey("test3"));
			assertEquals(0, store.size(), "key 'test3' was not removed");
		});
	}

	@Test
	void sizeTest()
	{
		KeyValueStore<TestKey, String> store = new KeyValueStore<TestKey, String>();
		assertAll(() -> assertEquals(0, store.size()), () ->
		{
			store.add(new TestKey("test"), "test");
			assertEquals(1, store.size(), "'test' was not added");
		}, () ->
		{
			store.add(new TestKey("test"), "test");
			assertEquals(1, store.size(), "'test' was added twice");
		}, () ->
		{
			store.add(new TestKey("test2"), "test");
			assertEquals(2, store.size(), "'test2' was not added");
		});
	}

	@Test
	void updateTest()
	{
		TestKey parent = new TestKey("parent");
		KeyValueStore<TestKey, String> store = new KeyValueStore<TestKey, String>();
		store.add(new TestKey(parent, "test"), "test");
		store.add(new TestKey(parent, "test2"), "test2");
		store.add(new TestKey("test3"), "test3");
		assertAll(() -> assertEquals(3, store.size(), "KeyValueStore size was not 3"),
			() -> assertTrue(store.update(new TestKey(parent, "test"), "test_update"),
				"value for key 'test' could not be updated"),
			() -> assertEquals(3, store.size(), "KeyValueStore size was not 3 after update key 'test'"),
			() -> assertEquals("test_update", store.get(new TestKey(parent, "test")),
				"wrong value for key 'test' after update key 'test'"),
			() -> assertEquals("test2", store.get(new TestKey(parent, "test2")),
				"wrong value for key 'test2' after update key 'test'"),
			() -> assertEquals("test3", store.get(new TestKey("test3")),
				"wrong value for key 'test3' after update key 'test'"),
			() -> assertFalse(store.update(new TestKey(parent, "test4"), "test_update"),
				"non existing key 'test4' could be updated"),
			() -> assertEquals(3, store.size(), "KeyValueStore size was not 3 after update key 'test4'"),
			() -> assertTrue(store.update(new TestKey("test3"), "test_update"),
				"value for key 'test3' could not be updated"),
			() -> assertEquals(3, store.size(), "KeyValueStore size was not 3 after update key 'test3'"),
			() -> assertEquals("test_update", store.get(new TestKey("test3")),
				"wrong value for key 'test3' after update key 'test3'"));
	}
}
