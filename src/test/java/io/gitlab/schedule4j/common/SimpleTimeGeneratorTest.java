/*
 * Created on 02.06.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.common;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import io.gitlab.schedule4j.util.TestHelper;

/**
 * @author Dirk Buchhorn
 */
public class SimpleTimeGeneratorTest
{

	@Test
	void constructorTest_repeatCountNegative_throwsIllegalArgumentException() throws ParseException
	{
		IllegalArgumentException assertThrows = Assertions.assertThrows(IllegalArgumentException.class,
			() -> new SimpleTimeGenerator(-1, 1));
		assertTrue(assertThrows.getMessage().contains("repeatCount"));
	}

	@Test
	void constructorTest_repeatIntervalNegative_throwsIllegalArgumentException() throws ParseException
	{
		IllegalArgumentException assertThrows = assertThrows(IllegalArgumentException.class,
			() -> new SimpleTimeGenerator(1, -1));
		assertTrue(assertThrows.getMessage().contains("repeatInterval"));
	}

	@Test
	void constructorTest() throws ParseException
	{
		SimpleTimeGenerator stg = new SimpleTimeGenerator(1, 100);
		assertAll(() -> assertEquals(1, stg.getRepeatCount(), "repeatCount was not 1"),
			() -> assertEquals(100, stg.getRepeatInterval(), "repeatInterval was not 100"));
	}

	@Test
	void setAndGetRepeatCountTest()
	{
		SimpleTimeGenerator stg = new SimpleTimeGenerator(10, 1000);
		stg.setRepeatCount(20);
		assertEquals(20, stg.getRepeatCount());
	}

	@Test
	void setAndGetRepeatIntervalTest()
	{
		SimpleTimeGenerator stg = new SimpleTimeGenerator(10, 1000);
		stg.setRepeatInterval(2000);
		assertEquals(2000, stg.getRepeatInterval());
	}

	@Test
	void getFirstTimeTest() throws ParseException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T00:00:00");
		ZonedDateTime endTime = null;

		SimpleTimeGenerator stg = new SimpleTimeGenerator(0, 1);
		SimpleTimeGenerator stg2 = new SimpleTimeGenerator(1, 1000 * 60);

		assertAll(() -> assertEquals(startTime, stg.getFirstTime(startTime, endTime), "test 1"),
			() -> assertEquals(startTime, stg2.getFirstTime(startTime, endTime), "test 2"));
	}

	static Stream<Arguments> getLastTimeParameter()
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T00:00:00");
		return Stream.of(Arguments.of(new SimpleTimeGenerator(0, 1), startTime, null, startTime),
			Arguments.of(new SimpleTimeGenerator(0, 1), startTime, startTime.plusMinutes(1), startTime),
			Arguments.of(new SimpleTimeGenerator(1, 1000 * 60), startTime, null,
				TestHelper.createZonedDateTime("2010-01-01T00:01:00")),
			Arguments.of(new SimpleTimeGenerator(10, 1000 * 60), startTime, null,
				TestHelper.createZonedDateTime("2010-01-01T00:10:00")),
			Arguments.of(new SimpleTimeGenerator(10, 1000 * 60), startTime,
				TestHelper.createZonedDateTime("2010-01-01T00:05:00"),
				TestHelper.createZonedDateTime("2010-01-01T00:04:00")),
			Arguments.of(new SimpleTimeGenerator(10, 1000 * 60), startTime,
				TestHelper.createZonedDateTime("2010-01-01T00:05:01"),
				TestHelper.createZonedDateTime("2010-01-01T00:05:00")),
			Arguments.of(new SimpleTimeGenerator(10, 1000 * 60), startTime,
				TestHelper.createZonedDateTime("2010-01-01T00:10:01"),
				TestHelper.createZonedDateTime("2010-01-01T00:10:00")));
	}

	@ParameterizedTest(name = "{index} => {0}, startTime={1}, endTime={2}")
	@MethodSource("getLastTimeParameter")
	void getLastTimeTest(SimpleTimeGenerator stg, ZonedDateTime startTime, ZonedDateTime endTime,
		ZonedDateTime expectedTime)
	{
		assertEquals(expectedTime, stg.getLastTime(startTime, endTime));
	}

	static Stream<Arguments> getNextTimeParameter()
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		ZonedDateTime endTime = TestHelper.createZonedDateTime("2010-01-01T01:05:00");
		SimpleTimeGenerator stgTenTimesEveryMinute = new SimpleTimeGenerator(10, 1000 * 60);

		return Stream.of(
			Arguments.of(stgTenTimesEveryMinute, TestHelper.createZonedDateTime("2010-01-01T00:00:00"), startTime, null,
				startTime),
			Arguments.of(stgTenTimesEveryMinute, TestHelper.createZonedDateTime("2010-01-01T01:01:00"), startTime, null,
				TestHelper.createZonedDateTime("2010-01-01T01:02:00")),
			Arguments.of(stgTenTimesEveryMinute, TestHelper.createZonedDateTime("2010-01-01T01:09:00"), startTime, null,
				TestHelper.createZonedDateTime("2010-01-01T01:10:00")),
			Arguments.of(stgTenTimesEveryMinute, TestHelper.createZonedDateTime("2010-01-01T01:10:00"), startTime, null,
				null),
			Arguments.of(new SimpleTimeGenerator(10, 5), TestHelper.createZonedDateTime("2010-01-01T00:00:00"),
				startTime, null, TestHelper.createZonedDateTime("2010-01-01T01:00:00")),
			Arguments.of(stgTenTimesEveryMinute, TestHelper.createZonedDateTime("2010-01-01T01:03:00"), startTime,
				endTime, TestHelper.createZonedDateTime("2010-01-01T01:04:00")),
			Arguments.of(stgTenTimesEveryMinute, TestHelper.createZonedDateTime("2010-01-01T01:05:00"), startTime,
				endTime, null));
	}

	@ParameterizedTest(name = "{index} => {0}, calculationTime={1}, startTime={2}, endTime={3}")
	@MethodSource("getNextTimeParameter")
	void getNextTimeTest(SimpleTimeGenerator stg, ZonedDateTime calculationTime, ZonedDateTime startTime,
		ZonedDateTime endTime, ZonedDateTime expectedTime)
	{
		assertEquals(expectedTime, stg.getNextTime(calculationTime, startTime, endTime));
	}

	@Test
	void getNextTime_startTimeIsNull_Test() throws Exception
	{
		SimpleTimeGenerator stg = new SimpleTimeGenerator(10, 5);
		Assertions.assertThrows(IllegalArgumentException.class,
			() -> stg.getNextTime(TestHelper.createZonedDateTime("2010-01-01T00:00:00"), null,
				TestHelper.createZonedDateTime("2010-01-01T00:00:00")));
	}

	@Test
	void getNextTime_startTimeGreaterEndTime_Test() throws Exception
	{
		SimpleTimeGenerator stg = new SimpleTimeGenerator(10, 5);
		Assertions.assertThrows(IllegalArgumentException.class,
			() -> stg.getNextTime(TestHelper.createZonedDateTime("2010-01-01T00:00:00"),
				TestHelper.createZonedDateTime("2010-01-01T01:00:00"),
				TestHelper.createZonedDateTime("2010-01-01T00:00:00")));
	}

	static Stream<Arguments> getPreviousTimeParameter()
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		ZonedDateTime endTime = startTime.plusMinutes(1);
		SimpleTimeGenerator stgTenTimesEveryMinute = new SimpleTimeGenerator(10, 1000 * 60);

		return Stream.of(
			Arguments.of(stgTenTimesEveryMinute, TestHelper.createZonedDateTime("2010-01-01T01:15:00"), startTime, null,
				TestHelper.createZonedDateTime("2010-01-01T01:10:00")),
			Arguments.of(stgTenTimesEveryMinute, TestHelper.createZonedDateTime("2010-01-01T01:01:00"), startTime, null,
				TestHelper.createZonedDateTime("2010-01-01T01:00:00")),
			Arguments.of(stgTenTimesEveryMinute, TestHelper.createZonedDateTime("2010-01-01T01:00:00"), startTime, null,
				null),
			Arguments.of(stgTenTimesEveryMinute, TestHelper.createZonedDateTime("2010-01-01T01:10:00"), startTime,
				endTime, startTime),
			Arguments.of(new SimpleTimeGenerator(10, 5000), TestHelper.createZonedDateTime("2010-01-01T01:00:10"),
				startTime, null, TestHelper.createZonedDateTime("2010-01-01T01:00:05")));
	}

	@ParameterizedTest(name = "{index} => {0}, calculationTime={1}, startTime={2}, endTime={3}")
	@MethodSource("getPreviousTimeParameter")
	void getPreviousTimeTest(SimpleTimeGenerator stg, ZonedDateTime calculationTime, ZonedDateTime startTime,
		ZonedDateTime endTime, ZonedDateTime expectedTime)
	{
		assertEquals(expectedTime, stg.getPreviousTime(calculationTime, startTime, endTime));
	}

	@Test
	void getPreviousTimeTest_startTimeIsNull__throwsIllegalArgumentException()
	{
		SimpleTimeGenerator stg = new SimpleTimeGenerator(10, 5);
		assertThrows(IllegalArgumentException.class,
			() -> stg.getPreviousTime(TestHelper.createZonedDateTime("2010-01-01T01:00:10"), null,
				TestHelper.createZonedDateTime("2010-01-01T00:00:00")));
	}

	@Test
	void getPreviousTimeTest_startTimeGreaterEndTime_throwsIllegalArgumentException()
	{
		SimpleTimeGenerator stg = new SimpleTimeGenerator(10, 5);
		assertThrows(IllegalArgumentException.class,
			() -> stg.getPreviousTime(TestHelper.createZonedDateTime("2010-01-01T01:00:10"),
				TestHelper.createZonedDateTime("2010-01-01T01:00:00"),
				TestHelper.createZonedDateTime("2010-01-01T00:00:00")));
	}
}
