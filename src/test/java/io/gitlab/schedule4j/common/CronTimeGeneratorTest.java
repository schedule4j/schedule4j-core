/*
 * Created on 20.03.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.common;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import io.gitlab.schedule4j.cron.CronExpression;
import io.gitlab.schedule4j.util.TestHelper;

/**
 * @author Dirk Buchhorn
 */
public class CronTimeGeneratorTest
{
	@Test
	void constructorTest_cronExpressionIsNull__throwsIllegalArgumentException() throws ParseException
	{
		assertThrows(IllegalArgumentException.class, () -> new CronTimeGenerator((CronExpression) null));
	}

	@Test
	void constructorTest() throws ParseException
	{
		CronExpression cronExpression = new CronExpression("0 0/1 * * * ?");
		CronTimeGenerator ctg = new CronTimeGenerator(cronExpression);
		assertEquals(cronExpression, ctg.getCronExpression());
	}

	@Test
	void getAndSetCronExpressionTest() throws ParseException
	{
		CronTimeGenerator ctg = new CronTimeGenerator(new CronExpression("0 0/1 * * * ?"));

		CronExpression cronExpression = new CronExpression("0 0/30 * * * ?");
		ctg.setCronExpression(cronExpression);
		assertEquals(cronExpression, ctg.getCronExpression());
	}

	@Test
	void getFirstTimeTest() throws ParseException
	{
		CronExpression cronExpression = new CronExpression("0 0/1 * * * ?");
		final CronTimeGenerator ctg = new CronTimeGenerator(cronExpression);

		assertAll(() -> assertNull(ctg.getFirstTime(null, null), "test 1"), //
			() -> assertEquals(TestHelper.createZonedDateTime("2010-01-01T00:00:00"),
				ctg.getFirstTime(TestHelper.createZonedDateTime("2010-01-01T00:00:00"), null), "test 2"));
	}

	@Test
	void getLastTimeTest() throws ParseException
	{
		CronExpression cronExpression = new CronExpression("0 0/1 * * * ?");
		final CronTimeGenerator ctg = new CronTimeGenerator(cronExpression);

		assertAll(() -> assertNull(ctg.getLastTime(null, null), "test 1"), //
			() -> assertEquals(TestHelper.createZonedDateTime("2010-01-01T00:59:00"),
				ctg.getLastTime(null, TestHelper.createZonedDateTime("2010-01-01T01:00:00")), "test 2"));
	}

	static Stream<Arguments> getNextTimeParameter() throws ParseException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		ZonedDateTime endTime = TestHelper.createZonedDateTime("2010-01-01T01:10:00");
		CronExpression cronExpression = new CronExpression("0 0/1 * * * ?");
		CronTimeGenerator ctg = new CronTimeGenerator(cronExpression);

		return Stream.of(Arguments.of(ctg, null, null, null, null),
			Arguments.of(ctg, TestHelper.createZonedDateTime("2010-01-01T00:00:00"), null, null,
				TestHelper.createZonedDateTime("2010-01-01T00:01:00")),
			Arguments.of(ctg, TestHelper.createZonedDateTime("2010-01-01T00:00:00"), startTime, null,
				TestHelper.createZonedDateTime("2010-01-01T01:00:00")),
			Arguments.of(ctg, TestHelper.createZonedDateTime("2010-01-01T01:00:00"), startTime, null,
				TestHelper.createZonedDateTime("2010-01-01T01:01:00")),
			Arguments.of(ctg, TestHelper.createZonedDateTime("2010-01-01T01:08:00"), startTime, endTime,
				TestHelper.createZonedDateTime("2010-01-01T01:09:00")),
			Arguments.of(ctg, TestHelper.createZonedDateTime("2010-01-01T01:09:00"), startTime, endTime, null));
	}

	@ParameterizedTest(name = "{index} => {0}, calculationTime={1}, startTime={2}, endTime={3}")
	@MethodSource("getNextTimeParameter")
	void getNextTimeTest(CronTimeGenerator ctg, ZonedDateTime calculationTime, ZonedDateTime startTime,
		ZonedDateTime endTime, ZonedDateTime expectedTime)
	{
		assertEquals(expectedTime, ctg.getNextTime(calculationTime, startTime, endTime));
	}

	static Stream<Arguments> getPreviousTimeParameter() throws ParseException
	{
		ZonedDateTime startTime = TestHelper.createZonedDateTime("2010-01-01T01:00:00");
		ZonedDateTime endTime = TestHelper.createZonedDateTime("2010-01-01T01:10:00");
		CronExpression cronExpression = new CronExpression("0 0/1 * * * ?");
		CronTimeGenerator ctg = new CronTimeGenerator(cronExpression);

		return Stream.of(Arguments.of(ctg, null, null, null, null),
			Arguments.of(ctg, TestHelper.createZonedDateTime("2010-01-01T01:00:00"), null, null,
				TestHelper.createZonedDateTime("2010-01-01T00:59:00")),
			Arguments.of(ctg, TestHelper.createZonedDateTime("2010-01-01T01:01:00"), startTime, null,
				TestHelper.createZonedDateTime("2010-01-01T01:00:00")),
			Arguments.of(ctg, TestHelper.createZonedDateTime("2010-01-01T01:10:00"), startTime, null,
				TestHelper.createZonedDateTime("2010-01-01T01:09:00")),
			Arguments.of(ctg, TestHelper.createZonedDateTime("2010-01-01T01:00:00"), startTime, null, null),
			Arguments.of(ctg, TestHelper.createZonedDateTime("2010-01-01T01:10:00"), startTime, endTime,
				TestHelper.createZonedDateTime("2010-01-01T01:09:00")),
			Arguments.of(ctg, TestHelper.createZonedDateTime("2010-01-01T01:11:00"), startTime, endTime,
				TestHelper.createZonedDateTime("2010-01-01T01:09:00")));
	}

	@ParameterizedTest(name = "{index} => {0}, calculationTime={1}, startTime={2}, endTime={3}")
	@MethodSource("getPreviousTimeParameter")
	void getPreviousTimeTest(CronTimeGenerator ctg, ZonedDateTime calculationTime, ZonedDateTime startTime,
		ZonedDateTime endTime, ZonedDateTime expectedTime)
	{
		assertEquals(expectedTime, ctg.getPreviousTime(calculationTime, startTime, endTime));
	}
}
