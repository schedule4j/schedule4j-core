/*
 * Created on 29.10.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.common;

/**
 * @author Dirk Buchhorn
 */
public class TestKey extends Key<TestKey>
{
	private static final long serialVersionUID = -5130861745803025134L;

	public TestKey(String name)
	{
		super(name);
	}

	public TestKey(TestKey parentKey, String name)
	{
		super(parentKey, name);
	}
}
