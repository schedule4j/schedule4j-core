/*
 * Created on 09.10.2015
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.common;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.ParseException;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * @author Dirk Buchhorn
 */
@TestInstance(Lifecycle.PER_CLASS)
public abstract class KeyTest<T extends Key<T>>
{
	protected abstract T create(T parentKey, String name);

	protected abstract T create(String... keyNames);

	@Test
	void wrongNameTest()
	{
		assertThrows(IllegalArgumentException.class, () -> create("group.test", null));
	}

	@Test
	void noNameTest()
	{
		assertThrows(IllegalArgumentException.class, () -> create(null, null));
	}

	@Test
	void compareTest()
	{
		T key = create("parent", "b");
		assertAll(() -> assertEquals(0, key.compareTo(create("parent", "b")), "compare result was not 0"),
			() -> assertTrue(key.compareTo(create("parent", "a")) > 0, "compare result was not >0"),
			() -> assertTrue(key.compareTo(create("parent", "c")) < 0, "compare result was not <0"));
	}

	@Test
	void isParentTest()
	{
		T parentKey = create("parent", "123");
		T key = create(parentKey, "abc");
		assertAll(() -> assertTrue(key.isParent(parentKey), "'parent.123' was no parent of 'parent.123.abc'"),
			() -> assertTrue(key.isParent(create("parent")), "'parent.123' was no parent of 'parent.123.abc'"),
			() -> assertFalse(key.isParent(create("123")), "'123' was parent of 'parent.123.abc'"),
			() -> assertFalse(key.isParent(create("parent", "1")), "'parent.1' was parent of 'parent.123.abc'"),
			() -> assertFalse(key.isParent(create("parent", "1234")), "'parent.1234' was parent of 'parent.123.abc'"));
	}

	@Test
	void hashCodeTest()
	{
		T key = create("parent", "b");
		assertAll(
			() -> assertEquals(key.hashCode(), create("parent", "b").hashCode(), "hashCode not equals to 'parent.b'"),
			() -> assertNotEquals(key.hashCode(), create("parent", "a").hashCode(), "hashCode equals to 'parent.a'"),
			() -> assertNotEquals(key.hashCode(), create("parent", "b", "a").hashCode(),
				"hashCode equals to 'parent.b.a'"),
			() -> assertNotEquals(key.hashCode(), create("parent").hashCode(), "hashCode equals to 'parent'"));
	}

	@Test
	void equalsTest()
	{
		T key = create("parent", "b");
		assertAll(() -> assertTrue(key.equals(create("parent", "b")), "not equals to 'parent.b'"),
			() -> assertFalse(key.equals(create("parent", "a")), "equals to 'parent.a'"),
			() -> assertFalse(key.equals(create("parent", "b", "a")), "equals to 'parent.b.a'"),
			() -> assertFalse(key.equals(create("parent")), "equals to 'parent'"),
			() -> assertFalse(key.equals(null), "equals to null"));
	}

	Stream<Arguments> toStringParameter()
	{
		return Stream.of(Arguments.of(create("test"), "test"), Arguments.of(create("parent", "test"), "parent.test"),
			Arguments.of(create("parent", "parent2", "test"), "parent.parent2.test"));
	}

	@ParameterizedTest(name = "{index} => key={0}, expectedStr={1}")
	@MethodSource("toStringParameter")
	void toStringTest(T key, String expectedStr) throws ParseException
	{
		assertEquals(expectedStr, key.toString());
	}
}
