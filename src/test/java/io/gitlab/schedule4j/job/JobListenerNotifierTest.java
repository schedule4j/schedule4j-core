/*
 * Created on 2020-12-16
 * 
 * Copyright 2020 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.job;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.ZonedDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import io.gitlab.schedule4j.trigger.SimpleTriggerConfig;
import io.gitlab.schedule4j.trigger.TriggerConfig;
import io.gitlab.schedule4j.trigger.TriggerEvent;
import io.gitlab.schedule4j.trigger.TriggerKey;

/**
 * @author Dirk Buchhorn
 */
public class JobListenerNotifierTest
{
	private static final Logger logger = (Logger) LoggerFactory
		.getLogger(JobListenerNotifier.class);

	private ListAppender<ILoggingEvent> appender;
	private JobListenerNotifier tested;
	private SimpleJobListener listener;
	private JobExecutionContext jobExecutionContext;

	@BeforeEach
	void beforeEach()
	{
		appender = new ListAppender<>();
		appender.start();
		logger.addAppender(appender);

		tested = new JobListenerNotifier();
		listener = new SimpleJobListener();
		tested.addJobListener(listener);

		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("testTrigger"),
			ZonedDateTime.now(), 100, 1000L);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"),
			SimpleStatefulTestJob.class, triggerConfig);
		ZonedDateTime now = ZonedDateTime.now();
		TriggerEvent triggerEvent = new TriggerEvent(now, now);
		jobExecutionContext = new JobExecutionContext(jobDescriptor, triggerEvent, now);
	}

	@Test
	void addJobListenerTest_givenListenerIsNull_nothingIsAdded()
	{
		tested = new JobListenerNotifier();
		tested.addJobListener(null);
		assertEquals(0, tested.getSize());
	}

	@Test
	void addJobListenerTest_givenListenerIsNotNull_listenerIsAdded()
	{
		tested = new JobListenerNotifier();
		assertEquals(0, tested.getSize());
		tested.addJobListener(new SimpleJobListener());
		assertEquals(1, tested.getSize());
	}

	@Test
	void removeJobListenerTest_givenListenerIsNull_nothingIsRemoved()
	{
		tested.removeJobListener(null);
		assertEquals(1, tested.getSize());
	}

	@Test
	void removeJobListenerTest_givenListenerIsNotNull_listenerIsRemoved()
	{
		tested = new JobListenerNotifier();
		SimpleJobListener sjl1 = new SimpleJobListener();
		SimpleJobListener sjl2 = new SimpleJobListener();
		tested.addJobListener(sjl1);
		tested.addJobListener(sjl2);
		tested.removeJobListener(sjl1);
		assertEquals(1, tested.getSize());
	}

	@Test
	void fireJobNotExecutedTest_noException()
	{
		tested.fireJobNotExecuted(jobExecutionContext, null);
		assertEquals(1, listener.getNotExecutedCount());
		assertEquals(0, listener.getExceptionCount());
	}

	@Test
	void fireJobNotExecutedTest_withException()
	{
		tested.fireJobNotExecuted(jobExecutionContext, new RuntimeException());
		assertEquals(1, listener.getNotExecutedCount());
		assertEquals(1, listener.getExceptionCount());
	}

	@Test
	void fireJobWaitingTest()
	{
		tested.fireJobWaiting(jobExecutionContext);
		assertEquals(1, listener.getWaitingCount());
	}

	@Test
	void fireVetoJobExecutionTest_noVeto()
	{
		assertFalse(tested.fireVetoJobExecution(jobExecutionContext));
		assertEquals(1, listener.getVetoCount());
		assertEquals(0, listener.getVetoedCount());
	}

	@Test
	void fireVetoJobExecutionTest_withVeto()
	{
		listener.setVeto(true);
		assertTrue(tested.fireVetoJobExecution(jobExecutionContext));
		assertEquals(1, listener.getVetoCount());
	}

	@Test
	void fireJobExecutionVetoedTest()
	{
		tested.fireJobExecutionVetoed(jobExecutionContext);
		assertEquals(1, listener.getVetoedCount());
	}

	@Test
	void fireJobRunningTest()
	{
		tested.fireJobRunning(jobExecutionContext);
		assertEquals(1, listener.getRunningCount());
	}

	@Test
	void fireJobExecutedTest_noException()
	{
		tested.fireJobExecuted(jobExecutionContext, null);
		assertEquals(1, listener.getExecutedCount());
		assertEquals(0, listener.getExceptionCount());
	}

	@Test
	void fireJobExecutedTest_withException()
	{
		tested.fireJobExecuted(jobExecutionContext, new RuntimeException());
		assertEquals(1, listener.getExecutedCount());
		assertEquals(1, listener.getExceptionCount());
	}

	@Test
	void handExceptionTest()
	{
		tested.addJobListener(new SimpleJobListener()
		{
			@Override
			public void jobExecuted(JobExecutionContext ctx, Exception exception)
			{
				throw new RuntimeException("test");
			}
		});
		assertEquals(0, listener.getExecutedCount());
		tested.fireJobExecuted(jobExecutionContext, null);
		assertEquals(1, listener.getExecutedCount());
		assertEquals(1, appender.list.size(), "size of captured log messages was not 1");
		assertAll(
			() -> assertEquals(Level.ERROR, appender.list.get(0).getLevel(), "wrong log level"),
			() -> assertEquals("test", appender.list.get(0).getMessage(), "wrong message"));
	}
}
