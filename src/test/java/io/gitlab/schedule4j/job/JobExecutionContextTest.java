/*
 * Created on 16.09.2017
 * 
 * Copyright 2017 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.job;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.text.ParseException;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;

import io.gitlab.schedule4j.trigger.SimpleTriggerConfig;
import io.gitlab.schedule4j.trigger.TriggerConfig;
import io.gitlab.schedule4j.trigger.TriggerEvent;
import io.gitlab.schedule4j.trigger.TriggerKey;

/**
 * @author Dirk Buchhorn
 */
public class JobExecutionContextTest
{
	@Test
	void constructorTest_jobDescriptorIsNull_throwsIllegalArgumentException()
	{
		TriggerEvent triggerEvent = new TriggerEvent(ZonedDateTime.now(), ZonedDateTime.now());
		ZonedDateTime fireTime = ZonedDateTime.now();
		assertThrows(IllegalArgumentException.class, () -> new JobExecutionContext(null, triggerEvent, fireTime));
	}

	@Test
	void constructorTest_triggerEventIsNull_throwsIllegalArgumentException()
	{
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test-job"), SimpleTestJob.class, triggerConfig);
		ZonedDateTime fireTime = ZonedDateTime.now();
		assertThrows(IllegalArgumentException.class, () -> new JobExecutionContext(jobDescriptor, null, fireTime));
	}

	@Test
	void constructorTest_schedulerFireTimeIsNull_throwsIllegalArgumentException()
	{
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test-job"), SimpleTestJob.class, triggerConfig);
		TriggerEvent triggerEvent = new TriggerEvent(ZonedDateTime.now(), ZonedDateTime.now());
		assertThrows(IllegalArgumentException.class, () -> new JobExecutionContext(jobDescriptor, triggerEvent, null));
	}

	@Test
	void constructorTest() throws ParseException
	{
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test-job"), SimpleTestJob.class, triggerConfig);
		TriggerEvent triggerEvent = new TriggerEvent(ZonedDateTime.now(), ZonedDateTime.now());
		ZonedDateTime fireTime = ZonedDateTime.now();
		JobExecutionContext ctx = new JobExecutionContext(jobDescriptor, triggerEvent, fireTime);
		assertAll(() -> assertEquals(jobDescriptor, ctx.getJobDescriptor(), "jobDescriptor not equals"), //
			() -> assertEquals(triggerEvent, ctx.getTriggerEvent(), "triggerEvent not equals"), //
			() -> assertEquals(fireTime, ctx.getSchedulerFireTime(), "schedulerFireTime not equals"));
	}
}
