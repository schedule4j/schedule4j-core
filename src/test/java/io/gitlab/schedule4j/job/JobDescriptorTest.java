/*
 * Created on 30.10.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.job;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;

import io.gitlab.schedule4j.trigger.SimpleTriggerConfig;
import io.gitlab.schedule4j.trigger.TriggerConfig;
import io.gitlab.schedule4j.trigger.TriggerKey;

/**
 * @author Dirk Buchhorn
 */
public class JobDescriptorTest
{
	@Test
	void constructorTest_keyNameIsNull_throwsIllegalArgumentException()
	{
		Class<? extends Job> jobClass = SimpleTestJob.class;
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		assertThrows(IllegalArgumentException.class, () -> new JobDescriptor((String) null, jobClass, triggerConfig));
	}

	@Test
	void constructorTest_jobKeyIsNull_throwsIllegalArgumentException()
	{
		Class<? extends Job> jobClass = SimpleTestJob.class;
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		assertThrows(IllegalArgumentException.class, () -> new JobDescriptor((JobKey) null, jobClass, triggerConfig));
	}

	@Test
	void constructorTest_jobClassIsNull_throwsIllegalArgumentException()
	{
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		assertThrows(IllegalArgumentException.class, () -> new JobDescriptor("test", null, triggerConfig));
	}

	@Test
	void constructorTest_triggerConfigIsNull_throwsIllegalArgumentException()
	{
		Class<? extends Job> jobClass = SimpleTestJob.class;
		assertThrows(IllegalArgumentException.class, () -> new JobDescriptor("test", jobClass, null));
	}

	@Test
	void constructorTest()
	{
		Class<? extends Job> jobClass = SimpleTestJob.class;
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jd = new JobDescriptor(JobKey.from("test"), jobClass, triggerConfig);
		assertAll(() -> assertEquals(JobKey.from("test"), jd.getKey()), //
			() -> assertEquals(jobClass, jd.getJobClass()), //
			() -> assertEquals(triggerConfig, jd.getTriggerConfig()));
	}

	@Test
	void constructorTest_jobDescriptorIsNull_throwsIllegalArgumentException()
	{
		assertThrows(IllegalArgumentException.class, () -> new JobDescriptor(null));
	}

	@Test
	void constructorTest_objectIsCloned()
	{
		Class<? extends Job> jobClass = SimpleTestJob.class;
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jd = new JobDescriptor(JobKey.from("test"), jobClass, triggerConfig);
		jd.putJobData("test", "test_value");
		JobDescriptor clone = new JobDescriptor(jd);
		assertAll(() -> assertEquals(jd.getKey(), clone.getKey(), "key was not equals"), //
			() -> assertEquals(jd.getJobClass(), clone.getJobClass(), "jobClass was not equals"), //
			() -> assertEquals(jd.getTriggerConfig(), clone.getTriggerConfig(), "triggerConfig was not equals"), //
			() -> assertNotEquals(System.identityHashCode(jd.getJobDataMap()),
				System.identityHashCode(clone.getJobDataMap()), "jobDataMap was the same"), //
			() -> assertEquals(jd.getJobDataMap(), clone.getJobDataMap(), "jobDataMap contains not the same values"), //
			() -> assertEquals(jd.getPriority(), clone.getPriority(), "priority was not equals"));
	}

	@Test
	void constructorTest_statefulParameterIsFalse_isStatefulReturnFalse()
	{
		Class<? extends Job> jobClass = SimpleTestJob.class;
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jd = new JobDescriptor(JobKey.from("test"), jobClass, triggerConfig, false);
		assertFalse(jd.isStateful());
	}

	@Test
	void constructorTest_statefulParameterIsTrue_isStatefulReturnTrue()
	{
		Class<? extends Job> jobClass = SimpleTestJob.class;
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jd = new JobDescriptor(JobKey.from("test"), jobClass, triggerConfig, true);
		assertTrue(jd.isStateful());
	}

	@Test
	void constructorTest_withoutPriority_getPriorityReturnZero()
	{
		Class<? extends Job> jobClass = SimpleTestJob.class;
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jd = new JobDescriptor(JobKey.from("test"), jobClass, triggerConfig, true);
		assertEquals(0, jd.getPriority());
	}

	@Test
	void constructorTest_withPriority_getPriorityReturnTheGivenValue()
	{
		Class<? extends Job> jobClass = SimpleTestJob.class;
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jd = new JobDescriptor(JobKey.from("test"), jobClass, triggerConfig, true, 10);
		assertEquals(10, jd.getPriority());
	}

	@Test
	void isStatefulTest_statelessJob_returnFalse()
	{
		Class<? extends Job> jobClass = SimpleTestJob.class;
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jd = new JobDescriptor("test", jobClass, triggerConfig);
		assertFalse(jd.isStateful());
	}

	@Test
	void isStatefulTest_statefulJob_returnTrue()
	{
		Class<? extends Job> jobClass = SimpleStatefulTestJob.class;
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jd = new JobDescriptor("test", jobClass, triggerConfig);
		assertTrue(jd.isStateful());
	}

	@Test
	void getJobDataMapTest_returnTheJobDataMap()
	{
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jd = new JobDescriptor("test", SimpleTestJob.class, triggerConfig);
		assertNotNull(jd.getJobDataMap());
		assertEquals(0, jd.getJobDataMap().size());
	}

	@Test
	void putJobDataTest_putOneValue_valueIsAdded()
	{
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jd = new JobDescriptor("test", SimpleTestJob.class, triggerConfig);
		jd.putJobData("test", "testValue");
		jd.putJobData("test2", "testValue2");
		assertEquals(2, jd.getJobDataMap().size());
		assertEquals("testValue", jd.getJobData("test"));
		assertEquals("testValue2", jd.getJobData("test2"));
	}

	@Test
	void removeJobDataTest_removeAnExistingValue_valueIsRemoved()
	{
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(), 10,
			100);
		JobDescriptor jd = new JobDescriptor("test", SimpleTestJob.class, triggerConfig);
		jd.putJobData("test", "testValue");
		jd.putJobData("test2", "testValue2");
		assertEquals("testValue2", jd.removeJobData("test2"));
		assertEquals(1, jd.getJobDataMap().size());
	}

	@Test
	void withTriggerConfigTest() throws Exception
	{
		SimpleTriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"),
			ZonedDateTime.now(), 10, 100);
		SimpleTriggerConfig triggerConfig2 = triggerConfig.withRepeatCount(100);
		JobDescriptor jd = new JobDescriptor("test", SimpleTestJob.class, triggerConfig);
		jd.putJobData("test", "testValue");
		JobDescriptor jd2 = jd.withTriggerConfig(triggerConfig2);
		assertNotNull(jd2);
		assertAll(() -> assertEquals(jd.getKey(), jd2.getKey(), "key was not equals"), //
			() -> assertEquals(jd.getJobClass(), jd2.getJobClass(), "jobClass was not equals"), //
			() -> assertEquals(triggerConfig2, jd2.getTriggerConfig(), "triggerConfig was not equals"), //
			() -> assertNotEquals(System.identityHashCode(jd.getJobDataMap()),
				System.identityHashCode(jd2.getJobDataMap()), "jobDataMap was the same"), //
			() -> assertEquals(jd.getJobDataMap(), jd2.getJobDataMap(), "jobDataMap contains not the same values"), //
			() -> assertEquals(jd.getPriority(), jd2.getPriority(), "priority was not equals"));
	}

	@Test
	void withPriorityTest() throws Exception
	{
		SimpleTriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"),
			ZonedDateTime.now(), 10, 100);
		JobDescriptor jd = new JobDescriptor("test", SimpleTestJob.class, triggerConfig);
		jd.putJobData("test", "testValue");
		JobDescriptor jd2 = jd.withPriority(10);
		assertNotNull(jd2);
		assertAll(() -> assertEquals(jd.getKey(), jd2.getKey(), "key was not equals"), //
			() -> assertEquals(jd.getJobClass(), jd2.getJobClass(), "jobClass was not equals"), //
			() -> assertEquals(triggerConfig, jd2.getTriggerConfig(), "triggerConfig was not equals"), //
			() -> assertNotEquals(System.identityHashCode(jd.getJobDataMap()),
				System.identityHashCode(jd2.getJobDataMap()), "jobDataMap was the same"), //
			() -> assertEquals(jd.getJobDataMap(), jd2.getJobDataMap(), "jobDataMap contains not the same values"), //
			() -> assertEquals(10, jd2.getPriority(), "priority was not equals"));
	}
}
