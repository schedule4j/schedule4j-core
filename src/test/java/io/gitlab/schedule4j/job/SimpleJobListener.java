/*
 * Created on 30.10.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.job;

import java.util.concurrent.atomic.AtomicInteger;

import lombok.ToString;

/**
 * @author Dirk Buchhorn
 */
@ToString
public class SimpleJobListener implements JobListener
{
	private AtomicInteger waitingCount = new AtomicInteger();
	private AtomicInteger runningCount = new AtomicInteger();
	private AtomicInteger executedCount = new AtomicInteger();
	private AtomicInteger notExecutedCount = new AtomicInteger();
	private AtomicInteger vetoCount = new AtomicInteger();
	private AtomicInteger vetoedCount = new AtomicInteger();
	private AtomicInteger exceptionCount = new AtomicInteger();
	private volatile boolean veto = false;

	@Override
	public void jobWaiting(JobExecutionContext ctx)
	{
		waitingCount.incrementAndGet();
	}

	@Override
	public void jobRunning(JobExecutionContext ctx)
	{
		runningCount.incrementAndGet();
	}

	@Override
	public void jobExecuted(JobExecutionContext ctx, Exception exception)
	{
		executedCount.incrementAndGet();
		if (exception != null)
		{
			exceptionCount.incrementAndGet();
		}
	}

	@Override
	public void jobNotExecuted(JobExecutionContext ctx, Exception exception)
	{
		notExecutedCount.incrementAndGet();
		if (exception != null)
		{
			exceptionCount.incrementAndGet();
		}
	}

	@Override
	public boolean vetoJobExecution(JobExecutionContext ctx)
	{
		vetoCount.incrementAndGet();
		return veto;
	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext ctx)
	{
		vetoedCount.incrementAndGet();
	}

	public void setVeto(boolean veto)
	{
		this.veto = veto;
	}

	public int getWaitingCount()
	{
		return waitingCount.get();
	}

	public int getRunningCount()
	{
		return runningCount.get();
	}

	public int getExecutedCount()
	{
		return executedCount.get();
	}

	public int getNotExecutedCount()
	{
		return notExecutedCount.get();
	}

	public int getVetoCount()
	{
		return vetoCount.get();
	}

	public int getVetoedCount()
	{
		return vetoedCount.get();
	}

	public int getExceptionCount()
	{
		return exceptionCount.get();
	}
}
