/*
 * Created on 30.10.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.job;


/**
 * @author Dirk Buchhorn
 */
public class SimpleStatefulTestJob extends SimpleTestJob implements StatefulJob
{
	private static final long serialVersionUID = 9110802342636183756L;

	@Override
	public void execute(JobExecutionContext ctx)
	{
		super.execute(ctx);
	}
}
