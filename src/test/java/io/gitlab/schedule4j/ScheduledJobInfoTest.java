/*
 * Created on 16.07.2018
 * 
 * Copyright 2018 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;

import io.gitlab.schedule4j.job.JobKey;

/**
 * @author Dirk Buchhorn
 */
public class ScheduledJobInfoTest
{
	@Test
	void constructorTest()
	{
		ScheduledJobInfo sji = new ScheduledJobInfo(JobKey.from("test"));
		assertNotNull(sji.getKey());
		assertEquals(JobKey.from("test"), sji.getKey());
	}

	@Test
	void constructorTest_jobKeyIsNull_throwsException()
	{
		assertThrows(IllegalArgumentException.class, () -> new ScheduledJobInfo((JobKey) null));
	}

	@Test
	void constructorTest_scheduledJobInfoIsNull_throwsException()
	{
		assertThrows(IllegalArgumentException.class, () -> new ScheduledJobInfo((ScheduledJobInfo) null));
	}

	@Test
	void jobPausedTest()
	{
		ScheduledJobInfo sji = new ScheduledJobInfo(JobKey.from("test"));
		assertFalse(sji.isJobPaused());
		sji.setJobPaused(true);
		assertTrue(sji.isJobPaused());
		sji.setJobPaused(false);
		assertFalse(sji.isJobPaused());
	}

	@Test
	void unscheduleTest()
	{
		ScheduledJobInfo sji = new ScheduledJobInfo(JobKey.from("test"));
		assertFalse(sji.isUnscheduled());
		sji.setUnscheduled();
		assertTrue(sji.isUnscheduled());
	}

	@Test
	void getScheduledJobStatisticsTest()
	{
		ScheduledJobInfo sji = new ScheduledJobInfo(JobKey.from("test"));
		ScheduledJobStatistics stat = sji.getScheduledJobStatistics();
		assertNotNull(stat);
		assertAll(() -> assertEquals(0, stat.getWaitingCount(), "waiting count was not zero"),
			() -> assertEquals(0, stat.getRunningCount(), "running count was not zero"),
			() -> assertEquals(0, stat.getExecutionCount(), "execution count was not zero"));
	}

	@Test
	void jobWaitingTest()
	{
		ScheduledJobInfo sji = new ScheduledJobInfo(JobKey.from("test"));
		sji.jobWaiting();
		ScheduledJobStatistics stat = sji.getScheduledJobStatistics();
		assertAll(() -> assertEquals(1, stat.getWaitingCount(), "waiting count was not one"),
			() -> assertEquals(0, stat.getRunningCount(), "running count was not zero"),
			() -> assertEquals(0, stat.getExecutionCount(), "execution count was not zero"));
	}

	@Test
	void jobRunningTest()
	{
		ScheduledJobInfo sji = new ScheduledJobInfo(JobKey.from("test"));
		sji.jobWaiting();
		sji.jobRunning();
		ScheduledJobStatistics stat = sji.getScheduledJobStatistics();
		assertNotNull(stat);
		assertAll(() -> assertEquals(0, stat.getWaitingCount(), "waiting count was not zero"),
			() -> assertEquals(1, stat.getRunningCount(), "running count was not one"),
			() -> assertEquals(0, stat.getExecutionCount(), "execution count was not zero"));
	}

	@Test
	void jobExecutedTest()
	{
		ScheduledJobInfo sji = new ScheduledJobInfo(JobKey.from("test"));
		sji.jobWaiting();
		sji.jobRunning();
		sji.jobExecuted();
		ScheduledJobStatistics stat = sji.getScheduledJobStatistics();
		assertNotNull(stat);
		assertAll(() -> assertEquals(0, stat.getWaitingCount(), "waiting count was not zero"),
			() -> assertEquals(0, stat.getRunningCount(), "running count was not zero"),
			() -> assertEquals(1, stat.getExecutionCount(), "execution count was not one"));
	}

	@Test
	void jobNotExecutedTest()
	{
		ScheduledJobInfo sji = new ScheduledJobInfo(JobKey.from("test"));
		sji.jobWaiting();
		sji.jobNotExecuted();
		ScheduledJobStatistics stat = sji.getScheduledJobStatistics();
		assertNotNull(stat);
		assertAll(() -> assertEquals(0, stat.getWaitingCount(), "waiting count was not zero"),
			() -> assertEquals(0, stat.getRunningCount(), "running count was not zero"),
			() -> assertEquals(0, stat.getExecutionCount(), "execution count was not zero"));
	}

	@Test
	void setLastScheduleFireTimeTest()
	{
		ScheduledJobInfo sji = new ScheduledJobInfo(JobKey.from("test"));
		ZonedDateTime zdt = ZonedDateTime.now();
		assertNull(sji.getLastScheduleFireTime());
		sji.setLastScheduleFireTime(zdt);
		assertEquals(zdt, sji.getLastScheduleFireTime());
	}

	@Test
	void setNextScheduleFireTimeTest()
	{
		ScheduledJobInfo sji = new ScheduledJobInfo(JobKey.from("test"));
		ZonedDateTime zdt = ZonedDateTime.now();
		assertNull(sji.getNextScheduleFireTime());
		sji.setNextScheduleFireTime(zdt);
		assertEquals(zdt, sji.getNextScheduleFireTime());
	}

	@Test
	void cloneTest()
	{
		// Immutable test for ScheduledJobStatistics
		ScheduledJobInfo sji = new ScheduledJobInfo(JobKey.from("test"));
		ScheduledJobInfo clone = new ScheduledJobInfo(sji);
		sji.jobWaiting();
		sji.jobRunning();
		sji.jobExecuted();
		ScheduledJobStatistics stat = sji.getScheduledJobStatistics();
		assertNotNull(stat);
		assertEquals(0, stat.getWaitingCount());
		assertEquals(0, stat.getRunningCount());
		assertEquals(1, stat.getExecutionCount());

		stat = clone.getScheduledJobStatistics();
		assertNotNull(stat);
		assertEquals(0, stat.getWaitingCount());
		assertEquals(0, stat.getRunningCount());
		assertEquals(0, stat.getExecutionCount());
	}
}
