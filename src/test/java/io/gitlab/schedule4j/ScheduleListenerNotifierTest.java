/*
 * Created on 2020-12-16
 * 
 * Copyright 2020 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.ZonedDateTime;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import io.gitlab.schedule4j.job.JobDescriptor;
import io.gitlab.schedule4j.job.JobKey;
import io.gitlab.schedule4j.job.SimpleStatefulTestJob;
import io.gitlab.schedule4j.trigger.CreateTriggerException;
import io.gitlab.schedule4j.trigger.SimpleTriggerConfig;
import io.gitlab.schedule4j.trigger.TriggerConfig;
import io.gitlab.schedule4j.trigger.TriggerKey;

/**
 * @author Dirk Buchhorn
 */
public class ScheduleListenerNotifierTest
{
	private static final Logger logger = (Logger) LoggerFactory
		.getLogger(ScheduleListenerNotifier.class);

	private ListAppender<ILoggingEvent> appender;

	private ScheduleListenerNotifier tested;
	private SimpleSchedulerListener listener;
	private ScheduledJob scheduledJob;

	@BeforeEach
	void beforeEach() throws CreateTriggerException
	{
		appender = new ListAppender<>();
		appender.start();
		logger.addAppender(appender);

		tested = new ScheduleListenerNotifier();
		listener = new SimpleSchedulerListener();
		tested.addSchedulerListener(listener);
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("testTrigger"),
			ZonedDateTime.now(), 100, 1000L);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"),
			SimpleStatefulTestJob.class, triggerConfig);
		scheduledJob = new ScheduledJob(jobDescriptor, triggerConfig.createTrigger());
	}

	@AfterEach
	public void afterEach()
	{
		logger.detachAppender(appender);
	}

	@Test
	void addSchedulerListenerTest_listenerIsNotNull_listenerIsAdded()
	{
		ScheduleListenerNotifier tested = new ScheduleListenerNotifier();
		assertEquals(0, tested.getSize());
		SimpleSchedulerListener listener = new SimpleSchedulerListener();
		tested.addSchedulerListener(listener);
		assertEquals(1, tested.getSize());
	}

	@Test
	void addSchedulerListenerTest_listenerIsNull_nothingIsAdded()
	{
		ScheduleListenerNotifier tested = new ScheduleListenerNotifier();
		tested.addSchedulerListener(null);
		assertEquals(0, tested.getSize());
	}

	@Test
	void removeSchedulerListenerTest_listenerIsNotNullAndNotifierContainsListener_listenerIsRemoved()
	{
		ScheduleListenerNotifier tested = new ScheduleListenerNotifier();
		SimpleSchedulerListener listener = new SimpleSchedulerListener();
		tested.addSchedulerListener(listener);
		assertEquals(1, tested.getSize());
		tested.removeSchedulerListener(listener);
		assertEquals(0, tested.getSize());
	}

	@Test
	void removeSchedulerListenerTest_listenerIsNull_nothingIsRemoved()
	{
		ScheduleListenerNotifier tested = new ScheduleListenerNotifier();
		SimpleSchedulerListener listener = new SimpleSchedulerListener();
		tested.addSchedulerListener(listener);
		assertEquals(1, tested.getSize());
		tested.removeSchedulerListener(null);
		assertEquals(1, tested.getSize());
	}

	@Test
	void fireSchedulerStoppedTest()
	{
		assertFalse(listener.isSchedulerStopped());
		tested.fireSchedulerStopped();
		assertTrue(listener.isSchedulerStopped());
	}

	@Test
	void fireSchedulerPausedTest()
	{
		assertEquals(0, listener.getSchedulerPausedCount());
		tested.fireSchedulerPaused();
		assertEquals(1, listener.getSchedulerPausedCount());
	}

	@Test
	void fireSchedulerResumedTest()
	{
		assertEquals(0, listener.getSchedulerResumedCount());
		tested.fireSchedulerResumed();
		assertEquals(1, listener.getSchedulerResumedCount());
	}

	@Test
	void fireJobScheduledTest()
	{
		assertEquals(0, listener.getJobScheduledCount());
		tested.fireJobScheduled(scheduledJob);
		assertEquals(1, listener.getJobScheduledCount());
	}

	@Test
	void fireJobUnscheduledTest()
	{
		assertEquals(0, listener.getJobUnscheduledCount());
		tested.fireJobUnscheduled(scheduledJob);
		assertEquals(1, listener.getJobUnscheduledCount());
	}

	@Test
	void fireJobPausedTest()
	{
		assertEquals(0, listener.getJobPausedCount());
		tested.fireJobPaused(scheduledJob);
		assertEquals(1, listener.getJobPausedCount());
	}

	@Test
	void fireJobResumedTest()
	{
		assertEquals(0, listener.getJobResumedCount());
		tested.fireJobResumed(scheduledJob);
		assertEquals(1, listener.getJobResumedCount());
	}

	@Test
	void handExceptionTest()
	{
		tested.addSchedulerListener(new SimpleSchedulerListener()
		{
			@Override
			public void jobScheduled(ScheduledJob scheduledJob)
			{
				throw new RuntimeException("test");
			}
		});
		assertEquals(0, listener.getJobScheduledCount());
		tested.fireJobScheduled(scheduledJob);
		assertEquals(1, listener.getJobScheduledCount());
		assertEquals(1, appender.list.size(), "size of captured log messages was not 1");
		assertAll(
			() -> assertEquals(Level.ERROR, appender.list.get(0).getLevel(), "wrong log level"),
			() -> assertEquals("test", appender.list.get(0).getMessage(), "wrong message"));
	}
}
