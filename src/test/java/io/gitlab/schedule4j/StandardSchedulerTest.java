/*
 * Created on 06.09.2017
 * 
 * Copyright 2017 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.gitlab.schedule4j.execute.JobExecutor;
import io.gitlab.schedule4j.job.JobDescriptor;
import io.gitlab.schedule4j.job.JobKey;
import io.gitlab.schedule4j.job.SimpleJobListener;
import io.gitlab.schedule4j.job.SimpleStatefulTestJob;
import io.gitlab.schedule4j.job.SimpleTestJob;
import io.gitlab.schedule4j.priority.BoundedPriorityBlockingQueue;
import io.gitlab.schedule4j.trigger.CreateTriggerException;
import io.gitlab.schedule4j.trigger.CronTriggerConfig;
import io.gitlab.schedule4j.trigger.SimpleTriggerConfig;
import io.gitlab.schedule4j.trigger.TriggerConfig;
import io.gitlab.schedule4j.trigger.TriggerKey;

/**
 * @author Dirk Buchhorn
 */
public class StandardSchedulerTest
{
	private BoundedPriorityBlockingQueue<JobExecutor> queue;
	private StandardScheduler tested;
	private SimpleSchedulerListener schedulerListener;
	private SimpleJobListener jobListener;

	@BeforeEach
	void boforeEach()
	{
		queue = new BoundedPriorityBlockingQueue<>(100);
		tested = new StandardScheduler(queue);
		schedulerListener = new SimpleSchedulerListener();
		tested.addSchedulerListener(schedulerListener);
		jobListener = new SimpleJobListener();
		tested.addJobListener(jobListener);
	}

	@AfterEach
	void afterEach()
	{
		tested.stop();
	}

	@Test
	void constructorTest_blockingQueueIsNull_throwsException()
	{
		assertThrows(IllegalArgumentException.class, () -> new StandardScheduler(null));
	}

	@Test
	void constructorTest_blockingQueueIsGiven_schedulerIsCreated()
	{
		// then
		assertNotNull(tested.getQueue());
		assertNotNull(tested.getRunnableQueue());
	}

	@Test
	void stopTest() throws ParseException, CreateTriggerException
	{
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test"), ZonedDateTime.now(), 1000, 100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleTestJob.class, triggerConfig);
		tested.schedule(jobDescriptor);
		assertFalse(tested.isStopped());

		// when
		tested.stop();
		await().atMost(500L, TimeUnit.MILLISECONDS).until(() -> tested.isStopped());
		// wait for SchedulerThread is stopped
		await().atMost(500L, TimeUnit.MILLISECONDS).until(() -> schedulerListener.isSchedulerStopped());
		// call stop method another time - nothing should happen
		tested.stop();
		assertTrue(tested.isStopped());
	}

	@Test
	void scheduleTest_noJobIsScheduled_jobCanBeScheduled() throws ParseException, CreateTriggerException
	{
		// given
		TriggerConfig triggerConfig = new CronTriggerConfig(TriggerKey.from("test1"), "0/30 * * * * ?");
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("job-group", "test1"), SimpleTestJob.class,
			triggerConfig);

		// when
		boolean scheduled = tested.schedule(jobDescriptor);

		// then
		assertTrue(scheduled);
		assertEquals(1, schedulerListener.getJobScheduledCount());
		assertEquals(1, tested.getScheduledJobCount());
	}

	@Test
	void scheduleTest_noTriggerEventInTheFuture_jobCanBeScheduled() throws ParseException, CreateTriggerException
	{
		// given
		TriggerConfig triggerConfig = new CronTriggerConfig(TriggerKey.from("test1"), "0/30 * * * * ? 1990");
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("job-group", "test1"), SimpleTestJob.class,
			triggerConfig);

		// when
		boolean scheduled = tested.schedule(jobDescriptor);

		// then
		assertFalse(scheduled);
		assertEquals(0, schedulerListener.getJobScheduledCount());
		assertEquals(0, tested.getScheduledJobCount());
	}

	@Test
	void scheduleTest_jobWithKeyIsAlreadyScheduled_throwsException() throws ParseException, CreateTriggerException
	{
		// given
		TriggerConfig triggerConfig1 = new CronTriggerConfig(TriggerKey.from("test1"), "0/30 * * * * ?");
		JobDescriptor jobDescriptor1 = new JobDescriptor(JobKey.from("job-group", "test1"), SimpleTestJob.class,
			triggerConfig1);
		tested.schedule(jobDescriptor1);

		JobDescriptor jobDescriptor2 = new JobDescriptor(JobKey.from("job-group", "test1"), SimpleTestJob.class,
			triggerConfig1);

		// when
		// then
		assertThrows(IllegalArgumentException.class, () -> tested.schedule(jobDescriptor2));
	}

	@Test
	void scheduleTest_scheduleListenersAreGiven_allListenersAreAddedToTheScheduledJob()
		throws ParseException, CreateTriggerException
	{
		// given
		TriggerConfig triggerConfig1 = new CronTriggerConfig(TriggerKey.from("test1"), "0/30 * * * * ?");
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("job-group", "test1"), SimpleTestJob.class,
			triggerConfig1);
		SimpleJobListener listener1 = new SimpleJobListener();
		SimpleJobListener listener2 = new SimpleJobListener();

		// when
		tested.schedule(jobDescriptor, listener1, listener2);
		tested.runJobNow(jobDescriptor.getKey());

		// then
		await().atMost(500L, TimeUnit.MILLISECONDS).until(() -> listener1.getWaitingCount() == 1);
		assertEquals(1, listener1.getWaitingCount());
		assertEquals(1, listener2.getWaitingCount());
	}

	@Test
	void unscheduleTest_jobIsScheduled_jobIsUnscheduled() throws ParseException, CreateTriggerException
	{
		// given
		TriggerConfig triggerConfig = new CronTriggerConfig(TriggerKey.from("test1"), "0/30 * * * * ?");
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("job-group", "test1"), SimpleTestJob.class,
			triggerConfig);
		tested.schedule(jobDescriptor);

		// when
		boolean unscheduled = tested.unschedule(jobDescriptor.getKey());

		// then
		assertTrue(unscheduled);
		assertEquals(0, tested.getScheduledJobCount());
		assertEquals(1, schedulerListener.getJobUnscheduledCount());
	}

	@Test
	void unscheduleTest_noJobIsScheduled_jobIsNotUnscheduled() throws ParseException, CreateTriggerException
	{
		// given

		// when
		boolean unscheduled = tested.unschedule(JobKey.from("job-group", "test1"));

		// then
		assertFalse(unscheduled);
		assertEquals(0, tested.getScheduledJobCount());
		assertEquals(0, schedulerListener.getJobUnscheduledCount());
	}

	@Test
	void runJobNowTest_jobIsFound_jobCanBeStarted() throws CreateTriggerException
	{
		// given
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test"), ZonedDateTime.now().plusHours(1),
			1000, 100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleTestJob.class, triggerConfig);
		tested.schedule(jobDescriptor);
		await().atMost(500L, TimeUnit.MILLISECONDS).until(() -> schedulerListener.getJobScheduledCount() == 1);

		// when
		// then
		assertTrue(tested.runJobNow(jobDescriptor.getKey()));
		await().atMost(500L, TimeUnit.MILLISECONDS).until(() -> jobListener.getWaitingCount() == 1);
	}

	@Test
	void runJobNowTest_jobIsNotFound_jobCanNotBeStarted() throws CreateTriggerException
	{
		// given

		// when
		// then
		assertFalse(tested.runJobNow(JobKey.from("another-key")));
	}

	@Test
	void pauseResumeTest() throws CreateTriggerException
	{
		tested.pause();
		assertEquals(1, schedulerListener.getSchedulerPausedCount());
		assertTrue(tested.isPaused());
		// call pause again - nothing should change
		tested.pause();
		assertEquals(1, schedulerListener.getSchedulerPausedCount());
		assertTrue(tested.isPaused());
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test"), ZonedDateTime.now(), 10, 100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleTestJob.class, triggerConfig);
		tested.schedule(jobDescriptor);
		await().atMost(400L, TimeUnit.MILLISECONDS).until(() -> schedulerListener.getJobScheduledCount() > 0);
		assertEquals(0, jobListener.getWaitingCount());
		tested.resume();
		assertEquals(1, schedulerListener.getSchedulerResumedCount());
		// call resume again - nothing should change
		tested.resume();
		assertEquals(1, schedulerListener.getSchedulerResumedCount());
		await().atMost(400L, TimeUnit.MILLISECONDS).until(() -> jobListener.getWaitingCount() > 0);
		assertTrue(jobListener.getWaitingCount() > 0);
	}

	@Test
	void pauseJobTest_jobIsFound_jobIsPaused() throws CreateTriggerException
	{
		// given
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(),
			1000, 10000);
		JobDescriptor jobDescriptor1 = new JobDescriptor(JobKey.from("test-job-1"), SimpleTestJob.class, triggerConfig);
		JobDescriptor jobDescriptor2 = new JobDescriptor(JobKey.from("test-job-2"), SimpleTestJob.class, triggerConfig);
		tested.schedule(jobDescriptor1);
		tested.schedule(jobDescriptor2);

		// when
		tested.pauseJob(jobDescriptor1.getKey());
		// call a second time - nothing should happen
		tested.pauseJob(jobDescriptor1.getKey());

		// then
		Optional<ScheduledJobInfo> scheduledJobInfo1 = tested.getScheduledJobInfo(jobDescriptor1.getKey());
		assertTrue(scheduledJobInfo1.get().isJobPaused());
		Optional<ScheduledJobInfo> scheduledJobInfo2 = tested.getScheduledJobInfo(jobDescriptor2.getKey());
		assertFalse(scheduledJobInfo2.get().isJobPaused());
	}

	@Test
	void pauseJobTest_jobIsNotFound_noJobIsPaused() throws CreateTriggerException
	{
		// given
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(),
			1000, 10000);
		JobDescriptor jobDescriptor1 = new JobDescriptor(JobKey.from("test-job-1"), SimpleTestJob.class, triggerConfig);
		tested.schedule(jobDescriptor1);

		// when
		tested.pauseJob(JobKey.from("test-job"));

		// then
		assertFalse(tested.isPaused());
		Optional<ScheduledJobInfo> scheduledJobInfo1 = tested.getScheduledJobInfo(jobDescriptor1.getKey());
		assertFalse(scheduledJobInfo1.get().isJobPaused());
	}

	@Test
	void resumeJobTest_jobIsFound_jobIsResumed() throws CreateTriggerException
	{
		// given
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(),
			1000, 10000);
		JobDescriptor jobDescriptor1 = new JobDescriptor(JobKey.from("test-job-1"), SimpleTestJob.class, triggerConfig);
		tested.schedule(jobDescriptor1);
		tested.pauseJob(jobDescriptor1.getKey());

		// when
		tested.resumeJob(jobDescriptor1.getKey());
		// call a second time - nothing should happen
		tested.resumeJob(jobDescriptor1.getKey());

		// then
		Optional<ScheduledJobInfo> scheduledJobInfo1 = tested.getScheduledJobInfo(jobDescriptor1.getKey());
		assertFalse(scheduledJobInfo1.get().isJobPaused());
	}

	@Test
	void resumeJobTest_jobIsNotFound_noJobIsResumed() throws CreateTriggerException
	{
		// given
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(),
			1000, 10000);
		JobDescriptor jobDescriptor1 = new JobDescriptor(JobKey.from("test-job-1"), SimpleTestJob.class, triggerConfig);
		tested.schedule(jobDescriptor1);
		tested.pauseJob(jobDescriptor1.getKey());

		// when
		tested.resumeJob(JobKey.from("test-job"));

		// then
		assertFalse(tested.isPaused());
		Optional<ScheduledJobInfo> scheduledJobInfo1 = tested.getScheduledJobInfo(jobDescriptor1.getKey());
		assertTrue(scheduledJobInfo1.get().isJobPaused());
	}

	@Test
	void pauseGroupTest_jobsAreFound_jobsArePaused() throws CreateTriggerException
	{
		// given
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(),
			1000, 10000);
		JobKey groupKey = JobKey.from("group");
		JobDescriptor jobDescriptor1 = new JobDescriptor(JobKey.from(groupKey, "test-job-1"), SimpleTestJob.class,
			triggerConfig);
		JobDescriptor jobDescriptor2 = new JobDescriptor(JobKey.from(groupKey, "test-job-2"), SimpleTestJob.class,
			triggerConfig);
		JobDescriptor jobDescriptor3 = new JobDescriptor(JobKey.from("test-job-3"), SimpleTestJob.class, triggerConfig);
		tested.schedule(jobDescriptor1);
		tested.schedule(jobDescriptor2);
		tested.schedule(jobDescriptor3);

		// when
		tested.pauseGroup(groupKey);
		// call a second time - nothing should happen
		tested.pauseGroup(groupKey);

		// then
		Optional<ScheduledJobInfo> scheduledJobInfo1 = tested.getScheduledJobInfo(jobDescriptor1.getKey());
		assertTrue(scheduledJobInfo1.get().isJobPaused());
		Optional<ScheduledJobInfo> scheduledJobInfo2 = tested.getScheduledJobInfo(jobDescriptor2.getKey());
		assertTrue(scheduledJobInfo2.get().isJobPaused());
		Optional<ScheduledJobInfo> scheduledJobInfo3 = tested.getScheduledJobInfo(jobDescriptor3.getKey());
		assertFalse(scheduledJobInfo3.get().isJobPaused());
	}

	@Test
	void pauseGroupTest_jobsAreNotFound_noJobsArePaused() throws CreateTriggerException
	{
		// given
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(),
			1000, 10000);
		JobDescriptor jobDescriptor1 = new JobDescriptor(JobKey.from("test-job-1"), SimpleTestJob.class, triggerConfig);
		tested.schedule(jobDescriptor1);

		// when
		tested.pauseGroup(JobKey.from("group"));

		// then
		assertFalse(tested.isPaused());
		Optional<ScheduledJobInfo> scheduledJobInfo1 = tested.getScheduledJobInfo(jobDescriptor1.getKey());
		assertFalse(scheduledJobInfo1.get().isJobPaused());
	}

	@Test
	void resumeGroupTest_jobsAreFound_jobsAreResumed() throws CreateTriggerException
	{
		// given
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(),
			1000, 10000);
		JobKey groupKey = JobKey.from("group");
		JobDescriptor jobDescriptor1 = new JobDescriptor(JobKey.from(groupKey, "test-job-1"), SimpleTestJob.class,
			triggerConfig);
		JobDescriptor jobDescriptor2 = new JobDescriptor(JobKey.from(groupKey, "test-job-2"), SimpleTestJob.class,
			triggerConfig);
		JobDescriptor jobDescriptor3 = new JobDescriptor(JobKey.from("group2", "test-job-3"), SimpleTestJob.class,
			triggerConfig);
		tested.schedule(jobDescriptor1);
		tested.schedule(jobDescriptor2);
		tested.schedule(jobDescriptor3);
		tested.pauseGroup(groupKey);
		tested.pauseGroup(jobDescriptor3.getKey().getParentKey());

		// when
		tested.resumeGroup(groupKey);
		// call a second time - nothing should happen
		tested.resumeGroup(groupKey);

		// then
		Optional<ScheduledJobInfo> scheduledJobInfo1 = tested.getScheduledJobInfo(jobDescriptor1.getKey());
		assertFalse(scheduledJobInfo1.get().isJobPaused());
		Optional<ScheduledJobInfo> scheduledJobInfo2 = tested.getScheduledJobInfo(jobDescriptor2.getKey());
		assertFalse(scheduledJobInfo2.get().isJobPaused());
		Optional<ScheduledJobInfo> scheduledJobInfo3 = tested.getScheduledJobInfo(jobDescriptor3.getKey());
		assertTrue(scheduledJobInfo3.get().isJobPaused());
	}

	@Test
	void resumeGroupTest_jobsAreNotFound_noJobsAreResumed() throws CreateTriggerException
	{
		// given
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(),
			1000, 10000);
		JobDescriptor jobDescriptor1 = new JobDescriptor(JobKey.from("group2", "test-job-1"), SimpleTestJob.class,
			triggerConfig);
		tested.schedule(jobDescriptor1);
		tested.pauseGroup(jobDescriptor1.getKey().getParentKey());

		// when
		tested.resumeGroup(JobKey.from("group"));

		// then
		assertFalse(tested.isPaused());
		Optional<ScheduledJobInfo> scheduledJobInfo1 = tested.getScheduledJobInfo(jobDescriptor1.getKey());
		assertTrue(scheduledJobInfo1.get().isJobPaused());
	}

	@Test
	void pauseJobByTriggerKeyTest_jobsAreFound_jobsArePaused() throws CreateTriggerException
	{
		// given
		TriggerConfig triggerConfig1 = new SimpleTriggerConfig(TriggerKey.from("test-trigger-1"), ZonedDateTime.now(),
			1000, 10000);
		JobDescriptor jobDescriptor1 = new JobDescriptor(JobKey.from("test-job-1"), SimpleTestJob.class,
			triggerConfig1);
		TriggerConfig triggerConfig2 = new SimpleTriggerConfig(TriggerKey.from("test-trigger-2"), ZonedDateTime.now(),
			1000, 10000);
		JobDescriptor jobDescriptor2 = new JobDescriptor(JobKey.from("test-job-2"), SimpleTestJob.class,
			triggerConfig2);
		tested.schedule(jobDescriptor1);
		tested.schedule(jobDescriptor2);

		// when
		tested.pauseJob(triggerConfig1.getKey());
		// call a second time - nothing should happen
		tested.pauseJob(triggerConfig1.getKey());

		// then
		Optional<ScheduledJobInfo> scheduledJobInfo1 = tested.getScheduledJobInfo(jobDescriptor1.getKey());
		assertTrue(scheduledJobInfo1.get().isJobPaused());
		Optional<ScheduledJobInfo> scheduledJobInfo2 = tested.getScheduledJobInfo(jobDescriptor2.getKey());
		assertFalse(scheduledJobInfo2.get().isJobPaused());
	}

	@Test
	void pauseJobByTriggerKeyTest_jobsAreNotFound_jobsAreNotPaused() throws CreateTriggerException
	{
		// given
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(),
			1000, 10000);
		JobDescriptor jobDescriptor1 = new JobDescriptor(JobKey.from("test-job-1"), SimpleTestJob.class, triggerConfig);
		tested.schedule(jobDescriptor1);

		// when
		tested.pauseJob(TriggerKey.from("test-trigger-not-found"));

		// then
		Optional<ScheduledJobInfo> scheduledJobInfo1 = tested.getScheduledJobInfo(jobDescriptor1.getKey());
		assertFalse(scheduledJobInfo1.get().isJobPaused());
	}

	@Test
	void resumeJobByTriggerKeyTest_jobsAreFound_jobsAreResumed() throws CreateTriggerException
	{
		// given
		TriggerConfig triggerConfig1 = new SimpleTriggerConfig(TriggerKey.from("test-trigger-1"), ZonedDateTime.now(),
			1000, 10000);
		JobDescriptor jobDescriptor1 = new JobDescriptor(JobKey.from("test-job-1"), SimpleTestJob.class,
			triggerConfig1);
		TriggerConfig triggerConfig2 = new SimpleTriggerConfig(TriggerKey.from("test-trigger-2"), ZonedDateTime.now(),
			1000, 10000);
		JobDescriptor jobDescriptor2 = new JobDescriptor(JobKey.from("test-job-2"), SimpleTestJob.class,
			triggerConfig2);
		tested.schedule(jobDescriptor1);
		tested.schedule(jobDescriptor2);
		tested.pauseJob(triggerConfig1.getKey());

		// when
		tested.resumeJob(triggerConfig1.getKey());
		// call a second time - nothing should happen
		tested.resumeJob(triggerConfig1.getKey());

		// then
		Optional<ScheduledJobInfo> scheduledJobInfo1 = tested.getScheduledJobInfo(jobDescriptor1.getKey());
		assertFalse(scheduledJobInfo1.get().isJobPaused());
		Optional<ScheduledJobInfo> scheduledJobInfo2 = tested.getScheduledJobInfo(jobDescriptor2.getKey());
		assertFalse(scheduledJobInfo2.get().isJobPaused());
	}

	@Test
	void resumeJobByTriggerKeyTest_jobsAreNotFound_jobsAreNotResumed() throws CreateTriggerException
	{
		// given
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test-trigger"), ZonedDateTime.now(),
			1000, 10000);
		JobDescriptor jobDescriptor1 = new JobDescriptor(JobKey.from("test-job-1"), SimpleTestJob.class, triggerConfig);
		tested.schedule(jobDescriptor1);
		tested.pauseJob(triggerConfig.getKey());

		// when
		tested.resumeJob(TriggerKey.from("test-trigger-not_found"));

		// then
		Optional<ScheduledJobInfo> scheduledJobInfo1 = tested.getScheduledJobInfo(jobDescriptor1.getKey());
		assertTrue(scheduledJobInfo1.get().isJobPaused());
	}

	@Test
	void pauseGroupByTriggerKeyTest_jobsAreFound_jobsArePaused() throws CreateTriggerException
	{
		// given
		TriggerKey groupKey = TriggerKey.from("group");
		TriggerConfig triggerConfig1 = new SimpleTriggerConfig(TriggerKey.from(groupKey, "test-trigger-1"),
			ZonedDateTime.now(), 1000, 10000);
		JobDescriptor jobDescriptor1 = new JobDescriptor(JobKey.from("test-job-1"), SimpleTestJob.class,
			triggerConfig1);
		JobDescriptor jobDescriptor3 = new JobDescriptor(JobKey.from("test-job-3"), SimpleTestJob.class,
			triggerConfig1);
		TriggerConfig triggerConfig2 = new SimpleTriggerConfig(TriggerKey.from("test-trigger-2"), ZonedDateTime.now(),
			1000, 10000);
		JobDescriptor jobDescriptor2 = new JobDescriptor(JobKey.from("test-job-2"), SimpleTestJob.class,
			triggerConfig2);
		tested.schedule(jobDescriptor1);
		tested.schedule(jobDescriptor2);
		tested.schedule(jobDescriptor3);

		// when
		tested.pauseGroup(groupKey);
		// call a second time - nothing should happen
		tested.pauseGroup(groupKey);

		// then
		Optional<ScheduledJobInfo> scheduledJobInfo1 = tested.getScheduledJobInfo(jobDescriptor1.getKey());
		assertTrue(scheduledJobInfo1.get().isJobPaused());
		Optional<ScheduledJobInfo> scheduledJobInfo3 = tested.getScheduledJobInfo(jobDescriptor1.getKey());
		assertTrue(scheduledJobInfo3.get().isJobPaused());
		Optional<ScheduledJobInfo> scheduledJobInfo2 = tested.getScheduledJobInfo(jobDescriptor2.getKey());
		assertFalse(scheduledJobInfo2.get().isJobPaused());
	}

	@Test
	void resumeGroupByTriggerKeyTest_jobsAreFound_jobsAreResumed() throws CreateTriggerException
	{
		// given
		TriggerKey groupKey = TriggerKey.from("group");
		TriggerConfig triggerConfig1 = new SimpleTriggerConfig(TriggerKey.from(groupKey, "test-trigger-1"),
			ZonedDateTime.now(), 1000, 10000);
		JobDescriptor jobDescriptor1 = new JobDescriptor(JobKey.from("test-job-1"), SimpleTestJob.class,
			triggerConfig1);
		JobDescriptor jobDescriptor3 = new JobDescriptor(JobKey.from("test-job-3"), SimpleTestJob.class,
			triggerConfig1);
		TriggerConfig triggerConfig2 = new SimpleTriggerConfig(TriggerKey.from("group2", "test-trigger-2"),
			ZonedDateTime.now(), 1000, 10000);
		JobDescriptor jobDescriptor2 = new JobDescriptor(JobKey.from("test-job-2"), SimpleTestJob.class,
			triggerConfig2);
		tested.schedule(jobDescriptor1);
		tested.schedule(jobDescriptor2);
		tested.schedule(jobDescriptor3);
		tested.pauseGroup(groupKey);
		tested.pauseGroup(triggerConfig2.getKey().getParentKey());

		// when
		tested.resumeGroup(groupKey);
		// call a second time - nothing should happen
		tested.resumeGroup(groupKey);

		// then
		Optional<ScheduledJobInfo> scheduledJobInfo1 = tested.getScheduledJobInfo(jobDescriptor1.getKey());
		assertFalse(scheduledJobInfo1.get().isJobPaused());
		Optional<ScheduledJobInfo> scheduledJobInfo3 = tested.getScheduledJobInfo(jobDescriptor1.getKey());
		assertFalse(scheduledJobInfo3.get().isJobPaused());
		Optional<ScheduledJobInfo> scheduledJobInfo2 = tested.getScheduledJobInfo(jobDescriptor2.getKey());
		assertTrue(scheduledJobInfo2.get().isJobPaused());
	}

	@Test
	void getScheduledJobInfoTest_jobExist_returnJobInfo() throws ParseException, CreateTriggerException
	{
		// given
		TriggerConfig triggerConfig = new CronTriggerConfig(TriggerKey.from("test"), "0 0 23 * * ?");
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleStatefulTestJob.class,
			triggerConfig);
		tested.schedule(jobDescriptor);

		// when
		Optional<ScheduledJobInfo> jobInfo = tested.getScheduledJobInfo(jobDescriptor.getKey());

		// then
		assertTrue(jobInfo.isPresent());
		assertEquals(JobKey.from("test"), jobInfo.get().getKey());
	}

	@Test
	void getScheduledJobInfoTest_jobDoesNotExist_returnEmptyOptional() throws ParseException, CreateTriggerException
	{
		// given

		// when
		Optional<ScheduledJobInfo> jobInfo = tested.getScheduledJobInfo(new JobKey("not_found"));

		// then
		assertFalse(jobInfo.isPresent());
	}

	@Test
	void getScheduledJobInfosTest_jobDoesNotExist_returnEmptyCollection() throws ParseException, CreateTriggerException
	{
		// given

		// when
		Collection<ScheduledJobInfo> col = tested.getScheduledJobInfos();

		// then
		assertNotNull(col);
		assertTrue(col.isEmpty());
	}

	@Test
	void getScheduledJobInfosTest_jobsExist_returnNonEmptyCollection() throws ParseException, CreateTriggerException
	{
		// given
		TriggerConfig triggerConfig1 = new CronTriggerConfig(TriggerKey.from("test1"), "0 0 21 * * ?");
		JobDescriptor jd1 = new JobDescriptor(JobKey.from("test1"), SimpleStatefulTestJob.class, triggerConfig1);
		tested.schedule(jd1);
		TriggerConfig triggerConfig2 = new CronTriggerConfig(TriggerKey.from("test2"), "0 0 22 * * ?");
		JobDescriptor jd2 = new JobDescriptor(JobKey.from("test2"), SimpleStatefulTestJob.class, triggerConfig2);
		tested.schedule(jd2);

		// when
		Collection<ScheduledJobInfo> col = tested.getScheduledJobInfos();

		// then
		assertNotNull(col);
		assertEquals(2, col.size());
	}

	@Test
	void getJobDescriptorsTest_jobDoesNotExist_returnEmptyCollection() throws ParseException, CreateTriggerException
	{
		// given

		// when
		Collection<JobDescriptor> col = tested.getJobDescriptors();

		// then
		assertNotNull(col);
		assertTrue(col.isEmpty());
	}

	@Test
	void getJobDescriptorsTest_jobsExist_returnNonEmptyCollection() throws ParseException, CreateTriggerException
	{
		// given
		TriggerConfig triggerConfig1 = new CronTriggerConfig(TriggerKey.from("test1"), "0 0 21 * * ?");
		JobDescriptor jd1 = new JobDescriptor(JobKey.from("test1"), SimpleStatefulTestJob.class, triggerConfig1);
		tested.schedule(jd1);
		TriggerConfig triggerConfig2 = new CronTriggerConfig(TriggerKey.from("test2"), "0 0 22 * * ?");
		JobDescriptor jd2 = new JobDescriptor(JobKey.from("test2"), SimpleStatefulTestJob.class, triggerConfig2);
		tested.schedule(jd2);

		// when
		Collection<JobDescriptor> col = tested.getJobDescriptors();

		// then
		assertNotNull(col);
		assertEquals(2, col.size());
	}

	@Test
	void updateTest_jobIsNotFound_noJobIsUpdated() throws Exception
	{
		// given
		TriggerConfig triggerConfig1 = new CronTriggerConfig(TriggerKey.from("trigger-key"), "0 0 21 * * ? 2200");
		JobDescriptor jd1 = new JobDescriptor(JobKey.from("job-key"), SimpleStatefulTestJob.class, triggerConfig1);

		// when
		boolean updated = tested.update(jd1);

		// then
		assertFalse(updated);
	}

	@Test
	void updateTest_jobIsFound_jobIsUpdated() throws Exception
	{
		// given
		TriggerConfig triggerConfig1 = new CronTriggerConfig(TriggerKey.from("trigger-key"), "0 0 21 * * ? 2200");
		JobDescriptor jd1 = new JobDescriptor(JobKey.from("job-key"), SimpleStatefulTestJob.class, triggerConfig1);
		tested.schedule(jd1);
		TriggerConfig triggerConfig2 = new CronTriggerConfig(TriggerKey.from("trigger-key-2"), "0 0 22 * * ? 2200");
		JobDescriptor jd2 = new JobDescriptor(JobKey.from("job-key"), SimpleStatefulTestJob.class, triggerConfig2);

		// when
		boolean updated = tested.update(jd2);

		// then
		assertTrue(updated);
		Optional<JobDescriptor> jd = tested.getJobDescriptor(jd2.getKey());
		assertEquals(triggerConfig2, jd.get().getTriggerConfig());
	}

	@Test
	void updateTriggerTest_jobIsNotFound_triggerIsNotUpdated() throws Exception
	{
		// given
		TriggerConfig triggerConfig1 = new CronTriggerConfig(TriggerKey.from("trigger-key"), "0 0 21 * * ? 2200");

		// when
		boolean updated = tested.updateTrigger(JobKey.from("job-key"), triggerConfig1);

		// then
		assertFalse(updated);
	}

	@Test
	void updateTriggerTest_jobIsFound_triggerIsUpdated() throws Exception
	{
		// given
		TriggerConfig triggerConfig1 = new CronTriggerConfig(TriggerKey.from("trigger-key"), "0 0 21 * * ? 2200");
		JobDescriptor jd1 = new JobDescriptor(JobKey.from("job-key"), SimpleStatefulTestJob.class, triggerConfig1);
		tested.schedule(jd1);
		TriggerConfig triggerConfig2 = new CronTriggerConfig(TriggerKey.from("trigger-key-2"), "0 0 22 * * ? 2200");

		// when
		boolean updated = tested.updateTrigger(jd1.getKey(), triggerConfig2);

		// then
		assertTrue(updated);
		Optional<JobDescriptor> jd = tested.getJobDescriptor(jd1.getKey());
		assertEquals(triggerConfig2, jd.get().getTriggerConfig());
	}

	@Test
	void updateTriggerTest_jobIsFoundAndNewTriggerHasNoEventInFutire_jobIsUnscheduled() throws Exception
	{
		// given
		TriggerConfig triggerConfig1 = new CronTriggerConfig(TriggerKey.from("trigger-key"), "0 0 21 * * ? 2200");
		JobDescriptor jd1 = new JobDescriptor(JobKey.from("job-key"), SimpleStatefulTestJob.class, triggerConfig1);
		tested.schedule(jd1);
		TriggerConfig triggerConfig2 = new CronTriggerConfig(TriggerKey.from("trigger-key-2"),
			"0 0 22 * * ? 1900-1910");

		// when
		boolean updated = tested.updateTrigger(jd1.getKey(), triggerConfig2);

		// then
		assertTrue(updated);
		Optional<JobDescriptor> jd = tested.getJobDescriptor(jd1.getKey());
		assertFalse(jd.isPresent());
	}

	@Test
	void addRemoveJobListenerTest() throws CreateTriggerException
	{
		SimpleJobListener jobListener2 = new SimpleJobListener();
		tested.addJobListener(jobListener2);
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test"), ZonedDateTime.now(), 1000, 100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleTestJob.class, triggerConfig);
		tested.schedule(jobDescriptor);

		await().atMost(400L, TimeUnit.MILLISECONDS).until(() -> jobListener2.getWaitingCount() > 0);
		tested.removeJobListener(jobListener2);
		await().atMost(400L, TimeUnit.MILLISECONDS)
			.until(() -> jobListener.getWaitingCount() > jobListener2.getWaitingCount());

		assertTrue(jobListener.getWaitingCount() > jobListener2.getWaitingCount());
	}

	@Test
	void addRemoveScheduleListenerTest() throws CreateTriggerException
	{
		SimpleSchedulerListener schedulerListener2 = new SimpleSchedulerListener();
		tested.addSchedulerListener(schedulerListener2);
		TriggerConfig triggerConfig = new SimpleTriggerConfig(TriggerKey.from("test"), ZonedDateTime.now(), 1000, 100);
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleTestJob.class, triggerConfig);
		tested.schedule(jobDescriptor);
		assertEquals(1, schedulerListener2.getJobScheduledCount());
		tested.removeSchedulerListener(schedulerListener2);
		tested.unschedule(jobDescriptor.getKey());
		assertEquals(0, schedulerListener2.getJobUnscheduledCount());
		assertEquals(1, schedulerListener.getJobUnscheduledCount());
	}

	@Test
	void addJobListenerWithJobKeyTest_jobIsNotFound_noListenerIsAdded() throws Exception
	{
		// given

		// when
		boolean success = tested.addJobListener(JobKey.from("not-found"), new SimpleJobListener());

		// then
		assertFalse(success);
	}

	@Test
	void addJobListenerWithJobKeyTest_jobIsFound_listenerIsAdded() throws Exception
	{
		// given
		TriggerConfig triggerConfig1 = new CronTriggerConfig(TriggerKey.from("trigger-key"), "0 0 21 * * ? 2200");
		JobDescriptor jd1 = new JobDescriptor(JobKey.from("job-key"), SimpleStatefulTestJob.class, triggerConfig1);
		tested.schedule(jd1);

		// when
		boolean success = tested.addJobListener(jd1.getKey(), new SimpleJobListener());

		// then
		assertTrue(success);
	}

	@Test
	void removeJobListenerWithJobKeyTest_jobIsNotFound_noListenerIsRemoved() throws Exception
	{
		// given

		// when
		boolean success = tested.removeJobListener(JobKey.from("not-found"), new SimpleJobListener());

		// then
		assertFalse(success);
	}

	@Test
	void removeJobListenerWithJobKeyTest_jobIsFound_listenerIsRemoved() throws Exception
	{
		// given
		TriggerConfig triggerConfig1 = new CronTriggerConfig(TriggerKey.from("trigger-key"), "0 0 21 * * ? 2200");
		JobDescriptor jd1 = new JobDescriptor(JobKey.from("job-key"), SimpleStatefulTestJob.class, triggerConfig1);
		SimpleJobListener listener = new SimpleJobListener();
		tested.schedule(jd1, listener);

		// when
		boolean success = tested.removeJobListener(jd1.getKey(), listener);

		// then
		assertTrue(success);
	}

	@Test
	void callJobListenerTest() throws ParseException, CreateTriggerException
	{
		TriggerConfig triggerConfig = new CronTriggerConfig(TriggerKey.from("test"), ZonedDateTime.now().plusDays(1),
			null, "0 0 23 * * ?");
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleTestJob.class, triggerConfig);
		tested.schedule(jobDescriptor);
		tested.runJobNow(jobDescriptor.getKey());
		await().atMost(400L, TimeUnit.MILLISECONDS).until(() -> queue.size() == 1);

		JobExecutor jobExecutor = queue.remove();
		jobExecutor.run();
		assertEquals(1, jobListener.getWaitingCount());
		assertEquals(1, jobListener.getRunningCount());
		assertEquals(1, jobListener.getExecutedCount());
		assertEquals(1, jobListener.getVetoCount());
		assertEquals(0, jobListener.getVetoedCount());
		assertEquals(0, jobListener.getNotExecutedCount());

		tested.runJobNow(jobDescriptor.getKey());
		await().atMost(500L, TimeUnit.MILLISECONDS).until(() -> queue.size() == 1);
		tested.runJobNow(jobDescriptor.getKey());
		await().atMost(500L, TimeUnit.MILLISECONDS).until(() -> queue.size() == 2);
		assertEquals(3, jobListener.getWaitingCount());
		assertEquals(1, jobListener.getRunningCount());
		assertEquals(1, jobListener.getExecutedCount());
		assertEquals(3, jobListener.getVetoCount());
		assertEquals(0, jobListener.getVetoedCount());
		assertEquals(0, jobListener.getNotExecutedCount());
		// 2 job executors in the queue

		jobListener.setVeto(true);
		tested.runJobNow(jobDescriptor.getKey());
		await().atMost(400L, TimeUnit.MILLISECONDS).until(() -> jobListener.getVetoedCount() == 1);
		assertEquals(3, jobListener.getWaitingCount());
		assertEquals(1, jobListener.getRunningCount());
		assertEquals(1, jobListener.getExecutedCount());
		assertEquals(4, jobListener.getVetoCount());
		assertEquals(1, jobListener.getVetoedCount());
		assertEquals(0, jobListener.getNotExecutedCount());

		jobExecutor = queue.remove();
		jobExecutor.fireJobNotExecuted(null);
		assertEquals(3, jobListener.getWaitingCount());
		assertEquals(1, jobListener.getRunningCount());
		assertEquals(1, jobListener.getExecutedCount());
		assertEquals(4, jobListener.getVetoCount());
		assertEquals(1, jobListener.getVetoedCount());
		assertEquals(1, jobListener.getNotExecutedCount());
		// 1 job executor in the queue

		jobExecutor = queue.remove();
		jobExecutor.run();
		assertEquals(3, jobListener.getWaitingCount());
		assertEquals(2, jobListener.getRunningCount());
		assertEquals(2, jobListener.getExecutedCount());
		assertEquals(4, jobListener.getVetoCount());
		assertEquals(1, jobListener.getVetoedCount());
		assertEquals(1, jobListener.getNotExecutedCount());
	}

	@Test
	void jobListenerTest() throws ParseException, CreateTriggerException
	{
		TriggerConfig triggerConfig = new CronTriggerConfig(TriggerKey.from("test"), ZonedDateTime.now().plusDays(1),
			null, "0 0 23 * * ?");
		JobDescriptor jobDescriptor = new JobDescriptor(JobKey.from("test"), SimpleTestJob.class, triggerConfig);
		tested.schedule(jobDescriptor);
		tested.runJobNow(jobDescriptor.getKey());
		await().atMost(400L, TimeUnit.MILLISECONDS).until(() -> queue.size() == 1);
		JobExecutor jobExecutor = queue.remove();
		jobExecutor.run();
		assertEquals(1, jobListener.getWaitingCount());
		assertEquals(1, jobListener.getRunningCount());
		assertEquals(1, jobListener.getExecutedCount());
		assertEquals(1, jobListener.getVetoCount());
		assertEquals(0, jobListener.getVetoedCount());
		assertEquals(0, jobListener.getNotExecutedCount());

		jobDescriptor.getJobDataMap().put(SimpleTestJob.THROW_EXCEPTION_FLAG, "true");
		tested.runJobNow(jobDescriptor.getKey());
		await().atMost(400L, TimeUnit.MILLISECONDS).until(() -> queue.size() == 1);
		jobExecutor = queue.remove();
		jobExecutor.run();
		assertEquals(1, jobListener.getExceptionCount());
		assertEquals(2, jobListener.getWaitingCount());
		assertEquals(2, jobListener.getRunningCount());
		assertEquals(2, jobListener.getExecutedCount());
		assertEquals(2, jobListener.getVetoCount());
		assertEquals(0, jobListener.getVetoedCount());
		assertEquals(0, jobListener.getNotExecutedCount());
	}
}
