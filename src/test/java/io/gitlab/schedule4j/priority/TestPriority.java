/*
 * Created on 24.09.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.priority;

import lombok.Data;

/**
 * @author Dirk Buchhorn
 */
@Data
public class TestPriority implements Priority
{
	private String name;
	private int priority;

	public TestPriority(String name, int priority)
	{
		this.name = name;
		this.priority = priority;
	}

	public String getName()
	{
		return name;
	}

	@Override
	public int getPriority()
	{
		return priority;
	}

	@Override
	public void setPriority(int priority)
	{
		this.priority = priority;
	}

	@Override
	public void plusPriority(int priority)
	{
		this.priority += priority;
	}

	@Override
	public void minusPriority(int priority)
	{
		this.priority -= priority;
	}

	@Override
	public String toString()
	{
		return "MyPriority (name=" + name + ", priority=" + priority + ")";
	}
}
