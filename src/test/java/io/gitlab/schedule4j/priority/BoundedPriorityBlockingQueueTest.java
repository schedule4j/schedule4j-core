/*
 * Created on 24.09.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.priority;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;

/**
 * @author Dirk Buchhorn
 */
public class BoundedPriorityBlockingQueueTest
{

	@Test
	void constructorTest_initWithMaxCapacity()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);

		assertAll(() -> assertEquals(10, queue.getMaxCapacity(), "wrong maxCapacity"),
			() -> assertEquals(DoNothingQueuedElementPriorityAdjuster.class, queue.getPriorityAdjuster().getClass(),
				"wrong priority adjuster"),
			() -> assertNotNull(queue.getPriorityComparator(), "priority comparator was null"));
	}

	@Test
	void constructorTest_initWithMaxCapacityAndPriorityAdjuster()
	{
		BalancedQueuedElementPriorityAdjuster priorityAdjuster = new BalancedQueuedElementPriorityAdjuster(1);
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10,
			priorityAdjuster);

		assertAll(() -> assertEquals(10, queue.getMaxCapacity(), "wrong maxCapacity"),
			() -> assertEquals(priorityAdjuster, queue.getPriorityAdjuster(), "wrong priority adjuster"),
			() -> assertNotNull(queue.getPriorityComparator(), "priority comparator was null"));
	}

	@Test
	void constructorTest_initWitAllArguments()
	{
		BalancedQueuedElementPriorityAdjuster priorityAdjuster = new BalancedQueuedElementPriorityAdjuster(1);
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10,
			priorityAdjuster);

		assertAll(() -> assertEquals(10, queue.getMaxCapacity(), "wrong maxCapacity"),
			() -> assertEquals(priorityAdjuster, queue.getPriorityAdjuster(), "wrong priority adjuster"),
			() -> assertEquals(PriorityComparator.class, queue.getPriorityComparator().getClass(),
				"wrong priority comparator"));
	}

	@Test
	void setMaxCapasityTest_capacityIsZero_throwsIllegalArgumentException()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(100);
		assertThrows(IllegalArgumentException.class, () -> queue.setMaxCapacity(0));
	}

	@Test
	void setMaxCapasityTest_capacityIsNegative_throwsIllegalArgumentException()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(100);
		assertThrows(IllegalArgumentException.class, () -> queue.setMaxCapacity(-1));
	}

	@Test
	void setMaxCapasityTest_capacityGreaterThanZero_capacityIsSet()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(100);
		queue.setMaxCapacity(50);
		assertEquals(50, queue.getMaxCapacity());
	}

	@Test
	void setPriorityAdjusterTest_priorityAdjusterIsNotNull_priorityAdjusterIsSet()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		QueuedElementPriorityAdjuster priorityAdjuster = new SimpleQueuedElementPriorityAdjuster(1, -1);
		queue.setPriorityAdjuster(priorityAdjuster);
		assertEquals(priorityAdjuster, queue.getPriorityAdjuster());
	}

	@Test
	void setPriorityAdjusterTest_priorityAdjusterIsNull_defaultPriorityAdjusterIsSet()
	{
		QueuedElementPriorityAdjuster priorityAdjuster = new SimpleQueuedElementPriorityAdjuster(1, -1);
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10,
			priorityAdjuster);
		queue.setPriorityAdjuster(null);
		assertNotNull(queue.getPriorityAdjuster());
		assertEquals(DoNothingQueuedElementPriorityAdjuster.class, queue.getPriorityAdjuster().getClass());
	}

	@Test
	void setPriorityComparatorTest_comparatorIsNotNull_comparatorIsSet()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		PriorityComparator priorityComparator = new PriorityComparator();
		queue.setPriorityComparator(priorityComparator);
		assertEquals(priorityComparator, queue.getPriorityComparator());
	}

	@Test
	void setPriorityComparatorTest_comparatorIsNull_comparatorIsSetToNull()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		queue.setPriorityComparator(null);
		assertNull(queue.getPriorityComparator());
	}

	@Test
	void remainingCapacityTest_queueIsEmpty_remainCapacityIsEqualsMaxCapacity()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		assertEquals(10, queue.remainingCapacity());
	}

	@Test
	void remainingCapacityTest_queueContainsElements_remainCapacityIsMaxCapacityMinusQueueSize()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		queue.offer(new TestPriority("Test 1", 0));
		queue.offer(new TestPriority("Test 2", 0));
		assertEquals(8, queue.remainingCapacity());
	}

	@Test
	void remainingCapacityTest_queueIsFull_remainCapacityIsZero()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(2);
		queue.offer(new TestPriority("Test 1", 0));
		queue.offer(new TestPriority("Test 2", 0));
		assertEquals(0, queue.remainingCapacity());
	}

	@Test
	void sizeTest_queueIsEmpty_sizeIsZero()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		assertEquals(0, queue.size());
	}

	@Test
	void sizeTest_queueContainsTwoElements_sizeIsTwo()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		queue.offer(new TestPriority("Test 1", 0));
		queue.offer(new TestPriority("Test 2", 0));
		assertEquals(2, queue.size());
	}

	@Test
	void pollTest_queueIsEmpty_returnNull() throws InterruptedException
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		TestPriority p = queue.poll();
		assertNull(p);
	}

	@Test
	void pollTest_queueIsNotEmpty_returnTheHeadElement() throws InterruptedException
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		TestPriority testPriority = new TestPriority("Test 1", 0);
		assertTrue(queue.offer(testPriority));
		assertTrue(queue.offer(new TestPriority("Test 2", 0)));
		TestPriority p = queue.poll();
		assertEquals(testPriority, p);
	}

	@Test
	void pollWithTimeoutTest_queueIsEmpty_returnNull() throws InterruptedException
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		long startTime = System.currentTimeMillis();
		TestPriority p = queue.poll(500, TimeUnit.MILLISECONDS);
		long delta = System.currentTimeMillis() - startTime;
		assertAll(() -> assertNull(p), //
			() -> assertTrue(delta >= 450, "poll does not wait"));
	}

	@Test
	void pollWithTimeoutTest_queueIsEmptyNewElementIsAddedBeforeTimeout_returnTheElement() throws InterruptedException
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		TestPriority testPriority = new TestPriority("test", 0);
		TestRunnableRunner runnable = new TestRunnableRunner(() -> queue.add(testPriority), 200);
		Thread t = new Thread(runnable);
		t.start();

		TestPriority p = queue.poll(500, TimeUnit.MILLISECONDS);
		assertEquals(testPriority, p);
	}

	@Test
	void takeTest_queueIsEmptyNewElementIsAdded_returnTheElement() throws InterruptedException
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		TestPriority testPriority = new TestPriority("test", 0);
		TestRunnableRunner runnable = new TestRunnableRunner(() -> queue.add(testPriority), 200);
		Thread t = new Thread(runnable);
		long startTime = System.currentTimeMillis();
		t.start();

		TestPriority p = queue.take();
		long delta = System.currentTimeMillis() - startTime;
		assertAll(() -> assertEquals(testPriority, p, "wrong object"), //
			() -> assertTrue(delta >= 150, "take does not wait"));
	}

	@Test
	void takeTest_queueIsNotEmpty_returnTheFirstElement() throws InterruptedException
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		TestPriority testPriority = new TestPriority("Test 1", 0);
		queue.offer(testPriority);
		queue.offer(new TestPriority("Test 2", 0));

		TestPriority p = queue.take();
		assertEquals(testPriority, p);
		p = queue.take();
		assertNotNull(p);
		assertEquals("Test 2", p.getName());
		assertEquals(0, queue.size(), 0);
	}

	@Test
	void peekTest_queueIsEmpty_returnNull()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);

		assertNull(queue.peek());
	}

	@Test
	void peekTest_queueIsNotEmpty_returnTheFirstElement()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		assertEquals(0, queue.size(), 0);
		TestPriority p1 = new TestPriority("Test 1", 0);
		TestPriority p2 = new TestPriority("Test 2", 0);
		queue.offer(p1);
		queue.offer(p2);

		assertEquals(2, queue.size(), "wrong size");
		assertEquals(p1, queue.peek());
		assertEquals(2, queue.size(), "wrong size");
		assertEquals(p1, queue.peek());
	}

	@Test
	void offerTest_queueHasSpace_elementIsAdded()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(5);
		assertTrue(queue.offer(new TestPriority("Test 1", 0)));
		assertTrue(queue.offer(new TestPriority("Test 2", 0)));
	}

	@Test
	void offerTest_queueHasNoSpace_elementIsNotAdded()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(1);
		assertTrue(queue.offer(new TestPriority("Test 1", 0)));
		assertFalse(queue.offer(new TestPriority("Test 2", 0)));
	}

	@Test
	void offerTest_withTimeout_elementIsAdded() throws InterruptedException
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		queue.offer(new TestPriority("Test 1", 0));

		TestRunnableRunner runnable = new TestRunnableRunner(() -> queue.poll(), 200);
		Thread t = new Thread(runnable);
		t.start();

		assertTrue(queue.offer(new TestPriority("Test 2", 0), 500, TimeUnit.MILLISECONDS));
	}

	@Test
	void offerTest_withTimeout_elementIsNotAdded() throws InterruptedException
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(1);
		queue.offer(new TestPriority("Test 1", 0));

		assertFalse(queue.offer(new TestPriority("Test 2", 0), 100, TimeUnit.MILLISECONDS));
	}

	@Test
	void offerTest_addNull_exceptionIsThrown()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(5);

		assertThrows(NullPointerException.class, () -> queue.offer(null));
	}

	@Test
	void offerTest_addNullWithTimeout_exceptionIsThrown()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(5);

		assertThrows(NullPointerException.class, () -> queue.offer(null, 10, TimeUnit.MILLISECONDS));
	}

	@Test
	void putTest_queueIsEmpty_elementIsAdded() throws InterruptedException
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		queue.put(new TestPriority("Test 1", 0));
		assertEquals(1, queue.size(), "wrong size");
	}

	@Test
	void putTest_queueIsNotEmptyWithPriority_elementIsAddedAsFirstElement() throws InterruptedException
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		queue.put(new TestPriority("Test 1", 0));

		TestPriority testPriority = new TestPriority("Test 2", 10);
		queue.put(testPriority);

		assertAll(() -> assertEquals(2, queue.size(), "wrong size"), () -> assertEquals(testPriority, queue.peek()));
	}

	@Test
	void putTest_queueIsNotEmptyWithPriorityAndPriorityAdjuster_elementIsAddedAsFirstElementAndPriorityIsAdjusted()
		throws InterruptedException
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10,
			new BalancedQueuedElementPriorityAdjuster(2));
		queue.put(new TestPriority("Test 1", 2));
		queue.put(new TestPriority("Test 2", 0));

		TestPriority testPriority = new TestPriority("Test 3", 10);
		queue.put(testPriority);

		assertAll(() -> assertEquals(3, queue.size(), "wrong size"),
			() -> assertEquals(new TestPriority("Test 3", 6), queue.poll()),
			() -> assertEquals(new TestPriority("Test 1", 4), queue.poll()),
			() -> assertEquals(new TestPriority("Test 2", 2), queue.poll()));
	}

	@Test
	void putTest_queueIsNotEmptyWithPriorityAndPriorityAdjuster_elementIsAddedAsSecondElementAndPriorityIsAdjusted()
		throws InterruptedException
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10,
			new BalancedQueuedElementPriorityAdjuster(2));
		queue.put(new TestPriority("Test 1", 8));
		queue.put(new TestPriority("Test 2", 0));

		TestPriority testPriority = new TestPriority("Test 3", 10);
		queue.put(testPriority);

		assertAll(() -> assertEquals(3, queue.size(), "wrong size"),
			() -> assertEquals(new TestPriority("Test 1", 8), queue.poll()),
			() -> assertEquals(new TestPriority("Test 3", 8), queue.poll()),
			() -> assertEquals(new TestPriority("Test 2", 2), queue.poll()));
	}

	@Test
	void putTest_queueIsNotEmptyWithPriorityAdjusterAndNoPriorityComparator_elementIsAddedAsLastElement()
		throws InterruptedException
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10,
			new BalancedQueuedElementPriorityAdjuster(2), null);
		queue.put(new TestPriority("Test 1", 2));
		queue.put(new TestPriority("Test 2", 0));

		TestPriority testPriority = new TestPriority("Test 3", 10);
		queue.put(testPriority);

		assertAll(() -> assertEquals(3, queue.size(), "wrong size"),
			() -> assertEquals(new TestPriority("Test 1", 2), queue.poll()),
			() -> assertEquals(new TestPriority("Test 2", 0), queue.poll()),
			() -> assertEquals(new TestPriority("Test 3", 10), queue.poll()));
	}

	@Test
	void putTest_queueHasNoSpace_waitForSpaceAndAddElement() throws InterruptedException
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(1);
		queue.put(new TestPriority("Test 1", 0));
		TestRunnableRunner runnable = new TestRunnableRunner(() -> queue.poll(), 200);
		Thread t = new Thread(runnable);
		t.start();

		queue.put(new TestPriority("Test 1", 0));
		assertEquals(1, queue.size(), "wrong size");
	}

	@Test
	void putTest_addNullWithTimeout_exceptionIsThrown() throws InterruptedException
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);

		assertThrows(NullPointerException.class, () -> queue.put(null));
	}

	@Test
	void drainToTest_queueHasNoElements_collectionIsEmpty()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);

		Collection<TestPriority> col = new ArrayList<>();
		queue.drainTo(col);

		assertEquals(0, col.size(), "collection has wrong size");
	}

	@Test
	void drainToTest_queueHasElements_collectionContainsThisElements()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		queue.offer(new TestPriority("Test 1", 0));
		queue.offer(new TestPriority("Test 2", 0));
		queue.offer(new TestPriority("Test 3", 0));
		queue.offer(new TestPriority("Test 4", 0));
		queue.offer(new TestPriority("Test 5", 0));

		ArrayList<TestPriority> col = new ArrayList<>();

		assertEquals(5, queue.drainTo(col), "wrong drainTo result");
		assertEquals(0, queue.size(), "queue has wrong size");
		assertEquals(5, col.size(), "collection has wrong size");
		for (int i = 1; i < 6; i++)
		{
			TestPriority tp = col.get(i - 1);
			assertEquals("Test " + i, tp.getName());
		}
	}

	@Test
	void drainToTest_queueHasElementsLimitToThree_collectionContainsOnlyThreeElements()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		queue.offer(new TestPriority("Test 1", 0));
		queue.offer(new TestPriority("Test 2", 0));
		queue.offer(new TestPriority("Test 3", 0));
		queue.offer(new TestPriority("Test 4", 0));
		queue.offer(new TestPriority("Test 5", 0));

		ArrayList<TestPriority> col = new ArrayList<>();

		assertEquals(3, queue.drainTo(col, 3), "wrong drainTo result");
		assertEquals(2, queue.size(), "queue has wrong size");
		assertEquals(3, col.size(), "collection has wrong size");
		for (int i = 1; i < 4; i++)
		{
			TestPriority tp = col.get(i - 1);
			assertEquals("Test " + i, tp.getName());
		}
	}

	@Test
	void drainToTest_maxElementsIsZero_noElementsAreAdded()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		queue.offer(new TestPriority("Test 1", 0));

		ArrayList<TestPriority> col = new ArrayList<>();

		assertEquals(0, queue.drainTo(col, 0), "wrong drainTo result");
		assertEquals(1, queue.size(), "queue has wrong size");
		assertEquals(0, col.size(), "collection has wrong size");
	}

	@Test
	void drainToTest_collectionIsNull_throwsException()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);

		assertThrows(NullPointerException.class, () -> queue.drainTo(null));
	}

	@Test
	void drainToTest_collectionIsTheQueue_throwsException()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);

		assertThrows(IllegalArgumentException.class, () -> queue.drainTo(queue));
	}

	@Test
	void toArrayTest()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		queue.offer(new TestPriority("Test 1", 0));
		queue.offer(new TestPriority("Test 2", 0));
		queue.offer(new TestPriority("Test 3", 0));
		queue.offer(new TestPriority("Test 4", 0));
		queue.offer(new TestPriority("Test 5", 0));
		assertEquals(5, queue.size(), 0);

		Object[] array = queue.toArray();
		assertAll(() -> assertNotNull(array), //
			() -> assertEquals(5, array.length), //
			() -> assertEquals("Test 1", ((TestPriority) array[0]).getName()), //
			() -> assertEquals("Test 2", ((TestPriority) array[1]).getName()), //
			() -> assertEquals("Test 3", ((TestPriority) array[2]).getName()), //
			() -> assertEquals("Test 4", ((TestPriority) array[3]).getName()), //
			() -> assertEquals("Test 5", ((TestPriority) array[4]).getName()));
	}

	@Test
	void toStringTest()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		queue.offer(new TestPriority("Test 1", 0));
		queue.offer(new TestPriority("Test 2", 0));

		String s = queue.toString();
		assertAll(() -> assertTrue(s.startsWith("BoundedPriorityBlockingQueue [")), () -> assertTrue(s.endsWith("]")), //
			() -> assertTrue(s.contains("Test 1")), //
			() -> assertTrue(s.contains("Test 2")));
	}

	@Test
	void iteratorHasNextTest_queueIsEmpty_returnFalse()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		Iterator<TestPriority> iter = queue.iterator();

		assertFalse(iter.hasNext());
	}

	@Test
	void iteratorHasNextTest_queueIsNotEmpty_returnTrue()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		queue.offer(new TestPriority("Test 1", 0));
		Iterator<TestPriority> iter = queue.iterator();

		assertTrue(iter.hasNext());
	}

	@Test
	void iteratorNextTest_queueIsEmpty_throwsException()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		Iterator<TestPriority> iter = queue.iterator();

		assertThrows(NoSuchElementException.class, () -> iter.next());
	}

	@Test
	void iteratorNextTest_queueIsNotEmpty_returnTheElements()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		queue.offer(new TestPriority("Test 1", 0));
		queue.offer(new TestPriority("Test 2", 0));
		Iterator<TestPriority> iter = queue.iterator();

		assertAll(() -> assertTrue(iter.hasNext(), "no next element"),
			() -> assertEquals("Test 1", iter.next().getName(), "Name was not 'Test 1'"),
			() -> assertTrue(iter.hasNext(), "no next element"),
			() -> assertEquals("Test 2", iter.next().getName(), "Name was not 'Test 2'"),
			() -> assertFalse(iter.hasNext(), "has next element"));
	}

	@Test
	void iteratorRemoveTest_nextWasNotCalled_throwsException()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		queue.offer(new TestPriority("Test 1", 0));
		Iterator<TestPriority> iter = queue.iterator();

		assertThrows(IllegalStateException.class, () -> iter.remove());
	}

	@Test
	void iteratorRemoveTest_removeWasCalledTwice_throwsException()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		queue.offer(new TestPriority("Test 1", 0));
		queue.offer(new TestPriority("Test 2", 0));
		Iterator<TestPriority> iter = queue.iterator();
		iter.next();
		iter.remove();

		assertThrows(IllegalStateException.class, () -> iter.remove());
	}

	@Test
	void iteratorRemoveTest_queueIsNotEmpty_removeTheFirstElement()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		queue.offer(new TestPriority("Test 1", 0));
		queue.offer(new TestPriority("Test 2", 0));
		Iterator<TestPriority> iter = queue.iterator();
		iter.next();
		iter.remove();

		assertAll(() -> assertEquals(1, queue.size(), "wrong size"),
			() -> assertEquals("Test 2", queue.poll().getName()));
	}

	@Test
	void iteratorRemoveTest_queueIsNotEmpty_removeTheLastElement()
	{
		BoundedPriorityBlockingQueue<TestPriority> queue = new BoundedPriorityBlockingQueue<TestPriority>(10);
		queue.offer(new TestPriority("Test 1", 0));
		queue.offer(new TestPriority("Test 2", 0));
		Iterator<TestPriority> iter = queue.iterator();
		iter.next();
		iter.next();
		iter.remove();

		assertAll(() -> assertEquals(1, queue.size(), "wrong size"),
			() -> assertEquals("Test 1", queue.poll().getName()));
	}

	private class TestRunnableRunner implements Runnable
	{
		private Runnable runnable;
		private long waitBefore;
		private long waitAfter;

		private TestRunnableRunner(Runnable runnable, long waitBefore)
		{
			this(runnable, waitBefore, -1);
		}

		private TestRunnableRunner(Runnable runnable, long waitBefore, long waitAfter)
		{
			this.runnable = runnable;
			this.waitBefore = waitBefore;
			this.waitAfter = waitAfter;
		}

		@Override
		public void run()
		{
			sleep(waitBefore);
			runnable.run();
			sleep(waitAfter);
		}

		private void sleep(long time)
		{
			if (time <= 0)
			{
				return;
			}
			try
			{
				synchronized (this)
				{
					wait(time);
				}
			}
			catch (InterruptedException e)
			{}
		}
	}
}
