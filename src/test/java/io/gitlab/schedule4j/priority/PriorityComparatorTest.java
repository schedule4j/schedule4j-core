/*
 * Created on 24.09.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.priority;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import io.gitlab.schedule4j.priority.PriorityComparator.SortOrder;

/**
 * @author Dirk Buchhorn
 */
public class PriorityComparatorTest
{
	@Test
	void compareTest()
	{
		PriorityComparator comparator = new PriorityComparator(SortOrder.ASC);
		Priority p1 = new TestPriority("Test 1", 10);
		Priority p2 = new TestPriority("Test 2", 20);
		Priority p3 = new TestPriority("Test 3", 10);
		Priority p4 = new TestPriority("Test 4", 40);

		assertTrue(comparator.compare(null, null) == 0);
		assertTrue(comparator.compare(null, p2) > 0);
		assertTrue(comparator.compare(p1, null) < 0);
		assertTrue(comparator.compare(p1, p2) < 0);
		assertTrue(comparator.compare(p2, p3) > 0);
		assertTrue(comparator.compare(p1, p3) == 0);

		List<Priority> list = new ArrayList<Priority>();
		list.add(p4);
		list.add(p1);
		list.add(p2);
		list.add(p3);
		list.sort(comparator);
		assertEquals(10, list.get(0).getPriority());
		assertEquals(10, list.get(1).getPriority());
		assertEquals(20, list.get(2).getPriority());
		assertEquals(40, list.get(3).getPriority());

		comparator = new PriorityComparator(SortOrder.DESC);
		list.sort(comparator);
		assertEquals(40, list.get(0).getPriority());
		assertEquals(20, list.get(1).getPriority());
		assertEquals(10, list.get(2).getPriority());
		assertEquals(10, list.get(3).getPriority());

		// needed for get 100% test coverage
		SortOrder.valueOf("ASC");
		SortOrder.valueOf("DESC");
	}
}
