/*
 * Created on 24.09.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.priority;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * @author Dirk Buchhorn
 */
public class SimpleQueuePriorityAdjusterTest
{
	@Test
	void getterTest()
	{
		SimpleQueuedElementPriorityAdjuster priorityAdjuster = new SimpleQueuedElementPriorityAdjuster(1, -1);
		assertEquals(1, priorityAdjuster.getLowPriorityAdjustment(), 0);
		assertEquals(-1, priorityAdjuster.getHighPriorityAdjustment(), 0);
	}

	@Test
	void adjustPriorityTest()
	{
		SimpleQueuedElementPriorityAdjuster priorityAdjuster = new SimpleQueuedElementPriorityAdjuster(1, -1);
		TestPriority lowPriority = new TestPriority("Test 1", 10);
		TestPriority highPriority = new TestPriority("Test 2", 20);
		priorityAdjuster.adjustPriority(lowPriority, highPriority);
		assertEquals(11, lowPriority.getPriority());
		assertEquals(19, highPriority.getPriority());
	}
}
