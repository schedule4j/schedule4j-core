/*
 * Created on 31.10.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * @author Dirk Buchhorn
 */
public class ScheduledJobStatisticsTest
{
	@Test
	void constructorTest()
	{
		ScheduledJobStatistics sjs = new ScheduledJobStatistics();
		assertEquals(0, sjs.getWaitingCount());
		assertEquals(0, sjs.getRunningCount());
		assertEquals(0, sjs.getExecutionCount());

		sjs = new ScheduledJobStatistics(1, 2, 3);
		assertEquals(1, sjs.getWaitingCount());
		assertEquals(2, sjs.getRunningCount());
		assertEquals(3, sjs.getExecutionCount());
	}

	@Test
	void jobWaitingTest()
	{
		ScheduledJobStatistics sjs = new ScheduledJobStatistics();
		sjs = sjs.jobWaiting();
		assertEquals(1, sjs.getWaitingCount());
		assertEquals(0, sjs.getRunningCount());
		assertEquals(0, sjs.getExecutionCount());
		sjs = sjs.jobWaiting();
		assertEquals(2, sjs.getWaitingCount());
		assertEquals(0, sjs.getRunningCount());
		assertEquals(0, sjs.getExecutionCount());
	}

	@Test
	void jobRunningTest()
	{
		ScheduledJobStatistics sjs = new ScheduledJobStatistics();
		sjs = sjs.jobWaiting();
		sjs = sjs.jobWaiting();
		sjs = sjs.jobRunning();
		assertEquals(1, sjs.getWaitingCount());
		assertEquals(1, sjs.getRunningCount());
		assertEquals(0, sjs.getExecutionCount());
		sjs = sjs.jobRunning();
		assertEquals(0, sjs.getWaitingCount());
		assertEquals(2, sjs.getRunningCount());
		assertEquals(0, sjs.getExecutionCount());
		sjs = sjs.jobRunning();
		assertEquals(0, sjs.getWaitingCount());
		assertEquals(3, sjs.getRunningCount());
		assertEquals(0, sjs.getExecutionCount());
	}

	@Test
	void jobExecutedTest()
	{
		ScheduledJobStatistics sjs = new ScheduledJobStatistics();
		sjs = sjs.jobWaiting();
		sjs = sjs.jobRunning();
		sjs = sjs.jobRunning();
		sjs = sjs.jobExecuted();
		assertEquals(0, sjs.getWaitingCount());
		assertEquals(1, sjs.getRunningCount());
		assertEquals(1, sjs.getExecutionCount());
		sjs = sjs.jobExecuted();
		assertEquals(0, sjs.getWaitingCount());
		assertEquals(0, sjs.getRunningCount());
		assertEquals(2, sjs.getExecutionCount());
		sjs = sjs.jobExecuted();
		assertEquals(0, sjs.getWaitingCount());
		assertEquals(0, sjs.getRunningCount());
		assertEquals(3, sjs.getExecutionCount());
	}

	@Test
	void jobNotExecutedTest()
	{
		ScheduledJobStatistics sjs = new ScheduledJobStatistics();
		sjs = sjs.jobWaiting();
		sjs = sjs.jobWaiting();
		sjs = sjs.jobNotExecuted();
		assertEquals(1, sjs.getWaitingCount());
		assertEquals(0, sjs.getRunningCount());
		assertEquals(0, sjs.getExecutionCount());
		sjs = sjs.jobNotExecuted();
		assertEquals(0, sjs.getWaitingCount());
		assertEquals(0, sjs.getRunningCount());
		assertEquals(0, sjs.getExecutionCount());
		sjs = sjs.jobNotExecuted();
		assertEquals(0, sjs.getWaitingCount());
		assertEquals(0, sjs.getRunningCount());
		assertEquals(0, sjs.getExecutionCount());
	}

	@Test
	void toStringTest()
	{
		ScheduledJobStatistics sjs = new ScheduledJobStatistics();
		assertTrue(sjs.toString().startsWith("ScheduledJobStatistics ("));
	}
}
