/*
 * Created on 23.10.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.schedule;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Optional;
import java.util.TreeMap;
import java.util.concurrent.locks.ReentrantLock;

import io.gitlab.schedule4j.ScheduledJob;
import io.gitlab.schedule4j.Scheduler;
import io.gitlab.schedule4j.common.KeyValueStore;
import io.gitlab.schedule4j.job.JobKey;

/**
 * A {@link Scheduler} stores the next {@link ScheduleEvent}s in a schedule object. The schedule can
 * only store one {@link ScheduleEvent} for a {@link ScheduledJob}.
 * 
 * @author Dirk Buchhorn
 */
public class Schedule
{
	private ReentrantLock lock = new ReentrantLock(true);
	private NavigableMap<ZonedDateTime, List<ScheduleEvent>> timeToJobMap = new TreeMap<>();
	private KeyValueStore<JobKey, ZonedDateTime> jobToTimeMap = new KeyValueStore<>();

	/**
	 * Gets the schedule event count.
	 * 
	 * @return the schedule event count
	 */
	public int count()
	{
		lock.lock();
		try
		{
			return jobToTimeMap.size();
		}
		finally
		{
			lock.unlock();
		}
	}

	/**
	 * Removes all {@link ScheduleEvent}s from this schedule.
	 */
	public void clear()
	{
		lock.lock();
		try
		{
			timeToJobMap.clear();
			jobToTimeMap.clear();
		}
		finally
		{
			lock.unlock();
		}
	}

	/**
	 * Return <code>true</code> if this schedule contains a {@link ScheduleEvent} for the given
	 * {@link JobKey}, <code>false</code> otherwise.
	 * 
	 * @param jobKey the {@link JobKey}
	 * @return <code>true</code> if this schedule contains a {@link ScheduleEvent} for the given
	 *         {@link JobKey} , <code>false</code> otherwise
	 */
	public boolean contains(JobKey jobKey)
	{
		lock.lock();
		try
		{
			return jobToTimeMap.containsKey(jobKey);
		}
		finally
		{
			lock.unlock();
		}
	}

	/**
	 * Returns the {@link ScheduleEvent} for the given {@link JobKey}.
	 * 
	 * @param jobKey the {@link JobKey}
	 * @return the {@link ScheduleEvent} for the given {@link JobKey}
	 */
	public Optional<ScheduleEvent> getScheduleEvent(JobKey jobKey)
	{
		lock.lock();
		try
		{
			if (jobToTimeMap.containsKey(jobKey))
			{
				ZonedDateTime currentScheduleTime = jobToTimeMap.get(jobKey);
				List<ScheduleEvent> list = timeToJobMap.get(currentScheduleTime);
				return list.stream().filter(e -> e.getJobKey().equals(jobKey)).findFirst();
			}
		}
		finally
		{
			lock.unlock();
		}
		return Optional.empty();
	}

	/**
	 * Returns the first (smallest) schedule time.
	 * 
	 * @return the first schedule time
	 */
	public Optional<ZonedDateTime> getFirstScheduleTime()
	{
		lock.lock();
		try
		{
			return timeToJobMap.size() > 0 ? Optional.of(timeToJobMap.firstKey())
				: Optional.empty();
		}
		finally
		{
			lock.unlock();
		}
	}

	/**
	 * Adds a {@link ScheduleEvent} to this schedule. If the schedule contains already a
	 * {@link ScheduleEvent} for the given {@link JobKey} then nothing is added and
	 * <code>false</code> is returned.
	 * 
	 * @param event the {@link ScheduleEvent} to add
	 * @return <code>true</code> if the {@link ScheduleEvent} could be added, <code>false</code>
	 *         otherwise
	 */
	public boolean add(ScheduleEvent event)
	{
		lock.lock();
		try
		{
			if (jobToTimeMap.add(event.getJobKey(), event.getScheduleTime()))
			{
				List<ScheduleEvent> list = timeToJobMap.get(event.getScheduleTime());
				if (list == null)
				{
					list = new ArrayList<>(4);
					timeToJobMap.put(event.getScheduleTime(), list);
				}
				list.add(event);
				return true;
			}
		}
		finally
		{
			lock.unlock();
		}
		return false;
	}

	/**
	 * Replace a {@link ScheduleEvent} with the given one. If no {@link ScheduleEvent} is found for
	 * the given JobKey then nothing is replaced or added and <code>false</code> is returned.
	 * 
	 * @param event the {@link ScheduleEvent} to replace the existing one
	 * @return <code>true</code> if a {@link ScheduleEvent} could be replaced, <code>false</code>
	 *         otherwise
	 */
	public boolean replace(ScheduleEvent event)
	{
		lock.lock();
		try
		{
			if (remove(event.getJobKey()))
			{
				return add(event);
			}
		}
		finally
		{
			lock.unlock();
		}
		return false;
	}

	/**
	 * Search the old {@link ScheduleEvent} and replace it with the new ScheduleEvent. If the old
	 * ScheduleEvent could not be found then nothing is replaced and <code>false</code> is returned.
	 * 
	 * @param oldEvent the old {@link ScheduleEvent}
	 * @param newEvent the new {@link ScheduleEvent}
	 * @return <code>true</code> if the old {@link ScheduleEvent} could be replaced with the new
	 *         {@link ScheduleEvent}, <code>false</code> otherwise
	 */
	public boolean replace(ScheduleEvent oldEvent, ScheduleEvent newEvent)
	{
		lock.lock();
		try
		{
			if (!oldEvent.getJobKey().equals(newEvent.getJobKey()))
			{
				return false;
			}
			if (remove(oldEvent))
			{
				add(newEvent);
				return true;
			}
		}
		finally
		{
			lock.unlock();
		}
		return false;
	}

	/**
	 * Remove a {@link ScheduleEvent} for the given {@link JobKey}.
	 * 
	 * @param jobKey the {@link JobKey} to remove the {@link ScheduleEvent}
	 * @return <code>true</code> if a ScheduleEvent was found and removed, <code>false</code>
	 *         otherwise
	 */
	public boolean remove(JobKey jobKey)
	{
		lock.lock();
		try
		{
			ZonedDateTime currentScheduleTime = jobToTimeMap.remove(jobKey);
			if (currentScheduleTime != null)
			{
				List<ScheduleEvent> list = timeToJobMap.get(currentScheduleTime);
				// remove the event
				list.removeIf(e -> e.getJobKey().equals(jobKey));
				if (list.isEmpty())
				{
					timeToJobMap.remove(currentScheduleTime);
				}
				return true;
			}
		}
		finally
		{
			lock.unlock();
		}
		return false;
	}

	/**
	 * Removed the given {@link ScheduleEvent}.
	 * 
	 * @param event the {@link ScheduleEvent} to remove
	 * @return <code>true</code> if a ScheduleEvent was found and removed, <code>false</code>
	 *         otherwise
	 */
	public boolean remove(ScheduleEvent event)
	{
		lock.lock();
		try
		{
			JobKey jobKey = event.getJobKey();
			if (jobToTimeMap.containsKey(jobKey))
			{
				ZonedDateTime currentScheduleTime = jobToTimeMap.get(jobKey);
				if (currentScheduleTime.equals(event.getScheduleTime()))
				{
					List<ScheduleEvent> list = timeToJobMap.get(currentScheduleTime);
					// remove the event
					if (list.removeIf(e -> e.equals(event)))
					{
						jobToTimeMap.remove(jobKey);
						if (list.isEmpty())
						{
							timeToJobMap.remove(currentScheduleTime);
						}
					}
					return true;
				}
			}
		}
		finally
		{
			lock.unlock();
		}
		return false;
	}

	/**
	 * Returns all {@link ScheduleEvent}s where the scheduleTime is less or equals to the given
	 * time.
	 * 
	 * @param time the time to search {@link ScheduleEvent}s
	 * @return all {@link ScheduleEvent}s where the scheduleTime is less or equals to the given time
	 */
	public List<ScheduleEvent> getScheduleEvents(ZonedDateTime time)
	{
		lock.lock();
		try
		{
			List<ScheduleEvent> events = new ArrayList<>();
			timeToJobMap.headMap(time, true).values().forEach(events::addAll);
			return events;
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public String toString()
	{
		lock.lock();
		try
		{
			StringBuilder sb = new StringBuilder("Schedule (");
			int index = 0;
			for (Entry<ZonedDateTime, List<ScheduleEvent>> entry : timeToJobMap.entrySet())
			{
				if (index > 0)
				{
					sb.append("); ");
				}
				sb.append(entry.getKey().format(DateTimeFormatter.ISO_DATE_TIME)).append(" [");
				List<ScheduleEvent> events = entry.getValue();
				for (int i = 0; i < events.size(); i++)
				{
					if (i > 0)
					{
						sb.append(", ");
					}
					sb.append(events.get(i).getJobKey());
				}
				sb.append("]");
				index++;
			}
			sb.append(")");
			return sb.toString();
		}
		finally
		{
			lock.unlock();
		}
	}
}
