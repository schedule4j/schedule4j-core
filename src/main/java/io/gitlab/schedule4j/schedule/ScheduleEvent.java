/*
 * Created on 16.07.2017
 * 
 * Copyright 2017 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.schedule;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import io.gitlab.schedule4j.ScheduledJob;
import io.gitlab.schedule4j.job.JobKey;
import io.gitlab.schedule4j.trigger.TriggerEvent;

/**
 * A {@link ScheduleEvent} is normally generated from a TriggerEvent and associated with a
 * {@link ScheduledJob}.
 * 
 * @author Dirk Buchhorn
 */
public class ScheduleEvent
{
	private final ScheduledJob scheduledJob;
	private final TriggerEvent triggerEvent;

	/**
	 * Creates a new {@link ScheduleEvent}.
	 * 
	 * @param scheduledJob the {@link ScheduledJob}
	 * @param triggerEvent the {@link TriggerEvent}
	 */
	public ScheduleEvent(ScheduledJob scheduledJob, TriggerEvent triggerEvent)
	{
		if (scheduledJob == null)
		{
			throw new IllegalArgumentException("scheduledJob can't be null");
		}
		if (triggerEvent == null)
		{
			throw new IllegalArgumentException("triggerEvent can't be null");
		}
		this.scheduledJob = scheduledJob;
		this.triggerEvent = triggerEvent;
	}

	/**
	 * Gets the {@link JobKey} of the {@link ScheduledJob}.
	 * 
	 * @return the {@link JobKey} of the {@link ScheduledJob}
	 */
	public JobKey getJobKey()
	{
		return scheduledJob.getJobKey();
	}

	/**
	 * Gets the schedule time. The fire time from the {@link TriggerEvent} is used as schedule time.
	 * 
	 * @return the schedule time
	 */
	public ZonedDateTime getScheduleTime()
	{
		return triggerEvent.getFireTime();
	}

	/**
	 * Gets the {@link ScheduledJob}.
	 * 
	 * @return the {@link ScheduledJob}
	 */
	public ScheduledJob getScheduledJob()
	{
		return scheduledJob;
	}

	/**
	 * Gets the {@link TriggerEvent}.
	 * 
	 * @return the {@link TriggerEvent}
	 */
	public TriggerEvent getTriggerEvent()
	{
		return triggerEvent;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + triggerEvent.hashCode();
		result = prime * result + getJobKey().hashCode();
		result = prime * result + getScheduleTime().hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		ScheduleEvent other = (ScheduleEvent) obj;
		if (!getJobKey().equals(other.getJobKey()))
		{
			return false;
		}
		return triggerEvent.equals(other.triggerEvent);
	}

	@Override
	public String toString()
	{
		return getJobKey() + " (" + getScheduleTime().format(DateTimeFormatter.ISO_DATE_TIME) + ")";
	}
}
