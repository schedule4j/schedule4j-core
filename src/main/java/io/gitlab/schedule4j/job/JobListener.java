/*
 * Created on 16.11.2015
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.job;

import io.gitlab.schedule4j.Scheduler;
import io.gitlab.schedule4j.execute.JobExecutionListener;

/**
 * A JobListener is called from the {@link Scheduler}. All JobListeners must be thread save.<br>
 * The three possible calling orders are:<br>
 * vetoJobExecution, jobRunning, jobExecuted<br>
 * vetoJobExecution, jobExecutionVetoed<br>
 * jobNotExecuted
 * 
 * @author Dirk Buchhorn
 */
public interface JobListener extends JobExecutionListener
{
	/**
	 * Called if a {@link Job} is waiting for execution.
	 * 
	 * @param ctx the {@link JobExecutionContext}
	 */
	void jobWaiting(JobExecutionContext ctx);

	/**
	 * A listener can veto the job execution. If one listener return <code>true</code> then no other listener
	 * is called.
	 * 
	 * @param ctx the {@link JobExecutionContext}
	 * @return <code>true</code> if the job should not be executed, <code>false</code> otherwise
	 */
	boolean vetoJobExecution(JobExecutionContext ctx);

	/**
	 * A job execution was vetoed.
	 * 
	 * @param ctx the {@link JobExecutionContext}
	 */
	void jobExecutionVetoed(JobExecutionContext ctx);
}
