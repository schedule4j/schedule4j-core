/*
 * Created on 09.10.2015
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.job;

import io.gitlab.schedule4j.common.Key;

/**
 * @author Dirk Buchhorn
 */
public final class JobKey extends Key<JobKey>
{
	private static final long serialVersionUID = 3088511576589094584L;

	/**
	 * Construct a new key with the given name.
	 * 
	 * @param name the key name
	 */
	public JobKey(String name)
	{
		super(name);
	}

	/**
	 * Construct a new key with the given name and parent key.
	 * 
	 * @param parentKey the parent key
	 * @param name the key name
	 */
	public JobKey(JobKey parentKey, String name)
	{
		super(parentKey, name);
	}

	/**
	 * Creates a new key with the given name and parent key.
	 * 
	 * @param parentKey the parent key
	 * @param name the key name
	 * @return the created key
	 */
	public static JobKey from(JobKey parentKey, String name)
	{
		return new JobKey(parentKey, name);
	}

	/**
	 * Creates a new key with the given names. The last name is the name of the created key. All
	 * other names are the names of the parent keys.
	 * 
	 * @param keyNames the key names
	 * @return the created key
	 */
	public static JobKey from(String... keyNames)
	{
		JobKey key = null;
		if (keyNames != null && keyNames.length > 0)
		{
			for (int i = 0; i < keyNames.length; i++)
			{
				key = new JobKey(key, keyNames[i]);
			}
		}
		return key;
	}
}
