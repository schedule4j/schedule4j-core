/*
 * Created on 2020-12-16
 * 
 * Copyright 2020 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.job;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Dirk Buchhorn
 */
public class JobListenerNotifier
{
	private static final Logger log = LoggerFactory.getLogger(JobListenerNotifier.class);

	private Set<JobListener> jobListeners = new HashSet<>();

	public int getSize()
	{
		return jobListeners.size();
	}

	public void addJobListener(JobListener listener)
	{
		if (listener == null)
		{
			return;
		}
		synchronized (jobListeners)
		{
			jobListeners.add(listener);
		}
	}

	public boolean removeJobListener(JobListener listener)
	{
		if (listener == null)
		{
			return false;
		}
		synchronized (jobListeners)
		{
			return jobListeners.remove(listener);
		}
	}

	private Collection<JobListener> getJobListeners()
	{
		// copy the collection in a new one to prevent a deadlock
		synchronized (jobListeners)
		{
			return new ArrayList<>(jobListeners);
		}
	}

	public void fireJobNotExecuted(JobExecutionContext ctx, Exception exception)
	{
		callListeners(jl -> jl.jobNotExecuted(ctx, exception));
	}

	public void fireJobWaiting(JobExecutionContext ctx)
	{
		callListeners(jl -> jl.jobWaiting(ctx));
	}

	public boolean fireVetoJobExecution(JobExecutionContext ctx)
	{
		final boolean[] veto = new boolean[] {false};
		callListeners(jl ->
		{
			if (jl.vetoJobExecution(ctx))
			{
				veto[0] = true;
				throw new StopCallListenersException();
			}
		});
		return veto[0];
	}

	public void fireJobExecutionVetoed(JobExecutionContext ctx)
	{
		callListeners(jl -> jl.jobExecutionVetoed(ctx));
	}

	public void fireJobRunning(JobExecutionContext ctx)
	{
		callListeners(jl -> jl.jobRunning(ctx));
	}

	public void fireJobExecuted(JobExecutionContext ctx, Exception exception)
	{
		callListeners(jl -> jl.jobExecuted(ctx, exception));
	}

	private void callListeners(Callable callable)
	{
		for (JobListener jobListener : getJobListeners())
		{
			// call it in a save way, because we don't know what the called method do
			try
			{
				callable.callFunction(jobListener);
			}
			catch (StopCallListenersException e)
			{
				break;
			}
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
			}
		}
	}

	@FunctionalInterface
	private interface Callable
	{
		void callFunction(JobListener jobListener);
	}

	private class StopCallListenersException extends RuntimeException
	{
		private static final long serialVersionUID = 1544053317148862464L;
	}
}
