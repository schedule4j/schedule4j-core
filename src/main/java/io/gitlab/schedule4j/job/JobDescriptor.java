/*
 * Created on 16.11.2015
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.job;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import io.gitlab.schedule4j.trigger.TriggerConfig;

/**
 * Describe a job and holds the job data. A JobDescriptor is immutable.
 * 
 * @author Dirk Buchhorn
 */
public final class JobDescriptor implements Serializable
{
	private static final long serialVersionUID = 6531294794472936825L;

	private final JobKey jobKey;
	private final Class<? extends Job> jobClass;
	private final TriggerConfig triggerConfig;
	private final Map<String, Serializable> jobDataMap = new ConcurrentHashMap<>();
	private final boolean isStateful;
	private final int priority;

	/**
	 * Creates a new job descriptor and set the group inside the key object to null.
	 * 
	 * @param name the name for this job descriptor
	 * @param jobClass the job class what should be executed
	 * @param triggerConfig the trigger configuration
	 */
	public JobDescriptor(String name, Class<? extends Job> jobClass, TriggerConfig triggerConfig)
	{
		this(JobKey.from(name), jobClass, triggerConfig);
	}

	/**
	 * Creates a new job descriptor.
	 * 
	 * @param jobKey the key for this job descriptor
	 * @param jobClass the job class what should be executed
	 * @param triggerConfig the trigger configuration
	 */
	public JobDescriptor(JobKey jobKey, Class<? extends Job> jobClass, TriggerConfig triggerConfig)
	{
		this(jobKey, jobClass, triggerConfig, jobClass != null && StatefulJob.class.isAssignableFrom(jobClass), 0);
	}

	/**
	 * Creates a new job descriptor.
	 * 
	 * @param jobKey the key for this job descriptor
	 * @param jobClass the job class what should be executed
	 * @param triggerConfig the trigger configuration
	 * @param isStateful should the job stateless or stateful
	 */
	public JobDescriptor(JobKey jobKey, Class<? extends Job> jobClass, TriggerConfig triggerConfig, boolean isStateful)
	{
		this(jobKey, jobClass, triggerConfig, isStateful, 0);
	}

	/**
	 * Creates a new job descriptor.
	 * 
	 * @param jobKey the key for this job descriptor
	 * @param jobClass the job class what should be executed
	 * @param triggerConfig the trigger configuration
	 * @param isStateful should the job stateless or stateful
	 * @param priority the job priority
	 */
	public JobDescriptor(JobKey jobKey, Class<? extends Job> jobClass, TriggerConfig triggerConfig, boolean isStateful,
		int priority)
	{
		if (jobKey == null)
		{
			throw new IllegalArgumentException("Parameter jobKey can't be null!");
		}
		if (jobClass == null)
		{
			throw new IllegalArgumentException("Parameter jobClass can't be null!");
		}
		if (triggerConfig == null)
		{
			throw new IllegalArgumentException("Parameter triggerConfig can't be null!");
		}
		this.jobKey = jobKey;
		this.jobClass = jobClass;
		this.triggerConfig = triggerConfig;
		this.isStateful = isStateful;
		this.priority = priority;
	}

	/**
	 * Constructor for cloning a JobDescriptor.
	 * 
	 * @param jobDescriptor the JobDescriptor to clone
	 */
	public JobDescriptor(JobDescriptor jobDescriptor)
	{
		if (jobDescriptor == null)
		{
			throw new IllegalArgumentException("JobDescriptor can't be null!");
		}
		jobKey = jobDescriptor.getKey();
		jobClass = jobDescriptor.getJobClass();
		isStateful = jobDescriptor.isStateful();
		jobDataMap.putAll(jobDescriptor.jobDataMap);
		triggerConfig = jobDescriptor.getTriggerConfig();
		priority = jobDescriptor.getPriority();
	}

	/**
	 * Gets the key for this job descriptor.
	 * 
	 * @return the key
	 */
	public JobKey getKey()
	{
		return jobKey;
	}

	/**
	 * Gets the job class what should be used.
	 * 
	 * @return the job class
	 */
	public Class<? extends Job> getJobClass()
	{
		return jobClass;
	}

	/**
	 * Returns true if the job class is from type stateful.
	 * 
	 * @return true if the job class is stateful
	 */
	public boolean isStateful()
	{
		return isStateful;
	}

	/**
	 * In this {@link Map} meta information for the job can be stored.
	 * 
	 * @return a {@link Map} what contains the meta information for the job.
	 */
	public Map<String, Serializable> getJobDataMap()
	{
		return jobDataMap;
	}

	/**
	 * Puts a value to the job data map.
	 * 
	 * @param key the key
	 * @param value the value
	 */
	public void putJobData(String key, Serializable value)
	{
		jobDataMap.put(key, value);
	}

	/**
	 * Removes the key from the job data map.
	 * 
	 * @param key the key
	 * @return the removed value for the given key
	 */
	public Serializable removeJobData(String key)
	{
		return jobDataMap.remove(key);
	}

	/**
	 * Gets the value for the given key.
	 * 
	 * @param key the key
	 * @return the value for the given key
	 */
	public Serializable getJobData(String key)
	{
		return jobDataMap.get(key);
	}

	/**
	 * Get the {@link TriggerConfig} for this job.
	 * 
	 * @return the triggerConfig
	 */
	public TriggerConfig getTriggerConfig()
	{
		return triggerConfig;
	}

	/**
	 * Creates a new JobDescriptor with the given triggerConfig.
	 * 
	 * @param triggerConfig the triggerConfig to set
	 * @return the new {@link JobDescriptor}
	 */
	public JobDescriptor withTriggerConfig(TriggerConfig triggerConfig)
	{
		return createWith(triggerConfig, priority);
	}

	/**
	 * Get the priority for this job.
	 * 
	 * @return the priority
	 */
	public int getPriority()
	{
		return priority;
	}

	/**
	 * Creates a new JobDescriptor with the given priority.
	 * 
	 * @param priority the priority to set
	 * @return the new {@link JobDescriptor}
	 */
	public JobDescriptor withPriority(int priority)
	{
		return createWith(triggerConfig, priority);
	}

	private JobDescriptor createWith(TriggerConfig triggerConfig, int priority)
	{
		JobDescriptor newJobDescriptor = new JobDescriptor(jobKey, jobClass, triggerConfig, isStateful, priority);
		newJobDescriptor.jobDataMap.putAll(jobDataMap);
		return newJobDescriptor;
	}
}
