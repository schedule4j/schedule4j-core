/*
 * Created on 16.11.2015
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.job;

import java.io.Serializable;
import java.time.ZonedDateTime;

import io.gitlab.schedule4j.trigger.TriggerEvent;

/**
 * @author Dirk Buchhorn
 */
public final class JobExecutionContext implements Serializable
{
	private static final long serialVersionUID = -4453168619175317829L;

	private JobDescriptor jobDescriptor;
	private TriggerEvent triggerEvent;
	private ZonedDateTime schedulerFireTime;

	public JobExecutionContext(JobDescriptor jobDescriptor, TriggerEvent triggerEvent,
		ZonedDateTime schedulerFireTime)
	{
		if (jobDescriptor == null)
		{
			throw new IllegalArgumentException("Parameter jobDescriptor can't be null");
		}
		if (triggerEvent == null)
		{
			throw new IllegalArgumentException("Parameter triggerEvent can't be null");
		}
		if (schedulerFireTime == null)
		{
			throw new IllegalArgumentException("Parameter fireTime can't be null");
		}
		this.jobDescriptor = jobDescriptor;
		this.triggerEvent = triggerEvent;
		this.schedulerFireTime = schedulerFireTime;
	}

	public JobDescriptor getJobDescriptor()
	{
		return jobDescriptor;
	}

	public TriggerEvent getTriggerEvent()
	{
		return triggerEvent;
	}

	public ZonedDateTime getSchedulerFireTime()
	{
		return schedulerFireTime;
	}
}
