/*
 * Created on 17.09.2016
 *
 * (c) Copyright by dibu-Soft.
 * All Rights Reserved.
 */
package io.gitlab.schedule4j.priority;

import java.util.AbstractQueue;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import io.gitlab.schedule4j.priority.PriorityComparator.SortOrder;

/**
 * A {@link BlockingQueue} implementation for prioritized queuing. If an object is queued then it
 * can pass all objects with lower priority. If an object pass other objects then the priorities of
 * both objects can be adjusted by a {@link QueuedElementPriorityAdjuster}.
 * 
 * @author Dirk Buchhorn
 */
public class BoundedPriorityBlockingQueue<E extends Priority> extends AbstractQueue<E>
	implements BlockingQueue<E>
{
	private int maxCapacity;
	private Node head;
	private Node last;
	private int size = 0;
	private Comparator<Priority> priorityComparator;
	private QueuedElementPriorityAdjuster priorityAdjuster;

	/**
	 * Lock used for all public operations
	 */
	private final ReentrantLock reentrantLock;

	/**
	 * Condition for blocking when empty
	 */
	private final Condition notEmpty;

	/**
	 * Condition for blocking when full
	 */
	private final Condition notFull;

	/**
	 * Creates a new queue with the given max capacity. The priority sort order is descending.
	 * 
	 * @param maxCapacity the max queue capacity
	 */
	public BoundedPriorityBlockingQueue(int maxCapacity)
	{
		this(maxCapacity, null, new PriorityComparator(SortOrder.DESC));
	}

	/**
	 * Creates a new queue with the given max capacity and {@link QueuedElementPriorityAdjuster}.
	 * The priority sort order is descending.
	 * 
	 * @param maxCapacity the max queue capacity
	 * @param priorityAdjuster the priority adjuster
	 */
	public BoundedPriorityBlockingQueue(int maxCapacity,
		QueuedElementPriorityAdjuster priorityAdjuster)
	{
		this(maxCapacity, priorityAdjuster, new PriorityComparator(SortOrder.DESC));
	}

	/**
	 * Creates a new queue with the given max capacity, {@link QueuedElementPriorityAdjuster} and
	 * priority comparator.
	 * 
	 * @param maxCapacity the max queue capacity
	 * @param priorityAdjuster the priority adjuster
	 * @param comparator the priority comparator
	 */
	public BoundedPriorityBlockingQueue(int maxCapacity,
		QueuedElementPriorityAdjuster priorityAdjuster, Comparator<Priority> comparator)
	{
		setMaxCapacity(maxCapacity);
		setPriorityAdjuster(priorityAdjuster);
		setPriorityComparator(comparator);
		this.reentrantLock = new ReentrantLock();
		this.notEmpty = reentrantLock.newCondition();
		this.notFull = reentrantLock.newCondition();
		head = new Node(null, null, null);
		last = head;
	}

	/**
	 * Gets the max queue capacity.
	 * 
	 * @return the maxCapacity
	 */
	public int getMaxCapacity()
	{
		return maxCapacity;
	}

	/**
	 * Sets the max queue capacity.
	 * 
	 * @param maxCapacity the maxCapacity to set
	 */
	public void setMaxCapacity(int maxCapacity)
	{
		if (maxCapacity <= 0)
		{
			throw new IllegalArgumentException("Parameter maxCapacity must be greater than zero.");
		}
		this.maxCapacity = maxCapacity;
	}

	/**
	 * Gets the priority adjuster.
	 * 
	 * @return the priorityAdjuster
	 */
	public QueuedElementPriorityAdjuster getPriorityAdjuster()
	{
		return priorityAdjuster;
	}

	/**
	 * Sets the priority adjuster. If the priority adjuster is null then a new
	 * {@link DoNothingQueuedElementPriorityAdjuster} is set.
	 * 
	 * @param priorityAdjuster the priorityAdjuster to set
	 */
	public void setPriorityAdjuster(QueuedElementPriorityAdjuster priorityAdjuster)
	{
		if (priorityAdjuster == null)
		{
			this.priorityAdjuster = new DoNothingQueuedElementPriorityAdjuster();
		}
		else
		{
			this.priorityAdjuster = priorityAdjuster;
		}
	}

	/**
	 * Gets the priority comparator.
	 * 
	 * @return the priorityComparator
	 */
	public Comparator<Priority> getPriorityComparator()
	{
		return priorityComparator;
	}

	/**
	 * Sets the priority comparator.
	 * 
	 * @param priorityComparator the priorityComparator to set
	 */
	public void setPriorityComparator(Comparator<Priority> priorityComparator)
	{
		this.priorityComparator = priorityComparator;
	}

	@Override
	public int remainingCapacity()
	{
		final ReentrantLock lock = reentrantLock;
		lock.lock();
		try
		{
			int remainCapacity = maxCapacity - size;
			return remainCapacity > 0 ? remainCapacity : 0;
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public int size()
	{
		final ReentrantLock lock = reentrantLock;
		lock.lock();
		try
		{
			return size;
		}
		finally
		{
			lock.unlock();
		}
	}

	/**
	 * Call only while holding lock.
	 */
	private void queue(E e)
	{
		Node newNode = new Node(null, e, null);
		if (priorityComparator != null)
		{
			queueWithPriority(e, newNode);
		}
		else
		{
			newNode.insertAfter(last);
			last = newNode;
		}
		++size;
		notEmpty.signal();
	}

	private void queueWithPriority(E e, Node newNode)
	{
		Node node = last;
		boolean added = false;
		Priority p1;
		Priority p2 = e;
		while (node.item != null && !added)
		{
			p1 = node.item;
			int compareResult = priorityComparator.compare(p1, p2);
			if (compareResult <= 0)
			{
				newNode.insertAfter(node);
				if (node == last)
				{
					last = newNode;
				}
				added = true;
			}
			else
			{
				getPriorityAdjuster().adjustPriority(node.item, newNode.item);
			}
			node = node.prev;
		}
		if (!added)
		{
			// insert at position 0
			newNode.insertAfter(head);
			if (newNode.next == null)
			{
				last = newNode;
			}
		}
	}

	/**
	 * Call only while holding lock.
	 */
	private E dequeue()
	{
		if (size == 0)
		{
			return null;
		}
		else
		{
			Node node = head.next;
			E result = node.item;
			node.remove();
			if (node == last)
			{
				last = head;
			}
			--size;
			notFull.signal();
			return result;
		}
	}

	@Override
	public E poll()
	{
		final ReentrantLock lock = reentrantLock;
		lock.lock();
		try
		{
			return dequeue();
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public E poll(long timeout, TimeUnit unit) throws InterruptedException
	{
		long nanos = unit.toNanos(timeout);
		final ReentrantLock lock = reentrantLock;
		lock.lockInterruptibly();
		E result = null;
		try
		{
			while ((result = dequeue()) == null && nanos > 0)
			{
				nanos = notEmpty.awaitNanos(nanos);
			}
		}
		finally
		{
			lock.unlock();
		}
		return result;
	}

	@Override
	public E take() throws InterruptedException
	{
		final ReentrantLock lock = reentrantLock;
		lock.lockInterruptibly();
		E result = null;
		try
		{
			while ((result = dequeue()) == null)
			{
				notEmpty.await();
			}
		}
		finally
		{
			lock.unlock();
		}
		return result;
	}

	@Override
	public E peek()
	{
		final ReentrantLock lock = reentrantLock;
		lock.lock();
		try
		{
			return (size == 0) ? null : head.next.item;
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public boolean offer(E e)
	{
		if (e == null)
		{
			throw new NullPointerException("A null object can't be added to the queue");
		}
		final ReentrantLock lock = reentrantLock;
		lock.lock();
		try
		{
			if (size < maxCapacity)
			{
				queue(e);
				return true;
			}
		}
		finally
		{
			lock.unlock();
		}
		return false;
	}

	@Override
	public boolean offer(E e, long timeout, TimeUnit unit) throws InterruptedException
	{
		if (e == null)
		{
			throw new NullPointerException();
		}
		long nanos = unit.toNanos(timeout);
		final ReentrantLock lock = reentrantLock;
		lock.lockInterruptibly();
		try
		{
			while (size >= maxCapacity && nanos > 0)
			{
				nanos = notEmpty.awaitNanos(nanos);
			}
			if (size < maxCapacity)
			{
				queue(e);
				return true;
			}
			return false;
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public void put(E e) throws InterruptedException
	{
		if (e == null)
		{
			throw new NullPointerException();
		}
		final ReentrantLock lock = reentrantLock;
		lock.lock();
		try
		{
			while (size >= maxCapacity)
			{
				notFull.await();
			}
			queue(e);
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public int drainTo(Collection<? super E> c)
	{
		return drainTo(c, Integer.MAX_VALUE);
	}

	@Override
	public int drainTo(Collection<? super E> c, int maxElements)
	{
		if (c == null)
		{
			throw new NullPointerException();
		}
		if (c == this)
		{
			throw new IllegalArgumentException();
		}
		if (maxElements <= 0)
		{
			return 0;
		}
		final ReentrantLock lock = reentrantLock;
		lock.lock();
		try
		{
			int n = Math.min(size, maxElements);
			for (int i = 0; i < n; i++)
			{
				c.add(dequeue());
			}
			return n;
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public Object[] toArray()
	{
		final ReentrantLock lock = reentrantLock;
		lock.lock();
		try
		{
			Object[] array = new Object[size];
			int index = 0;
			Node node = head;
			while ((node = node.next) != null)
			{
				array[index] = node.item;
				index++;
			}
			return array;
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public Iterator<E> iterator()
	{
		return new Itr(toArray());
	}

	@Override
	public String toString()
	{
		final ReentrantLock lock = reentrantLock;
		lock.lock();
		try
		{
			StringBuilder sb = new StringBuilder();
			sb.append(this.getClass().getSimpleName()).append(" [");
			int count = 0;
			Node node = head;
			while ((node = node.next) != null)
			{
				if (count > 0)
				{
					sb.append(", ");
				}
				sb.append(node.item);
				count++;
			}
			sb.append("]");
			return sb.toString();
		}
		finally
		{
			lock.unlock();
		}
	}

	private class Node
	{
		Node prev;
		E item;
		Node next;

		Node(Node prev, E element, Node next)
		{
			this.prev = prev;
			this.item = element;
			this.next = next;
		}

		void insertAfter(Node node)
		{
			next = node.next;
			if (node.next != null)
			{
				node.next.prev = this;
			}
			prev = node;
			node.next = this;
		}

		void remove()
		{
			// prev is always != null, because we use a head object
			prev.next = next;
			if (next != null)
			{
				next.prev = prev;
			}
			prev = null;
			next = null;
		}
	}

	private class Itr implements Iterator<E>
	{
		final Object[] array;
		int cursor;
		int lastCursor;

		Itr(Object[] array)
		{
			this.lastCursor = -1;
			this.array = array;
		}

		@Override
		public boolean hasNext()
		{
			return cursor < array.length;
		}

		@Override
		@SuppressWarnings("unchecked")
		public E next()
		{
			if (cursor >= array.length)
			{
				throw new NoSuchElementException();
			}
			lastCursor = cursor;
			return (E) array[cursor++];
		}

		@Override
		public void remove()
		{
			if (lastCursor < 0)
			{
				throw new IllegalStateException();
			}
			removeInternal(array[lastCursor]);
			lastCursor = -1;
		}

		private void removeInternal(Object o)
		{
			final ReentrantLock lock = reentrantLock;
			lock.lock();
			try
			{
				Node node = head;
				while ((node = node.next) != null)
				{
					if (node.item == o)
					{
						node.remove();
						--size;
						break;
					}
				}
			}
			finally
			{
				lock.unlock();
			}
		}
	}
}
