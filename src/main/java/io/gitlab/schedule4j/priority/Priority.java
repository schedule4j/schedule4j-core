/*
 * Created on 21.02.2016
 *
 * (c) Copyright by dibu-Soft.
 * All Rights Reserved.
 */
package io.gitlab.schedule4j.priority;

/**
 * @author Dirk Buchhorn
 */
public interface Priority
{
	/**
	 * Gets the priority.
	 * 
	 * @return the priority
	 */
	public int getPriority();

	/**
	 * Sets the priority.
	 * 
	 * @param priority the new priority to set
	 */
	public void setPriority(int priority);

	/**
	 * Adds the given priority value.
	 * 
	 * @param priority the priority to add
	 */
	public void plusPriority(int priority);

	/**
	 * Subtracts the given priority value.
	 * 
	 * @param priority the priority to subtract
	 */
	public void minusPriority(int priority);
}
