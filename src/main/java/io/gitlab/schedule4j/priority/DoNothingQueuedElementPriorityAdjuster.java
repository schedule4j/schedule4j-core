/*
 * Created on 15.09.2016
 *
 * (c) Copyright by dibu-Soft.
 * All Rights Reserved.
 */
package io.gitlab.schedule4j.priority;

/**
 * This priority adjuster does not change the priorities.
 * 
 * @author Dirk Buchhorn
 */
public class DoNothingQueuedElementPriorityAdjuster implements QueuedElementPriorityAdjuster
{
	@Override
	public void adjustPriority(Priority lowPriority, Priority highPriority)
	{
		// we don't change the priorities
	}
}
