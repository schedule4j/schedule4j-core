/*
 * Created on 21.02.2016
 *
 * (c) Copyright by dibu-Soft.
 * All Rights Reserved.
 */
package io.gitlab.schedule4j.priority;

/**
 * Adjust the priority of {@link Priority} objects if they pass queued objects. If a {@link Priority} is added
 * to a queue and the queue contains at least one entry then the priorities of both objects are compared. If
 * the priority of the added object is greater then it can pass the queued object and the
 * QueuePriorityAdjuster is called to adjust the priorities. An adjuster implementation can give passed
 * objects some extra priority points and every time an object pass a queued object then it can lose some
 * priority points. An adjuster can make the prioritized queuing fair.
 * 
 * @author Dirk Buchhorn
 */
public interface QueuedElementPriorityAdjuster
{
	void adjustPriority(Priority lowPriority, Priority highPriority);
}
