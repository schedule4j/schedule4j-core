/*
 * Created on 24.09.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.priority;

import java.util.Comparator;

/**
 * An instance of this class can compare object of type {@link Priority}.
 * 
 * @author Dirk Buchhorn
 */
public class PriorityComparator implements Comparator<Priority>
{
	public enum SortOrder
	{
		ASC, DESC
	}

	private SortOrder sortOrder;

	public PriorityComparator()
	{
		this(SortOrder.DESC);
	}

	public PriorityComparator(SortOrder sortOrder)
	{
		this.sortOrder = sortOrder;
	}

	@Override
	public int compare(Priority o1, Priority o2)
	{
		int result = 0;
		if (o1 == null && o2 == null)
		{
			result = 0;
		}
		else if (o1 == null)
		{
			result = 1;
		}
		else if (o2 == null)
		{
			result = -1;
		}
		else if (o1.getPriority() < o2.getPriority())
		{
			result = -1;
		}
		else if (o1.getPriority() > o2.getPriority())
		{
			result = 1;
		}
		if (sortOrder == SortOrder.DESC)
		{
			result = result * -1;
		}
		return result;
	}
}
