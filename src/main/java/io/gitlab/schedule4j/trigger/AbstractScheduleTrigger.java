/*
 * Created on 19.12.2015
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.trigger;

import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * The rule for handling missed events are defined by the {@link CalculateNextEventRule} enumeration
 * class.
 * 
 * @author Dirk Buchhorn
 */
abstract class AbstractScheduleTrigger<T extends AbstractScheduleTriggerConfig> implements Trigger
{
	private T triggerConfig;
	private long plusNanosValue;

	/**
	 * Create a new {@link AbstractScheduleTrigger} object.
	 * 
	 * @param triggerConfig the trigger configuration
	 * @param precisionInNanos the precision to use for calculation
	 */
	protected AbstractScheduleTrigger(T triggerConfig, long precisionInNanos)
	{
		if (triggerConfig == null)
		{
			throw new IllegalArgumentException("TriggerConfig can't be null!");
		}
		this.triggerConfig = triggerConfig;
		this.plusNanosValue = precisionInNanos;
	}

	/**
	 * Gets the trigger configuration.
	 * 
	 * @return the trigger configuration
	 */
	protected T getTriggerConfig()
	{
		return triggerConfig;
	}

	@Override
	public TriggerKey getKey()
	{
		return triggerConfig.getKey();
	}

	@Override
	public TriggerEvent getNextTriggerEvent(TriggerEvent pastTriggerEvent, ZonedDateTime calcTime)
	{
		ZonedDateTime calculationTime = calcTime.withZoneSameInstant(getZone());
		ZonedDateTime fireTime = null;
		if (pastTriggerEvent == null)
		{
			fireTime = getNextFireTime(calculationTime);
		}
		else
		{
			fireTime = getNextFireTime(pastTriggerEvent.getFireTime());
			if (fireTime != null && fireTime.isBefore(calculationTime))
			{
				// we missed some trigger events
				if (getCalculateNextEventRule() == CalculateNextEventRule.NEAREST)
				{
					fireTime = getPreviousFireTime(calculationTime.plusNanos(plusNanosValue));
				}
				else if (getCalculateNextEventRule() == CalculateNextEventRule.NEXT)
				{
					fireTime = getNextFireTime(calculationTime);
				}
				// else ONE_BY_ONE - we use the calculated fireTime
			}
		}
		return fireTime != null ? new TriggerEvent(fireTime, calculationTime) : null;
	}

	private ZoneId getZone()
	{
		T config = getTriggerConfig();
		return config.getZone() != null ? config.getZone() : ZoneId.systemDefault();
	}

	public abstract ZonedDateTime getNextFireTime(ZonedDateTime calculationTime);

	public abstract ZonedDateTime getPreviousFireTime(ZonedDateTime dateTime);

	/**
	 * Gets the {@link CalculateNextEventRule} what should be used for calculating next events if
	 * the past event was missed.
	 * 
	 * @return the {@link CalculateNextEventRule}
	 */
	public CalculateNextEventRule getCalculateNextEventRule()
	{
		return triggerConfig.getCalculateNextEventRule();
	}
}
