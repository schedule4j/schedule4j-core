/*
 * Created on 2019-09-01
 * 
 * Copyright 2019 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.trigger;

import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * @author Dirk Buchhorn
 */
public abstract class AbstractScheduleTriggerConfig implements TriggerConfig
{
	private static final long serialVersionUID = -7084630894175735959L;

	private final TriggerKey key;
	private CalculateNextEventRule calculateNextEventRule;
	private ZoneId zoneId = null;
	private ZonedDateTime startTime;
	private ZonedDateTime endTime;
	private boolean startTimeOptional = false;

	/**
	 * Constructor for creating an {@link AbstractScheduleTriggerConfig}.
	 * 
	 * @param key the trigger key
	 * @param calculateNextEventRule the CalculateNextEventRule
	 * @param zone the time zone
	 * @param startTimeOptional if the start time optional
	 * @param startTime the start time
	 * @param endTime the end time
	 */
	protected AbstractScheduleTriggerConfig(TriggerKey key, CalculateNextEventRule calculateNextEventRule, ZoneId zone,
		boolean startTimeOptional, ZonedDateTime startTime, ZonedDateTime endTime)
	{
		if (key == null)
		{
			throw new IllegalArgumentException("TriggerKey can't be null");
		}
		this.key = key;
		setCalculateNextEventRule(calculateNextEventRule);
		setZone(zone);
		this.startTimeOptional = startTimeOptional;
		setStartTime(startTime);
		setEndTime(endTime);
	}

	/**
	 * Is the start time optional.
	 * 
	 * @return <code>true</code> if the start time is optional
	 */
	protected boolean isStartTimeOptional()
	{
		return startTimeOptional;
	}

	@Override
	public TriggerKey getKey()
	{
		return key;
	}

	/**
	 * Gets the {@link CalculateNextEventRule} what should be used for calculating next events if the past event was
	 * missed.
	 * 
	 * @return the {@link CalculateNextEventRule}
	 */
	public CalculateNextEventRule getCalculateNextEventRule()
	{
		return calculateNextEventRule;
	}

	/**
	 * Sets the {@link CalculateNextEventRule}.
	 * 
	 * @param calculateNextEventRule the rule to set
	 */
	private void setCalculateNextEventRule(CalculateNextEventRule calculateNextEventRule)
	{
		if (calculateNextEventRule == null)
		{
			this.calculateNextEventRule = CalculateNextEventRule.NEAREST;
		}
		else
		{
			this.calculateNextEventRule = calculateNextEventRule;
		}
	}

	/**
	 * Gets the zone or if not set the system default zone.
	 * 
	 * @return the zone or if null the system default zone
	 */
	public ZoneId getZone()
	{
		return zoneId != null ? zoneId : ZoneId.systemDefault();
	}

	/**
	 * Sets the {@link ZoneId}. A trigger must generate {@link TriggerEvent}s for this zone.
	 * 
	 * @param zoneId the zone id to set
	 */
	private void setZone(ZoneId zoneId)
	{
		this.zoneId = zoneId;
		if (zoneId != null)
		{
			// we must change the start and end time, else the calculated time has not the right
			// zone
			if (startTime != null)
			{
				startTime = startTime.withZoneSameInstant(getZone());
			}
			if (endTime != null)
			{
				endTime = endTime.withZoneSameInstant(getZone());
			}
		}
	}

	/**
	 * Gets the trigger start time.
	 * 
	 * @return the schedule start time
	 */
	public ZonedDateTime getStartTime()
	{
		return startTime;
	}

	/**
	 * Sets the trigger start time. If no zone is set then the zone of the start time is used as new time zone.
	 * 
	 * @param startTime the start time to set
	 */
	private void setStartTime(ZonedDateTime startTime)
	{
		if (!startTimeOptional && startTime == null)
		{
			throw new IllegalArgumentException("start time can't be null!");
		}
		if (startTime != null && endTime != null && !startTime.isBefore(endTime))
		{
			throw new IllegalArgumentException("startTime must be less than endTime!");
		}
		this.startTime = correctZoneForDateTimeOrSetZone(startTime);
	}

	/**
	 * Gets the trigger end time. If there is no end time null is returned. The end time is always exclusive.
	 * 
	 * @return the schedule end time
	 */
	public ZonedDateTime getEndTime()
	{
		return endTime;
	}

	/**
	 * Sets the trigger end time. If no zone is set then the zone of the end time is used as new time zone.
	 * 
	 * @param endTime the end time to set
	 */
	private void setEndTime(ZonedDateTime endTime)
	{
		if (startTime != null && endTime != null && !startTime.isBefore(endTime))
		{
			throw new IllegalArgumentException("endTime must be greater than startTime!");
		}
		this.endTime = correctZoneForDateTimeOrSetZone(endTime);
	}

	protected ZonedDateTime correctZoneForDateTimeOrSetZone(ZonedDateTime dateTime)
	{
		ZonedDateTime zdt = dateTime;
		if (zdt != null)
		{
			if (zoneId == null)
			{
				// we use the zone of the date time
				setZone(zdt.getZone());
			}
			else if (!getZone().equals(zdt.getZone()))
			{
				zdt = zdt.withZoneSameInstant(getZone());
			}
		}
		return zdt;
	}
}
