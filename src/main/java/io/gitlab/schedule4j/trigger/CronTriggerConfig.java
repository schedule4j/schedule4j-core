/*
 * Created on 2019-08-26
 * 
 * Copyright 2019 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.trigger;

import java.text.ParseException;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import io.gitlab.schedule4j.cron.CronExpression;

/**
 * @author Dirk Buchhorn
 */
public class CronTriggerConfig extends AbstractScheduleTriggerConfig
{
	private static final long serialVersionUID = 8288680739409550598L;

	private String cronExpression;

	/**
	 * Creates a new {@link CronTriggerConfig} with the given cron expression.
	 * 
	 * @param key the trigger key
	 * @param cronExpression the cron expression
	 * @throws ParseException if the cron expression could not be parsed
	 */
	public CronTriggerConfig(TriggerKey key, String cronExpression) throws ParseException
	{
		this(key, null, null, cronExpression);
	}

	/**
	 * Creates a new {@link CronTriggerConfig} with the given cronExpression, start time and end
	 * time. Please note the end time is always exclusive.
	 * 
	 * @param key the trigger key
	 * @param cronExpression the cron expression
	 * @param startTime the trigger start time, this time can be null
	 * @param endTime the schedule end time, this time can be null
	 * @throws ParseException if the cron expression could not be parsed
	 */
	public CronTriggerConfig(TriggerKey key, ZonedDateTime startTime, ZonedDateTime endTime,
		String cronExpression) throws ParseException
	{
		this(key, null, startTime, endTime, cronExpression);
	}

	/**
	 * Creates a new {@link CronTriggerConfig} with the given cronExpression, start time and end
	 * time. Please note the end time is always exclusive.
	 * 
	 * @param key the trigger key
	 * @param calculateNextEventRule the CalculateNextEventRule
	 * @param cronExpression the cron expression
	 * @param startTime the trigger start time, this time can be null
	 * @param endTime the schedule end time, this time can be null
	 * @throws ParseException if the cron expression could not be parsed
	 */
	public CronTriggerConfig(TriggerKey key, CalculateNextEventRule calculateNextEventRule,
		ZonedDateTime startTime, ZonedDateTime endTime, String cronExpression) throws ParseException
	{
		super(key, calculateNextEventRule, null, true, startTime, endTime);
		setCronExpression(cronExpression);
	}

	private CronTriggerConfig(TriggerKey key, CalculateNextEventRule calculateNextEventRule,
		ZoneId zone, ZonedDateTime startTime, ZonedDateTime endTime, String cronExpression)
	{
		super(key, calculateNextEventRule, zone, true, startTime, endTime);
		this.cronExpression = cronExpression;
	}

	/**
	 * Gets the cron expression.
	 * 
	 * @return the cronExpression
	 */
	public String getCronExpression()
	{
		return cronExpression;
	}

	/**
	 * Sets the cron expression.
	 * 
	 * @param cronExpression the cronExpression to set
	 * @throws ParseException
	 */
	private void setCronExpression(String cronExpression) throws ParseException
	{
		if (cronExpression == null)
		{
			throw new IllegalArgumentException("Argument cronExpression can't be null");
		}
		// test/parse the cronExpression
		new CronExpression(cronExpression);
		this.cronExpression = cronExpression;
	}

	@Override
	public CronTrigger createTrigger() throws CreateTriggerException
	{
		try
		{
			return new CronTrigger(this);
		}
		catch (ParseException e)
		{
			throw new CreateTriggerException(e.getMessage(), e);
		}
	}

	public CronTriggerConfig withTriggerKey(TriggerKey key)
	{
		return new CronTriggerConfig(key, getCalculateNextEventRule(), getZone(), getStartTime(),
			getEndTime(), getCronExpression());
	}

	public CronTriggerConfig withCalculateNextEventRule(
		CalculateNextEventRule calculateNextEventRule)
	{
		return new CronTriggerConfig(getKey(), calculateNextEventRule, getZone(), getStartTime(),
			getEndTime(), getCronExpression());
	}

	public CronTriggerConfig withZone(ZoneId zone)
	{
		return new CronTriggerConfig(getKey(), getCalculateNextEventRule(), zone, getStartTime(),
			getEndTime(), getCronExpression());
	}

	public CronTriggerConfig withStartTime(ZonedDateTime startTime)
	{
		return new CronTriggerConfig(getKey(), getCalculateNextEventRule(), getZone(), startTime,
			getEndTime(), getCronExpression());
	}

	public CronTriggerConfig withEndTime(ZonedDateTime endTime)
	{
		return new CronTriggerConfig(getKey(), getCalculateNextEventRule(), getZone(),
			getStartTime(), endTime, getCronExpression());
	}

	public CronTriggerConfig withCronExpression(String cronExpression) throws ParseException
	{
		CronTriggerConfig ctc = new CronTriggerConfig(getKey(), getCalculateNextEventRule(),
			getZone(), getStartTime(), getEndTime(), cronExpression);
		ctc.setCronExpression(cronExpression);
		return ctc;
	}
}
