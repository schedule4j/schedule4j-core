/*
 * Created on 11.12.2015
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.trigger;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import io.gitlab.schedule4j.schedule.ScheduleEvent;

/**
 * The calculated trigger event. This class holds a fire time and the calculation time. An instance of this class is
 * immutable.
 * 
 * @author Dirk Buchhorn
 */
public final class TriggerEvent implements Serializable
{
	private static final long serialVersionUID = -4172113881430428052L;

	private final ZonedDateTime fireTime;
	private final ZonedDateTime calculationTime;

	/**
	 * Create a new {@link TriggerEvent} with the given fire time and calculation time.
	 * 
	 * @param fireTime the fire time
	 * @param calculationTime the calculation time
	 */
	public TriggerEvent(ZonedDateTime fireTime, ZonedDateTime calculationTime)
	{
		if (fireTime == null)
		{
			throw new IllegalArgumentException("fireTime can't be null!");
		}
		if (calculationTime == null)
		{
			throw new IllegalArgumentException("calculationTime can't be null!");
		}
		this.fireTime = fireTime;
		this.calculationTime = calculationTime;
	}

	/**
	 * Gets the fire time.
	 * 
	 * @return the fire time
	 */
	public ZonedDateTime getFireTime()
	{
		return fireTime;
	}

	/**
	 * Gets the calculation time.
	 * 
	 * @return the calculation time
	 */
	public ZonedDateTime getCalculationTime()
	{
		return calculationTime;
	}

	/**
	 * Test if the fire time of the given {@link ScheduleEvent} is after this fire time.
	 * 
	 * @param triggerEvent the trigger event
	 * @return true if the fire time of the given {@link ScheduleEvent} is after this fire time
	 */
	public boolean isFireTimeAfter(TriggerEvent triggerEvent)
	{
		return triggerEvent != null && fireTime.isAfter(triggerEvent.getFireTime());
	}

	/**
	 * Test if the fire time of the given {@link ScheduleEvent} is before this fire time.
	 * 
	 * @param triggerEvent the trigger event
	 * @return true if the fire time of the given {@link ScheduleEvent} is before this fire time
	 */
	public boolean isFireTimeBefore(TriggerEvent triggerEvent)
	{
		return triggerEvent != null && fireTime.isBefore(triggerEvent.getFireTime());
	}

	/**
	 * Test is the fire time is equals to the fire time of the given {@link ScheduleEvent}.
	 * 
	 * @param triggerEvent the trigger event
	 * @return true the fire time is equals to the fire time of the given {@link ScheduleEvent}
	 */
	public boolean isFireTimeEquals(TriggerEvent triggerEvent)
	{
		return triggerEvent.getFireTime().equals(fireTime);
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + calculationTime.hashCode();
		result = prime * result + fireTime.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		TriggerEvent other = (TriggerEvent) obj;
		if (!calculationTime.equals(other.calculationTime)) return false;
		return fireTime.equals(other.fireTime);
	}

	@Override
	public String toString()
	{
		return "TriggerEvent (" + fireTime.format(DateTimeFormatter.ISO_DATE_TIME) + ")";
	}
}
