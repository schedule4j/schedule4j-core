/*
 * Created on 16.12.2015
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.trigger;

import io.gitlab.schedule4j.job.StatefulJob;

/**
 * If some trigger events are missed how next trigger event should be calculated?
 * 
 * @author Dirk Buchhorn
 */
public enum CalculateNextEventRule
{
	/**
	 * Use the nearest trigger event. This can be the past one if this was not already executed or the next
	 * one. This rule should be the default way.
	 */
	NEAREST,
	/**
	 * Wait for the next trigger event to execute.
	 */
	NEXT,
	/**
	 * Every trigger event must be fired. Missed trigger events will be fired again. Be careful with this. If a
	 * {@link StatefulJob} needs a long time then more and more trigger events are waiting for execution.
	 */
	ONE_BY_ONE
}
