/*
 * Created on 2019-08-26
 * 
 * Copyright 2019 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.trigger;

import java.io.Serializable;

/**
 * An implementation of this interface holds a trigger configuration. A trigger configuration is
 * immutable.
 * 
 * @author Dirk Buchhorn
 */
public interface TriggerConfig extends Serializable
{
	/**
	 * Gets the trigger key.
	 * 
	 * @return the trigger key
	 */
	TriggerKey getKey();

	/**
	 * Create a {@link Trigger} for a {@link TriggerConfig} and returns it.
	 * 
	 * @return the created trigger instance
	 * @throws CreateTriggerException if the {@link Trigger} could not be created
	 */
	Trigger createTrigger() throws CreateTriggerException;
}
