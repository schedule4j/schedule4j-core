/*
 * Created on 17.12.2015
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.trigger;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Triggers can be grouped by this class. So it's possible to schedule a job with more than one
 * trigger.
 * 
 * @author Dirk Buchhorn
 */
public class TriggerGroup implements Trigger
{
	private TriggerGroupConfig triggerGroupConfig;
	private Map<TriggerKey, Trigger> triggerMap = new HashMap<>();

	/**
	 * Creates a new TriggerGroup with the given configuration.
	 * 
	 * @param config the trigger group configuration
	 * @throws CreateTriggerException if the sub {@link Trigger}s could not be created
	 */
	public TriggerGroup(TriggerGroupConfig config) throws CreateTriggerException
	{
		this.triggerGroupConfig = config;
		for (TriggerConfig triggerConfig : config.getTriggerConfigs())
		{
			Trigger trigger = triggerConfig.createTrigger();
			triggerMap.put(trigger.getKey(), trigger);
		}
	}

	/**
	 * Gets the trigger group configuration.
	 * 
	 * @return the trigger group configuration
	 */
	protected TriggerGroupConfig getTriggerConfig()
	{
		return triggerGroupConfig;
	}

	@Override
	public TriggerKey getKey()
	{
		return triggerGroupConfig.getKey();
	}

	/**
	 * Counts the trigger in this group.
	 * 
	 * @return the trigger count in this group
	 */
	public int getSize()
	{
		return triggerMap.size();
	}

	/**
	 * Return all trigger keys of this group.
	 * 
	 * @return all trigger keys of this group
	 */
	public Set<TriggerKey> getTriggerKeys()
	{
		return Collections.unmodifiableSet(triggerMap.keySet());
	}

	/**
	 * Returns all triggers of this group.
	 * 
	 * @return all triggers of this group
	 */
	public Collection<Trigger> getTriggers()
	{
		return Collections.unmodifiableCollection(triggerMap.values());
	}

	/**
	 * Gets the trigger for the given key.
	 * 
	 * @param triggerKey the trigger key to search for
	 * @return the trigger for the given key
	 */
	public Optional<Trigger> getTrigger(TriggerKey triggerKey)
	{
		return Optional.ofNullable(triggerMap.get(triggerKey));
	}

	@Override
	public TriggerEvent getNextTriggerEvent(TriggerEvent pastTriggerEvent,
		ZonedDateTime calculationTime)
	{
		TriggerEvent nte = null;
		for (Trigger trigger : triggerMap.values())
		{
			TriggerEvent nextTriggerEvent = trigger.getNextTriggerEvent(pastTriggerEvent,
				calculationTime);
			if (nte == null || (nextTriggerEvent != null && nextTriggerEvent.isFireTimeBefore(nte)))
			{
				nte = nextTriggerEvent;
			}
		}
		return nte;
	}

	@Override
	public ZonedDateTime getFirstFireTime()
	{
		ZonedDateTime result = null;
		for (Trigger trigger : triggerMap.values())
		{
			ZonedDateTime firstFireTime = trigger.getFirstFireTime();
			if (result == null || (firstFireTime != null && firstFireTime.isBefore(result)))
			{
				result = firstFireTime;
			}
		}
		return result;
	}

	@Override
	public ZonedDateTime getLastFireTime()
	{
		ZonedDateTime result = null;
		for (Trigger trigger : triggerMap.values())
		{
			ZonedDateTime lastFireTime = trigger.getLastFireTime();
			if (result == null || (lastFireTime != null && lastFireTime.isAfter(result)))
			{
				result = lastFireTime;
			}
		}
		return result;
	}
}
