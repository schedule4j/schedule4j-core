/*
 * Created on 17.11.2015
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.trigger;

import java.time.ZonedDateTime;

import io.gitlab.schedule4j.common.SimpleTimeGenerator;

/**
 * This trigger has a start time, an optional end time, a repeat count and a repeat interval.
 * 
 * @author Dirk Buchhorn
 */
public class SimpleTrigger extends AbstractScheduleTrigger<SimpleTriggerConfig>
{
	private SimpleTimeGenerator simpleTimeGenerator;

	/**
	 * Creates a new {@link SimpleTrigger} with the given start time, repeat count and repeat
	 * interval.
	 * 
	 * @param config the simple trigger configuration
	 */
	public SimpleTrigger(SimpleTriggerConfig config)
	{
		super(config, 1000000L);
		simpleTimeGenerator = new SimpleTimeGenerator(config.getRepeatCount(),
			config.getRepeatInterval());
	}

	/**
	 * Gets the trigger start time.
	 * 
	 * @return the schedule start time
	 */
	public ZonedDateTime getStartTime()
	{
		return getTriggerConfig().getStartTime();
	}

	/**
	 * Gets the trigger end time. If there is no end time null is returned. The end time is always
	 * exclusive.
	 * 
	 * @return the schedule end time
	 */
	public ZonedDateTime getEndTime()
	{
		return getTriggerConfig().getEndTime();
	}

	/**
	 * How often the trigger should fire.
	 * 
	 * @return how often the trigger should fire
	 */
	public int getRepeatCount()
	{
		return getTriggerConfig().getRepeatCount();
	}

	/**
	 * The time between two trigger times in milliseconds.
	 * 
	 * @return the time between two trigger times in milliseconds
	 */
	public long getRepeatInterval()
	{
		return getTriggerConfig().getRepeatInterval();
	}

	@Override
	public ZonedDateTime getFirstFireTime()
	{
		return getStartTime();
	}

	@Override
	public ZonedDateTime getLastFireTime()
	{
		return simpleTimeGenerator.getLastTime(getStartTime(), getEndTime());
	}

	@Override
	public ZonedDateTime getNextFireTime(ZonedDateTime dateTime)
	{
		return simpleTimeGenerator.getNextTime(dateTime, getStartTime(), getEndTime());
	}

	@Override
	public ZonedDateTime getPreviousFireTime(ZonedDateTime dateTime)
	{
		return simpleTimeGenerator.getPreviousTime(dateTime, getStartTime(), getEndTime());
	}
}
