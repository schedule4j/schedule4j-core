/*
 * Created on 2019-09-11
 * 
 * Copyright 2019 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.trigger;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import io.gitlab.schedule4j.common.SimpleTimeGenerator;

/**
 * This trigger configuration has a start time, an optional end time, a repeat count and a repeat
 * interval.
 * 
 * @author Dirk Buchhorn
 */
public class SimpleTriggerConfig extends AbstractScheduleTriggerConfig
{
	private static final long serialVersionUID = -1061261673831170736L;

	private int repeatCount;
	private long repeatInterval;

	/**
	 * Creates a new {@link SimpleTriggerConfig} with the given start time, repeat count and repeat
	 * interval.
	 * 
	 * @param key the trigger key
	 * @param startTime the trigger start time
	 * @param repeatCount how often the trigger should fire
	 * @param repeatInterval the time between two trigger times in milliseconds
	 */
	public SimpleTriggerConfig(TriggerKey key, ZonedDateTime startTime, int repeatCount,
		long repeatInterval)
	{
		this(key, startTime, null, repeatCount, repeatInterval);
	}

	/**
	 * Creates a new {@link SimpleTriggerConfig} with the given start time, end time, repeat count
	 * and repeat interval. Please note the end time is always exclusive.
	 * 
	 * @param key the trigger key
	 * @param startTime the trigger start time
	 * @param endTime the schedule end time, this time can be null
	 * @param repeatCount how often the trigger should fire
	 * @param repeatInterval the time between two trigger times in milliseconds
	 */
	public SimpleTriggerConfig(TriggerKey key, ZonedDateTime startTime, ZonedDateTime endTime,
		int repeatCount, long repeatInterval)
	{
		super(key, null, null, false, startTime, endTime);
		setRepeatCount(repeatCount);
		setRepeatInterval(repeatInterval);
	}

	/**
	 * Creates a new {@link SimpleTriggerConfig} with the given start time, end time, repeat count
	 * and repeat interval. Please note the end time is always exclusive.
	 * 
	 * @param key the trigger key
	 * @param calculateNextEventRule the CalculateNextEventRule
	 * @param startTime the trigger start time
	 * @param endTime the schedule end time, this time can be null
	 * @param repeatCount how often the trigger should fire
	 * @param repeatInterval the time between two trigger times in milliseconds
	 */
	public SimpleTriggerConfig(TriggerKey key, CalculateNextEventRule calculateNextEventRule,
		ZonedDateTime startTime, ZonedDateTime endTime, int repeatCount, long repeatInterval)
	{
		super(key, calculateNextEventRule, null, false, startTime, endTime);
		setRepeatCount(repeatCount);
		setRepeatInterval(repeatInterval);
	}

	private SimpleTriggerConfig(TriggerKey key, CalculateNextEventRule calculateNextEventRule,
		ZoneId zone, ZonedDateTime startTime, ZonedDateTime endTime, int repeatCount,
		long repeatInterval)
	{
		super(key, calculateNextEventRule, zone, false, startTime, endTime);
		setRepeatCount(repeatCount);
		setRepeatInterval(repeatInterval);
	}

	/**
	 * How often the trigger should fire.
	 * 
	 * @return how often the trigger should fire
	 */
	public int getRepeatCount()
	{
		return repeatCount;
	}

	/**
	 * Sets the repeat count.
	 * 
	 * @param repeatCount the repeatCount to set
	 */
	private void setRepeatCount(int repeatCount)
	{
		SimpleTimeGenerator.checkRepeatCount(repeatCount);
		this.repeatCount = repeatCount;
	}

	/**
	 * The time between two trigger times in milliseconds.
	 * 
	 * @return the time between two trigger times in milliseconds
	 */
	public long getRepeatInterval()
	{
		return repeatInterval;
	}

	/**
	 * Sets the repeat interval.
	 * 
	 * @param repeatInterval the repeatInterval to set
	 */
	private void setRepeatInterval(long repeatInterval)
	{
		SimpleTimeGenerator.checkRepeatInterval(repeatInterval);
		this.repeatInterval = repeatInterval;
	}

	@Override
	public SimpleTrigger createTrigger()
	{
		return new SimpleTrigger(this);
	}

	public SimpleTriggerConfig withTriggerKey(TriggerKey key)
	{
		return new SimpleTriggerConfig(key, getCalculateNextEventRule(), getZone(), getStartTime(),
			getEndTime(), getRepeatCount(), getRepeatInterval());
	}

	public SimpleTriggerConfig withCalculateNextEventRule(
		CalculateNextEventRule calculateNextEventRule)
	{
		return new SimpleTriggerConfig(getKey(), calculateNextEventRule, getZone(), getStartTime(),
			getEndTime(), getRepeatCount(), getRepeatInterval());
	}

	public SimpleTriggerConfig withZone(ZoneId zone)
	{
		return new SimpleTriggerConfig(getKey(), getCalculateNextEventRule(), zone, getStartTime(),
			getEndTime(), getRepeatCount(), getRepeatInterval());
	}

	public SimpleTriggerConfig withStartTime(ZonedDateTime startTime)
	{
		return new SimpleTriggerConfig(getKey(), getCalculateNextEventRule(), getZone(), startTime,
			getEndTime(), getRepeatCount(), getRepeatInterval());
	}

	public SimpleTriggerConfig withEndTime(ZonedDateTime endTime)
	{
		return new SimpleTriggerConfig(getKey(), getCalculateNextEventRule(), getZone(),
			getStartTime(), endTime, getRepeatCount(), getRepeatInterval());
	}

	public SimpleTriggerConfig withRepeatCount(int repeatCount)
	{
		return new SimpleTriggerConfig(getKey(), getCalculateNextEventRule(), getZone(),
			getStartTime(), getEndTime(), repeatCount, getRepeatInterval());
	}

	public SimpleTriggerConfig withRepeatInterval(long repeatInterval)
	{
		return new SimpleTriggerConfig(getKey(), getCalculateNextEventRule(), getZone(),
			getStartTime(), getEndTime(), getRepeatCount(), repeatInterval);
	}
}
