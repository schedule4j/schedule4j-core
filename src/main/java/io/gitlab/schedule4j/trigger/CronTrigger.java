/*
 * Created on 17.11.2015
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.trigger;

import java.text.ParseException;
import java.time.ZonedDateTime;

import io.gitlab.schedule4j.common.CronTimeGenerator;
import io.gitlab.schedule4j.cron.CronExpression;

/**
 * @author Dirk Buchhorn
 */
public class CronTrigger extends AbstractScheduleTrigger<CronTriggerConfig>
{
	private CronTimeGenerator cronTimeGenerator;

	/**
	 * Creates a new {@link CronTrigger} with the given cron expression.
	 * 
	 * @param config the cron trigger configuration
	 * @throws ParseException if the cron expression could not be parsed
	 */
	public CronTrigger(CronTriggerConfig config) throws ParseException
	{
		super(config, 1000000000L);
		cronTimeGenerator = new CronTimeGenerator(config.getCronExpression());
	}

	/**
	 * Gets the trigger start time.
	 * 
	 * @return the schedule start time
	 */
	public ZonedDateTime getStartTime()
	{
		return getTriggerConfig().getStartTime();
	}

	/**
	 * Gets the trigger end time. If there is no end time null is returned. The end time is always
	 * exclusive.
	 * 
	 * @return the schedule end time
	 */
	public ZonedDateTime getEndTime()
	{
		return getTriggerConfig().getEndTime();
	}

	/**
	 * Gets the cronExpression.
	 * 
	 * @return the cronExpression
	 */
	public CronExpression getCronExpression()
	{
		return cronTimeGenerator.getCronExpression();
	}

	@Override
	public ZonedDateTime getFirstFireTime()
	{
		ZonedDateTime startTime = getStartTime();
		if (startTime != null)
		{
			TriggerEvent nextEvent = getNextTriggerEvent(null, startTime.minusSeconds(1L));
			if (nextEvent != null)
			{
				return nextEvent.getFireTime();
			}
		}
		return null;
	}

	@Override
	public ZonedDateTime getLastFireTime()
	{
		ZonedDateTime endTime = getEndTime();
		return endTime != null ? getPreviousFireTime(endTime) : null;
	}

	@Override
	public ZonedDateTime getNextFireTime(ZonedDateTime calculationTime)
	{
		return cronTimeGenerator.getNextTime(calculationTime, getStartTime(), getEndTime());
	}

	@Override
	public ZonedDateTime getPreviousFireTime(ZonedDateTime calculationTime)
	{
		return cronTimeGenerator.getPreviousTime(calculationTime, getStartTime(), getEndTime());
	}
}
