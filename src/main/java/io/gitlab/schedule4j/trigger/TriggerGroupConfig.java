/*
 * Created on 2019-09-12
 * 
 * Copyright 2019 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.trigger;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * @author Dirk Buchhorn
 */
public class TriggerGroupConfig implements TriggerConfig
{
	private static final long serialVersionUID = 8445882341630577139L;

	private final TriggerKey key;
	private Map<TriggerKey, TriggerConfig> triggerConfigMap = new HashMap<>();

	/**
	 * Creates a new {@link TriggerGroupConfig}.
	 * 
	 * @param key the trigger key
	 */
	public TriggerGroupConfig(TriggerKey key)
	{
		if (key == null)
		{
			throw new IllegalArgumentException("TriggerKey can't be null");
		}
		this.key = key;
	}

	/**
	 * Creates a new {@link TriggerGroupConfig}.
	 * 
	 * @param key the trigger key
	 * @param triggerConfigs the trigger configurations to add to this trigger group configuration
	 */
	public TriggerGroupConfig(TriggerKey key, TriggerConfig... triggerConfigs)
	{
		this(key);
		addInternal(triggerConfigs);
	}

	/**
	 * Creates a new {@link TriggerGroupConfig}.
	 * 
	 * @param key the trigger key
	 * @param triggerConfigs the trigger configurations to add to this trigger group configuration
	 */
	public TriggerGroupConfig(TriggerKey key, Collection<TriggerConfig> triggerConfigs)
	{
		this(key);
		addInternal(triggerConfigs);
	}

	private TriggerGroupConfig(TriggerKey key, Map<TriggerKey, TriggerConfig> triggerConfigMap)
	{
		this(key);
		this.triggerConfigMap.putAll(triggerConfigMap);
	}

	@Override
	public TriggerKey getKey()
	{
		return key;
	}

	/**
	 * Return all trigger keys of this group.
	 * 
	 * @return all trigger keys of this group
	 */
	public Set<TriggerKey> getTriggerKeys()
	{
		return Collections.unmodifiableSet(triggerConfigMap.keySet());
	}

	/**
	 * Counts the trigger configurations in this group.
	 * 
	 * @return the trigger configurations count in this group
	 */
	public int getSize()
	{
		return triggerConfigMap.size();
	}

	public Collection<TriggerConfig> getTriggerConfigs()
	{
		return triggerConfigMap.values();
	}

	public Optional<TriggerConfig> getTriggerConfig(TriggerKey key)
	{
		return Optional.ofNullable(triggerConfigMap.get(key));
	}

	/**
	 * CLones the trigger configuration and adds one or more trigger configurations to this trigger
	 * group configuration. If a trigger configuration with the {@link TriggerKey} is already in
	 * this group then the trigger configuration is replaced.
	 * 
	 * @param triggerConfigs the trigger configurations to add
	 * @return a new {@link TriggerGroupConfig} object
	 */
	public TriggerGroupConfig withAdd(TriggerConfig... triggerConfigs)
	{
		TriggerGroupConfig clone = new TriggerGroupConfig(this.key, this.triggerConfigMap);
		clone.addInternal(triggerConfigs);
		return clone;
	}

	private void addInternal(TriggerConfig... triggerConfigs)
	{
		if (triggerConfigs == null)
		{
			return;
		}
		for (int i = 0; i < triggerConfigs.length; i++)
		{
			addInternal(triggerConfigs[i], triggerConfigMap);
		}
	}

	/**
	 * CLones the trigger configuration and adds one or more trigger configurations to this trigger
	 * group configuration. If a trigger configuration with the {@link TriggerKey} is already in
	 * this group then the trigger configuration is replaced.
	 * 
	 * @param triggerConfigs the trigger configurations to add
	 * @return a new {@link TriggerGroupConfig} object
	 */
	public TriggerGroupConfig withAdd(Collection<TriggerConfig> triggerConfigs)
	{
		TriggerGroupConfig clone = new TriggerGroupConfig(this.key, this.triggerConfigMap);
		clone.addInternal(triggerConfigs);
		return clone;
	}

	private void addInternal(Collection<TriggerConfig> triggerConfigs)
	{
		if (triggerConfigs == null)
		{
			return;
		}
		for (TriggerConfig triggerConfig : triggerConfigs)
		{
			addInternal(triggerConfig, triggerConfigMap);
		}
	}

	private void addInternal(TriggerConfig triggerConfig, Map<TriggerKey, TriggerConfig> map)
	{
		if (triggerConfig == null)
		{
			return;
		}
		map.put(triggerConfig.getKey(), triggerConfig);
	}

	public TriggerGroupConfig withTriggerKey(TriggerKey key)
	{
		return new TriggerGroupConfig(key, this.triggerConfigMap);
	}

	@Override
	public TriggerGroup createTrigger() throws CreateTriggerException
	{
		return new TriggerGroup(this);
	}
}
