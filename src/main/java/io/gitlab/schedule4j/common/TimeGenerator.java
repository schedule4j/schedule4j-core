/*
 * Created on 20.03.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.common;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * @author Dirk Buchhorn
 */
public interface TimeGenerator extends Serializable
{
	/**
	 * Gets the first time or null if the time is not defined.
	 * 
	 * @param startTime the start time
	 * @param endTime the end time
	 * @return the first time or null if the time is not defined
	 */
	ZonedDateTime getFirstTime(ZonedDateTime startTime, ZonedDateTime endTime);

	/**
	 * Gets the last time or null if the time is not defined.
	 * 
	 * @param startTime the start time
	 * @param endTime the end time
	 * @return the last time or null if the time is not defined
	 */
	ZonedDateTime getLastTime(ZonedDateTime startTime, ZonedDateTime endTime);

	/**
	 * Gets the next time after the given time or null if there is no next time.
	 * 
	 * @param calculationTime the calculation time
	 * @param startTime the start time
	 * @param endTime the end time
	 * @return the next time or null if there is no next time
	 */
	ZonedDateTime getNextTime(ZonedDateTime calculationTime, ZonedDateTime startTime, ZonedDateTime endTime);

	/**
	 * Gets the previous time before the given time or null if there is no previous time.
	 * 
	 * @param calculationTime the calculation time
	 * @param startTime the start time
	 * @param endTime the end time
	 * @return the previous time or null if there is no previous time
	 */
	ZonedDateTime getPreviousTime(ZonedDateTime calculationTime, ZonedDateTime startTime, ZonedDateTime endTime);
}
