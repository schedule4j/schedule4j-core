/*
 * Created on 08.10.2015
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.common;

import java.io.Serializable;

/**
 * This is the base class for all key classes. This class and all it's sub classes are immutable.
 * 
 * @author Dirk Buchhorn
 */
public abstract class Key<K extends Key<K>> implements Comparable<K>, Serializable
{
	private static final long serialVersionUID = -6600522626069219992L;

	public static final String KEY_NAME_SEPARATOR = ".";

	protected final K parentKey;
	protected final String name;
	protected final String keyPath;

	/**
	 * Construct a new key with the given name.
	 * 
	 * @param name the key name
	 */
	protected Key(String name)
	{
		this(null, name);
	}

	/**
	 * Construct a new key with the given name and parent key.
	 * 
	 * @param parentKey the parent key
	 * @param name the key name
	 */
	protected Key(K parentKey, String name)
	{
		if (name == null)
		{
			throw new IllegalArgumentException("Key name can't be null!");
		}
		if (name.indexOf(KEY_NAME_SEPARATOR) >= 0)
		{
			throw new IllegalArgumentException("The '.' (dot) character is not allowed in key name!");
		}
		this.name = name;
		this.parentKey = parentKey;
		this.keyPath = parentKey != null ? parentKey.keyPath + name + KEY_NAME_SEPARATOR : name + KEY_NAME_SEPARATOR;
	}

	/**
	 * Gets the key name.
	 * 
	 * @return the key name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Gets the parent key.
	 * 
	 * @return the parent key or null
	 */
	public K getParentKey()
	{
		return parentKey;
	}

	/**
	 * Test if the given key is a parent key of this key.
	 * 
	 * @param parent the parent key
	 * @return true if the given key is a parent key of this key, false otherwise
	 */
	public boolean isParent(K parent)
	{
		if (this.parentKey == parent)
		{
			return true;
		}
		else if (this.parentKey != null && parent != null)
		{
			return this.parentKey.keyPath.startsWith(parent.keyPath);
		}
		return false;
	}

	@Override
	public int hashCode()
	{
		return keyPath.hashCode();
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		@SuppressWarnings("unchecked")
		Key<K> other = (Key<K>) obj;
		return keyPath.equals(other.keyPath);
	}

	@Override
	public int compareTo(K key)
	{
		String strThis = toString();
		String strOther = key.toString();
		return strThis.compareTo(strOther);
	}

	@Override
	public String toString()
	{
		return toString(KEY_NAME_SEPARATOR);
	}

	private String toString(String separator)
	{
		StringBuilder sb = new StringBuilder();
		fillIn(sb, separator);
		return sb.toString();
	}

	/**
	 * Builds the string representation of this key.
	 * 
	 * @param sb the {@link StringBuilder}
	 * @param separator the separator to use
	 */
	protected final void fillIn(StringBuilder sb, String separator)
	{
		if (getParentKey() != null)
		{
			getParentKey().fillIn(sb, separator);
			sb.append(separator);
		}
		sb.append(getName());
	}
}
