/*
 * Created on 02.06.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.common;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;

/**
 * @author Dirk Buchhorn
 */
public class SimpleTimeGenerator implements TimeGenerator
{
	private static final long serialVersionUID = 1180252739513743979L;

	private int repeatCount;
	private long repeatInterval;

	/**
	 * Creates a new {@link SimpleTimeGenerator} with the given repeat count and repeat interval.
	 * 
	 * @param repeatCount how often the generator should fire
	 * @param repeatInterval the time between two generator times in milliseconds, must be greater
	 *        than 0
	 */
	public SimpleTimeGenerator(int repeatCount, long repeatInterval)
	{
		setRepeatCount(repeatCount);
		setRepeatInterval(repeatInterval);
	}

	/**
	 * How often the generator should fire.
	 * 
	 * @return how often the generator should fire
	 */
	public int getRepeatCount()
	{
		return repeatCount;
	}

	/**
	 * Sets the repeat count.
	 * 
	 * @param repeatCount the repeatCount to set
	 */
	public void setRepeatCount(int repeatCount)
	{
		checkRepeatCount(repeatCount);
		this.repeatCount = repeatCount;
	}

	/**
	 * The time between two generator times in milliseconds.
	 * 
	 * @return the time between two generator times in milliseconds
	 */
	public long getRepeatInterval()
	{
		return repeatInterval;
	}

	/**
	 * Sets the repeat interval.
	 * 
	 * @param repeatInterval the repeatInterval to set, must be greater than 0
	 */
	public void setRepeatInterval(long repeatInterval)
	{
		checkRepeatInterval(repeatInterval);
		this.repeatInterval = repeatInterval;
	}

	@Override
	public ZonedDateTime getFirstTime(ZonedDateTime startTime, ZonedDateTime endTime)
	{
		return startTime;
	}

	@Override
	public ZonedDateTime getLastTime(ZonedDateTime startTime, ZonedDateTime endTime)
	{
		checkStartTimeAndEndTime(startTime, endTime);
		// calculate the last fire time
		ZonedDateTime lastFireTime = startTime.plusNanos(repeatCount * repeatInterval * 1000000L);
		if (endTime != null && !lastFireTime.isBefore(endTime))
		{
			// endTime is exclusive -> subtract one millisecond more
			long delta = (endTime.getLong(ChronoField.INSTANT_SECONDS)
				- startTime.getLong(ChronoField.INSTANT_SECONDS)) * 1000L - 1
				+ (endTime.getLong(ChronoField.NANO_OF_SECOND)
					- startTime.getLong(ChronoField.NANO_OF_SECOND)) / 1000000L;
			// repeatInterval can't be 0
			int repeatIndex = (int) (delta / repeatInterval);
			repeatIndex = Math.min(repeatIndex, repeatCount);
			lastFireTime = startTime.plusNanos(repeatIndex * repeatInterval * 1000000L);
		}
		return lastFireTime;
	}

	@Override
	public ZonedDateTime getNextTime(ZonedDateTime calculationTime, ZonedDateTime startTime,
		ZonedDateTime endTime)
	{
		checkStartTimeAndEndTime(startTime, endTime);
		ZonedDateTime result = null;
		long delta = (calculationTime.getLong(ChronoField.INSTANT_SECONDS)
			- startTime.getLong(ChronoField.INSTANT_SECONDS)) * 1000L
			+ (calculationTime.getLong(ChronoField.NANO_OF_SECOND)
				- startTime.getLong(ChronoField.NANO_OF_SECOND)) / 1000000L;
		int repeatIndex = (int) ((delta / repeatInterval) + 1);
		repeatIndex = Math.max(repeatIndex, 0);
		if (repeatIndex <= repeatCount)
		{
			ZonedDateTime scheduleTime = startTime
				.plusNanos(repeatIndex * repeatInterval * 1000000L);
			if ((endTime == null || scheduleTime.isBefore(endTime))
				&& scheduleTime.isAfter(calculationTime))
			{
				result = scheduleTime;
			}
		}
		return result;
	}

	@Override
	public ZonedDateTime getPreviousTime(ZonedDateTime calculationTime, ZonedDateTime startTime,
		ZonedDateTime endTime)
	{
		checkStartTimeAndEndTime(startTime, endTime);
		ZonedDateTime result = null;
		// dateTime is exclusive -> subtract one millisecond more
		long delta = (calculationTime.getLong(ChronoField.INSTANT_SECONDS)
			- startTime.getLong(ChronoField.INSTANT_SECONDS)) * 1000L - 1
			+ (calculationTime.getLong(ChronoField.NANO_OF_SECOND)
				- startTime.getLong(ChronoField.NANO_OF_SECOND)) / 1000000L;
		int repeatIndex = (int) (delta / repeatInterval);
		repeatIndex = Math.min(repeatIndex, repeatCount);
		// we must check the delta, because it can be less than 0 and the repeatIndex is 0
		if (repeatIndex >= 0 && delta > 0)
		{
			ZonedDateTime scheduleTime = startTime
				.plusNanos(repeatIndex * repeatInterval * 1000000L);
			if (endTime == null || scheduleTime.isBefore(endTime))
			{
				result = scheduleTime;
			}
			else
			{
				result = getLastTime(startTime, endTime);
			}
		}
		return result;
	}

	private void checkStartTimeAndEndTime(ZonedDateTime startTime, ZonedDateTime endTime)
	{
		if (startTime == null)
		{
			throw new IllegalArgumentException("startTime can't be null!");
		}
		if (endTime != null && endTime.isBefore(startTime))
		{
			throw new IllegalArgumentException("endTime can't be before startTime!");
		}
	}

	/**
	 * Checks the repeat count.
	 * 
	 * @param repeatCount the repeat count to check
	 */
	public static void checkRepeatCount(int repeatCount)
	{
		if (repeatCount < 0)
		{
			throw new IllegalArgumentException("repeatCount must be greater or equals to zero!");
		}
	}

	/**
	 * Checks the repeat interval.
	 * 
	 * @param repeatInterval the repeat interval to check
	 */
	public static void checkRepeatInterval(long repeatInterval)
	{
		if (repeatInterval < 1)
		{
			throw new IllegalArgumentException("repeatInterval must be greater than zero!");
		}
	}

	@Override
	public String toString()
	{
		return getClass().getSimpleName() + "(count=" + repeatCount + ", interval=" + repeatInterval
			+ ")";
	}
}
