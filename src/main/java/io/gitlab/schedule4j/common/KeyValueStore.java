/*
 * Created on 23.10.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.common;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * @author Dirk Buchhorn
 */
public class KeyValueStore<K extends Key<K>, V>
{
	private Map<K, V> keyValueMap = new HashMap<>();

	/**
	 * Adds a key value pair to this store. If the store already contains the given key then the value is not changed
	 * and false is returned.
	 * 
	 * @param key the key
	 * @param value the value
	 * @return true if the value could be added, false otherwise
	 */
	public boolean add(K key, V value)
	{
		final boolean[] added = new boolean[] {false};
		keyValueMap.computeIfAbsent(key, k ->
		{
			added[0] = true;
			return value;
		});
		return added[0];
	}

	/**
	 * Clears the store.
	 */
	public void clear()
	{
		keyValueMap.clear();
	}

	/**
	 * Returns true id the store contains the given key.
	 * 
	 * @param key the key to check
	 * @return true if the store contains the given key
	 */
	public boolean containsKey(K key)
	{
		return keyValueMap.containsKey(key);
	}

	/**
	 * Finds all values with the same key or where the given key is a parent key.
	 * 
	 * @param key the key to find
	 * @return all values with the same key or where the given key is a parent key
	 */
	public Collection<V> find(K key)
	{
		Collection<V> col = new LinkedList<>();
		for (Map.Entry<K, V> entry : keyValueMap.entrySet())
		{
			K k = entry.getKey();
			if (k.equals(key) || k.isParent(key))
			{
				col.add(entry.getValue());
			}
		}
		return col;
	}

	/**
	 * Gets the value for the given key.
	 * 
	 * @param key the key
	 * @return the value for the given key
	 */
	public V get(K key)
	{
		return keyValueMap.get(key);
	}

	public Collection<V> getAll()
	{
		Collection<V> col = new LinkedList<>();
		for (Map.Entry<K, V> entry : keyValueMap.entrySet())
		{
			col.add(entry.getValue());
		}
		return col;
	}

	/**
	 * Remove a key value pair from this store.
	 * 
	 * @param key the key
	 * @return the removed value
	 */
	public V remove(K key)
	{
		return keyValueMap.remove(key);
	}

	/**
	 * The store size.
	 * 
	 * @return the store size
	 */
	public int size()
	{
		return keyValueMap.size();
	}

	/**
	 * Updates a value for the given key. Return true if the value could be updated. If the store don't contains the
	 * given key then false is returned.
	 * 
	 * @param key the key
	 * @param value the value
	 * @return true if the value could be updated, false otherwise
	 */
	public boolean update(K key, V value)
	{
		final boolean[] updated = new boolean[] {false};
		keyValueMap.computeIfPresent(key, (k, v) ->
		{
			updated[0] = true;
			return value;
		});
		return updated[0];
	}
}
