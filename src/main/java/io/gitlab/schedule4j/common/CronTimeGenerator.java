/*
 * Created on 20.03.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.common;

import java.text.ParseException;
import java.time.ZonedDateTime;

import io.gitlab.schedule4j.cron.CronExpression;

/**
 * @author Dirk Buchhorn
 */
public class CronTimeGenerator implements TimeGenerator
{
	private static final long serialVersionUID = 8351784717109618310L;

	private CronExpression cronExpression;

	/**
	 * Creates a new {@link CronTimeGenerator} with the given cron expression.
	 * 
	 * @param cronExpression the cron expression for calculating time values
	 */
	public CronTimeGenerator(CronExpression cronExpression)
	{
		setCronExpression(cronExpression);
	}

	/**
	 * Creates a new {@link CronTimeGenerator} with the given cron expression.
	 * 
	 * @param cronExpression the cron expression for calculating time values
	 * @throws ParseException if the cron expression string can't be passed
	 */
	public CronTimeGenerator(String cronExpression) throws ParseException
	{
		this(new CronExpression(cronExpression));
	}

	/**
	 * Gets the cronExpression.
	 * 
	 * @return the cronExpression
	 */
	public CronExpression getCronExpression()
	{
		return cronExpression;
	}

	/**
	 * Sets the cron expression.
	 * 
	 * @param cronExpression the cron expression to set
	 */
	public void setCronExpression(CronExpression cronExpression)
	{
		if (cronExpression == null)
		{
			throw new IllegalArgumentException("CronExpression can't be null!");
		}
		this.cronExpression = cronExpression;
	}

	@Override
	public ZonedDateTime getFirstTime(ZonedDateTime startTime, ZonedDateTime endTime)
	{
		return startTime != null ? getNextTime(startTime.minusSeconds(1L), startTime, endTime)
			: null;
	}

	@Override
	public ZonedDateTime getLastTime(ZonedDateTime startTime, ZonedDateTime endTime)
	{
		return endTime != null ? getPreviousTime(endTime, startTime, endTime) : null;
	}

	@Override
	public ZonedDateTime getNextTime(ZonedDateTime calculationTime, ZonedDateTime startTime,
		ZonedDateTime endTime)
	{
		if (calculationTime == null)
		{
			return null;
		}
		if (startTime != null && calculationTime.isBefore(startTime))
		{
			calculationTime = startTime.minusSeconds(1L);
		}
		ZonedDateTime fireTime = cronExpression.getNextTime(calculationTime);
		if (endTime != null && !fireTime.isBefore(endTime))
		{
			fireTime = null;
		}
		return fireTime;
	}

	@Override
	public ZonedDateTime getPreviousTime(ZonedDateTime calculationTime, ZonedDateTime startTime,
		ZonedDateTime endTime)
	{
		if (calculationTime == null)
		{
			return null;
		}
		if (endTime != null && calculationTime.isAfter(endTime))
		{
			calculationTime = endTime;
		}
		ZonedDateTime result = cronExpression.getPreviousTime(calculationTime);
		if (startTime != null && result.isBefore(startTime))
		{
			result = null;
		}
		return result;
	}

	@Override
	public String toString()
	{
		return getClass().getSimpleName() + "(" + cronExpression.getCronExpression() + ")";
	}
}
