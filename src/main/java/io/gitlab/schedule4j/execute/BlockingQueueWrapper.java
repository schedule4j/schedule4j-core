/*
 * Created on 16.07.2017
 * 
 * Copyright 2017 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.execute;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import io.gitlab.schedule4j.StandardScheduler;

/**
 * <p>
 * This class wraps another {@link BlockingQueue}. The generic type of the wrapped queue must be a
 * sub type of this queue.
 * </p>
 * <p>
 * For example, the {@link StandardScheduler} need a {@link BlockingQueue} where the generic type
 * must be a {@link JobExecutor}. If we want to run the jobs with a {@link ThreadPoolExecutor} then
 * we can't use the scheduler queue directly, because the {@link ThreadPoolExecutor} accepts only a
 * {@link BlockingQueue} of {@link Runnable}s. With this wrapper the scheduler queue can be used
 * with a {@link ThreadPoolExecutor}.
 * </p>
 * 
 * @param <S> the generic type of this queue
 * @param <T> the generic type of the wrapped queue must be a sub type of S
 * @author Dirk Buchhorn
 */
public class BlockingQueueWrapper<S, T extends S> implements BlockingQueue<S>
{
	private final BlockingQueue<T> queue;

	/**
	 * Create a new BlockingQueueWrapper.
	 * 
	 * @param queue the {@link BlockingQueue} to wrap
	 */
	public BlockingQueueWrapper(BlockingQueue<T> queue)
	{
		if (queue == null)
		{
			throw new IllegalArgumentException("Queue can't be null");
		}
		this.queue = queue;
	}

	@Override
	public boolean add(S e)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(Collection<? extends S> c)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear()
	{
		queue.clear();
	}

	@Override
	public boolean contains(Object o)
	{
		return queue.contains(o);
	}

	@Override
	public boolean containsAll(Collection<?> c)
	{
		return queue.containsAll(c);
	}

	@Override
	public int drainTo(Collection<? super S> c)
	{
		return queue.drainTo(c);
	}

	@Override
	public int drainTo(Collection<? super S> c, int maxElements)
	{
		return queue.drainTo(c, maxElements);
	}

	@Override
	public S element()
	{
		return queue.element();
	}

	@Override
	public boolean isEmpty()
	{
		return queue.isEmpty();
	}

	@Override
	public Iterator<S> iterator()
	{
		return new Itr(toArray());
	}

	@Override
	public boolean offer(S e)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean offer(S e, long timeout, TimeUnit unit) throws InterruptedException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public S peek()
	{
		return queue.peek();
	}

	@Override
	public S poll()
	{
		return queue.poll();
	}

	@Override
	public S poll(long timeout, TimeUnit unit) throws InterruptedException
	{
		return queue.poll(timeout, unit);
	}

	@Override
	public void put(S e) throws InterruptedException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public int remainingCapacity()
	{
		return queue.remainingCapacity();
	}

	@Override
	public S remove()
	{
		return queue.remove();
	}

	@Override
	public boolean remove(Object o)
	{
		return removeInternal(o);
	}

	private boolean removeInternal(Object o)
	{
		return queue.remove(o);
	}

	@Override
	public boolean removeAll(Collection<?> c)
	{
		return queue.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public int size()
	{
		return queue.size();
	}

	@Override
	public S take() throws InterruptedException
	{
		return queue.take();
	}

	@Override
	public Object[] toArray()
	{
		return queue.toArray();
	}

	@Override
	public <U> U[] toArray(U[] a)
	{
		return queue.toArray(a);
	}

	private class Itr implements Iterator<S>
	{
		final Object[] array;
		int cursor;
		int lastCursor;

		Itr(Object[] array)
		{
			this.lastCursor = -1;
			this.array = array;
		}

		@Override
		public boolean hasNext()
		{
			return cursor < array.length;
		}

		@SuppressWarnings("unchecked")
		@Override
		public S next()
		{
			if (cursor >= array.length)
			{
				throw new NoSuchElementException();
			}
			lastCursor = cursor;
			return (S) array[cursor++];
		}

		@Override
		public void remove()
		{
			if (lastCursor < 0)
			{
				throw new IllegalStateException();
			}
			removeInternal(array[lastCursor]);
			lastCursor = -1;
		}
	}
}
