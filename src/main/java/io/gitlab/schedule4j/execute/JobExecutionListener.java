/*
 * Created on 02.09.2017
 * 
 * Copyright 2017 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.execute;

import io.gitlab.schedule4j.Scheduler;
import io.gitlab.schedule4j.job.Job;
import io.gitlab.schedule4j.job.JobExecutionContext;

/**
 * A JobExecutionListener is called from a job executor (e.g. {@link JobExecutor}). A {@link Scheduler}
 * register an execution listener, so he is notified when a job is running, is executed or can't be executed.
 * 
 * @author Dirk Buchhorn
 */
public interface JobExecutionListener
{
	/**
	 * Called before the {@link Job} start execution.
	 * 
	 * @param ctx the {@link JobExecutionContext}
	 */
	void jobRunning(JobExecutionContext ctx);

	/**
	 * Called after a {@link Job} was executed.
	 * 
	 * @param ctx the {@link JobExecutionContext}
	 * @param exception the caught {@link Exception} if there is one
	 */
	void jobExecuted(JobExecutionContext ctx, Exception exception);

	/**
	 * Called if a {@link Job} could not be executed. This can happen if no new job instance could be created
	 * or an executor can not execute the job.
	 * 
	 * @param ctx the {@link JobExecutionContext}
	 * @param exception the caught {@link Exception} if there is one
	 */
	void jobNotExecuted(JobExecutionContext ctx, Exception exception);
}
