/*
 * Created on 02.09.2017
 * 
 * Copyright 2017 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.execute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.gitlab.schedule4j.job.Job;
import io.gitlab.schedule4j.job.JobExecutionContext;
import io.gitlab.schedule4j.priority.Priority;

/**
 * An executor for running the scheduled jobs.
 * 
 * @author Dirk Buchhorn
 */
public class JobExecutor implements Priority, Runnable
{
	private static final Logger logger = LoggerFactory.getLogger(JobExecutor.class);
	private static final byte FIRE_JOB_RUNNING_CALLED = 1;
	private static final byte FIRE_JOB_EXECUTED_CALLED = 2;
	private static final byte FIRE_JOB_NOT_EXECUTED_CALLED = 4;

	private final JobExecutionContext ctx;
	private final JobExecutionListener listener;
	private volatile byte fireFlag = 0;
	private int priority;

	/**
	 * Creates a new job executor. The listener is used to notify the scheduler about the execution state.
	 * 
	 * @param ctx the job execution context
	 * @param listener the job execution listener
	 */
	public JobExecutor(JobExecutionContext ctx, JobExecutionListener listener)
	{
		if (ctx == null)
		{
			throw new IllegalArgumentException("JobExecutionContext can't be null");
		}
		if (listener == null)
		{
			throw new IllegalArgumentException("JobExecutionListener can't be null");
		}
		this.ctx = ctx;
		this.listener = listener;
		this.priority = ctx.getJobDescriptor().getPriority();
	}

	@Override
	public void run()
	{
		fireJobRunning();
		logger.debug("Run job {}", ctx.getJobDescriptor().getKey());
		try
		{
			Job job = ctx.getJobDescriptor().getJobClass().getDeclaredConstructor().newInstance();
			job.execute(ctx);
			fireJobExecuted(null);
		}
		catch (Exception e)
		{
			fireJobExecuted(e);
		}
	}

	@Override
	public int getPriority()
	{
		return priority;
	}

	@Override
	public void setPriority(int priority)
	{
		this.priority = priority;
	}

	@Override
	public void plusPriority(int priority)
	{
		this.priority += priority;
	}

	@Override
	public void minusPriority(int priority)
	{
		this.priority -= priority;
	}

	/**
	 * Returns the {@link JobExecutionContext}.
	 * 
	 * @return the {@link JobExecutionContext}
	 */
	public JobExecutionContext getJobExecutionContext()
	{
		return ctx;
	}

	private boolean wasFireJobRunningCalled()
	{
		return (fireFlag & FIRE_JOB_RUNNING_CALLED) == FIRE_JOB_RUNNING_CALLED;
	}

	private boolean wasFireJobExecutedCalled()
	{
		return (fireFlag & FIRE_JOB_EXECUTED_CALLED) == FIRE_JOB_EXECUTED_CALLED;
	}

	private boolean wasFireJobNotExecutedCalled()
	{
		return (fireFlag & FIRE_JOB_NOT_EXECUTED_CALLED) == FIRE_JOB_NOT_EXECUTED_CALLED;
	}

	/**
	 * Called before the job execute method is called.
	 */
	public void fireJobRunning()
	{
		if (fireFlag == 0)
		{
			try
			{
				fireFlag = FIRE_JOB_RUNNING_CALLED;
				listener.jobRunning(ctx);
			}
			catch (Exception e)
			{
				logger.error("Exception occurs on method call JobExecutionListener.jobRunning() (class="
					+ listener.getClass().getName() + ") Message: " + e.getMessage(), e);
			}
		}
		else
		{
			if (wasFireJobRunningCalled())
			{
				logger.warn("Method fireJobRunning can only be called once");
			}
			if (wasFireJobNotExecutedCalled())
			{
				logger.warn("Method fireJobNotExecuted was already called");
			}
		}
	}

	/**
	 * Called after the job execute method was called. If the exception is not null then the jobs execute method throws
	 * an error.
	 * 
	 * @param exception the exception thrown by the job
	 */
	public void fireJobExecuted(Exception exception)
	{
		if (wasFireJobNotExecutedCalled())
		{
			logger.warn("Method fireJobNotExecuted was already called");
		}
		else if (!wasFireJobExecutedCalled())
		{
			try
			{
				if (!wasFireJobRunningCalled())
				{
					fireJobRunning();
					logger.warn("Method fireJobRunning was not called, call it for you now!");
				}
				fireFlag = FIRE_JOB_RUNNING_CALLED + FIRE_JOB_EXECUTED_CALLED;
				listener.jobExecuted(ctx, exception);
			}
			catch (Exception e)
			{
				logger.error("Exception occurs on method call JobExecutionListener.jobExecuted() (class="
					+ listener.getClass().getName() + ")", e);
			}
		}
		else
		{
			logger.warn("Method fireJobExecuted can only be called once");
		}
	}

	/**
	 * Called if the job could not be executed. If the exception is not null then this can be the reason why the job
	 * could not be executed.
	 * 
	 * @param exception maybe the reason why the job could not be executed
	 */
	public void fireJobNotExecuted(Exception exception)
	{
		if (wasFireJobExecutedCalled())
		{
			logger.warn("Method fireJobExecuted was already called");
		}
		else if (!wasFireJobNotExecutedCalled())
		{
			try
			{
				fireFlag = FIRE_JOB_NOT_EXECUTED_CALLED;
				listener.jobNotExecuted(ctx, exception);
			}
			catch (Exception e)
			{
				logger.error("Exception occurs on method call JobExecutionListener.jobNotExecuted() (class="
					+ listener.getClass().getName() + ")", e);
			}
		}
		else
		{
			logger.warn("Method fireJobNotExecuted can only be called once");
		}
	}
}
