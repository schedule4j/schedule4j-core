/*
 * Created on 2020-12-16
 * 
 * Copyright 2020 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Dirk Buchhorn
 */
public class ScheduleListenerNotifier
{
	private static final Logger log = LoggerFactory.getLogger(ScheduleListenerNotifier.class);

	private Set<SchedulerListener> schedulerListeners = new HashSet<>();

	public int getSize()
	{
		return schedulerListeners.size();
	}

	public void addSchedulerListener(SchedulerListener listener)
	{
		if (listener == null)
		{
			return;
		}
		synchronized (schedulerListeners)
		{
			schedulerListeners.add(listener);
		}
	}

	public boolean removeSchedulerListener(SchedulerListener listener)
	{
		if (listener == null)
		{
			return false;
		}
		synchronized (schedulerListeners)
		{
			return schedulerListeners.remove(listener);
		}
	}

	private Collection<SchedulerListener> getSchedulerListeners()
	{
		// copy the collection in a new one to prevent a deadlock
		synchronized (schedulerListeners)
		{
			return new ArrayList<>(schedulerListeners);
		}
	}

	public void fireSchedulerStopped()
	{
		callListeners(SchedulerListener::schedulerStopped);
	}

	public void fireSchedulerPaused()
	{
		callListeners(SchedulerListener::schedulerPaused);
	}

	public void fireSchedulerResumed()
	{
		callListeners(SchedulerListener::schedulerResumed);
	}

	public void fireJobScheduled(ScheduledJob scheduledJob)
	{
		callListeners(sl -> sl.jobScheduled(scheduledJob));
	}

	public void fireJobUnscheduled(ScheduledJob scheduledJob)
	{
		callListeners(sl -> sl.jobUnscheduled(scheduledJob));
	}

	public void fireJobPaused(ScheduledJob scheduledJob)
	{
		callListeners(sl -> sl.jobPaused(scheduledJob));
	}

	public void fireJobResumed(ScheduledJob scheduledJob)
	{
		callListeners(sl -> sl.jobResumed(scheduledJob));
	}

	private void callListeners(Callable callable)
	{
		for (SchedulerListener schedulerListener : getSchedulerListeners())
		{
			// call it in a save way, because we don't know what the called method do
			try
			{
				callable.callFunction(schedulerListener);
			}
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
			}
		}
	}

	@FunctionalInterface
	private interface Callable
	{
		void callFunction(SchedulerListener schedulerListener);
	}
}
