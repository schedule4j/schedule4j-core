/*
 * Created on 01.03.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.gitlab.schedule4j.execute.BlockingQueueWrapper;
import io.gitlab.schedule4j.execute.JobExecutionListener;
import io.gitlab.schedule4j.execute.JobExecutor;
import io.gitlab.schedule4j.job.JobDescriptor;
import io.gitlab.schedule4j.job.JobExecutionContext;
import io.gitlab.schedule4j.job.JobKey;
import io.gitlab.schedule4j.job.JobListener;
import io.gitlab.schedule4j.job.JobListenerNotifier;
import io.gitlab.schedule4j.schedule.Schedule;
import io.gitlab.schedule4j.schedule.ScheduleEvent;
import io.gitlab.schedule4j.trigger.CreateTriggerException;
import io.gitlab.schedule4j.trigger.Trigger;
import io.gitlab.schedule4j.trigger.TriggerConfig;
import io.gitlab.schedule4j.trigger.TriggerEvent;
import io.gitlab.schedule4j.trigger.TriggerKey;

/**
 * @author Dirk Buchhorn
 */
public class StandardScheduler implements Scheduler
{
	private static final Logger log = LoggerFactory.getLogger(StandardScheduler.class);

	private final ReentrantLock globalLock = new ReentrantLock();
	private final BlockingQueue<JobExecutor> queue;
	private final Schedule schedule = new Schedule();
	private final SchedulerRunnable schedulerRunnable;
	private final Thread schedulerThread;
	private volatile boolean paused = false;
	private volatile boolean stopped = false;
	private ConcurrentHashMap<JobKey, ScheduledJob> scheduledJobMap = new ConcurrentHashMap<>();

	private JobListenerNotifier jobListenerNotifier = new JobListenerNotifier();
	private ScheduleListenerNotifier scheduleListenerNotifier = new ScheduleListenerNotifier();

	/**
	 * Creates a new scheduler. {@link JobExecutor} objects are added to the given queue. The objects in the queue are processed from an
	 * executor (e.g. a {@link ThreadPoolExecutor}).
	 * 
	 * @param queue a {@link BlockingQueue} to add {@link JobExecutor} objects
	 */
	public StandardScheduler(BlockingQueue<JobExecutor> queue)
	{
		if (queue == null)
		{
			throw new IllegalArgumentException("Parameter queue can't be null");
		}
		this.queue = queue;
		schedulerRunnable = new SchedulerRunnable();
		schedulerThread = new Thread(schedulerRunnable);
		schedulerThread.start();
	}

	/**
	 * Returns a {@link BlockingQueue} of type {@link Runnable}.
	 * 
	 * @return a {@link BlockingQueue} of type {@link Runnable}
	 */
	public BlockingQueue<Runnable> getRunnableQueue()
	{
		return new BlockingQueueWrapper<>(queue);
	}

	/**
	 * Returns the {@link BlockingQueue}.
	 * 
	 * @return the {@link BlockingQueue}
	 */
	public BlockingQueue<JobExecutor> getQueue()
	{
		return queue;
	}

	@Override
	public void stop()
	{
		final ReentrantLock lock = this.globalLock;
		lock.lock();
		try
		{
			if (!stopped)
			{
				log.info("Stop signal received");
				// wake up scheduler thread
				schedulerRunnable.wakeup();
				try
				{
					schedulerThread.join(500);
				}
				catch (InterruptedException e)
				{
					Thread.currentThread().interrupt();
				}
				// call listeners
				scheduleListenerNotifier.fireSchedulerStopped();
			}
			else
			{
				log.info("Scheduler already stopped");
			}
		}
		finally
		{
			stopped = true;
			lock.unlock();
		}
	}

	@Override
	public boolean isStopped()
	{
		return stopped;
	}

	@Override
	public boolean isPaused()
	{
		return paused;
	}

	@Override
	public void pause()
	{
		final ReentrantLock lock = this.globalLock;
		lock.lock();
		try
		{
			if (!paused)
			{
				paused = true;
				log.info("Scheduler paused");
				scheduleListenerNotifier.fireSchedulerPaused();
			}
			else
			{
				log.info("Scheduler already paused");
			}
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public void pauseJob(JobKey jobKey)
	{
		ScheduledJob job = scheduledJobMap.get(jobKey);
		if (job != null)
		{
			synchronized (job)
			{
				if (!job.isJobPaused())
				{
					job.setJobPaused(true);
					scheduleListenerNotifier.fireJobPaused(job);
				}
			}
		}
	}

	@Override
	public void pauseGroup(JobKey jobGroupKey)
	{
		for (Entry<JobKey, ScheduledJob> entry : scheduledJobMap.entrySet())
		{
			if (entry.getKey().isParent(jobGroupKey))
			{
				ScheduledJob job = entry.getValue();
				synchronized (job)
				{
					if (!job.isJobPaused())
					{
						job.setJobPaused(true);
						scheduleListenerNotifier.fireJobPaused(job);
					}
				}
			}
		}
	}

	@Override
	public void pauseJob(TriggerKey triggerKey)
	{
		for (ScheduledJob job : scheduledJobMap.values())
		{
			synchronized (job)
			{
				if (job.getTrigger().getKey().equals(triggerKey) && !job.isJobPaused())
				{
					job.setJobPaused(true);
					scheduleListenerNotifier.fireJobPaused(job);
				}
			}
		}
	}

	@Override
	public void pauseGroup(TriggerKey triggerGroupKey)
	{
		for (ScheduledJob job : scheduledJobMap.values())
		{
			synchronized (job)
			{
				if (job.getTrigger().getKey().isParent(triggerGroupKey) && !job.isJobPaused())
				{
					job.setJobPaused(true);
					scheduleListenerNotifier.fireJobPaused(job);
				}
			}
		}
	}

	@Override
	public void resume()
	{
		final ReentrantLock lock = this.globalLock;
		lock.lock();
		try
		{
			if (paused)
			{
				paused = false;
				log.info("Scheduler resumed");
				scheduleListenerNotifier.fireSchedulerResumed();
			}
			else
			{
				log.info("Scheduler already running");
			}
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public void resumeJob(JobKey jobKey)
	{
		ScheduledJob job = scheduledJobMap.get(jobKey);
		if (job != null)
		{
			synchronized (job)
			{
				if (job.isJobPaused())
				{
					job.setJobPaused(false);
					scheduleListenerNotifier.fireJobResumed(job);
				}
			}
		}
	}

	@Override
	public void resumeGroup(JobKey jobGroupKey)
	{
		for (Entry<JobKey, ScheduledJob> entry : scheduledJobMap.entrySet())
		{
			if (entry.getKey().isParent(jobGroupKey))
			{
				ScheduledJob job = entry.getValue();
				synchronized (job)
				{
					if (job.isJobPaused())
					{
						job.setJobPaused(false);
						scheduleListenerNotifier.fireJobResumed(job);
					}
				}
			}
		}
	}

	@Override
	public void resumeJob(TriggerKey triggerKey)
	{
		for (ScheduledJob job : scheduledJobMap.values())
		{
			if (job.getTrigger().getKey().equals(triggerKey) && job.isJobPaused())
			{
				job.setJobPaused(false);
				scheduleListenerNotifier.fireJobResumed(job);
			}
		}
	}

	@Override
	public void resumeGroup(TriggerKey triggerGroupKey)
	{
		for (ScheduledJob job : scheduledJobMap.values())
		{
			if (job.getTrigger().getKey().isParent(triggerGroupKey) && job.isJobPaused())
			{
				job.setJobPaused(false);
				scheduleListenerNotifier.fireJobResumed(job);
			}
		}
	}

	@Override
	public int getScheduledJobCount()
	{
		return scheduledJobMap.size();
	}

	@Override
	public boolean schedule(JobDescriptor jobDescriptor) throws CreateTriggerException
	{
		return schedule(jobDescriptor, new JobListener[0]);
	}

	public boolean schedule(JobDescriptor jobDescriptor, JobListener... jobListeners) throws CreateTriggerException
	{
		// scheduled one
		Trigger trigger = jobDescriptor.getTriggerConfig().createTrigger();
		ScheduledJob scheduledJob = new ScheduledJob(jobDescriptor, trigger);
		if (jobListeners != null && jobListeners.length > 0)
		{
			Arrays.stream(jobListeners).filter(Objects::nonNull).forEach(scheduledJob::addJobListener);
		}
		boolean scheduled = false;
		if (!stopped)
		{
			synchronized (scheduledJob)
			{
				ScheduledJob previousValue = scheduledJobMap.putIfAbsent(jobDescriptor.getKey(), scheduledJob);
				if (previousValue != null)
				{
					throw new IllegalArgumentException("A job with the key '" + jobDescriptor.getKey() + "' is already scheduled");
				}
				// calculate the next trigger event and add a schedule event to the schedule
				TriggerEvent triggerEvent = trigger.getNextTriggerEvent(null, ZonedDateTime.now());
				if (triggerEvent != null)
				{
					// add to the schedule
					ScheduleEvent scheduleEvent = new ScheduleEvent(scheduledJob, triggerEvent);
					scheduled = schedule.add(scheduleEvent);
					// wake up scheduler thread
					schedulerRunnable.wakeup();
					// call schedule listeners
					scheduleListenerNotifier.fireJobScheduled(scheduledJob);
				}
				else
				{
					// remove from scheduledJobMap
					scheduledJobMap.remove(jobDescriptor.getKey());
				}
			}
		}
		return scheduled;
	}

	@Override
	public boolean unschedule(JobKey jobKey)
	{
		ScheduledJob scheduledJob = scheduledJobMap.get(jobKey);
		boolean unscheduled = false;
		if (scheduledJob != null)
		{
			synchronized (scheduledJob)
			{
				// mark job as unscheduled
				scheduledJob.setUnscheduled();
				// remove from schedule
				schedule.remove(jobKey);
				unscheduleInternal(scheduledJob);
				unscheduled = true;
			}
		}
		return unscheduled;
	}

	private void unscheduleInternal(ScheduledJob scheduledJob)
	{
		ScheduledJobStatistics stat = scheduledJob.getScheduledJobStatistics();
		// check if jobs are waiting or running
		if (stat.getWaitingCount() == 0 && stat.getRunningCount() == 0)
		{
			scheduledJobMap.remove(scheduledJob.getJobKey());
			// call schedule listeners
			scheduleListenerNotifier.fireJobUnscheduled(scheduledJob);
		}
	}

	@Override
	public boolean update(JobDescriptor jobDescriptor) throws CreateTriggerException
	{
		ScheduledJob scheduledJob = scheduledJobMap.get(jobDescriptor.getKey());
		if (scheduledJob != null)
		{
			synchronized (scheduledJob)
			{
				scheduledJob.setJobDescriptor(jobDescriptor);
				Trigger trigger = jobDescriptor.getTriggerConfig().createTrigger();
				scheduledJob.setTrigger(trigger);
				triggerChangedInternal(scheduledJob);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean updateTrigger(JobKey jobKey, TriggerConfig triggerConfig) throws CreateTriggerException
	{
		ScheduledJob scheduledJob = scheduledJobMap.get(jobKey);
		if (scheduledJob != null)
		{
			synchronized (scheduledJob)
			{
				JobDescriptor jobDescriptor = scheduledJob.getJobDescriptor();
				JobDescriptor newJobDescriptor = jobDescriptor.withTriggerConfig(triggerConfig);
				scheduledJob.setTrigger(triggerConfig.createTrigger());
				scheduledJob.setJobDescriptor(newJobDescriptor);
				triggerChangedInternal(scheduledJob);
				return true;
			}
		}
		return false;
	}

	private void triggerChangedInternal(ScheduledJob scheduledJob)
	{
		if (!scheduledJob.isUnscheduled())
		{
			// calculate a new next trigger event
			TriggerEvent nextTriggerEvent = scheduledJob.getTrigger().getNextTriggerEvent(null, ZonedDateTime.now());
			if (nextTriggerEvent != null)
			{
				scheduledJob.setNextScheduleFireTime(nextTriggerEvent.getFireTime());
				ScheduleEvent nextScheduleEvent = new ScheduleEvent(scheduledJob, nextTriggerEvent);
				if (schedule.replace(nextScheduleEvent))
				{
					// wake up scheduler thread
					schedulerRunnable.wakeup();
				}
			}
			else
			{
				scheduledJob.setNextScheduleFireTime(null);
				unschedule(scheduledJob.getJobKey());
			}
		}
	}

	@Override
	public boolean runJobNow(JobKey jobKey)
	{
		ScheduledJob scheduledJob = scheduledJobMap.get(jobKey);
		if (scheduledJob != null)
		{
			synchronized (scheduledJob)
			{
				ZonedDateTime now = ZonedDateTime.now();
				ScheduleEvent event = new ScheduleEvent(scheduledJob, new TriggerEvent(now, now));
				if (schedule.replace(event))
				{
					// wake up scheduler thread
					schedulerRunnable.wakeup();
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public Optional<JobDescriptor> getJobDescriptor(JobKey jobKey)
	{
		ScheduledJob job = scheduledJobMap.get(jobKey);
		return job != null ? Optional.of(job.getJobDescriptor()) : Optional.empty();
	}

	@Override
	public Collection<JobDescriptor> getJobDescriptors()
	{
		ArrayList<JobDescriptor> list = new ArrayList<>(scheduledJobMap.size());
		for (ScheduledJob scheduledJob : scheduledJobMap.values())
		{
			list.add(scheduledJob.getJobDescriptor());
		}
		return list;
	}

	@Override
	public Optional<ScheduledJobInfo> getScheduledJobInfo(JobKey jobKey)
	{
		ScheduledJob job = scheduledJobMap.get(jobKey);
		return job != null ? Optional.of(job.getScheduledJobInfo()) : Optional.empty();
	}

	@Override
	public Collection<ScheduledJobInfo> getScheduledJobInfos()
	{
		ArrayList<ScheduledJobInfo> list = new ArrayList<>(scheduledJobMap.size());
		for (ScheduledJob scheduledJob : scheduledJobMap.values())
		{
			// clone the ScheduledJobInfo
			list.add(new ScheduledJobInfo(scheduledJob.getScheduledJobInfo()));
		}
		return list;
	}

	@Override
	public void addJobListener(JobListener listener)
	{
		jobListenerNotifier.addJobListener(listener);
	}

	@Override
	public boolean addJobListener(JobKey jobKey, JobListener listener)
	{
		ScheduledJob scheduledJob = scheduledJobMap.get(jobKey);
		if (scheduledJob != null)
		{
			synchronized (scheduledJob)
			{
				scheduledJob.addJobListener(listener);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean removeJobListener(JobListener listener)
	{
		return jobListenerNotifier.removeJobListener(listener);
	}

	@Override
	public boolean removeJobListener(JobKey jobKey, JobListener listener)
	{
		ScheduledJob scheduledJob = scheduledJobMap.get(jobKey);
		if (scheduledJob != null)
		{
			synchronized (scheduledJob)
			{
				return scheduledJob.removeJobListener(listener);
			}
		}
		return false;
	}

	@Override
	public void addSchedulerListener(SchedulerListener listener)
	{
		scheduleListenerNotifier.addSchedulerListener(listener);
	}

	@Override
	public boolean removeSchedulerListener(SchedulerListener listener)
	{
		return scheduleListenerNotifier.removeSchedulerListener(listener);
	}

	protected class SchedulerRunnable implements Runnable
	{
		private static final long MAX_WAIT_TIME_SECONDS = 10L;
		private static final long MAX_WAIT_TIME = MAX_WAIT_TIME_SECONDS * 1000L;

		private ReentrantLock lock = new ReentrantLock();
		private Condition wait = lock.newCondition();

		private void logInformations(List<ScheduleEvent> list)
		{
			if (log.isDebugEnabled())
			{
				int eventCount = list.size();
				log.debug("eventCount={}, queueSize={}", eventCount, queue.size());
				if (eventCount > 0)
				{
					log.debug("events: {}", list);
				}
			}
		}

		protected long calculateWaitTime()
		{
			long waitTime = MAX_WAIT_TIME;
			ZonedDateTime now = ZonedDateTime.now();
			Optional<ZonedDateTime> nextTime = schedule.getFirstScheduleTime();
			if (nextTime.isPresent())
			{
				if (nextTime.get().isBefore(now))
				{
					waitTime = -1l;
				}
				else
				{
					Duration duration = Duration.between(now, nextTime.get());
					waitTime = duration.toMillis();
				}
			}
			if (waitTime >= MAX_WAIT_TIME)
			{
				// calculate wait time
				long seconds = now.getSecond() % MAX_WAIT_TIME_SECONDS;
				ZonedDateTime nextRunTime = now.minusNanos(now.getNano()).plusSeconds(MAX_WAIT_TIME_SECONDS - seconds);
				Duration duration = Duration.between(now, nextRunTime);
				waitTime = duration.toMillis();
			}
			return waitTime;
		}

		@Override
		public void run()
		{
			while (!Thread.interrupted())
			{
				ZonedDateTime calculationTime = ZonedDateTime.now();
				List<ScheduleEvent> list = schedule.getScheduleEvents(calculationTime);
				logInformations(list);
				for (ScheduleEvent event : list)
				{
					ScheduleEventWorker worker = new ScheduleEventWorker(event, calculationTime);
					worker.process();
				}
				// calculate wait time
				long waitTime = calculateWaitTime();
				if (waitTime <= 0)
				{
					continue;
				}
				try
				{
					lock.lock();
					if (!Thread.currentThread().isInterrupted())
					{
						log.debug("wait({})", waitTime);
						if (wait.await(waitTime, TimeUnit.MILLISECONDS))
						{
							log.debug("wait finished");
						}
						else
						{
							log.debug("wakeup signal received");
						}
					}
				}
				catch (InterruptedException e)
				{
					// set the interrupted flag
					Thread.currentThread().interrupt();
				}
				finally
				{
					lock.unlock();
				}
			}
			log.info("Stop completed");
		}

		private void wakeup()
		{
			try
			{
				lock.lock();
				wait.signal();
			}
			finally
			{
				lock.unlock();
			}
		}
	}

	private class ScheduleEventWorker
	{
		private ScheduleEvent event;
		private ZonedDateTime calculationTime;

		public ScheduleEventWorker(ScheduleEvent event, ZonedDateTime calculationTime)
		{
			this.event = event;
			this.calculationTime = calculationTime;
		}

		public void process()
		{
			ScheduledJob job = event.getScheduledJob();
			Trigger trigger = event.getScheduledJob().getTrigger();
			synchronized (job)
			{
				boolean addedToQueue = false;
				if (job.isUnscheduled())
				{
					// maybe we get an schedule event and before we can process it the job was
					// unscheduled
					return;
				}
				if (!isPaused() && !job.isJobPaused())
				{
					addedToQueue = addJobToQueue();
				}
				// we do it here because we want to set the next schedule fire time for stateful
				// jobs too
				// calculate the next trigger event
				TriggerEvent nextTriggerEvent = trigger.getNextTriggerEvent(event.getTriggerEvent(), calculationTime);
				// check if the next ScheduleEvent is after the next TriggerEvent
				if (nextTriggerEvent != null && !nextTriggerEvent.getFireTime().isAfter(event.getTriggerEvent().getFireTime()))
				{
					nextTriggerEvent = null;
				}
				if (nextTriggerEvent != null)
				{
					job.setNextScheduleFireTime(nextTriggerEvent.getFireTime());
				}
				else
				{
					job.setNextScheduleFireTime(null);
				}

				if (addedToQueue && job.getJobDescriptor().isStateful())
				{
					schedule.remove(job.getJobKey());
				}
				else
				{
					if (nextTriggerEvent != null)
					{
						ScheduleEvent nextScheduleEvent = new ScheduleEvent(job, nextTriggerEvent);
						schedule.replace(event, nextScheduleEvent);
					}
					else
					{
						unschedule(job.getJobKey());
					}
				}
			}
		}

		private boolean addJobToQueue()
		{
			boolean addedToQueue = false;
			ScheduledJob job = event.getScheduledJob();
			job.setLastScheduleFireTime(event.getScheduleTime());
			JobExecutionContext ctx = new JobExecutionContext(new JobDescriptor(job.getJobDescriptor()), event.getTriggerEvent(),
				calculationTime);
			final boolean veto = jobListenerNotifier.fireVetoJobExecution(ctx)
				|| event.getScheduledJob().getJobListenerNotifier().fireVetoJobExecution(ctx);
			if (!veto)
			{
				// add to queue
				JobExecutionListener listener = new SchedulerJobExecutionListener(job);
				if (queue.offer(new JobExecutor(ctx, listener)))
				{
					event.getScheduledJob().jobWaiting();
					jobListenerNotifier.fireJobWaiting(ctx);
					event.getScheduledJob().getJobListenerNotifier().fireJobWaiting(ctx);
					addedToQueue = true;
				}
				else
				{
					jobListenerNotifier.fireJobNotExecuted(ctx, null);
					event.getScheduledJob().getJobListenerNotifier().fireJobNotExecuted(ctx, null);
				}
			}
			else
			{
				jobListenerNotifier.fireJobExecutionVetoed(ctx);
				event.getScheduledJob().getJobListenerNotifier().fireJobExecutionVetoed(ctx);
			}
			return addedToQueue;
		}
	}

	private class SchedulerJobExecutionListener implements JobExecutionListener
	{
		private final ScheduledJob job;

		private SchedulerJobExecutionListener(ScheduledJob job)
		{
			this.job = job;
		}

		@Override
		public void jobRunning(JobExecutionContext ctx)
		{
			synchronized (job)
			{
				job.jobRunning();
				jobListenerNotifier.fireJobRunning(ctx);
				job.getJobListenerNotifier().fireJobRunning(ctx);
			}
		}

		@Override
		public void jobExecuted(JobExecutionContext ctx, Exception e)
		{
			synchronized (job)
			{
				job.jobExecuted();
				jobListenerNotifier.fireJobExecuted(ctx, e);
				job.getJobListenerNotifier().fireJobExecuted(ctx, e);
				processStatefulJobs(ctx);
			}
		}

		@Override
		public void jobNotExecuted(JobExecutionContext ctx, Exception e)
		{
			synchronized (job)
			{
				job.jobNotExecuted();
				jobListenerNotifier.fireJobNotExecuted(ctx, e);
				job.getJobListenerNotifier().fireJobNotExecuted(ctx, e);
				processStatefulJobs(ctx);
			}
		}

		private void processStatefulJobs(JobExecutionContext ctx)
		{
			if (job.getJobDescriptor().isStateful())
			{
				if (!job.isUnscheduled())
				{
					calculateNextScheduleEvent(ctx);
				}
				else
				{
					unscheduleInternal(job);
				}
			}
		}

		private void calculateNextScheduleEvent(JobExecutionContext ctx)
		{
			// calculate the next schedule event (trigger event) for stateful jobs
			TriggerEvent nextTriggerEvent = job.getTrigger().getNextTriggerEvent(ctx.getTriggerEvent(), ZonedDateTime.now());
			if (nextTriggerEvent != null)
			{
				job.setNextScheduleFireTime(nextTriggerEvent.getFireTime());
				ScheduleEvent nextScheduleEvent = new ScheduleEvent(job, nextTriggerEvent);
				schedule.add(nextScheduleEvent);
				// wake up scheduler thread
				schedulerRunnable.wakeup();
			}
			else
			{
				job.setNextScheduleFireTime(null);
				// unschedule stateful jobs
				unschedule(ctx.getJobDescriptor().getKey());
			}
		}
	}
}
