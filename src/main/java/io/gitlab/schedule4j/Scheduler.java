/*
 * Created on 05.02.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j;

import java.util.Collection;
import java.util.Optional;

import io.gitlab.schedule4j.job.JobDescriptor;
import io.gitlab.schedule4j.job.JobKey;
import io.gitlab.schedule4j.job.JobListener;
import io.gitlab.schedule4j.trigger.CreateTriggerException;
import io.gitlab.schedule4j.trigger.TriggerConfig;
import io.gitlab.schedule4j.trigger.TriggerKey;

/**
 * A scheduler must implement this interface.
 * 
 * @author Dirk Buchhorn
 */
public interface Scheduler
{
	/**
	 * Stops the scheduler. After a scheduler was stopped he can't be started again.
	 */
	void stop();

	/**
	 * Returns <code>true</code> if this scheduler is stopped or <code>false</code> otherwise
	 * 
	 * @return <code>true</code> if this scheduler is stopped or <code>false</code> otherwise
	 */
	boolean isStopped();

	/**
	 * Returns <code>true</code> if this scheduler is paused or <code>false</code> otherwise
	 * 
	 * @return <code>true</code> if this scheduler is paused, <code>false</code> otherwise
	 */
	public boolean isPaused();

	/**
	 * Pause the scheduler, no schedule event will be executed.
	 */
	void pause();

	/**
	 * Pause the job with the given job key.
	 * 
	 * @param jobKey the job key
	 */
	void pauseJob(JobKey jobKey);

	/**
	 * Pause all jobs whose job key is a child of the given job group key.
	 * 
	 * @param jobGroupKey the job group key
	 */
	void pauseGroup(JobKey jobGroupKey);

	/**
	 * Pause all jobs whose trigger key are equals to the given key.
	 * 
	 * @param triggerKey the trigger key
	 */
	void pauseJob(TriggerKey triggerKey);

	/**
	 * Pause all jobs whose trigger key is a child of the given trigger group key.
	 * 
	 * @param triggerGroupKey the trigger group key
	 */
	void pauseGroup(TriggerKey triggerGroupKey);

	/**
	 * Resume the scheduler.
	 */
	void resume();

	/**
	 * Resume the job with the given job key.
	 * 
	 * @param jobKey the job key
	 */
	void resumeJob(JobKey jobKey);

	/**
	 * Resume all jobs whose job key is a child of the given job group key.
	 * 
	 * @param jobGroupKey the job group key
	 */
	void resumeGroup(JobKey jobGroupKey);

	/**
	 * Resume all jobs whose trigger key are equals to the given key.
	 * 
	 * @param triggerKey the trigger key
	 */
	void resumeJob(TriggerKey triggerKey);

	/**
	 * Resume all jobs whose trigger key is a child of the given trigger group key.
	 * 
	 * @param triggerGroupKey the trigger group key
	 */
	void resumeGroup(TriggerKey triggerGroupKey);

	/**
	 * Counts the scheduled jobs.
	 * 
	 * @return the scheduled job count
	 */
	int getScheduledJobCount();

	/**
	 * Schedule a job described through a {@link JobDescriptor}. An {@link IllegalArgumentException}
	 * is thrown if a job with the key is already scheduled.
	 * 
	 * @param jobDescriptor describe the job
	 * @return true if the job could be scheduled, false otherwise
	 * @throws IllegalArgumentException if a job with the key is already scheduled
	 * @throws CreateTriggerException if the Trigger could not be created
	 */
	boolean schedule(JobDescriptor jobDescriptor) throws CreateTriggerException;

	/**
	 * Schedule a job described through a {@link JobDescriptor}. An {@link IllegalArgumentException}
	 * is thrown if a job with the key is already scheduled.
	 * 
	 * @param jobDescriptor describe the job
	 * @param jobListeners optional {@link JobListener}s
	 * @return true if the job could be scheduled, false otherwise
	 * @throws IllegalArgumentException if a job with the key is already scheduled
	 * @throws CreateTriggerException if the Trigger could not be created
	 */
	boolean schedule(JobDescriptor jobDescriptor, JobListener... jobListeners)
		throws CreateTriggerException;

	/**
	 * If the job for the given key could be unscheduled or marked for remove from schedule then
	 * <code>true</code> is returned. Return <code>false</code> if the job was not found.
	 * 
	 * @param jobKey the key of the job what should be unscheduled
	 * @return <code>true</code> if the job could be unscheduled or marked for remove from schedule,
	 *         <code>false</code> if the job was not found
	 */
	boolean unschedule(JobKey jobKey);

	/**
	 * Update a scheduled job.
	 * 
	 * @param jobDescriptor the new job descriptor
	 * @return <code>true</code> if the job was found and the could be updated, <code>false</code>
	 *         if the job was not found
	 * @throws CreateTriggerException if the Trigger could not be created
	 */
	boolean update(JobDescriptor jobDescriptor) throws CreateTriggerException;

	/**
	 * Update a trigger for the scheduled job with the given job key.
	 * 
	 * @param jobKey the key of the job to search for
	 * @param triggerConfig the new trigger configuration
	 * @return <code>true</code> if the job was found and the trigger could be updated,
	 *         <code>false</code> if the job was not found
	 * @throws CreateTriggerException if the Trigger could not be created
	 */
	boolean updateTrigger(JobKey jobKey, TriggerConfig triggerConfig) throws CreateTriggerException;

	/**
	 * Change the schedule for the given job key, so the job is executed.
	 * 
	 * @param jobKey the job key
	 * @return <code>true</code> if a {@link ScheduledJob} was found and a run now event could be
	 *         set, <code>false</code> otherwise
	 */
	boolean runJobNow(JobKey jobKey);

	/**
	 * Gets a {@link JobDescriptor} for the given key.
	 * 
	 * @param jobKey the job key
	 * @return a {@link JobDescriptor} for the given key
	 */
	Optional<JobDescriptor> getJobDescriptor(JobKey jobKey);

	/**
	 * Gets all {@link JobDescriptor}s.
	 * 
	 * @return all {@link JobDescriptor}s
	 */
	Collection<JobDescriptor> getJobDescriptors();

	/**
	 * Gets a {@link ScheduledJobInfo} for the given key.
	 * 
	 * @param jobKey the job key
	 * @return a {@link ScheduledJobInfo} for the given key
	 */
	Optional<ScheduledJobInfo> getScheduledJobInfo(JobKey jobKey);

	/**
	 * Gets all {@link ScheduledJobInfo}s.
	 * 
	 * @return all {@link ScheduledJobInfo}s
	 */
	Collection<ScheduledJobInfo> getScheduledJobInfos();

	/**
	 * Adds a {@link JobListener} to this scheduler.
	 * 
	 * @param listener the {@link JobListener} to add
	 */
	void addJobListener(JobListener listener);

	/**
	 * Adds a {@link JobListener} to the job with the given job key.
	 * 
	 * @param jobKey the job key
	 * @param listener the {@link JobListener} to add
	 * @return <code>true</code> if the given job was found, <code>false</code> otherwise
	 */
	boolean addJobListener(JobKey jobKey, JobListener listener);

	/**
	 * Removes a {@link JobListener} from this scheduler.
	 * 
	 * @param listener the {@link JobListener} to remove
	 * @return <code>true</code> if the given listener was found and could be removed,
	 *         <code>false</code> otherwise
	 */
	boolean removeJobListener(JobListener listener);

	/**
	 * Removes a {@link JobListener} from the job with the given job key.
	 * 
	 * @param jobKey the job key
	 * @param listener the {@link JobListener} to remove
	 * @return <code>true</code> if the given listener was found and could be removed,
	 *         <code>false</code> otherwise
	 */
	boolean removeJobListener(JobKey jobKey, JobListener listener);

	/**
	 * Adds a {@link SchedulerListener} to this scheduler.
	 * 
	 * @param listener the {@link SchedulerListener} to add
	 */
	void addSchedulerListener(SchedulerListener listener);

	/**
	 * Removes a {@link SchedulerListener} from this scheduler.
	 * 
	 * @param listener the {@link SchedulerListener} to remove
	 * @return <code>true</code> if the given listener was found and could be removed,
	 *         <code>false</code> otherwise
	 */
	boolean removeSchedulerListener(SchedulerListener listener);
}
