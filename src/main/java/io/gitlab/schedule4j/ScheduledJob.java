/*
 * Created on 30.10.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j;

import java.io.Serializable;
import java.time.ZonedDateTime;

import io.gitlab.schedule4j.job.JobDescriptor;
import io.gitlab.schedule4j.job.JobKey;
import io.gitlab.schedule4j.job.JobListener;
import io.gitlab.schedule4j.job.JobListenerNotifier;
import io.gitlab.schedule4j.trigger.Trigger;

/**
 * After schedule a {@link JobDescriptor} the Scheduler creates a ScheduledJob object. This object
 * also stores runtime information like paused, unscheduled and the job statistic in a separate
 * {@link ScheduledJobInfo} object.<br>
 * This class can't be Serializable, because then most of the schedule4j classes must be
 * {@link Serializable} too and then the hole scheduler object tree will be serialized.
 * 
 * @author Dirk Buchhorn
 */
public final class ScheduledJob
{
	private JobDescriptor jobDescriptor;
	private Trigger trigger;
	private ScheduledJobInfo scheduledJobInfo;
	private JobListenerNotifier jobListenerNotifier = new JobListenerNotifier();

	/**
	 * Creates a new ScheduledJob with the given {@link JobDescriptor} and {@link Trigger}
	 * 
	 * @param jobDescriptor the job descriptor
	 * @param trigger the trigger
	 */
	public ScheduledJob(JobDescriptor jobDescriptor, Trigger trigger)
	{
		if (jobDescriptor == null)
		{
			throw new IllegalArgumentException("jobDescriptor can't be null");
		}
		if (trigger == null)
		{
			throw new IllegalArgumentException("trigger can't be null");
		}
		this.jobDescriptor = jobDescriptor;
		this.trigger = trigger;
		this.scheduledJobInfo = new ScheduledJobInfo(jobDescriptor.getKey());
	}

	/**
	 * Get the {@link JobKey}.
	 * 
	 * @return the {@link JobKey}
	 */
	public JobKey getJobKey()
	{
		return jobDescriptor.getKey();
	}

	/**
	 * Gets the {@link JobDescriptor}
	 * 
	 * @return the job descriptor
	 */
	public JobDescriptor getJobDescriptor()
	{
		return jobDescriptor;
	}

	/**
	 * Sets the job descriptor.
	 * 
	 * @param jobDescriptor the jobDescriptor to set
	 */
	public void setJobDescriptor(JobDescriptor jobDescriptor)
	{
		if (!this.jobDescriptor.getKey().equals(jobDescriptor.getKey()))
		{
			throw new IllegalArgumentException("The job key of the given jobDescriptor is wrong!");
		}
		this.jobDescriptor = jobDescriptor;
	}

	/**
	 * Gets the {@link Trigger}
	 * 
	 * @return the trigger
	 */
	public Trigger getTrigger()
	{
		return trigger;
	}

	/**
	 * Sets the {@link Trigger}
	 * 
	 * @param trigger the new trigger
	 */
	public void setTrigger(Trigger trigger)
	{
		this.trigger = trigger;
	}

	/**
	 * Adds a {@link JobListener} to this job descriptor.
	 * 
	 * @param listener a {@link JobListener} instance
	 */
	public void addJobListener(JobListener listener)
	{
		jobListenerNotifier.addJobListener(listener);
	}

	/**
	 * Removes a {@link JobListener} from this job descriptor.
	 * 
	 * @param listener a {@link JobListener} instance
	 * @return <code>true</code> if the given listener was found and could be removed,
	 *         <code>false</code> otherwise
	 */
	public boolean removeJobListener(JobListener listener)
	{
		return jobListenerNotifier.removeJobListener(listener);
	}

	/**
	 * Get the {@link JobListenerNotifier}.
	 * 
	 * @return the {@link JobListenerNotifier}
	 */
	public JobListenerNotifier getJobListenerNotifier()
	{
		return jobListenerNotifier;
	}

	/**
	 * Gets the {@link ScheduledJobInfo}.
	 * 
	 * @return the scheduledJobInfo
	 */
	public ScheduledJobInfo getScheduledJobInfo()
	{
		return scheduledJobInfo;
	}

	/**
	 * Return <code>true</code> if the job is paused, or <code>false</code> otherwise.
	 * 
	 * @return <code>true</code> if the job is paused, or <code>false</code> otherwise
	 */
	public boolean isJobPaused()
	{
		return scheduledJobInfo.isJobPaused();
	}

	/**
	 * Sets if the job is paused or not.
	 * 
	 * @param paused <code>true</code> to pause the job, <code>false</code> for resume the job
	 */
	public void setJobPaused(boolean paused)
	{
		scheduledJobInfo.setJobPaused(paused);
	}

	/**
	 * Return <code>true</code> if the job is unscheduled, or <code>false</code> otherwise.
	 * 
	 * @return <code>true</code> if the job is unscheduled, or <code>false</code> otherwise
	 */
	public boolean isUnscheduled()
	{
		return scheduledJobInfo.isUnscheduled();
	}

	/**
	 * Sets if the job is unscheduled.
	 */
	public void setUnscheduled()
	{
		scheduledJobInfo.setUnscheduled();
	}

	/**
	 * Gets the statistics for this scheduled job.
	 * 
	 * @return the scheduledJobStatistics
	 */
	public ScheduledJobStatistics getScheduledJobStatistics()
	{
		return scheduledJobInfo.getScheduledJobStatistics();
	}

	/**
	 * Increment the waiting job count.
	 */
	public void jobWaiting()
	{
		scheduledJobInfo.jobWaiting();
	}

	/**
	 * Decrement the waiting job count and increment the running job count.
	 */
	public void jobRunning()
	{
		scheduledJobInfo.jobRunning();
	}

	/**
	 * Decrement the running job count increment the executed job count.
	 */
	public void jobExecuted()
	{
		scheduledJobInfo.jobExecuted();
	}

	/**
	 * Decrement the waiting job count.
	 */
	public void jobNotExecuted()
	{
		scheduledJobInfo.jobNotExecuted();
	}

	/**
	 * Gets the last schedule fire time.
	 * 
	 * @return the last schedule fire time
	 */
	public ZonedDateTime getLastScheduleFireTime()
	{
		return scheduledJobInfo.getLastScheduleFireTime();
	}

	/**
	 * Sets the last schedule fire time.
	 * 
	 * @param lastScheduleFireTime the lastScheduleFireTime to set
	 */
	public void setLastScheduleFireTime(ZonedDateTime lastScheduleFireTime)
	{
		scheduledJobInfo.setLastScheduleFireTime(lastScheduleFireTime);
	}

	/**
	 * Gets the next schedule fire time.
	 * 
	 * @return the next schedule fire time
	 */
	public ZonedDateTime getNextScheduleFireTime()
	{
		return scheduledJobInfo.getNextScheduleFireTime();
	}

	/**
	 * Sets the next schedule fire time.
	 * 
	 * @param nextScheduleFireTime the nextScheduleFireTime to set
	 */
	public void setNextScheduleFireTime(ZonedDateTime nextScheduleFireTime)
	{
		scheduledJobInfo.setNextScheduleFireTime(nextScheduleFireTime);
	}
}
