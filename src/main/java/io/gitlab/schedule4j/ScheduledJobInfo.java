/*
 * Created on 11.07.2018
 * 
 * Copyright 2018 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j;

import java.io.Serializable;
import java.time.ZonedDateTime;

import io.gitlab.schedule4j.job.JobKey;

/**
 * The runtime information for a scheduled job is stored in this class. So this class can be
 * {@link Serializable}. The {@link ScheduledJob} class can't, because then most of the schedule4j
 * classes must be {@link Serializable} too and then the hole scheduler object tree will be
 * serialized.
 * 
 * @author Dirk Buchhorn
 */
public final class ScheduledJobInfo implements Serializable
{
	private static final long serialVersionUID = 2486846756421422943L;

	private final JobKey jobKey;
	private boolean jobPaused = false;
	private boolean jobUnscheduled = false;
	private ScheduledJobStatistics scheduledJobStatistics;
	private ZonedDateTime lastScheduleFireTime;
	private ZonedDateTime nextScheduleFireTime;

	/**
	 * Create a new ScheduledJobInfo object with the given job key.
	 * 
	 * @param jobKey the job key
	 */
	protected ScheduledJobInfo(JobKey jobKey)
	{
		if (jobKey == null)
		{
			throw new IllegalArgumentException("jobKey can't be null");
		}
		this.jobKey = jobKey;
		scheduledJobStatistics = new ScheduledJobStatistics();
	}

	/**
	 * Used to clone a ScheduledJobInfo object.
	 * 
	 * @param scheduledJobInfo the {@link ScheduledJobInfo} to clone
	 */
	public ScheduledJobInfo(ScheduledJobInfo scheduledJobInfo)
	{
		if (scheduledJobInfo == null)
		{
			throw new IllegalArgumentException("Parameter scheduledJobInfo can't be null");
		}
		this.jobKey = scheduledJobInfo.jobKey;
		jobPaused = scheduledJobInfo.jobPaused;
		jobUnscheduled = scheduledJobInfo.jobUnscheduled;
		scheduledJobStatistics = scheduledJobInfo.scheduledJobStatistics;
		lastScheduleFireTime = scheduledJobInfo.lastScheduleFireTime;
		nextScheduleFireTime = scheduledJobInfo.nextScheduleFireTime;
	}

	/**
	 * Get the {@link JobKey}.
	 * 
	 * @return the {@link JobKey}
	 */
	public JobKey getKey()
	{
		return jobKey;
	}

	/**
	 * Return <code>true</code> if the job is paused, or <code>false</code> otherwise.
	 * 
	 * @return <code>true</code> if the job is paused, or <code>false</code> otherwise
	 */
	public boolean isJobPaused()
	{
		return jobPaused;
	}

	/**
	 * Sets if the job is paused or not.
	 * 
	 * @param paused <code>true</code> to pause the job, <code>false</code> for resume the job
	 */
	public void setJobPaused(boolean paused)
	{
		this.jobPaused = paused;
	}

	/**
	 * Return <code>true</code> if the job is unscheduled, or <code>false</code> otherwise.
	 * 
	 * @return <code>true</code> if the job is unscheduled, or <code>false</code> otherwise
	 */
	public boolean isUnscheduled()
	{
		return jobUnscheduled;
	}

	/**
	 * Sets if the job is unscheduled.
	 */
	public void setUnscheduled()
	{
		this.jobUnscheduled = true;
	}

	/**
	 * Gets the statistics for this scheduled job.
	 * 
	 * @return the scheduledJobStatistics
	 */
	public ScheduledJobStatistics getScheduledJobStatistics()
	{
		return scheduledJobStatistics;
	}

	/**
	 * Increment the waiting job count.
	 */
	public void jobWaiting()
	{
		scheduledJobStatistics = scheduledJobStatistics.jobWaiting();
	}

	/**
	 * Decrement the waiting job count and increment the running job count.
	 */
	public void jobRunning()
	{
		scheduledJobStatistics = scheduledJobStatistics.jobRunning();
	}

	/**
	 * Decrement the running job count increment the executed job count.
	 */
	public void jobExecuted()
	{
		scheduledJobStatistics = scheduledJobStatistics.jobExecuted();
	}

	/**
	 * Decrement the waiting job count.
	 */
	public void jobNotExecuted()
	{
		scheduledJobStatistics = scheduledJobStatistics.jobNotExecuted();
	}

	/**
	 * Gets the last schedule fire time.
	 * 
	 * @return the last schedule fire time
	 */
	public ZonedDateTime getLastScheduleFireTime()
	{
		return lastScheduleFireTime;
	}

	/**
	 * Sets the last schedule fire time.
	 * 
	 * @param lastScheduleFireTime the lastScheduleFireTime to set
	 */
	public void setLastScheduleFireTime(ZonedDateTime lastScheduleFireTime)
	{
		this.lastScheduleFireTime = lastScheduleFireTime;
	}

	/**
	 * Gets the next schedule fire time.
	 * 
	 * @return the next schedule fire time
	 */
	public ZonedDateTime getNextScheduleFireTime()
	{
		return nextScheduleFireTime;
	}

	/**
	 * Sets the next schedule fire time.
	 * 
	 * @param nextScheduleFireTime the nextScheduleFireTime to set
	 */
	public void setNextScheduleFireTime(ZonedDateTime nextScheduleFireTime)
	{
		this.nextScheduleFireTime = nextScheduleFireTime;
	}
}
