/*
 * Created on 31.10.2016
 * 
 * Copyright 2016 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j;

import java.io.Serializable;

/**
 * Instances of this class are immutable.
 * 
 * @author Dirk Buchhorn
 */
public class ScheduledJobStatistics implements Serializable
{
	private static final long serialVersionUID = 5813201848032186302L;

	// how many job instances are currently waiting for execution
	// if the job is stateful then the maximum value is 1
	private final long waitingCount;
	// how many job instances are currently running, if the job is stateful then the maximum value is 1
	private final long runningCount;
	// how often a job was executed
	private final long executionCount;

	public ScheduledJobStatistics()
	{
		this.waitingCount = 0;
		this.runningCount = 0;
		this.executionCount = 0;
	}

	protected ScheduledJobStatistics(long waitingCount, long runningCount, long executionCount)
	{
		this.waitingCount = waitingCount;
		this.runningCount = runningCount;
		this.executionCount = executionCount;
	}

	/**
	 * How many job instances are currently waiting for execution.
	 * 
	 * @return how many job instances are currently waiting for execution
	 */
	public long getWaitingCount()
	{
		return waitingCount;
	}

	/**
	 * How many job instances are currently running.
	 * 
	 * @return how many job instances are currently running
	 */
	public long getRunningCount()
	{
		return runningCount;
	}

	/**
	 * How often a job was executed.
	 * 
	 * @return how often a job was executed
	 */
	public long getExecutionCount()
	{
		return executionCount;
	}

	/**
	 * Increment the waiting job count.
	 * 
	 * @return a new {@link ScheduledJobStatistics} object
	 */
	public ScheduledJobStatistics jobWaiting()
	{
		return new ScheduledJobStatistics(waitingCount + 1, runningCount, executionCount);
	}

	/**
	 * Decrement the waiting job count and increment the running job count.
	 * 
	 * @return a new {@link ScheduledJobStatistics} object
	 */
	public ScheduledJobStatistics jobRunning()
	{
		return new ScheduledJobStatistics(Math.max(waitingCount - 1, 0), runningCount + 1, executionCount);
	}

	/**
	 * Decrement the running job count increment the executed job count.
	 * 
	 * @return a new {@link ScheduledJobStatistics} object
	 */
	public ScheduledJobStatistics jobExecuted()
	{
		return new ScheduledJobStatistics(waitingCount, Math.max(runningCount - 1, 0), executionCount + 1);
	}

	/**
	 * Decrement the waiting job count.
	 * 
	 * @return a new {@link ScheduledJobStatistics} object
	 */
	public ScheduledJobStatistics jobNotExecuted()
	{
		return new ScheduledJobStatistics(Math.max(waitingCount - 1, 0), runningCount, executionCount);
	}

	@Override
	public String toString()
	{
		return "ScheduledJobStatistics (waitingCount=" + waitingCount + ", runningCount=" + runningCount
			+ ", executionCount=" + executionCount + ")";
	}
}
