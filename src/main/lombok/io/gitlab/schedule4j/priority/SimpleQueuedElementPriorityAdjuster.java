/*
 * Created on 21.02.2016
 *
 * (c) Copyright by dibu-Soft.
 * All Rights Reserved.
 */
package io.gitlab.schedule4j.priority;

import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * This is a simple implementation of a {@link QueuedElementPriorityAdjuster}.
 * 
 * @author Dirk Buchhorn
 */
@EqualsAndHashCode
public class SimpleQueuedElementPriorityAdjuster implements QueuedElementPriorityAdjuster
{
	/**
	 * Gets the lower priority adjustment.
	 * 
	 * @return the lowPriorityAdjustment
	 */
	@Getter
	private int lowPriorityAdjustment;
	/**
	 * Gets the higher priority adjustment.
	 * 
	 * @return the highPriorityAdjustment
	 */
	@Getter
	private int highPriorityAdjustment;

	/**
	 * Creates a new priority adjuster.
	 * 
	 * @param lowPriorityAdjustment the adjustment for the element with the lower priority
	 * @param highPriorityAdjustment the adjustment for the element with the higher priority
	 */
	public SimpleQueuedElementPriorityAdjuster(int lowPriorityAdjustment,
		int highPriorityAdjustment)
	{
		this.lowPriorityAdjustment = lowPriorityAdjustment;
		this.highPriorityAdjustment = highPriorityAdjustment;
	}

	@Override
	public void adjustPriority(Priority lowPriority, Priority highPriority)
	{
		lowPriority.plusPriority(lowPriorityAdjustment);
		highPriority.plusPriority(highPriorityAdjustment);
	}
}
