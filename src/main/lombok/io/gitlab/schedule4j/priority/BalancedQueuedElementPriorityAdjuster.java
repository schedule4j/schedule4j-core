/*
 * Created on 21.02.2016
 *
 * (c) Copyright by dibu-Soft.
 * All Rights Reserved.
 */
package io.gitlab.schedule4j.priority;

import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * This is an implementation of a {@link QueuedElementPriorityAdjuster}.
 * 
 * @author Dirk Buchhorn
 */
@EqualsAndHashCode
public class BalancedQueuedElementPriorityAdjuster implements QueuedElementPriorityAdjuster
{
	/**
	 * The priority adjustment.
	 * 
	 * @return the priority adjustment
	 */
	@Getter
	private int priorityAdjustment;

	/**
	 * Creates a new priority adjuster. The given adjustment is added to the lower priority and
	 * subtracted from the higher priority.
	 * 
	 * @param adjustment the priority adjustment
	 */
	public BalancedQueuedElementPriorityAdjuster(int adjustment)
	{
		this.priorityAdjustment = adjustment;
	}

	@Override
	public void adjustPriority(Priority lowPriority, Priority highPriority)
	{
		lowPriority.plusPriority(priorityAdjustment);
		highPriority.minusPriority(priorityAdjustment);
	}
}
