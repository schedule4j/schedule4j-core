<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

	<modelVersion>4.0.0</modelVersion>
	<groupId>io.gitlab.schedule4j</groupId>
	<artifactId>schedule4j-core</artifactId>
	<version>1.0.0-SNAPSHOT</version>

	<name>schedule4j-core</name>
	<description>Library for scheduling jobs</description>
	<url>https://gitlab.com/schedule4j/schedule4j-core</url>

	<licenses>
		<license>
			<name>Apache License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>repo</distribution>
		</license>
	</licenses>

	<developers>
		<developer>
			<name>Dirk Buchhorn</name>
			<email>dirkbuchhorn@web.de</email>
			<url>https://gitlab.com/dbuchhorn</url>
		</developer>
	</developers>

	<scm>
		<connection>scm:git:https://gitlab.com/schedule4j/schedule4j-core.git</connection>
		<url>https://gitlab.com/schedule4j/schedule4j-core/tree/master</url>
	</scm>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<slf4j.version>1.7.30</slf4j.version>
		<junit.jupiter.version>5.6.2</junit.jupiter.version>
		<maven.build.timestamp.format>yyyyMMdd.HHmmss</maven.build.timestamp.format>
	</properties>

	<build>
		<finalName>${project.artifactId}-${project.version}-${maven.build.timestamp}</finalName>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.8.0</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>
			<!-- start: run juni5 test with maven versions < 3.6.0 -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.22.2</version>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-failsafe-plugin</artifactId>
				<version>2.22.2</version>
			</plugin>
			<!-- end -->
			<!-- see https://github.com/awhitford/lombok.maven -->
			<plugin>
				<groupId>org.projectlombok</groupId>
				<artifactId>lombok-maven-plugin</artifactId>
				<version>1.18.16.0</version>
				<executions>
					<execution>
						<phase>generate-sources</phase>
						<goals>
							<goal>delombok</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-gpg-plugin</artifactId>
				<version>1.6</version>
				<executions>
					<execution>
						<id>sign-artifacts</id>
						<phase>verify</phase>
						<goals>
							<goal>sign</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>3.2.0</version>
				<configuration>
					<source>8</source>
				</configuration>
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<version>3.0.1</version>
				<configuration>
				</configuration>
				<executions>
					<execution>
						<phase>package</phase>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>0.8.5</version>
				<executions>
					<execution>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
					</execution>
					<execution>
						<id>report</id>
						<phase>test</phase>
						<goals>
							<goal>report</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>external.atlassian.jgitflow</groupId>
				<artifactId>jgitflow-maven-plugin</artifactId>
				<version>1.0-m5.1</version>
				<configuration>
					<!-- Enable this to push to origin using SSH keys -->
					<enableSshAgent>false</enableSshAgent>
					<!-- Keep your maven submodules at the same version as the parent POM -->
					<autoVersionSubmodules>true</autoVersionSubmodules>
					<flowInitContext>
						<featureBranchPrefix>feature-</featureBranchPrefix>
						<releaseBranchPrefix>release-</releaseBranchPrefix>
						<hotfixBranchPrefix>hotfix-</hotfixBranchPrefix>
					</flowInitContext>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<dependencies>
		<dependency>
			<groupId>io.gitlab.schedule4j</groupId>
			<artifactId>schedule4j-cron</artifactId>
			<version>1.1.0-SNAPSHOT</version>
		</dependency>
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<version>1.18.16</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>${slf4j.version}</version>
			<type>jar</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>
			<version>1.2.3</version>
		</dependency>
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter</artifactId>
			<version>${junit.jupiter.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.awaitility</groupId>
			<artifactId>awaitility</artifactId>
			<version>4.0.3</version>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<repositories>
		<repository>
			<id>sonatype-nexus-snapshots</id>
			<name>Sonatype Nexus snapshot repository</name>
			<url>https://oss.sonatype.org/content/repositories/snapshots</url>
			<releases><enabled>false</enabled></releases>
			<snapshots><enabled>true</enabled></snapshots>
		</repository>
	</repositories>

	<distributionManagement>
		<snapshotRepository>
			<id>sonatype-nexus-snapshots</id>
			<name>Sonatype Nexus snapshot repository</name>
			<url>https://oss.sonatype.org/content/repositories/snapshots</url>
			<uniqueVersion>true</uniqueVersion>
		</snapshotRepository>
		<repository>
			<id>sonatype-nexus-staging</id>
			<name>Sonatype Nexus release repository</name>
			<url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
		</repository>
	</distributionManagement>

	<profiles>
		<profile>
			<id>release-sign-artifacts</id>
			<activation>
				<property>
					<name>performRelease</name>
					<value>true</value>
				</property>
			</activation>
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-gpg-plugin</artifactId>
						<version>1.6</version>
						<executions>
							<execution>
								<id>sign-artifacts</id>
								<phase>verify</phase>
								<goals>
									<goal>sign</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
		<profile>
			<id>sonar</id>
			<activation>
				<activeByDefault>false</activeByDefault>
			</activation>
			<properties>
				<!-- Optional URL to server. Default value is http://localhost:9000 -->
				<sonar.host.url>https://sonarcloud.io</sonar.host.url>
				<sonar.organization>schedule4j</sonar.organization>
				<sonar.version>3.9.0.2155</sonar.version>
			</properties>
			<build>
				<plugins>
					<plugin>
						<groupId>org.sonarsource.scanner.maven</groupId>
						<artifactId>sonar-maven-plugin</artifactId>
						<version>${sonar.version}</version>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
</project>
