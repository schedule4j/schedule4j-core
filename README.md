Develop Branch: [![pipeline status](https://gitlab.com/schedule4j/schedule4j-core/badges/develop/pipeline.svg)](https://gitlab.com/schedule4j/schedule4j-core/commits/develop) [![coverage report](https://gitlab.com/schedule4j/schedule4j-core/badges/develop/coverage.svg)](https://gitlab.com/schedule4j/schedule4j-core/commits/develop)

# Schedule4j
Schedule4j is a java scheduler for scheduling jobs.

## Why another java scheduler?
Schedule4j was designed as a modular system. Each of these modules is easily interchangeable. The complexity of the modules 
was kept as low as possible in order to maintain a high code quality on the one hand and to enable easy interchangeability 
on the other. This offers many ways to customize schedule4j to your needs. In comparison to other schedulers, the execution time 
is not recalculated if a job cannot be executed on time. This guarantees that every job will be executed. Even on high load no job 
will be preferred (without prioritisation).  
Schedule4j's StandardScheduler supports job prioritisation. So only one scheduler is needed for normal and maybe higher 
prioritized system jobs. Another unique feature is the support of job triggers with different time zones in one scheduler.

Modules

- Scheduler
- Calendar
- Job Executor
- Job Queue
- Trigger
 
## Features
- **Time Zones:** Every trigger can have it's own time zone.
- **Cron and Simple Trigger:** For schedule event calculation the CronTrigger or SimpleTrigger can be used. Own triggers can be implemented too.
- **Scheduled Job Queue:** If a schedule event occur then the job is added to a queue. The jobs in the queue are processed by a job executor.
- **Prioritized Jobs:** Jobs can have a priority. If jobs are waiting for execution in a queue then a job with a higher priority can pass these jobs and is executed as next.
- **Easy expandability and interchangeability:** Many parts can be extended or replaced by your own implementations.

## Schedule your first job in 5 minutes
TODO create a project!!!  
1. Create a maven project
2. Add the schedule4j maven dependency to your project
```xml
<dependency>
	<groupId>io.gitlab.schedule4j</groupId>
	<artifactId>schedule4j-core</artifactId>
	<version>1.0.0-SNAPSHOT</version>
</dependency>
```
3. Create a new java class and add the following code
```java
public static void main(String[] args)
{
}
```
4. Run this class
